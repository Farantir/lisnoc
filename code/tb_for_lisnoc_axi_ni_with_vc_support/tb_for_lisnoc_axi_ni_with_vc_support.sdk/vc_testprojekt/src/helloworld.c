/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#include "xil_io.h"

int main()
{
    init_platform();
    int semd1 = 64;
    int semd2 = 65;
    int semd3 = 66;
    int semd4 = 67;
    int semd5 = 68;
    print("start\n\r");

    Xil_Out32(0x43C01000,semd1);
    Xil_Out32(0x43C09000,semd4);
    Xil_Out32(0x43C01000,semd2);
    Xil_Out32(0x43C05000,semd3);
    Xil_Out32(0x43C0d000,semd5);

    print("send\n\r");

    int ret1 = Xil_In32(0x43C10000);
    int ret5 = Xil_In32(0x43C1c000);
    int ret4 = Xil_In32(0x43C18000);
    int ret3 = Xil_In32(0x43C14000);
    int ret2 = Xil_In32(0x43C10000);

    print("recieved\n\r");
    if(ret1 == semd1) print("nominal\n\r");
    else printf("faulty, got %d expected %d\r\n",ret1,semd1);

    if(ret5 == semd5) print("nominal\n\r");
    else printf("faulty, got %d expected %d\r\n",ret5,semd5);

    if(ret4 == semd4) print("nominal\n\r");
    else printf("faulty, got %d expected %d\r\n",ret4,semd4);

    if(ret3 == semd3) print("nominal\n\r");
    else printf("faulty, got %d expected %d\r\n",ret3,semd3);

    if(ret2 == semd2) print("nominal\n\r");
    else printf("faulty, got %d expected %d\r\n",ret2,semd2);









    print("done\n\r");
//0x43C20000 = core 2
//0x43C21300 = core 2 write to core 3 in vc 1
//0x43C31000 = core 3 read vc 1
    cleanup_platform();
    return 0;
}
