// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1733598 Wed Dec 14 22:35:42 MST 2016
// Date        : Thu Jul 25 12:37:51 2019
// Host        : zynq running 64-bit Ubuntu 14.04.5 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_NOC_AXI_testmodule_vc_support_0_0_stub.v
// Design      : design_1_NOC_AXI_testmodule_vc_support_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "NOC_AXI_testmodule,Vivado 2016.4" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(S0_packet_arrived_o, S1_packet_arrived_o, 
  S2_packet_arrived_o, S3_packet_arrived_o, s00_axi_aclk, s00_axi_aresetn, s00_axi_awid, 
  s00_axi_awaddr, s00_axi_awlen, s00_axi_awsize, s00_axi_awburst, s00_axi_awlock, 
  s00_axi_awcache, s00_axi_awprot, s00_axi_awqos, s00_axi_awregion, s00_axi_awuser, 
  s00_axi_awvalid, s00_axi_awready, s00_axi_wdata, s00_axi_wstrb, s00_axi_wlast, 
  s00_axi_wuser, s00_axi_wvalid, s00_axi_wready, s00_axi_bid, s00_axi_bresp, s00_axi_buser, 
  s00_axi_bvalid, s00_axi_bready, s00_axi_arid, s00_axi_araddr, s00_axi_arlen, 
  s00_axi_arsize, s00_axi_arburst, s00_axi_arlock, s00_axi_arcache, s00_axi_arprot, 
  s00_axi_arqos, s00_axi_arregion, s00_axi_aruser, s00_axi_arvalid, s00_axi_arready, 
  s00_axi_rid, s00_axi_rdata, s00_axi_rresp, s00_axi_rlast, s00_axi_ruser, s00_axi_rvalid, 
  s00_axi_rready, s01_axi_aclk, s01_axi_aresetn, s01_axi_awid, s01_axi_awaddr, s01_axi_awlen, 
  s01_axi_awsize, s01_axi_awburst, s01_axi_awlock, s01_axi_awcache, s01_axi_awprot, 
  s01_axi_awqos, s01_axi_awregion, s01_axi_awuser, s01_axi_awvalid, s01_axi_awready, 
  s01_axi_wdata, s01_axi_wstrb, s01_axi_wlast, s01_axi_wuser, s01_axi_wvalid, s01_axi_wready, 
  s01_axi_bid, s01_axi_bresp, s01_axi_buser, s01_axi_bvalid, s01_axi_bready, s01_axi_arid, 
  s01_axi_araddr, s01_axi_arlen, s01_axi_arsize, s01_axi_arburst, s01_axi_arlock, 
  s01_axi_arcache, s01_axi_arprot, s01_axi_arqos, s01_axi_arregion, s01_axi_aruser, 
  s01_axi_arvalid, s01_axi_arready, s01_axi_rid, s01_axi_rdata, s01_axi_rresp, s01_axi_rlast, 
  s01_axi_ruser, s01_axi_rvalid, s01_axi_rready, s02_axi_aclk, s02_axi_aresetn, s02_axi_awid, 
  s02_axi_awaddr, s02_axi_awlen, s02_axi_awsize, s02_axi_awburst, s02_axi_awlock, 
  s02_axi_awcache, s02_axi_awprot, s02_axi_awqos, s02_axi_awregion, s02_axi_awuser, 
  s02_axi_awvalid, s02_axi_awready, s02_axi_wdata, s02_axi_wstrb, s02_axi_wlast, 
  s02_axi_wuser, s02_axi_wvalid, s02_axi_wready, s02_axi_bid, s02_axi_bresp, s02_axi_buser, 
  s02_axi_bvalid, s02_axi_bready, s02_axi_arid, s02_axi_araddr, s02_axi_arlen, 
  s02_axi_arsize, s02_axi_arburst, s02_axi_arlock, s02_axi_arcache, s02_axi_arprot, 
  s02_axi_arqos, s02_axi_arregion, s02_axi_aruser, s02_axi_arvalid, s02_axi_arready, 
  s02_axi_rid, s02_axi_rdata, s02_axi_rresp, s02_axi_rlast, s02_axi_ruser, s02_axi_rvalid, 
  s02_axi_rready, s03_axi_aclk, s03_axi_aresetn, s03_axi_awid, s03_axi_awaddr, s03_axi_awlen, 
  s03_axi_awsize, s03_axi_awburst, s03_axi_awlock, s03_axi_awcache, s03_axi_awprot, 
  s03_axi_awqos, s03_axi_awregion, s03_axi_awuser, s03_axi_awvalid, s03_axi_awready, 
  s03_axi_wdata, s03_axi_wstrb, s03_axi_wlast, s03_axi_wuser, s03_axi_wvalid, s03_axi_wready, 
  s03_axi_bid, s03_axi_bresp, s03_axi_buser, s03_axi_bvalid, s03_axi_bready, s03_axi_arid, 
  s03_axi_araddr, s03_axi_arlen, s03_axi_arsize, s03_axi_arburst, s03_axi_arlock, 
  s03_axi_arcache, s03_axi_arprot, s03_axi_arqos, s03_axi_arregion, s03_axi_aruser, 
  s03_axi_arvalid, s03_axi_arready, s03_axi_rid, s03_axi_rdata, s03_axi_rresp, s03_axi_rlast, 
  s03_axi_ruser, s03_axi_rvalid, s03_axi_rready)
/* synthesis syn_black_box black_box_pad_pin="S0_packet_arrived_o[3:0],S1_packet_arrived_o[3:0],S2_packet_arrived_o[3:0],S3_packet_arrived_o[3:0],s00_axi_aclk,s00_axi_aresetn,s00_axi_awid[11:0],s00_axi_awaddr[15:0],s00_axi_awlen[7:0],s00_axi_awsize[2:0],s00_axi_awburst[1:0],s00_axi_awlock,s00_axi_awcache[3:0],s00_axi_awprot[2:0],s00_axi_awqos[3:0],s00_axi_awregion[3:0],s00_axi_awuser[0:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wlast,s00_axi_wuser[0:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bid[11:0],s00_axi_bresp[1:0],s00_axi_buser[0:0],s00_axi_bvalid,s00_axi_bready,s00_axi_arid[11:0],s00_axi_araddr[15:0],s00_axi_arlen[7:0],s00_axi_arsize[2:0],s00_axi_arburst[1:0],s00_axi_arlock,s00_axi_arcache[3:0],s00_axi_arprot[2:0],s00_axi_arqos[3:0],s00_axi_arregion[3:0],s00_axi_aruser[0:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rid[11:0],s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rlast,s00_axi_ruser[0:0],s00_axi_rvalid,s00_axi_rready,s01_axi_aclk,s01_axi_aresetn,s01_axi_awid[11:0],s01_axi_awaddr[15:0],s01_axi_awlen[7:0],s01_axi_awsize[2:0],s01_axi_awburst[1:0],s01_axi_awlock,s01_axi_awcache[3:0],s01_axi_awprot[2:0],s01_axi_awqos[3:0],s01_axi_awregion[3:0],s01_axi_awuser[0:0],s01_axi_awvalid,s01_axi_awready,s01_axi_wdata[31:0],s01_axi_wstrb[3:0],s01_axi_wlast,s01_axi_wuser[0:0],s01_axi_wvalid,s01_axi_wready,s01_axi_bid[11:0],s01_axi_bresp[1:0],s01_axi_buser[0:0],s01_axi_bvalid,s01_axi_bready,s01_axi_arid[11:0],s01_axi_araddr[15:0],s01_axi_arlen[7:0],s01_axi_arsize[2:0],s01_axi_arburst[1:0],s01_axi_arlock,s01_axi_arcache[3:0],s01_axi_arprot[2:0],s01_axi_arqos[3:0],s01_axi_arregion[3:0],s01_axi_aruser[0:0],s01_axi_arvalid,s01_axi_arready,s01_axi_rid[11:0],s01_axi_rdata[31:0],s01_axi_rresp[1:0],s01_axi_rlast,s01_axi_ruser[0:0],s01_axi_rvalid,s01_axi_rready,s02_axi_aclk,s02_axi_aresetn,s02_axi_awid[11:0],s02_axi_awaddr[15:0],s02_axi_awlen[7:0],s02_axi_awsize[2:0],s02_axi_awburst[1:0],s02_axi_awlock,s02_axi_awcache[3:0],s02_axi_awprot[2:0],s02_axi_awqos[3:0],s02_axi_awregion[3:0],s02_axi_awuser[0:0],s02_axi_awvalid,s02_axi_awready,s02_axi_wdata[31:0],s02_axi_wstrb[3:0],s02_axi_wlast,s02_axi_wuser[0:0],s02_axi_wvalid,s02_axi_wready,s02_axi_bid[11:0],s02_axi_bresp[1:0],s02_axi_buser[0:0],s02_axi_bvalid,s02_axi_bready,s02_axi_arid[11:0],s02_axi_araddr[15:0],s02_axi_arlen[7:0],s02_axi_arsize[2:0],s02_axi_arburst[1:0],s02_axi_arlock,s02_axi_arcache[3:0],s02_axi_arprot[2:0],s02_axi_arqos[3:0],s02_axi_arregion[3:0],s02_axi_aruser[0:0],s02_axi_arvalid,s02_axi_arready,s02_axi_rid[11:0],s02_axi_rdata[31:0],s02_axi_rresp[1:0],s02_axi_rlast,s02_axi_ruser[0:0],s02_axi_rvalid,s02_axi_rready,s03_axi_aclk,s03_axi_aresetn,s03_axi_awid[11:0],s03_axi_awaddr[15:0],s03_axi_awlen[7:0],s03_axi_awsize[2:0],s03_axi_awburst[1:0],s03_axi_awlock,s03_axi_awcache[3:0],s03_axi_awprot[2:0],s03_axi_awqos[3:0],s03_axi_awregion[3:0],s03_axi_awuser[0:0],s03_axi_awvalid,s03_axi_awready,s03_axi_wdata[31:0],s03_axi_wstrb[3:0],s03_axi_wlast,s03_axi_wuser[0:0],s03_axi_wvalid,s03_axi_wready,s03_axi_bid[11:0],s03_axi_bresp[1:0],s03_axi_buser[0:0],s03_axi_bvalid,s03_axi_bready,s03_axi_arid[11:0],s03_axi_araddr[15:0],s03_axi_arlen[7:0],s03_axi_arsize[2:0],s03_axi_arburst[1:0],s03_axi_arlock,s03_axi_arcache[3:0],s03_axi_arprot[2:0],s03_axi_arqos[3:0],s03_axi_arregion[3:0],s03_axi_aruser[0:0],s03_axi_arvalid,s03_axi_arready,s03_axi_rid[11:0],s03_axi_rdata[31:0],s03_axi_rresp[1:0],s03_axi_rlast,s03_axi_ruser[0:0],s03_axi_rvalid,s03_axi_rready" */;
  output [3:0]S0_packet_arrived_o;
  output [3:0]S1_packet_arrived_o;
  output [3:0]S2_packet_arrived_o;
  output [3:0]S3_packet_arrived_o;
  input s00_axi_aclk;
  input s00_axi_aresetn;
  input [11:0]s00_axi_awid;
  input [15:0]s00_axi_awaddr;
  input [7:0]s00_axi_awlen;
  input [2:0]s00_axi_awsize;
  input [1:0]s00_axi_awburst;
  input s00_axi_awlock;
  input [3:0]s00_axi_awcache;
  input [2:0]s00_axi_awprot;
  input [3:0]s00_axi_awqos;
  input [3:0]s00_axi_awregion;
  input [0:0]s00_axi_awuser;
  input s00_axi_awvalid;
  output s00_axi_awready;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_wstrb;
  input s00_axi_wlast;
  input [0:0]s00_axi_wuser;
  input s00_axi_wvalid;
  output s00_axi_wready;
  output [11:0]s00_axi_bid;
  output [1:0]s00_axi_bresp;
  output [0:0]s00_axi_buser;
  output s00_axi_bvalid;
  input s00_axi_bready;
  input [11:0]s00_axi_arid;
  input [15:0]s00_axi_araddr;
  input [7:0]s00_axi_arlen;
  input [2:0]s00_axi_arsize;
  input [1:0]s00_axi_arburst;
  input s00_axi_arlock;
  input [3:0]s00_axi_arcache;
  input [2:0]s00_axi_arprot;
  input [3:0]s00_axi_arqos;
  input [3:0]s00_axi_arregion;
  input [0:0]s00_axi_aruser;
  input s00_axi_arvalid;
  output s00_axi_arready;
  output [11:0]s00_axi_rid;
  output [31:0]s00_axi_rdata;
  output [1:0]s00_axi_rresp;
  output s00_axi_rlast;
  output [0:0]s00_axi_ruser;
  output s00_axi_rvalid;
  input s00_axi_rready;
  input s01_axi_aclk;
  input s01_axi_aresetn;
  input [11:0]s01_axi_awid;
  input [15:0]s01_axi_awaddr;
  input [7:0]s01_axi_awlen;
  input [2:0]s01_axi_awsize;
  input [1:0]s01_axi_awburst;
  input s01_axi_awlock;
  input [3:0]s01_axi_awcache;
  input [2:0]s01_axi_awprot;
  input [3:0]s01_axi_awqos;
  input [3:0]s01_axi_awregion;
  input [0:0]s01_axi_awuser;
  input s01_axi_awvalid;
  output s01_axi_awready;
  input [31:0]s01_axi_wdata;
  input [3:0]s01_axi_wstrb;
  input s01_axi_wlast;
  input [0:0]s01_axi_wuser;
  input s01_axi_wvalid;
  output s01_axi_wready;
  output [11:0]s01_axi_bid;
  output [1:0]s01_axi_bresp;
  output [0:0]s01_axi_buser;
  output s01_axi_bvalid;
  input s01_axi_bready;
  input [11:0]s01_axi_arid;
  input [15:0]s01_axi_araddr;
  input [7:0]s01_axi_arlen;
  input [2:0]s01_axi_arsize;
  input [1:0]s01_axi_arburst;
  input s01_axi_arlock;
  input [3:0]s01_axi_arcache;
  input [2:0]s01_axi_arprot;
  input [3:0]s01_axi_arqos;
  input [3:0]s01_axi_arregion;
  input [0:0]s01_axi_aruser;
  input s01_axi_arvalid;
  output s01_axi_arready;
  output [11:0]s01_axi_rid;
  output [31:0]s01_axi_rdata;
  output [1:0]s01_axi_rresp;
  output s01_axi_rlast;
  output [0:0]s01_axi_ruser;
  output s01_axi_rvalid;
  input s01_axi_rready;
  input s02_axi_aclk;
  input s02_axi_aresetn;
  input [11:0]s02_axi_awid;
  input [15:0]s02_axi_awaddr;
  input [7:0]s02_axi_awlen;
  input [2:0]s02_axi_awsize;
  input [1:0]s02_axi_awburst;
  input s02_axi_awlock;
  input [3:0]s02_axi_awcache;
  input [2:0]s02_axi_awprot;
  input [3:0]s02_axi_awqos;
  input [3:0]s02_axi_awregion;
  input [0:0]s02_axi_awuser;
  input s02_axi_awvalid;
  output s02_axi_awready;
  input [31:0]s02_axi_wdata;
  input [3:0]s02_axi_wstrb;
  input s02_axi_wlast;
  input [0:0]s02_axi_wuser;
  input s02_axi_wvalid;
  output s02_axi_wready;
  output [11:0]s02_axi_bid;
  output [1:0]s02_axi_bresp;
  output [0:0]s02_axi_buser;
  output s02_axi_bvalid;
  input s02_axi_bready;
  input [11:0]s02_axi_arid;
  input [15:0]s02_axi_araddr;
  input [7:0]s02_axi_arlen;
  input [2:0]s02_axi_arsize;
  input [1:0]s02_axi_arburst;
  input s02_axi_arlock;
  input [3:0]s02_axi_arcache;
  input [2:0]s02_axi_arprot;
  input [3:0]s02_axi_arqos;
  input [3:0]s02_axi_arregion;
  input [0:0]s02_axi_aruser;
  input s02_axi_arvalid;
  output s02_axi_arready;
  output [11:0]s02_axi_rid;
  output [31:0]s02_axi_rdata;
  output [1:0]s02_axi_rresp;
  output s02_axi_rlast;
  output [0:0]s02_axi_ruser;
  output s02_axi_rvalid;
  input s02_axi_rready;
  input s03_axi_aclk;
  input s03_axi_aresetn;
  input [11:0]s03_axi_awid;
  input [15:0]s03_axi_awaddr;
  input [7:0]s03_axi_awlen;
  input [2:0]s03_axi_awsize;
  input [1:0]s03_axi_awburst;
  input s03_axi_awlock;
  input [3:0]s03_axi_awcache;
  input [2:0]s03_axi_awprot;
  input [3:0]s03_axi_awqos;
  input [3:0]s03_axi_awregion;
  input [0:0]s03_axi_awuser;
  input s03_axi_awvalid;
  output s03_axi_awready;
  input [31:0]s03_axi_wdata;
  input [3:0]s03_axi_wstrb;
  input s03_axi_wlast;
  input [0:0]s03_axi_wuser;
  input s03_axi_wvalid;
  output s03_axi_wready;
  output [11:0]s03_axi_bid;
  output [1:0]s03_axi_bresp;
  output [0:0]s03_axi_buser;
  output s03_axi_bvalid;
  input s03_axi_bready;
  input [11:0]s03_axi_arid;
  input [15:0]s03_axi_araddr;
  input [7:0]s03_axi_arlen;
  input [2:0]s03_axi_arsize;
  input [1:0]s03_axi_arburst;
  input s03_axi_arlock;
  input [3:0]s03_axi_arcache;
  input [2:0]s03_axi_arprot;
  input [3:0]s03_axi_arqos;
  input [3:0]s03_axi_arregion;
  input [0:0]s03_axi_aruser;
  input s03_axi_arvalid;
  output s03_axi_arready;
  output [11:0]s03_axi_rid;
  output [31:0]s03_axi_rdata;
  output [1:0]s03_axi_rresp;
  output s03_axi_rlast;
  output [0:0]s03_axi_ruser;
  output s03_axi_rvalid;
  input s03_axi_rready;
endmodule
