`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/18/2019 01:17:32 PM
// Design Name: 
// Module Name: output_virtualiser
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//this module handels the seperate egress instances and the multiplexing necessary for virtualisation, while beeing transparant to the network or the axi slave.
module output_virtualiser
    #(
        parameter flit_data_width = 32,
        parameter flit_address_width = 4,
        parameter flit_type_width = 2,
        parameter vchannels = 1,
        //size of the in/out fifo buffers, corresponds to the amout of flits that can be stored 
        parameter buffer_size = 10,
        localparam flit_width = flit_data_width+flit_type_width,
        
        /*
         * Parameters needed for data to flit translation
         */
         //maximum number of flits of one packet
         parameter max_packet_size = 4,
         //maximum size of the packet, derived from the max flit amount nad the flit size
         localparam max_packet_size_bits = (max_packet_size*flit_data_width),
         //amount of bits needed to store the max packet size
         parameter packet_size_bits = $clog2(max_packet_size_bits+ 1),
    
         parameter packet_address_lenght = 8,
         parameter C_S00_AXI_ADDR_WIDTH = 6,
         parameter integer C_S00_AXI_DATA_WIDTH    = 32,
         parameter vchannel_select_lefoffset = 0,
         parameter vchannel_select_bits = 1
    )
    (
        input wire clk,
        input wire rst,
        
        //NOC interface
        input wire [vchannels-1:0] ready_vector_i,
        output wire [vchannels-1:0] valid_vector_o,
        wire [flit_width-1:0] out_flit_o,
        
        //axi interface
        output wire [$clog2(max_packet_size+1):0] packet_buffer_read_address_o,
        output reg done_reading_packet_buffer_o,
        input wire packet_ready_i,
        input wire [C_S00_AXI_DATA_WIDTH-1:0] data_to_noc_out_i,
        input wire [$clog2(max_packet_size+1):0]  current_packed_size_i,
        input wire [C_S00_AXI_ADDR_WIDTH-1 : 0]   packet_target_addr_i        
    );
    
    //wire containing the currently enabled vchannel
    wire [$clog2(vchannels):0] vch_select; 
    //vector multiplexing the packet ready lines
  //  reg [vchannels-1:0] packet_ready_vector;
    /*
    initial 
        begin 
            packet_ready_vector = 0;
            vch_select = 0;
        end
    
    //logick for selecting the right vchannnel based on the AXI write address
    always @(posedge(clk)) 
        begin
            if(packet_ready_i)
                begin
                    vch_select <= packet_target_addr_i[C_S00_AXI_ADDR_WIDTH - vchannel_select_lefoffset-1:C_S00_AXI_ADDR_WIDTH-vchannel_select_bits-vchannel_select_lefoffset];
                    packet_ready_vector[packet_target_addr_i[C_S00_AXI_ADDR_WIDTH - vchannel_select_lefoffset-1:C_S00_AXI_ADDR_WIDTH-vchannel_select_bits-vchannel_select_lefoffset]] <= 1'b1;
                end
            else packet_ready_vector <= 0;
        end
   */
    assign vch_select = packet_target_addr_i[C_S00_AXI_ADDR_WIDTH - vchannel_select_lefoffset-1:C_S00_AXI_ADDR_WIDTH-vchannel_select_bits-vchannel_select_lefoffset];

    
    //controling the inputs based on the selected vchannelvch_selected vchannel
    wire [vchannels-1:0] output_selector_packet_buffer_read_address_o [$clog2(max_packet_size+1):0];
    assign packet_buffer_read_address_o = output_selector_packet_buffer_read_address_o[vch_select];
    
    wire [vchannels-1:0] output_selector_done_reading_packet_buffer_o;
    always @(posedge(clk)) done_reading_packet_buffer_o <= output_selector_done_reading_packet_buffer_o[vch_select];
    
    wire [flit_width-1:0] output_selector_out_flit_o [vchannels-1:0];
    //the data will be cascaed throgh the array, so the last entry has the correct value
    assign out_flit_o = output_selector_out_flit_o[vchannels-1];
    
    
    
    //instanciating the needed logick for each v channel
    genvar vch_num;
    for(vch_num = 0; vch_num<vchannels; vch_num = vch_num + 1)
    begin
    
        //outwards connection, further logick required for multiplexing
        //*************************************************************************************
        //wires to conncet the output fifo to the noc
        wire send_flit_to_noc;
        wire out_fifo_has_data;
        wire [flit_width-1:0] out_flit;
        parameter p_size_bits = $clog2(max_packet_size+1);
        //wrires to connect the axi slave interface packet input buffer with the flit generator
        wire [p_size_bits:0] packet_buffer_read_address;
        wire done_reading_packet_buffer;
        wire packet_ready;
        wire [C_S00_AXI_DATA_WIDTH-1:0] data_to_noc_out;
        wire [p_size_bits:0]  current_packed_size;
        wire [C_S00_AXI_ADDR_WIDTH-1 : 0]   packet_target_addr;
        //*****************************************************************************************************************************
        
        
        
        //local connection only
        //********************************************************************************************************************************
        wire out_fifo_ready;
        wire out_fifo_write;
        wire [flit_width-1:0] out_fifo_next_flit;
        //**********************************************************
  /*  
        initial
        begin
            data_to_noc_out = 0;
            current_packed_size = 0;
            packet_target_addr = 0;
        end
    */
        //controling the inputs based on the selected vchannelvch_selected vchannel       
 //       always @(vch_select,data_to_noc_out_i/*,packet_ready*/) if(vch_select == vch_num /*&& packet_ready*/) data_to_noc_out <= data_to_noc_out_i;       
 //       always @(vch_select,current_packed_size_i/*,packet_ready*/) if(vch_select == vch_num /*&& packet_ready*/) current_packed_size <= current_packed_size_i; 
 //       always @(vch_select,packet_target_addr_i/*,packet_ready*/) if(vch_select == vch_num /*&& packet_ready*/) packet_target_addr <= packet_target_addr_i;
        
        
        assign data_to_noc_out = data_to_noc_out_i & {C_S00_AXI_DATA_WIDTH{(vch_select == vch_num)}};
        assign current_packed_size = current_packed_size_i & {p_size_bits{(vch_select == vch_num)}};
        assign packet_target_addr = packet_target_addr_i & {C_S00_AXI_ADDR_WIDTH{(vch_select == vch_num)}};
         
         
        assign output_selector_packet_buffer_read_address_o[vch_num] = packet_buffer_read_address;
        assign output_selector_done_reading_packet_buffer_o[vch_num] = done_reading_packet_buffer;
        assign packet_ready = packet_ready_i && (vch_num == vch_select);
        
        
        //module for converting the input data to flits
        stream_to_flits
        #(
            .flit_data_width(flit_data_width),
            .flit_address_width(flit_address_width),
            .flit_type_width(flit_type_width),
            //maximum number of axi words of one packet
            .max_packet_size(max_packet_size),
            //width of the axi address line
            .packet_address_lenght(packet_address_lenght)
        )
        output_to_flits
        (
            .clk(clk),
            .rst(rst),
    
            .packet_buffer_read_address_o(packet_buffer_read_address),
            .done_reading_packet_buffer_o(done_reading_packet_buffer),
            .packet_ready_i(packet_ready),
            .data_to_noc_out_i(data_to_noc_out),
            .current_packet_size_in_flits(current_packed_size),
            .packet_target_addr(packet_target_addr),
            
            .out_fifo_ready(out_fifo_ready),
            .out_fifo_write(out_fifo_write),
            .out_fifo_next_flit(out_fifo_next_flit)
         );
     
         //fifo sending data to the noc
         lisnoc_fifo
         #(
           .flit_data_width(flit_data_width),
          .flit_type_width(flit_type_width),
          .packet_length(0),
          .LENGTH(buffer_size)
    
         )
         sending_fifo
         (
            // Outputs
            .clk(clk),
            .rst(rst),
         
            // FIFO input side
            .in_flit(out_fifo_next_flit),   // input
            .in_valid(out_fifo_write),                   // write_enable
            .in_ready(out_fifo_ready),                   // accepting new data
         
            //FIFO output side
            .out_flit(out_flit),                     // data_out
            .out_valid(out_fifo_has_data),                   // data available
            .out_ready(send_flit_to_noc)                   // read request
        );
    
        //check if transfer with higher priority is ready
        wire is_higest_priority;
        if(vch_num == 0) assign is_higest_priority = 1'b1;
        else assign is_higest_priority = (valid_vector_o[vch_num-1:0] == 0);
        
    
        //assinig the data paths from/to the noc
        assign send_flit_to_noc = ready_vector_i[vch_num]&&valid_vector_o[vch_num];
        //transfer from this fifo only, if no higher priority fifio is ready
        assign valid_vector_o[vch_num] = ready_vector_i[vch_num] && out_fifo_has_data && is_higest_priority;
        
        wire [flit_width-1:0] cascadet_output;
        //cascade the previos values to the end o the array, so that the last fild always contains the data from the correct output
        if(vch_num == 0) assign cascadet_output = out_flit & {flit_width{valid_vector_o[vch_num]}};
        else assign cascadet_output =  {flit_width{valid_vector_o[vch_num]}} & out_flit | output_selector_out_flit_o[vch_num-1];
        
        assign  output_selector_out_flit_o[vch_num] = cascadet_output;

    end
    
endmodule
