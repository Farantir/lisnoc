`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/19/2019 04:46:07 PM
// Design Name: 
// Module Name: flits_to_stream
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
/*
 * Module for converting input flits to an output data stream
 * created by Jakob Hippe
 */
 
/*
                 Flits to steam
                +--------------------------------------------------------------------+
 AXI SLave      |                                                                    |  recieving FIFO
+-------------+ |                                +---------------+                   | +--------------+
|             | | current_packet_size_in_flits_o |               | next_flit_ready_o | |              |
|             <----------------------------------+ state machine +--------------------->              |
|             | |                                |               |                   | |              |
|             | | packet_ready_o                 |               | next_flit_^alid_i | |              |
|             <----------------------------------+               <---------------------+              |
|             | |                                |               |                   | |              |
|             | | done_reading_packet_buffer_i   |               | next_flit_in_i    | |              |
|             +---------------------------------->               <---------------------+              |
|             | |                                |               |                   | |              |
|             | |                                +--+------------+                   | +--------------+
|             | |                                   |                                |
|             | |                                   | data_from_flit                 |
|             | |                                   |                                |
|             | |                              +----v--------------------+           |
|             | | packet_buffer_read_address_i |                         |           |
|             +--------------------------------> recieving_packet_buffer |           |
|             | |                              |                         |           |
|             | | data_to_core_o               |                         |           |
|             <--------------------------------+                         |           |
|             | |                              +-------------------------+           |
+-------------+ |                                                                    |
                +--------------------------------------------------------------------+

*/

module flits_to_stream
#(
        
        parameter flit_data_width = 32,
        //dont change this
        parameter flit_type_width = 2,
        //bits in head flit, that will be used for routing
        parameter flit_address_width = 4,
        localparam flit_width = flit_data_width+flit_type_width,
        
        /*
         * Parameters needed for flit to data translation
         */
         //maximum number of flits of one packet
         parameter max_packet_size = 4,
         //maximum size of the packet, derived from the max flit amount nad the flit size
         localparam max_packet_size_bits = (max_packet_size*flit_data_width),
         //amount of bits needed to store the max packet size
         parameter packet_size_bits = $clog2(max_packet_size_bits+ 1)
)
(
    input wire clk,
    input wire rst,
    
    input wire [flit_width-1:0] next_flit_in_i,
    output wire next_flit_ready_o,
    input wire next_flit_valid_i,
    
    //ports for connection with the axi slave. it will read data from the input puffer, if packed ist ready.
    input wire [$clog2(max_packet_size+1):0]packet_buffer_read_address_i,
    input wire done_reading_packet_buffer_i,
    output wire packet_ready_o,
    output reg [flit_data_width-1:0]data_to_core_o,
    output wire [$clog2(max_packet_size+1):0] current_packet_size_in_flits_o
);

    reg [flit_data_width-1:0] recieving_packet_buffer [max_packet_size-1:0];

    //aktual packet size
    reg [packet_size_bits-1:0] current_paket_size;
    reg [packet_size_bits-1:0] current_paket_size_next;
    
    reg [$clog2(max_packet_size-1):0] current_position;
    reg [$clog2(max_packet_size-1):0] next_position;
    reg [flit_data_width-1:0] data_from_flit;
   
    reg recieve_ready;
    reg recieve_ready_next;
    
    wire is_first_flit;
    wire is_last_flit;
    
    assign is_first_flit = next_flit_in_i[flit_data_width:flit_data_width];
    assign is_last_flit = next_flit_in_i[flit_data_width+1:flit_data_width+1];
       
    //code needed to read the input packet buffer by the axi slave
    assign packet_ready_o = ~recieve_ready;
    assign current_packet_size_in_flits_o = current_paket_size;
       
    assign next_flit_ready_o = recieve_ready;
    
    //clocked logick, assing new values to outputs at rising edge
    always @(posedge clk)
    begin
        //resettting state at rst input
        if(rst)
        begin
            recieve_ready = 1'b1;
            current_position = 0;
            current_paket_size = 0;
        end
        else if(!rst && recieve_ready && next_flit_valid_i)
        begin
            recieving_packet_buffer[current_position] = data_from_flit;
            recieve_ready = recieve_ready_next;
            current_paket_size = current_paket_size_next;
            current_position = next_position;
        end
        
        if(!recieve_ready && done_reading_packet_buffer_i)
        begin 
            current_position <= 0;
            current_paket_size <= 0;
            recieve_ready <= 1'b1;
        end
    end
    //combinational logick to calculate next values for latching
    always @(*)
    begin
        //first flit will be written to buffer, but buffer address wont increse
        //-> the first flit containing data, will overwrite the head flit
        if(is_first_flit)
        begin 
            recieve_ready_next = recieve_ready;
            current_paket_size_next = 0;
            next_position = current_position;
            data_from_flit = 32'hffffffff;
        end
        //might be expandet later to read metadata stored in the last flit. 
        //last flit is urrently unused, since lisnoc cant handle 2 flt packets. head and tail are therefor used as buffer
        else if(is_last_flit)
        begin
            recieve_ready_next = 1'b0;
            current_paket_size_next = current_paket_size + 1;//flit_data_width - remainder;
            next_position = 0;
            data_from_flit = next_flit_in_i[flit_data_width-1:0];
        end
        else
        begin
            recieve_ready_next = recieve_ready;
            current_paket_size_next = current_paket_size + 1;//flit_data_width;
            next_position = current_position+1;
            data_from_flit = next_flit_in_i[flit_data_width-1:0];
        end
    end
    
    //buffer data output to the core is always the requested buffer row    
    always @(*)
    begin
        data_to_core_o = recieving_packet_buffer[packet_buffer_read_address_i];
    end
    
    
endmodule
