`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/16/2019 02:16:38 PM
// Design Name: 
// Module Name: lisnoc_mp_interface
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
/*
 * Message Passing interface to be connected to a lisnoc router, created by Jakob Hippe
 * the interface will translate axi write operations to flits and read packets from the buffer on axi read operation.
 * Please note: read address will be ignored! all addresses get lost after the Packet was translatet to flits.
 * write address will be used to determan the recieving router. Please see address AXI->flit translation in 
 * axi_lisnoc_address_translation.svh and your LISNoC router configuration for that purpose 
 *  
 
 
  AXI Master       AXI LISNoC NIC                                                                                       NOC
+----------+     +------------------------------------------------------------------------------------------------+   +------+
|          |     |                                                                                                |   |      |
|          |     |  +-------------------+                                                                         |   |      |
|          |     |  |AXI Slave          |               +--------------------+                                    |   |      |
|          |     |  |                   |               |Flits/packet to Data|                                    |   |      |
|          |     |  |                   |               |                    | Flits +--------------------+ Flits |   |      |
|          | AXI |  |                   | Read Data     |                    <-------+ Input FIFO Queue   <-----------+      |
|          <--------+                   <---------------+                    |       +--------------------+       |   |      |
|          |     |  |                   |               |                    |                                    |   |      |
|          +-------->                   |               |                    |                                    |   |      |
|          |     |  |                   |               +--------------------+                                    |   |      |
|          |     |  |                   |                                                                         |   |      |
|          |     |  |                   |               +--------------------+                                    |   |      |
|          |     |  |                   | Write Data    |Data to Packet/flit |                                    |   |      |
|          |     |  |                   +--------------->                    | Flits +--------------------+ Flits |   |      |
|          |     |  |                   |               |                    +------->  Output FIFO Queue +----------->      |
|          |     |  |                   | Write Address |                    |       +--------------------+       |   |      |
|          |     |  |                   +--------------->                    |                                    |   |      |
|          |     |  |                   |               |                    |                                    |   |      |
|          |     |  +-------------------+               +--------------------+                                    |   |      |
|          |     |                                                                                                |   |      |
+----------+     +------------------------------------------------------------------------------------------------+   +------+

 */

module lisnoc_mp_interface
#
(
        /*
         * Parameters needet for the NOC interface
         */
        //needs to be the same be as axi data width!!!!!!
        parameter flit_data_width = 32,
        //must not be changed!!!
        parameter flit_type_width = 2,
        localparam flit_width = flit_data_width+flit_type_width,
        //don't change for now
        parameter vchannels = 1,
        //size of the in/out fifo buffers, corresponds to the amout of flits that can be stored 
        parameter buffer_size = 10,
        //defines the amount of bits in the head flit, that are reseved for the packet address.
        //please check your routers routing configuration when changing this
        parameter flit_address_width = 4,
        
        //first bit of the axi adderess (counted from the left) that is part of the packets priority
        parameter vchannel_select_lefoffset = 0,
        //amount of bits ini the axi address, that are part of the packets priority
        parameter vchannel_select_bits = 1,
        
        /*
         * Parameters needed for stream to flit conversion
         */

         //the maximum axi words stored in a packet. 
         //bit size of the packet will be max_packet_size*C_S00_AXI_DATA_WIDTH
         //max flits per packet will be max_packet_size+1 (due to head flit)
         parameter max_packet_size = 10,

        // Parameters of Axi Slave Bus Interface S00_AXI
        // refer to axi dolcumentation for further insight
        parameter integer C_S00_AXI_ID_WIDTH    = 1,
        parameter integer C_S00_AXI_DATA_WIDTH    = 32,
        parameter integer C_S00_AXI_ADDR_WIDTH    = 6,
        parameter integer C_S00_AXI_AWUSER_WIDTH    = 0,
        parameter integer C_S00_AXI_ARUSER_WIDTH    = 0,
        parameter integer C_S00_AXI_WUSER_WIDTH    = 0,
        parameter integer C_S00_AXI_RUSER_WIDTH    = 0,
        parameter integer C_S00_AXI_BUSER_WIDTH    = 0
)
(
       
        /*
         * Data lines controling this module
         */
        input wire clk,
        input wire rst,

        
        /*
         * Data lines to be connected to a lisnoc router
         * see LISNoC protcol for further insight
         * (http://www.lisnoc.org/)
         */
        input wire [flit_width-1:0] in_flit_i,
        input wire [vchannels-1:0] in_valid_i,
        output wire [vchannels-1:0] in_ready_o,
        output wire [flit_width-1:0] out_flit_o,
        output wire [vchannels-1:0] out_valid_o,
        input wire [vchannels-1:0] out_ready_i,
       
        //interreupt line for the core 
        output wire [vchannels-1:0]packet_arrived_o,
        
        /***************************************/
        /*vivado auto generated axi port stuff */
        /**********************************************************************************/
        // User ports ends
        // Do not modify the ports beyond this line


        // Ports of Axi Slave Bus Interface S00_AXI
        input wire  s00_axi_aclk,
        input wire  s00_axi_aresetn,
        input wire [C_S00_AXI_ID_WIDTH-1 : 0] s00_axi_awid,
        input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
        input wire [7 : 0] s00_axi_awlen,
        input wire [2 : 0] s00_axi_awsize,
        input wire [1 : 0] s00_axi_awburst,
        input wire  s00_axi_awlock,
        input wire [3 : 0] s00_axi_awcache,
        input wire [2 : 0] s00_axi_awprot,
        input wire [3 : 0] s00_axi_awqos,
        input wire [3 : 0] s00_axi_awregion,
        input wire [C_S00_AXI_AWUSER_WIDTH-1 : 0] s00_axi_awuser,
        input wire  s00_axi_awvalid,
        output wire  s00_axi_awready,
        input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
        input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
        input wire  s00_axi_wlast,
        input wire [C_S00_AXI_WUSER_WIDTH-1 : 0] s00_axi_wuser,
        input wire  s00_axi_wvalid,
        output wire  s00_axi_wready,
        output wire [C_S00_AXI_ID_WIDTH-1 : 0] s00_axi_bid,
        output wire [1 : 0] s00_axi_bresp,
        output wire [C_S00_AXI_BUSER_WIDTH-1 : 0] s00_axi_buser,
        output wire  s00_axi_bvalid,
        input wire  s00_axi_bready,
        input wire [C_S00_AXI_ID_WIDTH-1 : 0] s00_axi_arid,
        input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
        input wire [7 : 0] s00_axi_arlen,
        input wire [2 : 0] s00_axi_arsize,
        input wire [1 : 0] s00_axi_arburst,
        input wire  s00_axi_arlock,
        input wire [3 : 0] s00_axi_arcache,
        input wire [2 : 0] s00_axi_arprot,
        input wire [3 : 0] s00_axi_arqos,
        input wire [3 : 0] s00_axi_arregion,
        input wire [C_S00_AXI_ARUSER_WIDTH-1 : 0] s00_axi_aruser,
        input wire  s00_axi_arvalid,
        output wire  s00_axi_arready,
        output wire [C_S00_AXI_ID_WIDTH-1 : 0] s00_axi_rid,
        output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
        output wire [1 : 0] s00_axi_rresp,
        output wire  s00_axi_rlast,
        output wire [C_S00_AXI_RUSER_WIDTH-1 : 0] s00_axi_ruser,
        output wire  s00_axi_rvalid,
        input wire  s00_axi_rready
        /**********************************************************************************/
    );
    
    //wires to connect to the output fifo
    wire send_flit_to_noc;
    wire out_fifo_has_data, out_fifo_ready;
    wire out_fifo_write;
    wire [flit_width-1:0] out_fifo_next_flit;
    
    //wires to connect to the input fifo
    //flit from the noch to be stored in in fifo
    wire store_recived_flit;
    //trigger for storing the flit from the noc in the fifo
    wire in_fifo_has_data, in_fifo_ready;
    wire [flit_width-1:0] in_fifo_next_flit;

    //wrires to connect the axi slave interface packet input buffer with the flit generator
    wire [$clog2(max_packet_size+1):0] packet_buffer_read_address;
    wire done_reading_packet_buffer;
    wire packet_ready;
    wire [C_S00_AXI_DATA_WIDTH-1:0] data_to_noc_out;
    wire [$clog2(max_packet_size+1):0]current_packed_size;
    wire [C_S00_AXI_ADDR_WIDTH-1 : 0] 	packet_target_addr;
    wire [C_S00_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr;
    
    wire [$clog2(max_packet_size+1):0]packet_buffer_read_address_in;
    wire done_reading_packet_buffer_in;
    wire packet_ready_in;
    wire [C_S00_AXI_DATA_WIDTH-1:0]data_to_core_in;
    wire [$clog2(max_packet_size+1):0] current_packet_size_in_flits_in;
    
    wire start_read_transaction;
     
     //Axi slave interconnect to connect to core
     //******************************************************************************************
     // Instantiation of Axi Bus Interface S00_AXI
         lisnoc_axi_slave_v1_0_S00_AXI # ( 
             .ramsize_in_flit(max_packet_size),
             .C_S_AXI_ID_WIDTH(C_S00_AXI_ID_WIDTH),
             .C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
             .C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH),
             .C_S_AXI_AWUSER_WIDTH(C_S00_AXI_AWUSER_WIDTH),
             .C_S_AXI_ARUSER_WIDTH(C_S00_AXI_ARUSER_WIDTH),
             .C_S_AXI_WUSER_WIDTH(C_S00_AXI_WUSER_WIDTH),
             .C_S_AXI_RUSER_WIDTH(C_S00_AXI_RUSER_WIDTH),
             .C_S_AXI_BUSER_WIDTH(C_S00_AXI_BUSER_WIDTH)
         ) lisnoc_axi_slave_v1_0_S00_AXI_inst (
         
              .start_read_transaction_o(start_read_transaction),
              .packet_buffer_read_address_i(packet_buffer_read_address),
              .done_reading_packet_buffer_i(done_reading_packet_buffer),
              .packet_ready_o(packet_ready),
              .data_to_noc_out(data_to_noc_out),
              .current_packed_size(current_packed_size),
              .packet_target_addr(packet_target_addr),
              
              .packet_buffer_read_address_o(packet_buffer_read_address_in),
              .done_reading_packet_buffer_o(done_reading_packet_buffer_in),
              .packet_ready_i(packet_ready_in),
              .data_to_core_i(data_to_core_in),
              .current_packet_size_in_flits_i(current_packet_size_in_flits_in),
              .axi_araddr(axi_araddr),
         
             .S_AXI_ACLK(s00_axi_aclk),
             .S_AXI_ARESETN(s00_axi_aresetn),
             .S_AXI_AWID(s00_axi_awid),
             .S_AXI_AWADDR(s00_axi_awaddr),
             .S_AXI_AWLEN(s00_axi_awlen),
             .S_AXI_AWSIZE(s00_axi_awsize),
             .S_AXI_AWBURST(s00_axi_awburst),
             .S_AXI_AWLOCK(s00_axi_awlock),
             .S_AXI_AWCACHE(s00_axi_awcache),
             .S_AXI_AWPROT(s00_axi_awprot),
             .S_AXI_AWQOS(s00_axi_awqos),
             .S_AXI_AWREGION(s00_axi_awregion),
             .S_AXI_AWUSER(s00_axi_awuser),
             .S_AXI_AWVALID(s00_axi_awvalid),
             .S_AXI_AWREADY(s00_axi_awready),
             .S_AXI_WDATA(s00_axi_wdata),
             .S_AXI_WSTRB(s00_axi_wstrb),
             .S_AXI_WLAST(s00_axi_wlast),
             .S_AXI_WUSER(s00_axi_wuser),
             .S_AXI_WVALID(s00_axi_wvalid),
             .S_AXI_WREADY(s00_axi_wready),
             .S_AXI_BID(s00_axi_bid),
             .S_AXI_BRESP(s00_axi_bresp),
             .S_AXI_BUSER(s00_axi_buser),
             .S_AXI_BVALID(s00_axi_bvalid),
             .S_AXI_BREADY(s00_axi_bready),
             .S_AXI_ARID(s00_axi_arid),
             .S_AXI_ARADDR(s00_axi_araddr),
             .S_AXI_ARLEN(s00_axi_arlen),
             .S_AXI_ARSIZE(s00_axi_arsize),
             .S_AXI_ARBURST(s00_axi_arburst),
             .S_AXI_ARLOCK(s00_axi_arlock),
             .S_AXI_ARCACHE(s00_axi_arcache),
             .S_AXI_ARPROT(s00_axi_arprot),
             .S_AXI_ARQOS(s00_axi_arqos),
             .S_AXI_ARREGION(s00_axi_arregion),
             .S_AXI_ARUSER(s00_axi_aruser),
             .S_AXI_ARVALID(s00_axi_arvalid),
             .S_AXI_ARREADY(s00_axi_arready),
             .S_AXI_RID(s00_axi_rid),
             .S_AXI_RDATA(s00_axi_rdata),
             .S_AXI_RRESP(s00_axi_rresp),
             .S_AXI_RLAST(s00_axi_rlast),
             .S_AXI_RUSER(s00_axi_ruser),
             .S_AXI_RVALID(s00_axi_rvalid),
             .S_AXI_RREADY(s00_axi_rready)
         );
         //****************************************************************************************** 
     
     
     
    //module for converting the input data to flits
    //utilising vitual channels
     output_virtualiser
         #(
             .flit_data_width(flit_data_width),
             .flit_address_width(flit_address_width),
             .flit_type_width(flit_type_width),
             .vchannels(vchannels),
             //size of the in/out fifo buffers, corresponds to the amout of flits that can be stored 
             .buffer_size(buffer_size),
             
             /*
              * Parameters needed for data to flit translation
              */
              //maximum number of flits of one packet
              .max_packet_size(max_packet_size),
         
              .packet_address_lenght(C_S00_AXI_ADDR_WIDTH),
              .C_S00_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH),
              .C_S00_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
              .vchannel_select_lefoffset(vchannel_select_lefoffset),
              .vchannel_select_bits(vchannel_select_bits)
         )multiplexed_output
         (
             .clk(clk),
             .rst(rst),
             
             //NOC interface
             .ready_vector_i(out_ready_i),
             .valid_vector_o(out_valid_o),
             .out_flit_o(out_flit_o),
             
             //axi interface
             .packet_buffer_read_address_o(packet_buffer_read_address),
             .done_reading_packet_buffer_o(done_reading_packet_buffer),
             .packet_ready_i(packet_ready),
             .data_to_noc_out_i(data_to_noc_out),
             .current_packed_size_i(current_packed_size),
             .packet_target_addr_i(packet_target_addr)       
         );
     
     
     //contains the needed logick to convert flits to stream for each virtual channel. 
     //handles the multiplexing, so the access is transparent
     input_virtualiser
             #(
                 .flit_data_width(flit_data_width),
                 .flit_type_width(flit_type_width),
              
                 .buffer_lenght(buffer_size),
                 //bits in head flit, that will be used for routing
                 .flit_address_width(flit_address_width),
                 
                 .vchannels(vchannels),
                 
                 //first bit of the axi adderess (counted from the left) that is part of the packets priority
                 .vchannel_select_lefoffset(vchannel_select_lefoffset),
                 //amount of bits ini the axi address, that are part of the packets priority
                 .vchannel_select_bits(vchannel_select_bits),
                 /*
                  * Parameters needed for flit to data translation
                  */
                  //maximum number of flits of one packet
                  .max_packet_size(max_packet_size),
                  .C_S00_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
                  .C_S00_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
             )multiplexed_input
             (
                 .clk(clk),
                 .rst(rst),
             
                 .in_flit_i(in_flit_i),
                 .in_valid_i(in_valid_i),
                 .in_ready_o(in_ready_o),
                 
                 .packet_buffer_read_address_i(packet_buffer_read_address_in),
                 .done_reading_packet_buffer_i(done_reading_packet_buffer_in),
                 
                 .packet_ready_o(packet_ready_in),
                 .data_to_core_o(data_to_core_in),
                 .current_packet_size_in_flits_o(current_packet_size_in_flits_in),
                 .axi_araddr(axi_araddr),
                 
                 .packet_arrived_o(packet_arrived_o),
                 .start_read_transaction_i(start_read_transaction)
             );
       
       /**********************************************************************************/
      
endmodule
