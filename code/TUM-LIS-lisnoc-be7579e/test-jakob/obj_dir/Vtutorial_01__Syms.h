// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header

#ifndef _Vtutorial_01__Syms_H_
#define _Vtutorial_01__Syms_H_

#include "verilated.h"

// INCLUDE MODULE CLASSES
#include "Vtutorial_01.h"
#include "Vtutorial_01_lisnoc_router_output.h"

// SYMS CLASS
class Vtutorial_01__Syms : public VerilatedSyms {
  public:
    
    // LOCAL STATE
    const char* __Vm_namep;
    bool __Vm_activity;  ///< Used by trace routines to determine change occurred
    bool __Vm_didInit;
    
    // SUBCELL STATE
    Vtutorial_01*                  TOPp;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs;
    Vtutorial_01_lisnoc_router_output TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs;
    
    // CREATORS
    Vtutorial_01__Syms(Vtutorial_01* topp, const char* namep);
    ~Vtutorial_01__Syms() {}
    
    // METHODS
    inline const char* name() { return __Vm_namep; }
    inline bool getClearActivity() { bool r=__Vm_activity; __Vm_activity=false; return r; }
    
} VL_ATTR_ALIGNED(64);

#endif // guard
