// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vtutorial_01.h for the primary calling header

#ifndef _Vtutorial_01_lisnoc_router_output_H_
#define _Vtutorial_01_lisnoc_router_output_H_

#include "verilated.h"

class Vtutorial_01__Syms;
class VerilatedVcd;

//----------

VL_MODULE(Vtutorial_01_lisnoc_router_output) {
  public:
    
    // PORTS
    VL_IN8(clk,0,0);
    VL_IN8(rst,0,0);
    VL_OUT8(link_valid,0,0);
    VL_IN8(link_ready,0,0);
    VL_IN8(switch_request,4,0);
    VL_OUT8(switch_read,4,0);
    VL_INW(switch_flit,169,0,6);
    VL_OUT64(link_flit,33,0);
    
    // LOCAL SIGNALS
    VL_SIG8(__PVT__ready,0,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__arbiter_valid,0,0);
    VL_SIG8(__PVT__output_arbiter__DOT__prev_channel,0,-1);
    VL_SIG8(__PVT__output_arbiter__DOT__channel,0,-1);
    VL_SIG8(__PVT__output_arbiter__DOT__sel_channel,0,-1);
    VL_SIG8(__PVT__output_arbiter__DOT__channel_selected,0,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__nxt_activeroute,0,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__activeroute,0,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__activeport,4,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__port,4,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__portnum,2,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__activeportnum,2,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__flit_type,1,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__req_masked,4,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__fifo__DOT__fifo_write_ptr,4,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__fifo__DOT__pop,0,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__fifo__DOT__push,0,0);
    VL_SIGW(__PVT__vchannel__BRA__0__KET____DOT__input_flits,169,0,6);
    VL_SIG(__PVT__vchannel__BRA__0__KET____DOT__fifo__DOT__shift_register_seq__DOT__i,31,0);
    VL_SIG64(__PVT__vchannel__BRA__0__KET____DOT__arbiter_flit,33,0);
    VL_SIG64(__PVT__output_arbiter__DOT__fifo_flit_i_array[1],33,0);
    VL_SIG64(__PVT__vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__flit_i_array[5],33,0);
    VL_SIG8(__PVT__vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__u_arb__DOT__mask[5],4,0);
    VL_SIG64(__PVT__vchannel__BRA__0__KET____DOT__fifo__DOT__fifo_data[4],33,0);
    VL_SIG64(__PVT__vchannel__BRA__0__KET____DOT__fifo__DOT__nxt_fifo_data[4],33,0);
    
    // LOCAL VARIABLES
    VL_SIG8(output_arbiter__DOT____Vlvbound1,0,0);
    VL_SIG8(output_arbiter__DOT____Vlvbound2,0,0);
    VL_SIG8(vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT____Vlvbound1,0,0);
    VL_SIG8(vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT____Vlvbound2,0,0);
    VL_SIG8(vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT____Vlvbound3,0,0);
    VL_SIG8(vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__u_arb__DOT____Vlvbound2,0,0);
    VL_SIG8(vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__u_arb__DOT____Vlvbound3,0,0);
    VL_SIG8(vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__u_arb__DOT____Vlvbound4,0,0);
    VL_SIG8(vchannel__BRA__0__KET____DOT__genblk2__DOT__arbiter__DOT__u_arb__DOT____Vlvbound5,0,0);
    VL_SIG8(__Vdly__vchannel__BRA__0__KET____DOT__fifo__DOT__fifo_write_ptr,4,0);
    VL_SIG64(__Vdlyvval__vchannel__BRA__0__KET____DOT__fifo__DOT__fifo_data__v0,33,0);
    VL_SIG64(__Vdlyvval__vchannel__BRA__0__KET____DOT__fifo__DOT__fifo_data__v1,33,0);
    VL_SIG64(__Vdlyvval__vchannel__BRA__0__KET____DOT__fifo__DOT__fifo_data__v2,33,0);
    VL_SIG64(__Vdlyvval__vchannel__BRA__0__KET____DOT__fifo__DOT__fifo_data__v3,33,0);
    
    // INTERNAL VARIABLES
  private:
    Vtutorial_01__Syms* __VlSymsp;  // Symbol table
  public:
    
    // PARAMETERS
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(Vtutorial_01_lisnoc_router_output);  ///< Copying not allowed
  public:
    Vtutorial_01_lisnoc_router_output(const char* name="TOP");
    ~Vtutorial_01_lisnoc_router_output();
    void trace(VerilatedVcdC* tfp, int levels, int options=0);
    
    // API METHODS
    
    // INTERNAL METHODS
    void __Vconfigure(Vtutorial_01__Syms* symsp, bool first);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__41(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__42(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__43(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__44(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__45(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__46(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__47(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__48(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__49(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__50(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__51(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__52(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__53(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__54(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__55(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__56(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__57(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__58(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__59(Vtutorial_01__Syms* __restrict vlSymsp);
    void _combo__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__60(Vtutorial_01__Syms* __restrict vlSymsp);
  private:
    void _ctor_var_reset();
  public:
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__21(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__22(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__23(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__24(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__25(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__26(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__27(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__28(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__29(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__30(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__31(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__32(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__33(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__34(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__35(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__36(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__37(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__38(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__39(Vtutorial_01__Syms* __restrict vlSymsp);
    void _sequent__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__40(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__1(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__2(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__3(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__4(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_0__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__5(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__6(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__7(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__8(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__9(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_0_1__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__10(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__11(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__12(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__13(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__14(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_0__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__15(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__0__KET____DOT__outputs__16(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__1__KET____DOT__outputs__17(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__2__KET____DOT__outputs__18(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__3__KET____DOT__outputs__19(Vtutorial_01__Syms* __restrict vlSymsp);
    void _settle__TOP__tutorial_01__DOT__mesh__DOT__u_router_1_1__DOT__u_router__DOT__outputs__BRA__4__KET____DOT__outputs__20(Vtutorial_01__Syms* __restrict vlSymsp);
    static void traceInit(VerilatedVcd* vcdp, void* userthis, uint32_t code);
    static void traceFull(VerilatedVcd* vcdp, void* userthis, uint32_t code);
    static void traceChg(VerilatedVcd* vcdp, void* userthis, uint32_t code);
} VL_ATTR_ALIGNED(128);

#endif // guard
