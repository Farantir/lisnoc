#!/bin/bash -f
xv_path="/opt/Xilinx/Vivado/2016.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim tb_lisnoc_behav -key {Behavioral:sim_1:Functional:tb_lisnoc} -tclbatch tb_lisnoc.tcl -view /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sim_1/imports/lmueller/LISNoC/tb_lisnoc_behav.wcfg -log simulate.log
