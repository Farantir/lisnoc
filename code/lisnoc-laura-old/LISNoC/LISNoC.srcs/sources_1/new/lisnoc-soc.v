`timescale 1ns / 1ps
`include "lisnoc_def.vh"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2018 12:07:52 PM
// Design Name: 
// Module Name: lisnoc-soc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lisnoc_soc(
//In-/Outputs des gesamten SOC 
// == In-/Outputs von lisnoc_mp_simple_wb.sv (Wishbone-Schnittstelle) 
//   (->alle, außer die In-/Outputs, die zum 2x2Mesh führen)
    //Outputs
    wb_dat_o_0, wb_ack_o_0, irq_0,
    wb_dat_o_1, wb_ack_o_1, irq_1,
    wb_dat_o_2, wb_ack_o_2, irq_2,
    wb_dat_o_3, wb_ack_o_3, irq_3,
    
    //Inputs
    clk, rst, 
    wb_adr_i_0, wb_we_i_0, wb_cyc_i_0, wb_stb_i_0, wb_dat_i_0,
    wb_adr_i_1, wb_we_i_1, wb_cyc_i_1, wb_stb_i_1, wb_dat_i_1,
    wb_adr_i_2, wb_we_i_2, wb_cyc_i_2, wb_stb_i_2, wb_dat_i_2,
    wb_adr_i_3, wb_we_i_3, wb_cyc_i_3, wb_stb_i_3, wb_dat_i_3
    );
   parameter noc_data_width = 32;
   parameter noc_type_width = 2;
   localparam noc_flit_width = noc_data_width + noc_type_width;
   parameter  fifo_depth = 16;

//Die gleiche In-/Output-Instanziierung, wie in lisnoc_mp_simple_wb.sv
    //Inputs
   input clk;
   input rst;
    
    //wb_0:
   input [5:0]                wb_adr_i_0;
   input                      wb_we_i_0;
   input                      wb_cyc_i_0;
   input                      wb_stb_i_0;
   input [noc_flit_width-1:0] wb_dat_i_0;
   
   //wb_1;
   input [5:0]                wb_adr_i_1;
   input                      wb_we_i_1;
   input                      wb_cyc_i_1;
   input                      wb_stb_i_1;
   input [noc_flit_width-1:0] wb_dat_i_1;
   
   //wb_2:
   input [5:0]                wb_adr_i_2;
   input                      wb_we_i_2;
   input                      wb_cyc_i_2;
   input                      wb_stb_i_2;
   input [noc_flit_width-1:0] wb_dat_i_2;  
   
   //wb_3:
   input [5:0]                wb_adr_i_3;
   input                      wb_we_i_3;
   input                      wb_cyc_i_3;
   input                      wb_stb_i_3;
   input [noc_flit_width-1:0] wb_dat_i_3;                    

    //Outputs
    
    //wb_0:
   output [noc_flit_width-1:0] wb_dat_o_0;
   output                     wb_ack_o_0;
   output                     irq_0;
   
   //wb_1:
   output [noc_flit_width-1:0] wb_dat_o_1;
   output                     wb_ack_o_1;
   output                     irq_1;
   
   //wb_2:
   output [noc_flit_width-1:0] wb_dat_o_2;
   output                     wb_ack_o_2;
   output                     irq_2;
   
   //wb_3:
   output [noc_flit_width-1:0] wb_dat_o_3;
   output                     wb_ack_o_3;
   output                     irq_3;
   
//Für ALLE In-/Outputs (der Wb-Schnittstelle) muss ein wire definiert werden (Wie in echt, dass man für jedes Signal ein Kabel braucht)
   // Bus side (generic)
   
   //wb_0:
   wire [5:0]                  bus_addr_0;
   wire                        bus_we_0;
   wire                        bus_en_0;
   wire [noc_flit_width-1:0]   bus_data_in_0;
   wire [noc_flit_width-1:0]   bus_data_out_0;
   wire                        bus_ack_0;
   wire                        bus_stb_i_0;
   wire                        bus_cyc_i_0;

   wire [noc_flit_width-1:0]   noc_out_flit_0;
   wire                        noc_out_valid_0;
   wire                        noc_out_ready_0;

   wire [noc_flit_width-1:0]   noc_in_flit_0;
   wire                        noc_in_valid_0;
   wire                        noc_in_ready_0;
   
   //wb_1:
   wire [5:0]                  bus_addr_1;
   wire                        bus_we_1;
   wire                        bus_en_1;
   wire [noc_flit_width-1:0]   bus_data_in_1;
   wire [noc_flit_width-1:0]   bus_data_out_1;
   wire                        bus_ack_1;
   wire                        bus_stb_i_1;
   wire                        bus_cyc_i_1;
   
   wire [noc_flit_width-1:0]   noc_out_flit_1;
   wire                        noc_out_valid_1;
   wire                        noc_out_ready_1;
   
   wire [noc_flit_width-1:0]   noc_in_flit_1;
   wire                        noc_in_valid_1;
   wire                        noc_in_ready_1;
   
   //wb_2:
   wire [5:0]                  bus_addr_2;
   wire                        bus_we_2;
   wire                        bus_en_2;
   wire [noc_flit_width-1:0]   bus_data_in_2;
   wire [noc_flit_width-1:0]   bus_data_out_2;
   wire                        bus_ack_2;
   wire                        bus_stb_i_2;
   wire                        bus_cyc_i_2;
   
   wire [noc_flit_width-1:0]   noc_out_flit_2;
   wire                        noc_out_valid_2;
   wire                        noc_out_ready_2;
   
   wire [noc_flit_width-1:0]   noc_in_flit_2;
   wire                        noc_in_valid_2;
   wire                        noc_in_ready_2;
   
   //wb_3:
   wire [5:0]                  bus_addr_3;
   wire                        bus_we_3;
   wire                        bus_en_3;
   wire [noc_flit_width-1:0]   bus_data_in_3;
   wire [noc_flit_width-1:0]   bus_data_out_3;
   wire                        bus_ack_3;
   wire                        bus_stb_i_3;
   wire                        bus_cyc_i_3;
   
   wire [noc_flit_width-1:0]   noc_out_flit_3;
   wire                        noc_out_valid_3;
   wire                        noc_out_ready_3;
   
   wire [noc_flit_width-1:0]   noc_in_flit_3;
   wire                        noc_in_valid_3;
   wire                        noc_in_ready_3;

//Für alle In-/Outputs (des gesamten SOC) Zuweisung machen (außer irq)
   //wb_0:
   assign bus_addr_0    = wb_adr_i_0; //Adresse fuer Datentransfer
   assign bus_we_0      = wb_we_i_0; //Write Enable -> Zugriffsrichtung
   assign bus_cyc_i_0   = wb_cyc_i_0;  
   assign bus_stb_i_0   = wb_stb_i_0; //CYC (Cycle) -> zeigt aktiven Bus Cycle, STB (Strobe) -> Transferanforderungen
   assign bus_data_in_0 = wb_dat_i_0; //Dateninput
   assign wb_dat_o_0    = bus_data_out_0; //Datenoutput
   assign wb_ack_o_0    = bus_ack_0; //ACK Acknowledgement -> Bestaetigung oder Transferende anzeigen
   
   //wb_1:
   assign bus_addr_1   = wb_adr_i_1;
   assign bus_we_1     = wb_we_i_1;
   assign bus_cyc_i_1  = wb_cyc_i_1;
   assign bus_stb_i_1  = wb_stb_i_1;
   assign bus_data_in_1 = wb_dat_i_1;
   assign wb_dat_o_1   = bus_data_out_1;
   assign wb_ack_o_1   = bus_ack_1;
   
   //wb_2:
   assign bus_addr_2   = wb_adr_i_2;
   assign bus_we_2     = wb_we_i_2;
   assign bus_cyc_i_2  = wb_cyc_i_2;
   assign bus_stb_i_2  = wb_stb_i_2;
   assign bus_data_in_2 = wb_dat_i_2;
   assign wb_dat_o_2   = bus_data_out_2;
   assign wb_ack_o_2   = bus_ack_2;
   
   //wb_3:
   assign bus_addr_3   = wb_adr_i_3;
   assign bus_we_3     = wb_we_i_3;
   assign bus_cyc_i_3  = wb_cyc_i_3;
   assign bus_stb_i_3  = wb_stb_i_3;
   assign bus_data_in_3 = wb_dat_i_3;
   assign wb_dat_o_3   = bus_data_out_3;
   assign wb_ack_o_3   = bus_ack_3;
   
   
   
//Die In-/Outputs der einzelnen Wb-Schnittstellen definieren: 
    //wb_0:
    lisnoc_mp_simple_wb
      #(.noc_data_width(noc_data_width),
        .noc_type_width(noc_type_width),
        .fifo_depth(fifo_depth))   
     u_mp_simple_wb_0 (
         // Outputs
         .noc_out_flit            (noc_out_flit_0[noc_flit_width-1:0]),
         .noc_out_valid           (noc_out_valid_0),
         .noc_in_ready            (noc_in_ready_0),
         .wb_dat_o               (bus_data_out_0[noc_flit_width-1:0]), 
         .wb_ack_o               (bus_ack_0),
         .irq                     (irq_0), 
         
         // Inputs
         .clk                     (clk),
         .rst                     (rst),
         .noc_out_ready           (noc_out_ready_0),
         .noc_in_flit             (noc_in_flit_0[noc_flit_width-1:0]),
         .noc_in_valid            (noc_in_valid_0),
         .wb_adr_i               (bus_addr_0[5:0]),
         .wb_we_i                (bus_we_0), 
         .wb_cyc_i               (bus_cyc_i_0),
         .wb_stb_i               (bus_stb_i_0),
         .wb_dat_i                (bus_data_in_0[noc_flit_width-1:0]));
         
    //wb_1:
    lisnoc_mp_simple_wb
      #(.noc_data_width(noc_data_width),
        .noc_type_width(noc_type_width),
        .fifo_depth(fifo_depth))
     u_mp_simple_wb_1(
         //Outputs:
         .noc_out_flit            (noc_out_flit_1[noc_flit_width-1:0]),
         .noc_out_valid           (noc_out_valid_1),
         .noc_in_ready            (noc_in_ready_1),
         .wb_dat_o                (bus_data_out_1[noc_flit_width-1:0]), 
         .wb_ack_o                (bus_ack_1),
         .irq                     (irq_1),
         
         //Inputs
         .clk                     (clk),
         .rst                     (rst),
         .noc_out_ready           (noc_out_ready_1),
         .noc_in_flit             (noc_in_flit_1[noc_flit_width-1:0]),
         .noc_in_valid            (noc_in_valid_1),
         .wb_adr_i                (bus_addr_1[5:0]),
         .wb_we_i                 (bus_we_1), 
         .wb_cyc_i                (bus_cyc_i_1),
         .wb_stb_i                (bus_stb_i_1),
         .wb_dat_i                (bus_data_in_1[noc_flit_width-1:0]));
         
     //wb_2:
     lisnoc_mp_simple_wb
       #(.noc_data_width(noc_data_width),
         .noc_type_width(noc_type_width),
         .fifo_depth(fifo_depth))
     u_mp_simple_wb_2(
         //Outputs:
          .noc_out_flit            (noc_out_flit_2[noc_flit_width-1:0]),
          .noc_out_valid           (noc_out_valid_2),
          .noc_in_ready            (noc_in_ready_2),
          .wb_dat_o                (bus_data_out_2[noc_flit_width-1:0]), 
          .wb_ack_o                (bus_ack_2),
          .irq                     (irq_2),
             
         //Inputs
          .clk                     (clk),
          .rst                     (rst),
          .noc_out_ready           (noc_out_ready_2),
          .noc_in_flit             (noc_in_flit_2[noc_flit_width-1:0]),
          .noc_in_valid            (noc_in_valid_2),
          .wb_adr_i                (bus_addr_2[5:0]),
          .wb_we_i                 (bus_we_2), 
          .wb_cyc_i                (bus_cyc_i_2),
          .wb_stb_i                (bus_stb_i_2),
          .wb_dat_i                (bus_data_in_2[noc_flit_width-1:0]));
          
     //wb_3:
     lisnoc_mp_simple_wb
       #(.noc_data_width(noc_data_width),
         .noc_type_width(noc_type_width),
         .fifo_depth(fifo_depth))
     u_mp_simple_wb_3(
         //Outputs:
          .noc_out_flit            (noc_out_flit_3[noc_flit_width-1:0]),
          .noc_out_valid           (noc_out_valid_3),
          .noc_in_ready            (noc_in_ready_3),
          .wb_dat_o                (bus_data_out_3[noc_flit_width-1:0]), 
          .wb_ack_o                (bus_ack_3),
          .irq                     (irq_3),
             
         //Inputs
          .clk                     (clk),
          .rst                     (rst),
          .noc_out_ready           (noc_out_ready_3),
          .noc_in_flit             (noc_in_flit_3[noc_flit_width-1:0]),
          .noc_in_valid            (noc_in_valid_3),
          .wb_adr_i                (bus_addr_3[5:0]),
          .wb_we_i                 (bus_we_3), 
          .wb_cyc_i                (bus_cyc_i_3),
          .wb_stb_i                (bus_stb_i_3),
          .wb_dat_i                (bus_data_in_3[noc_flit_width-1:0]));       
          
      



//Die In-/Outputs der 2x2mesh-Router mit den jeweiligen Wb-Schnittstellen verbinden
   lisnoc_mesh2x2
    u_lisnoc_mesh2x2 (
/*
    .link0_in_flit_i            (noc_out_flit_0[noc_flit_width-1:0]), 
    .link0_in_valid_i           (noc_out_valid_0),
    .link0_in_ready_o           (noc_out_ready_0),
    .link0_out_flit_o           (noc_in_flit_0[noc_flit_width-1:0]),
    .link0_out_valid_o          (noc_in_valid_0),
    .link0_out_ready_i          (noc_in_ready_0),

    .link1_in_flit_i            (noc_out_flit_1[noc_flit_width-1:0]),
    .link1_in_valid_i           (noc_out_valid_1),
    .link1_in_ready_o           (noc_out_ready_1),
    .link1_out_flit_o           (noc_in_flit_1[noc_flit_width-1:0]),
    .link1_out_valid_o          (noc_in_valid_1),
    .link1_out_ready_i          (noc_in_ready_1),

    .link2_in_flit_i            (noc_out_flit_2[noc_flit_width-1:0]),
    .link2_in_valid_i           (noc_out_valid_2),
    .link2_in_ready_o           (noc_out_ready_2),
    .link2_out_flit_o           (noc_in_flit_2[noc_flit_width-1:0]),
    .link2_out_valid_o          (noc_in_valid_2),
    .link2_out_ready_i          (noc_in_ready_2),

    .link3_in_flit_i            (noc_out_flit_3[noc_flit_width-1:0]),
    .link3_in_valid_i           (noc_out_valid_3),
    .link3_in_ready_o           (noc_out_ready_3),
    .link3_out_flit_o           (noc_in_flit_3[noc_flit_width-1:0]),
    .link3_out_valid_o          (noc_in_valid_3),
    .link3_out_ready_i          (noc_in_ready_3),
*/
    .clk                    (clk),              
    .rst                    (rst) 
    );


    
endmodule


