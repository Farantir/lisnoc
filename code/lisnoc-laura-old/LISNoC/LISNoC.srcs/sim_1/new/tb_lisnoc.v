`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/03/2018 12:16:48 PM
// Design Name: 
// Module Name: tb_lisnoc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_lisnoc (); 

    parameter  AW = 6;
    parameter  DW = 32; 

    reg clk, rstn, trig_write_0; 
    wire	wb_rst_0;
    wire	wb_cyc_0;
    wire	wb_stb_0;
    wire	wb_we_0;
    wire [(AW-1):0]	wb_adr_0;
    wire [(DW-1):0]	wb_data_i_0;
   // wire [(DW/8-1):0]	wb_sel_0;
    wire	wb_ack_0;
    wire	wb_stall_0;
    wire [(DW-1):0]	wb_data_o_0;
    //wire	wb_err_0;
    // wb1
    wire	wb_rst_1;
    wire    wb_cyc_1;
    wire    wb_stb_1;
    wire    wb_we_1;
    wire [(AW-1):0]    wb_adr_1;
    wire [(DW-1):0]    wb_data_i_1;
    wire [(DW/8-1):0]    wb_sel_1;
    wire    wb_ack_1;
    wire    wb_stall_1;
    wire [(DW-1):0]    wb_data_o_1;
    wire    wb_err_1;
    //
    wire    wb_rst_2;
    wire    wb_cyc_2;
    wire    wb_stb_2;
    wire    wb_we_2;
    wire [(AW-1):0]	wb_adr_2;
    wire [(DW-1):0]    wb_data_i_2;
    wire [(DW/8-1):0]    wb_sel_2;
    wire    wb_ack_2;
    wire    wb_stall_2;
    wire [(DW-1):0]    wb_data_o_2;
    wire    wb_err_2;
    wire    wb_rst_3;
    wire    wb_cyc_3;
    wire    wb_stb_3;
    wire    wb_we_3;
    wire [(AW-1):0]    wb_adr_3;
    wire [(DW-1):0]    wb_data_i_3;
    wire [(DW/8-1):0]    wb_sel_3;
    wire    wb_ack_3;
    wire    wb_stall_3;
    wire [(DW-1):0]    wb_data_o_3;
    wire    wb_err_3;    

initial
begin
    
	clk = 0; 
	rstn = 0;
	trig_write_0 = 0;
	
	# 3 rstn = 1;
	# 6 rstn = 0;

    /*commanding the start of the transfer*/
 	# 15 trig_write_0 = 1; 
	# 300 trig_write_0 = 0; 
end
/*
//old code 
 assign wb_rst_0 = ~rstn; 
 assign wb_rst_1 = ~rstn; 
 assign wb_rst_2 = ~rstn; 
 assign wb_rst_3 = ~rstn; 
 */
 
 assign wb_rst_0 = rstn; 
 assign wb_rst_1 = rstn; 
 assign wb_rst_2 = rstn; 
 assign wb_rst_3 = rstn; 
 
 
always #5 clk = ~clk; 


/*
    axis_wb_master
        #(
            .WB_DATA_WIDTH(DW),                    // width of data bus in bits (8, 16, 32, or 64)
            .WB_ADDR_WIDTH(AW)
        )
        core_0 (
        .clk (clk),
        .rst (wb_rst_0),
        .trig_write (trig_write_0),
        .wb_dat_i (wb_data_o_0),
        .wb_ack_i (wb_ack_0),
        .wb_err_i (wb_err_0),
        .wb_adr_o (wb_adr_0),
        .wb_dat_o (wb_data_i_0),
        .wb_we_o  (wb_we_0), 
        //.wb_sel_o (wb_sel_0),
        .wb_stb_o (wb_stb_0),
        .wb_cyc_o (wb_cyc_0)        
        ); 
      */ 

    wb_message_sender
        #(
          .WB_ADDR_WIDTH (AW),
          .WB_DATA_WIDTH (DW)
        )
        core_0 (
        .clk (clk),
        .wb_rst_i (wb_rst_0),
        .trig_write (trig_write_0),
        .wb_dat_i (wb_data_o_0),
        .wb_ack_i (wb_ack_0),
        //.wb_err_i (wb_err_0),
        .wb_adr_o (wb_adr_0),
        .wb_dat_o (wb_data_i_0),
        .wb_we_o  (wb_we_0), 
        //.wb_sel_o (wb_sel_0), //the wishbone select line is none existent in the NOC !
        .wb_stb_o (wb_stb_0),
        .wb_cyc_o (wb_cyc_0)        
        ); 
   

    lisnoc_soc
        u_lisnoc_soc (
        .clk (clk), 
        .rst (wb_rst_0), 
        .wb_dat_o_0 (wb_data_o_0), 
        
        .wb_ack_o_0 (wb_ack_0), 
        .irq_0 (irq_0) ,
        .wb_dat_o_1 (wb_data_o_1), 
        .wb_ack_o_1 (wb_ack_1), 
        .irq_1 (irq_1),
        .wb_dat_o_2 (wb_data_o_2), 
        .wb_ack_o_2 (wb_ack_2), 
        .irq_2 (irq_2),
        .wb_dat_o_3 (wb_data_o_3), 
        .wb_ack_o_3 (wb_ack_3), 
        .irq_3 (irq_3),
        .wb_adr_i_0 (wb_adr_0), 
        .wb_we_i_0 (wb_we_0), 
        .wb_cyc_i_0 (wb_cyc_0), 
        .wb_stb_i_0 (wb_stb_0), 
        .wb_dat_i_0 (wb_data_i_0),
        .wb_adr_i_1 (wb_adr_1), 
        .wb_we_i_1 (wb_we_1), 
        .wb_cyc_i_1 (wb_cyc_1), 
        .wb_stb_i_1 (wb_stb_1), 
        .wb_dat_i_1 (wb_data_i_1),
        .wb_adr_i_2 (wb_adr_2), 
        .wb_we_i_2 (wb_we_2), 
        .wb_cyc_i_2 (wb_cyc_2), 
        .wb_stb_i_2 (wb_stb_2), 
        .wb_dat_i_2 (wb_data_i_2),
        .wb_adr_i_3 (wb_adr_3), 
        .wb_we_i_3 (wb_we_3), 
        .wb_cyc_i_3 (wb_cyc_3), 
        .wb_stb_i_3 (wb_stb_3), 
        .wb_dat_i_3 (wb_data_i_3)     
     );

endmodule
