`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/11/2019 10:13:33 AM
// Design Name: 
// Module Name: wb_message_sender
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

/*This module must be connected to a core on the lisnoc-soc. it will then send messeges into the network*/

module wb_message_sender
    #(
        /*
         *The following parametars are needed for Wishbone configuration
         */
        parameter WB_DATA_WIDTH = 32,                    // width of data bus in bits (8, 16, 32, or 64)
        parameter WB_ADDR_WIDTH = 6  
        /*
         * Nonexistent in the noc!
         */                  // width of address bus in bits
       // parameter WB_SELECT_WIDTH = (WB_DATA_WIDTH/8)   // width of word select bus (1, 2, 4, or 8)
    )
    (
        input  wire                       clk,
        /*triggers the message sendig via wishbone on rising edge*/
        input  wire                trig_write,
        
        
        /*
         * Wishbone interface
         */
        /*currently unused*/input  wire [WB_DATA_WIDTH-1:0]   wb_dat_i,   // DAT_I() data in
        /*currently unused*/input  wire                       wb_ack_i,   // ACK_I acknowledge input
        /*currently unused*///input  wire                       wb_err_i,   // ERR_I error input
        input wire wb_rst_i,
        output reg [WB_ADDR_WIDTH-1:0]   wb_adr_o,   // ADR_O() address
        output reg [WB_DATA_WIDTH-1:0]   wb_dat_o,   // DAT_O() data out
        output reg                       wb_we_o,    // WE_O write enable output
        /*
         * Sel line nonexistent in NOC!
         */
        //output reg [WB_SELECT_WIDTH-1:0] wb_sel_o,   // SEL_O() select output
        output reg                       wb_stb_o,   // STB_O strobe output
        output reg                       wb_cyc_o   // CYC_O cycle output
        
    );
    
    /*
     * State of the simulation device
     */
    reg [3:0] state;
    
    /*state of the wb controler*/
    reg wb_state, wb_state_next;
    
    localparam
        wb_idle = 1'd0,
        wb_sending = 1'd1;
     /*
      * Signals the wb controler to send the given message
      */   
     reg request_sending;
     /*
      * message the wb controler should send
      */
     reg [WB_ADDR_WIDTH-1:0] send_addr;
     reg [WB_DATA_WIDTH-1:0] send_data;
     
    /*
     * Data for next WB outputs
     */
     reg wb_we_o_next;
     reg wb_stb_o_next;
     reg wb_cyc_o_next;
     reg [WB_ADDR_WIDTH-1:0] wb_adr_o_next;
     reg [WB_DATA_WIDTH-1:0] wb_dat_o_next;
    
    /*
     * funtion for resetting the state and all outputs, ensuring a known good state of the module
     */
    task initiate_reset;
        begin
          /*
           * resetting wb connecton
           */
          wb_adr_o = 0;
          wb_dat_o = 0;
          wb_we_o = 1'b0;
          wb_stb_o = 1'b0;
          wb_cyc_o = 1'b0;
          /*
           * resetting next output for wb connections
           */
           wb_adr_o_next = 0;
           wb_dat_o_next = 0;
           wb_we_o_next = 1'b0;
           wb_stb_o_next = 1'b0;
           wb_cyc_o_next = 1'b0;
          
          /*
           * resetting internal state of wb controler
           */
          wb_state = wb_idle;
          wb_state_next = wb_idle;
          
          //resetting state of simulation device
          state = 3'd0;
          request_sending = 1'b0;
      end
    endtask
    
    /*
     * Statemachine to simulate different outputs to the noc using the WB controler
     */
    always @(posedge clk)
    /*
     * only send the next wb messege, if the wb interface is idle
     */
    if(!wb_rst_i & wb_state == wb_idle & !request_sending & trig_write)
    begin
        case(state)
            0: 
            //sends a message to address 4, simulation will continue
            begin
                send_addr = 6'b000100; //enable device
                send_data = 'h10000000;
                request_sending = 1'b1;
                state = 3'd1;
            end
            //tries to send a message to address 0, simulation will loop continuosly
            1:
            begin
                send_addr = 6'b000000; //send packed
                send_data = 'h00000000;
                request_sending = 1'b1;
                state = 3'd2;
            end
            2:;
        endcase
    end
    
    /*resetting the current state on reset input*/
    always @(wb_rst_i)
    begin
        initiate_reset;
    end

    always @(posedge clk)
    /*
     * Wishbone specification requires all components to stay initialized while rst is high and until the rising clock edge after !rst
     */
    if(!wb_rst_i)
    /*
     * Latches all outputs set by the combinational logick at the rising clock edge
     */
    begin 
        wb_adr_o = wb_adr_o_next;
        wb_dat_o = wb_dat_o_next;
        wb_we_o = wb_we_o_next;
        wb_stb_o = wb_stb_o_next;
        wb_cyc_o = wb_cyc_o_next;
        wb_state = wb_state_next;
    end
    
    /*
     * Calculation for the WB outputs 
     */
    always @(*)
    if(!wb_rst_i)
    begin
        case (wb_state)
            /*
             * if the simulation statemachine requests the sending of a message and the wb interfade is idle,
             * the corresponding outputs will be prepared and latched, at the next clock edge
             */
            wb_idle: if(request_sending)
                begin
                    wb_state_next = wb_sending;
                    wb_we_o = 1'b1;
                    wb_dat_o_next = send_data; //data to be send
                    wb_adr_o_next = send_addr; //address to send to
                    wb_stb_o_next = 1'b1;
                    wb_cyc_o_next = 1'b1;
                end
            /*
             * any time the reciver aknoledges the wb data, the outputs will be reset and latched at the next rising clock edge
             */
            wb_sending: if(wb_ack_i) 
                begin
                    wb_cyc_o_next = 1'b0;
                    wb_stb_o_next = 1'b0;
                    wb_state_next = wb_idle;
                    request_sending = 1'b0;
                end
        endcase
    end
    
endmodule
