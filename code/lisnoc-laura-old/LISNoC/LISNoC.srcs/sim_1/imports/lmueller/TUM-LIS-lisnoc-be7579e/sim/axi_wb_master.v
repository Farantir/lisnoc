/*

Copyright (c) 2016 Alex Forencich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

// Language: Verilog 2001

`timescale 1ns / 1ps

/*
 * AXI stream wishbone master
 */
module axis_wb_master #
(
    parameter COUNT_SIZE = 16,                       // size of word count register
    parameter AXIS_DATA_WIDTH = 8,                   // width of AXI data bus
    parameter AXIS_KEEP_WIDTH = (AXIS_DATA_WIDTH/8), // width of AXI bus tkeep signal
    parameter WB_DATA_WIDTH = 32,                    // width of data bus in bits (8, 16, 32, or 64)
    parameter WB_ADDR_WIDTH = 32,                    // width of address bus in bits
    parameter WB_SELECT_WIDTH = (WB_DATA_WIDTH/8),   // width of word select bus (1, 2, 4, or 8)
    parameter WRITE_REQ = 8'hA2,                     // write requst type
    parameter READ_RESP = 8'hA3,                     // read response type
    parameter WRITE_RESP = 8'hA4                     // write response type
)
(
    input  wire                       clk,
    input  wire                       rst,
    input  wire                trig_write,


    /*
     * Wishbone interface
     */
    input  wire [WB_DATA_WIDTH-1:0]   wb_dat_i,   // DAT_I() data in
    input  wire                       wb_ack_i,   // ACK_I acknowledge input
    input  wire                       wb_err_i,   // ERR_I error input
    output wire [WB_ADDR_WIDTH-1:0]   wb_adr_o,   // ADR_O() address
    output wire [WB_DATA_WIDTH-1:0]   wb_dat_o,   // DAT_O() data out
    output wire                       wb_we_o,    // WE_O write enable output
    output wire [WB_SELECT_WIDTH-1:0] wb_sel_o,   // SEL_O() select output
    output wire                       wb_stb_o,   // STB_O strobe output
	output wire                       wb_cyc_o,   // CYC_O cycle output

    /*
     * Status
     */
    output wire                       busy
);

// bus word width
localparam AXIS_DATA_WORD_SIZE = AXIS_DATA_WIDTH / AXIS_KEEP_WIDTH;

// for interfaces that are more than one word wide, disable address lines
parameter WB_VALID_ADDR_WIDTH = WB_ADDR_WIDTH - $clog2(WB_SELECT_WIDTH);
// width of data port in words (1, 2, 4, or 8)
parameter WB_WORD_WIDTH = WB_SELECT_WIDTH;
// size of words (8, 16, 32, or 64 bits)
parameter WB_WORD_SIZE = WB_DATA_WIDTH/WB_WORD_WIDTH;

parameter WORD_PART_ADDR_WIDTH = $clog2(WB_WORD_SIZE/AXIS_DATA_WORD_SIZE);

parameter ADDR_WIDTH_ADJ = WB_ADDR_WIDTH+WORD_PART_ADDR_WIDTH;

parameter COUNT_WORD_WIDTH = (COUNT_SIZE+AXIS_DATA_WORD_SIZE-1)/AXIS_DATA_WORD_SIZE;
parameter ADDR_WORD_WIDTH = (ADDR_WIDTH_ADJ+AXIS_DATA_WORD_SIZE-1)/AXIS_DATA_WORD_SIZE;

localparam [2:0]
    STATE_IDLE = 3'd0,
    STATE_HEADER = 3'd1,
    //STATE_READ_1 = 3'd2,
    //STATE_READ_2 = 3'd3,
    STATE_WRITE_1 = 3'd4,
    STATE_WRITE_2 = 3'd5,
    STATE_WAIT_LAST = 3'd6;

reg [2:0] state_reg = STATE_IDLE, state_next;
reg input_axis_tlast = 1'b0; 

reg [7:0] ptr_reg = {8{1'b0}}, ptr_next;
reg [7:0] count_reg = 8'd0, count_next;
reg last_cycle_reg = 1'b0;

reg [ADDR_WIDTH_ADJ-1:0] addr_reg = {ADDR_WIDTH_ADJ{1'b0}}, addr_next;
reg [WB_DATA_WIDTH-1:0] data_reg = {WB_DATA_WIDTH{1'b0}}, data_next;

reg input_axis_tready_reg = 1'b0, input_axis_tready_next;

reg wb_we_o_reg = 1'b0, wb_we_o_next;
reg [WB_SELECT_WIDTH-1:0] wb_sel_o_reg = {WB_SELECT_WIDTH{1'b0}}, wb_sel_o_next;
reg wb_stb_o_reg = 1'b0, wb_stb_o_next;
reg wb_cyc_o_reg = 1'b0, wb_cyc_o_next;

reg busy_reg = 1'b0;

reg write_ongoing = 1'b0;
reg [AXIS_DATA_WIDTH-1:0] input_axis_tdata;
// internal datapath

reg [AXIS_DATA_WIDTH-1:0] output_axis_tdata_int;
reg [AXIS_KEEP_WIDTH-1:0] output_axis_tkeep_int;
reg                       output_axis_tvalid_int;
reg                       output_axis_tready_int_reg = 1'b0;
reg                       output_axis_tlast_int;
reg                       output_axis_tuser_int;
wire                      output_axis_tready_int_early;



// bus width assertions
initial begin
    input_axis_tdata = WRITE_REQ;
 /*   
    if (WB_WORD_WIDTH * WB_WORD_SIZE != WB_DATA_WIDTH) begin
        $error("Error: WB data width not evenly divisble");
        $finish;
    end
    
    if (2**$clog2(WB_WORD_WIDTH) != WB_WORD_WIDTH) begin
        $error("Error: WB word width must be even power of two");
        $finish;
    end */
end




/*assign input_axis_tready = input_axis_tready_reg;*/

assign wb_adr_o = {addr_reg[ADDR_WIDTH_ADJ-1:ADDR_WIDTH_ADJ-WB_VALID_ADDR_WIDTH], {WB_ADDR_WIDTH-WB_VALID_ADDR_WIDTH{1'b0}}};
//ADDR_WIDTH_ADJ = WB_ADDR_WIDTH+WORD_PART_ADDR_WIDTH -> 32 + $clog2(1)
//WB_ADDR_WIDTH = 32
//WORD_PART_ADDR_WIDTH = $clog2(WB_WORD_SIZE/AXIS_DATA_WORD_SIZE) -> 8 / 8 = 1
//WB_WORD_SIZE = WB_DATA_WIDTH/WB_WORD_WIDTH -> 32 / 4 = 8
//AXIS_DATA_WORD_SIZE = AXIS_DATA_WIDTH / AXIS_KEEP_WIDTH -> 8 / 1 = 8
//WB_DATA_WIDTH = 32
//WB_WORD_WIDTH = WB_SELECT_WIDTH -> 4
//WB_SELECT_WIDTH = (WB_DATA_WIDTH/8) -> 32 / 8 = 4
//AXIS_DATA_WIDTH = 8
//AXIS_KEEP_WIDTH = (AXIS_DATA_WIDTH/8) -> 8 / 8 = 1
//WB_VALID_ADDR_WIDTH = WB_ADDR_WIDTH - $clog2(WB_SELECT_WIDTH) -> 32 - $clog2(4)
assign wb_dat_o = data_reg;
assign wb_we_o = wb_we_o_reg;
assign wb_sel_o = wb_sel_o_reg;
assign wb_stb_o = wb_stb_o_reg;
assign wb_cyc_o = wb_cyc_o_reg;

assign busy = busy_reg;

always @* begin
//    state_next = STATE_IDLE;
    ptr_next = ptr_reg;
    count_next = count_reg;

    addr_next = addr_reg;
    //data_next = data_reg; <_ testing purpouse

    wb_we_o_next = wb_we_o_reg;
    wb_sel_o_next = wb_sel_o_reg;
    wb_stb_o_next = 1'b0;
    wb_cyc_o_next = 1'b0;

    case (state_reg)
    STATE_IDLE: begin   // 000
            // idle, wait for start indicator
            wb_we_o_next = 1'b0;

            if (write_ongoing) begin //ganze if-Bedingung weg??? <- i dont get that comment
                if (input_axis_tlast) begin
                    // last asserted, ignore cycle
                    state_next = STATE_IDLE;
                end else if (input_axis_tdata == WRITE_REQ) begin
                    // start of write
                    wb_we_o_next = 1'b1;
                    count_next = COUNT_WORD_WIDTH+ADDR_WORD_WIDTH-1;  //5
                    state_next = STATE_HEADER;
                end else begin
                        // drop packet
                        state_next = STATE_WAIT_LAST;
                end
            end else begin
                state_next = STATE_IDLE;
            end
        end
        STATE_HEADER: begin     // 001
            // store address and length
            if (write_ongoing) begin
                // store pointers
                if (count_reg < COUNT_WORD_WIDTH) begin
                    ptr_next[AXIS_DATA_WORD_SIZE*count_reg +: AXIS_DATA_WORD_SIZE] = 3; //input_axis_tdata; // todo:
                end else begin
                    addr_next[AXIS_DATA_WORD_SIZE*(count_reg-COUNT_WORD_WIDTH) +: AXIS_DATA_WORD_SIZE] = 32'h00000004; //input_axis_tdata; // todo:
                end
                count_next = count_reg - 1;
                if (count_reg == 0) begin
                    // end of header
                    // set initial word offset
                    if (WB_ADDR_WIDTH == WB_VALID_ADDR_WIDTH && WORD_PART_ADDR_WIDTH == 0) begin
                        count_next = 0;
                    end else begin
                        count_next = addr_reg[ADDR_WIDTH_ADJ-WB_VALID_ADDR_WIDTH-1:0];
                    end
                    wb_sel_o_next = {WB_SELECT_WIDTH{1'b0}};
                    data_next = {WB_DATA_WIDTH{1'b0}};
                    if (wb_we_o_reg) begin
                        // start writing
                        if (trig_write) begin
                            state_next = STATE_WRITE_1;
                        end
                    end
                end
            end
        end
        STATE_WRITE_1: begin        // 100
            // write data
            if (write_ongoing) begin
                // store word
                data_next[AXIS_DATA_WORD_SIZE*count_reg +: AXIS_DATA_WORD_SIZE] = input_axis_tdata; //32'h12345678; // todo:
                count_next = count_reg + 1;
                ptr_next = ptr_reg - 1;
                wb_sel_o_next[count_reg >> ((WB_WORD_SIZE/AXIS_DATA_WORD_SIZE)-1)] = 1'b1;
                if (count_reg == (WB_SELECT_WIDTH*WB_WORD_SIZE/AXIS_DATA_WORD_SIZE)-1 || ptr_reg == 1) begin
                    // have full word or at end of block, start write operation
                    count_next = 0;
                    wb_cyc_o_next = 1'b1;
                    wb_stb_o_next = 1'b1;
                    state_next = STATE_WRITE_2;
                end else begin
                    state_next = STATE_WRITE_1;
                end
            end else begin
                state_next = STATE_WRITE_1;
            end
        end
        STATE_WRITE_2: begin        // 101
            // wait for ack
            wb_cyc_o_next = 1'b1;
            wb_stb_o_next = 1'b1;

            if (wb_ack_i || wb_err_i) begin
                // end of write operation
                data_next = {WB_DATA_WIDTH{1'b0}};
                addr_next = addr_reg + (1 << (WB_ADDR_WIDTH-WB_VALID_ADDR_WIDTH+WORD_PART_ADDR_WIDTH));
                wb_cyc_o_next = 1'b0;
                wb_stb_o_next = 1'b0;
                wb_sel_o_next = {WB_SELECT_WIDTH{1'b0}};
                if (ptr_reg == 0) begin   // todo:
                    // done writing
                        state_next = STATE_WAIT_LAST;
                end else begin
                    // more to write
                    state_next = STATE_WRITE_1;
                end
            end else begin
                state_next = STATE_WRITE_2;
            end
        end
        STATE_WAIT_LAST: begin  //110
            // wait for end of frame

            if (write_ongoing) begin
                // wait for tlast
//                if (input_axis_tlast) begin  // todo:
                    input_axis_tready_next = output_axis_tready_int_early;
                    state_next = STATE_IDLE;
//                end else begin
//                    state_next = STATE_WAIT_LAST;
//                end
            end else begin
                state_next = STATE_WAIT_LAST;
            end
        end
    endcase
end

always @(posedge clk) begin
    if (rst) begin
        state_next = STATE_IDLE;
        state_reg <= STATE_IDLE;
        write_ongoing <= 1'b0; 
        //input_axis_tready_reg <= 1'b0;
        wb_stb_o_reg <= 1'b0;
        wb_cyc_o_reg <= 1'b0;
        busy_reg <= 1'b0;
    end else begin
        state_reg <= state_next;
        //input_axis_tready_reg <= input_axis_tready_next;
        wb_stb_o_reg <= wb_stb_o_next;
        wb_cyc_o_reg <= wb_cyc_o_next;
        busy_reg <= state_next != STATE_IDLE;
    end

    ptr_reg <= ptr_next;
    count_reg <= count_next;

    if (write_ongoing) begin
        last_cycle_reg <= input_axis_tlast;
    end

    if (trig_write == 1 && write_ongoing == 0) begin
      write_ongoing <= 1'b1;
    end // TODO: else if ()

    addr_reg <= addr_next;
    data_reg <= {'b010001,'h000};//data_next;

    wb_we_o_reg <= wb_we_o_next;
    wb_sel_o_reg <= wb_sel_o_next;
end


endmodule
