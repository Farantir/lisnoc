/*
 * this file contains the necesary information to translate the axi address into a NOC address.
 */

//number of msb dedicatet to the core address of the lisnoc network
localparam lookup_bit_left_offset = 2;
//number of address bits after the left offstet, that are used for the address translation
localparam lookup_bit_num = 2;
//register containing the address lookup table
reg  [3:0] address_lookup_table [3:0];
initial
begin
    address_lookup_table[2'd0] = 4'b0000;
    address_lookup_table[2'd1] = 4'b0001;
    address_lookup_table[2'd2] = 4'b0010;
    address_lookup_table[2'd3] = 4'b0011;
end
