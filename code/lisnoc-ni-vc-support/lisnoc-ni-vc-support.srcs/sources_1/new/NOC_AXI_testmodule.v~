`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/26/2019 10:50:23 AM
// Design Name: 
// Module Name: NOC_AXI_testmodule
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
/*
 * Testmodule containing linoc mesh and network interface controlers. 
 * can be connected to axi masters to function as a NOC
 * axi address to core address translation can be configured in axi_lisnoc_address_translation.svh
 * default translation as follows:
 * core 0: 16'h 0000 - 0FFF
 * core 1: 16'h 1000 - 1FFF
 * core 2: 16'h 2000 - 2FFF
 * core 3: 16'h 3000 - 3FFF
 * Please note: address wont be send over the network. Thus only 2 bits of the address are relevant.
 * e.g. for core 0: xx00xxxxxxxxxxxx, for core 1: xx01xxxxxxxxxxxx
 
 
              NOC AXI Testmodule
            +----------------------------------------------+
            |                                              |
 Core 0     |      AXI LISNoC NIC         2x2 LISNoC Mesh  |
+---------+ |     +--------------+       +---------------+ |
|         | | AXI |              | Flits |               | |
|         +------->              +------->               | |
|         <-------+              <-------+               | |
|         | |     |              |       |               | |
+---------+ |     +--------------+       |               | |
            |                            |               | |
 Core 1     |      AXI LISNoC NIC        |               | |
+---------+ |     +--------------+       |               | |
|         | | AXI |              | Flits |               | |
|         +------->              +------->               | |
|         <-------+              <-------+               | |
|         | |     |              |       |               | |
+---------+ |     +--------------+       |               | |
            |                            |               | |
 Core 2     |      AXI LISNoC NIC        |               | |
+---------+ |     +--------------+       |               | |
|         | | AXI |              | Flits |               | |
|         +------->              +------->               | |
|         <-------+              <-------+               | |
|         | |     |              |       |               | |
+---------+ |     +--------------+       |               | |
            |                            |               | |
 Core 3     |      AXI LISNoC NIC        |               | |
+---------+ |     +--------------+       |               | |
|         | | AXI |              | Flits |               | |
|         +------->              +------->               | |
|         <-------+              <-------+               | |
|         | |     |              |       |               | |
+---------+ |     +--------------+       +---------------+ |
            |                                              |
            +----------------------------------------------+


*/

module NOC_AXI_testmodule 
#
	(
		// Users to add parameters here
        /*
         * Configuration parameters of the lisnoc interface
         */
        parameter flit_data_width = 32,
        parameter flit_type_width = 2,
        localparam flit_width = flit_data_width+flit_type_width,
        parameter ph_dest_width = 4,
        
        parameter vchannels = 1,
        
        parameter in_fifo_length = 4,
        parameter out_fifo_length = 4,
		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_ID_WIDTH	= 12,
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 16,
		parameter integer C_S00_AXI_AWUSER_WIDTH	= 0,
		parameter integer C_S00_AXI_ARUSER_WIDTH	= 0,
		parameter integer C_S00_AXI_WUSER_WIDTH	= 0,
		parameter integer C_S00_AXI_RUSER_WIDTH	= 0,
		parameter integer C_S00_AXI_BUSER_WIDTH	= 0,
		
				// Parameters of Axi Slave Bus Interface S01_AXI
        parameter integer C_S01_AXI_ID_WIDTH    = 12,
        parameter integer C_S01_AXI_DATA_WIDTH    = 32,
        parameter integer C_S01_AXI_ADDR_WIDTH    = 16,
        parameter integer C_S01_AXI_AWUSER_WIDTH    = 0,
        parameter integer C_S01_AXI_ARUSER_WIDTH    = 0,
        parameter integer C_S01_AXI_WUSER_WIDTH    = 0,
        parameter integer C_S01_AXI_RUSER_WIDTH    = 0,
        parameter integer C_S01_AXI_BUSER_WIDTH    = 0,
        
        		// Parameters of Axi Slave Bus Interface S02_AXI
        parameter integer C_S02_AXI_ID_WIDTH    = 12,
        parameter integer C_S02_AXI_DATA_WIDTH    = 32,
        parameter integer C_S02_AXI_ADDR_WIDTH    = 16,
        parameter integer C_S02_AXI_AWUSER_WIDTH    = 0,
        parameter integer C_S02_AXI_ARUSER_WIDTH    = 0,
        parameter integer C_S02_AXI_WUSER_WIDTH    = 0,
        parameter integer C_S02_AXI_RUSER_WIDTH    = 0,
        parameter integer C_S02_AXI_BUSER_WIDTH    = 0,
        
        		// Parameters of Axi Slave Bus Interface S03_AXI
        parameter integer C_S03_AXI_ID_WIDTH    = 12,
        parameter integer C_S03_AXI_DATA_WIDTH    = 32,
        parameter integer C_S03_AXI_ADDR_WIDTH    = 16,
        parameter integer C_S03_AXI_AWUSER_WIDTH    = 0,
        parameter integer C_S03_AXI_ARUSER_WIDTH    = 0,
        parameter integer C_S03_AXI_WUSER_WIDTH    = 0,
        parameter integer C_S03_AXI_RUSER_WIDTH    = 0,
        parameter integer C_S03_AXI_BUSER_WIDTH    = 0
	)
	(
   
		// Ports of Axi Slave Bus Interface S00_AXI
        input wire  s00_axi_aclk,
        input wire  s00_axi_aresetn,
        input wire [C_S00_AXI_ID_WIDTH-1 : 0] s00_axi_awid,
        input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
        input wire [7 : 0] s00_axi_awlen,
        input wire [2 : 0] s00_axi_awsize,
        input wire [1 : 0] s00_axi_awburst,
        input wire  s00_axi_awlock,
        input wire [3 : 0] s00_axi_awcache,
        input wire [2 : 0] s00_axi_awprot,
        input wire [3 : 0] s00_axi_awqos,
        input wire [3 : 0] s00_axi_awregion,
        input wire [C_S00_AXI_AWUSER_WIDTH-1 : 0] s00_axi_awuser,
        input wire  s00_axi_awvalid,
        output wire  s00_axi_awready,
        input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
        input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
        input wire  s00_axi_wlast,
        input wire [C_S00_AXI_WUSER_WIDTH-1 : 0] s00_axi_wuser,
        input wire  s00_axi_wvalid,
        output wire  s00_axi_wready,
        output wire [C_S00_AXI_ID_WIDTH-1 : 0] s00_axi_bid,
        output wire [1 : 0] s00_axi_bresp,
        output wire [C_S00_AXI_BUSER_WIDTH-1 : 0] s00_axi_buser,
        output wire  s00_axi_bvalid,
        input wire  s00_axi_bready,
        input wire [C_S00_AXI_ID_WIDTH-1 : 0] s00_axi_arid,
        input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
        input wire [7 : 0] s00_axi_arlen,
        input wire [2 : 0] s00_axi_arsize,
        input wire [1 : 0] s00_axi_arburst,
        input wire  s00_axi_arlock,
        input wire [3 : 0] s00_axi_arcache,
        input wire [2 : 0] s00_axi_arprot,
        input wire [3 : 0] s00_axi_arqos,
        input wire [3 : 0] s00_axi_arregion,
        input wire [C_S00_AXI_ARUSER_WIDTH-1 : 0] s00_axi_aruser,
        input wire  s00_axi_arvalid,
        output wire  s00_axi_arready,
        output wire [C_S00_AXI_ID_WIDTH-1 : 0] s00_axi_rid,
        output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
        output wire [1 : 0] s00_axi_rresp,
        output wire  s00_axi_rlast,
        output wire [C_S00_AXI_RUSER_WIDTH-1 : 0] s00_axi_ruser,
        output wire  s00_axi_rvalid,
        input wire  s00_axi_rready,
        
        		// Ports of Axi Slave Bus Interface S01_AXI
        input wire  s01_axi_aclk,
        input wire  s01_axi_aresetn,
        input wire [C_S01_AXI_ID_WIDTH-1 : 0] s01_axi_awid,
        input wire [C_S01_AXI_ADDR_WIDTH-1 : 0] s01_axi_awaddr,
        input wire [7 : 0] s01_axi_awlen,
        input wire [2 : 0] s01_axi_awsize,
        input wire [1 : 0] s01_axi_awburst,
        input wire  s01_axi_awlock,
        input wire [3 : 0] s01_axi_awcache,
        input wire [2 : 0] s01_axi_awprot,
        input wire [3 : 0] s01_axi_awqos,
        input wire [3 : 0] s01_axi_awregion,
        input wire [C_S01_AXI_AWUSER_WIDTH-1 : 0] s01_axi_awuser,
        input wire  s01_axi_awvalid,
        output wire  s01_axi_awready,
        input wire [C_S01_AXI_DATA_WIDTH-1 : 0] s01_axi_wdata,
        input wire [(C_S01_AXI_DATA_WIDTH/8)-1 : 0] s01_axi_wstrb,
        input wire  s01_axi_wlast,
        input wire [C_S01_AXI_WUSER_WIDTH-1 : 0] s01_axi_wuser,
        input wire  s01_axi_wvalid,
        output wire  s01_axi_wready,
        output wire [C_S01_AXI_ID_WIDTH-1 : 0] s01_axi_bid,
        output wire [1 : 0] s01_axi_bresp,
        output wire [C_S01_AXI_BUSER_WIDTH-1 : 0] s01_axi_buser,
        output wire  s01_axi_bvalid,
        input wire  s01_axi_bready,
        input wire [C_S01_AXI_ID_WIDTH-1 : 0] s01_axi_arid,
        input wire [C_S01_AXI_ADDR_WIDTH-1 : 0] s01_axi_araddr,
        input wire [7 : 0] s01_axi_arlen,
        input wire [2 : 0] s01_axi_arsize,
        input wire [1 : 0] s01_axi_arburst,
        input wire  s01_axi_arlock,
        input wire [3 : 0] s01_axi_arcache,
        input wire [2 : 0] s01_axi_arprot,
        input wire [3 : 0] s01_axi_arqos,
        input wire [3 : 0] s01_axi_arregion,
        input wire [C_S01_AXI_ARUSER_WIDTH-1 : 0] s01_axi_aruser,
        input wire  s01_axi_arvalid,
        output wire  s01_axi_arready,
        output wire [C_S01_AXI_ID_WIDTH-1 : 0] s01_axi_rid,
        output wire [C_S01_AXI_DATA_WIDTH-1 : 0] s01_axi_rdata,
        output wire [1 : 0] s01_axi_rresp,
        output wire  s01_axi_rlast,
        output wire [C_S01_AXI_RUSER_WIDTH-1 : 0] s01_axi_ruser,
        output wire  s01_axi_rvalid,
        input wire  s01_axi_rready,
        
                		// Ports of Axi Slave Bus Interface S02_AXI
        input wire  s02_axi_aclk,
        input wire  s02_axi_aresetn,
        input wire [C_S02_AXI_ID_WIDTH-1 : 0] s02_axi_awid,
        input wire [C_S02_AXI_ADDR_WIDTH-1 : 0] s02_axi_awaddr,
        input wire [7 : 0] s02_axi_awlen,
        input wire [2 : 0] s02_axi_awsize,
        input wire [1 : 0] s02_axi_awburst,
        input wire  s02_axi_awlock,
        input wire [3 : 0] s02_axi_awcache,
        input wire [2 : 0] s02_axi_awprot,
        input wire [3 : 0] s02_axi_awqos,
        input wire [3 : 0] s02_axi_awregion,
        input wire [C_S02_AXI_AWUSER_WIDTH-1 : 0] s02_axi_awuser,
        input wire  s02_axi_awvalid,
        output wire  s02_axi_awready,
        input wire [C_S02_AXI_DATA_WIDTH-1 : 0] s02_axi_wdata,
        input wire [(C_S02_AXI_DATA_WIDTH/8)-1 : 0] s02_axi_wstrb,
        input wire  s02_axi_wlast,
        input wire [C_S02_AXI_WUSER_WIDTH-1 : 0] s02_axi_wuser,
        input wire  s02_axi_wvalid,
        output wire  s02_axi_wready,
        output wire [C_S02_AXI_ID_WIDTH-1 : 0] s02_axi_bid,
        output wire [1 : 0] s02_axi_bresp,
        output wire [C_S02_AXI_BUSER_WIDTH-1 : 0] s02_axi_buser,
        output wire  s02_axi_bvalid,
        input wire  s02_axi_bready,
        input wire [C_S02_AXI_ID_WIDTH-1 : 0] s02_axi_arid,
        input wire [C_S02_AXI_ADDR_WIDTH-1 : 0] s02_axi_araddr,
        input wire [7 : 0] s02_axi_arlen,
        input wire [2 : 0] s02_axi_arsize,
        input wire [1 : 0] s02_axi_arburst,
        input wire  s02_axi_arlock,
        input wire [3 : 0] s02_axi_arcache,
        input wire [2 : 0] s02_axi_arprot,
        input wire [3 : 0] s02_axi_arqos,
        input wire [3 : 0] s02_axi_arregion,
        input wire [C_S02_AXI_ARUSER_WIDTH-1 : 0] s02_axi_aruser,
        input wire  s02_axi_arvalid,
        output wire  s02_axi_arready,
        output wire [C_S02_AXI_ID_WIDTH-1 : 0] s02_axi_rid,
        output wire [C_S02_AXI_DATA_WIDTH-1 : 0] s02_axi_rdata,
        output wire [1 : 0] s02_axi_rresp,
        output wire  s02_axi_rlast,
        output wire [C_S02_AXI_RUSER_WIDTH-1 : 0] s02_axi_ruser,
        output wire  s02_axi_rvalid,
        input wire  s02_axi_rready,

        		// Ports of Axi Slave Bus Interface S03_AXI
        input wire  s03_axi_aclk,
        input wire  s03_axi_aresetn,
        input wire [C_S03_AXI_ID_WIDTH-1 : 0] s03_axi_awid,
        input wire [C_S03_AXI_ADDR_WIDTH-1 : 0] s03_axi_awaddr,
        input wire [7 : 0] s03_axi_awlen,
        input wire [2 : 0] s03_axi_awsize,
        input wire [1 : 0] s03_axi_awburst,
        input wire  s03_axi_awlock,
        input wire [3 : 0] s03_axi_awcache,
        input wire [2 : 0] s03_axi_awprot,
        input wire [3 : 0] s03_axi_awqos,
        input wire [3 : 0] s03_axi_awregion,
        input wire [C_S03_AXI_AWUSER_WIDTH-1 : 0] s03_axi_awuser,
        input wire  s03_axi_awvalid,
        output wire  s03_axi_awready,
        input wire [C_S03_AXI_DATA_WIDTH-1 : 0] s03_axi_wdata,
        input wire [(C_S03_AXI_DATA_WIDTH/8)-1 : 0] s03_axi_wstrb,
        input wire  s03_axi_wlast,
        input wire [C_S03_AXI_WUSER_WIDTH-1 : 0] s03_axi_wuser,
        input wire  s03_axi_wvalid,
        output wire  s03_axi_wready,
        output wire [C_S03_AXI_ID_WIDTH-1 : 0] s03_axi_bid,
        output wire [1 : 0] s03_axi_bresp,
        output wire [C_S03_AXI_BUSER_WIDTH-1 : 0] s03_axi_buser,
        output wire  s03_axi_bvalid,
        input wire  s03_axi_bready,
        input wire [C_S03_AXI_ID_WIDTH-1 : 0] s03_axi_arid,
        input wire [C_S03_AXI_ADDR_WIDTH-1 : 0] s03_axi_araddr,
        input wire [7 : 0] s03_axi_arlen,
        input wire [2 : 0] s03_axi_arsize,
        input wire [1 : 0] s03_axi_arburst,
        input wire  s03_axi_arlock,
        input wire [3 : 0] s03_axi_arcache,
        input wire [2 : 0] s03_axi_arprot,
        input wire [3 : 0] s03_axi_arqos,
        input wire [3 : 0] s03_axi_arregion,
        input wire [C_S03_AXI_ARUSER_WIDTH-1 : 0] s03_axi_aruser,
        input wire  s03_axi_arvalid,
        output wire  s03_axi_arready,
        output wire [C_S03_AXI_ID_WIDTH-1 : 0] s03_axi_rid,
        output wire [C_S03_AXI_DATA_WIDTH-1 : 0] s03_axi_rdata,
        output wire [1 : 0] s03_axi_rresp,
        output wire  s03_axi_rlast,
        output wire [C_S03_AXI_RUSER_WIDTH-1 : 0] s03_axi_ruser,
        output wire  s03_axi_rvalid,
        input wire  s03_axi_rready

    );
       
       
    wire clk;
    assign clk = s00_axi_aclk;
    wire rst;
    assign rst = ~s00_axi_aresetn;
    
    //connections to router 0 
    wire [flit_width-1:0] link0_in_flit_i;
    wire [vchannels-1:0] link0_in_valid_i;
    wire [vchannels-1:0] link0_in_ready_o;
    wire [flit_width-1:0] link0_out_flit_o;
    wire [vchannels-1:0] link0_out_valid_o;
    wire [vchannels-1:0] link0_out_ready_i;
    
    //connections to router 1
    wire [flit_width-1:0] link1_in_flit_i;
    wire [vchannels-1:0] link1_in_valid_i;
    wire [vchannels-1:0] link1_in_ready_o;
    wire [flit_width-1:0] link1_out_flit_o;
    wire [vchannels-1:0] link1_out_valid_o;
    wire [vchannels-1:0] link1_out_ready_i;
    
    //connections to router 2 
    wire [flit_width-1:0] link2_in_flit_i;
    wire [vchannels-1:0] link2_in_valid_i;
    wire [vchannels-1:0] link2_in_ready_o;
    wire [flit_width-1:0] link2_out_flit_o;
    wire [vchannels-1:0] link2_out_valid_o;
    wire [vchannels-1:0] link2_out_ready_i;
    
    //connections to router 3 
    wire [flit_width-1:0] link3_in_flit_i;
    wire [vchannels-1:0] link3_in_valid_i;
    wire [vchannels-1:0] link3_in_ready_o;
    wire [flit_width-1:0] link3_out_flit_o;
    wire [vchannels-1:0] link3_out_valid_o;
    wire [vchannels-1:0] link3_out_ready_i;

    
    /*Network interface controler to be connected to router 0*/
           // Instantiation of Axi Bus Interface S00_AXI
            lisnoc_mp_interface # ( 
                .C_S00_AXI_ID_WIDTH(C_S00_AXI_ID_WIDTH),
                .C_S00_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
                .C_S00_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH),
                .C_S00_AXI_AWUSER_WIDTH(C_S00_AXI_AWUSER_WIDTH),
                .C_S00_AXI_ARUSER_WIDTH(C_S00_AXI_ARUSER_WIDTH),
                .C_S00_AXI_WUSER_WIDTH(C_S00_AXI_WUSER_WIDTH),
                .C_S00_AXI_RUSER_WIDTH(C_S00_AXI_RUSER_WIDTH),
                .C_S00_AXI_BUSER_WIDTH(C_S00_AXI_BUSER_WIDTH),
                
                .flit_data_width(flit_data_width),
                .flit_type_width(flit_type_width),
                .flit_address_width(ph_dest_width),
                
                .vchannels(vchannels)
                
            ) AXI_NIC_0 (
                .s00_axi_aclk(s00_axi_aclk),
                .s00_axi_aresetn(s00_axi_aresetn),
                .s00_axi_awid(s00_axi_awid),
                .s00_axi_awaddr(s00_axi_awaddr),
                .s00_axi_awlen(s00_axi_awlen),
                .s00_axi_awsize(s00_axi_awsize),
                .s00_axi_awburst(s00_axi_awburst),
                .s00_axi_awlock(s00_axi_awlock),
                .s00_axi_awcache(s00_axi_awcache),
                .s00_axi_awprot(s00_axi_awprot),
                .s00_axi_awqos(s00_axi_awqos),
                .s00_axi_awregion(s00_axi_awregion),
                .s00_axi_awuser(s00_axi_awuser),
                .s00_axi_awvalid(s00_axi_awvalid),
                .s00_axi_awready(s00_axi_awready),
                .s00_axi_wdata(s00_axi_wdata),
                .s00_axi_wstrb(s00_axi_wstrb),
                .s00_axi_wlast(s00_axi_wlast),
                .s00_axi_wuser(s00_axi_wuser),
                .s00_axi_wvalid(s00_axi_wvalid),
                .s00_axi_wready(s00_axi_wready),
                .s00_axi_bid(s00_axi_bid),
                .s00_axi_bresp(s00_axi_bresp),
                .s00_axi_buser(s00_axi_buser),
                .s00_axi_bvalid(s00_axi_bvalid),
                .s00_axi_bready(s00_axi_bready),
                .s00_axi_arid(s00_axi_arid),
                .s00_axi_araddr(s00_axi_araddr),
                .s00_axi_arlen(s00_axi_arlen),
                .s00_axi_arsize(s00_axi_arsize),
                .s00_axi_arburst(s00_axi_arburst),
                .s00_axi_arlock(s00_axi_arlock),
                .s00_axi_arcache(s00_axi_arcache),
                .s00_axi_arprot(s00_axi_arprot),
                .s00_axi_arqos(s00_axi_arqos),
                .s00_axi_arregion(s00_axi_arregion),
                .s00_axi_aruser(s00_axi_aruser),
                .s00_axi_arvalid(s00_axi_arvalid),
                .s00_axi_arready(s00_axi_arready),
                .s00_axi_rid(s00_axi_rid),
                .s00_axi_rdata(s00_axi_rdata),
                .s00_axi_rresp(s00_axi_rresp),
                .s00_axi_rlast(s00_axi_rlast),
                .s00_axi_ruser(s00_axi_ruser),
                .s00_axi_rvalid(s00_axi_rvalid),
                .s00_axi_rready(s00_axi_rready),
                
                .clk(clk),
                .rst(rst),
             
                .in_flit_i(link0_out_flit_o), 
                .in_valid_i(link0_out_valid_o), 
                .in_ready_o(link0_out_ready_i),
                .out_flit_o(link0_in_flit_i), 
                .out_valid_o(link0_in_valid_i), 
                .out_ready_i(link0_in_ready_o)
            );
    
    /*Network interface controler to be connected to router 1*/
    
    lisnoc_mp_interface
    #(
        .C_S00_AXI_ID_WIDTH(C_S01_AXI_ID_WIDTH),
        .C_S00_AXI_DATA_WIDTH(C_S01_AXI_DATA_WIDTH),
        .C_S00_AXI_ADDR_WIDTH(C_S01_AXI_ADDR_WIDTH),
        .C_S00_AXI_AWUSER_WIDTH(C_S01_AXI_AWUSER_WIDTH),
        .C_S00_AXI_ARUSER_WIDTH(C_S01_AXI_ARUSER_WIDTH),
        .C_S00_AXI_WUSER_WIDTH(C_S01_AXI_WUSER_WIDTH),
        .C_S00_AXI_RUSER_WIDTH(C_S01_AXI_RUSER_WIDTH),
        .C_S00_AXI_BUSER_WIDTH(C_S01_AXI_BUSER_WIDTH),
    
        .flit_data_width(flit_data_width),
        .flit_type_width(flit_type_width),
        .flit_address_width(ph_dest_width),
        
        .vchannels(vchannels)
        
        //.in_fifo_length(in_fifo_length),
        //.out_fifo_length(out_fifo_length)
    ) AXI_NIC_1
    (
        .s00_axi_aclk(s01_axi_aclk),
        .s00_axi_aresetn(s01_axi_aresetn),
        .s00_axi_awid(s01_axi_awid),
        .s00_axi_awaddr(s01_axi_awaddr),
        .s00_axi_awlen(s01_axi_awlen),
        .s00_axi_awsize(s01_axi_awsize),
        .s00_axi_awburst(s01_axi_awburst),
        .s00_axi_awlock(s01_axi_awlock),
        .s00_axi_awcache(s01_axi_awcache),
        .s00_axi_awprot(s01_axi_awprot),
        .s00_axi_awqos(s01_axi_awqos),
        .s00_axi_awregion(s01_axi_awregion),
        .s00_axi_awuser(s01_axi_awuser),
        .s00_axi_awvalid(s01_axi_awvalid),
        .s00_axi_awready(s01_axi_awready),
        .s00_axi_wdata(s01_axi_wdata),
        .s00_axi_wstrb(s01_axi_wstrb),
        .s00_axi_wlast(s01_axi_wlast),
        .s00_axi_wuser(s01_axi_wuser),
        .s00_axi_wvalid(s01_axi_wvalid),
        .s00_axi_wready(s01_axi_wready),
        .s00_axi_bid(s01_axi_bid),
        .s00_axi_bresp(s01_axi_bresp),
        .s00_axi_buser(s01_axi_buser),
        .s00_axi_bvalid(s01_axi_bvalid),
        .s00_axi_bready(s01_axi_bready),
        .s00_axi_arid(s01_axi_arid),
        .s00_axi_araddr(s01_axi_araddr),
        .s00_axi_arlen(s01_axi_arlen),
        .s00_axi_arsize(s01_axi_arsize),
        .s00_axi_arburst(s01_axi_arburst),
        .s00_axi_arlock(s01_axi_arlock),
        .s00_axi_arcache(s01_axi_arcache),
        .s00_axi_arprot(s01_axi_arprot),
        .s00_axi_arqos(s01_axi_arqos),
        .s00_axi_arregion(s01_axi_arregion),
        .s00_axi_aruser(s01_axi_aruser),
        .s00_axi_arvalid(s01_axi_arvalid),
        .s00_axi_arready(s01_axi_arready),
        .s00_axi_rid(s01_axi_rid),
        .s00_axi_rdata(s01_axi_rdata),
        .s00_axi_rresp(s01_axi_rresp),
        .s00_axi_rlast(s01_axi_rlast),
        .s00_axi_ruser(s01_axi_ruser),
        .s00_axi_rvalid(s01_axi_rvalid),
        .s00_axi_rready(s01_axi_rready),
    
        .clk(clk),
        .rst(rst),
    
     
        .in_flit_i(link1_out_flit_o), 
        .in_valid_i(link1_out_valid_o), 
        .in_ready_o(link1_out_ready_i),
        .out_flit_o(link1_in_flit_i), 
        .out_valid_o(link1_in_valid_i), 
        .out_ready_i(link1_in_ready_o)
    );
    
    //Network interface controler to be connected to router 2
    lisnoc_mp_interface
    #(
        .C_S00_AXI_ID_WIDTH(C_S02_AXI_ID_WIDTH),
        .C_S00_AXI_DATA_WIDTH(C_S02_AXI_DATA_WIDTH),
        .C_S00_AXI_ADDR_WIDTH(C_S02_AXI_ADDR_WIDTH),
        .C_S00_AXI_AWUSER_WIDTH(C_S02_AXI_AWUSER_WIDTH),
        .C_S00_AXI_ARUSER_WIDTH(C_S02_AXI_ARUSER_WIDTH),
        .C_S00_AXI_WUSER_WIDTH(C_S02_AXI_WUSER_WIDTH),
        .C_S00_AXI_RUSER_WIDTH(C_S02_AXI_RUSER_WIDTH),
        .C_S00_AXI_BUSER_WIDTH(C_S02_AXI_BUSER_WIDTH),
    
        .flit_data_width(flit_data_width),
        .flit_type_width(flit_type_width),
        .flit_address_width(ph_dest_width),
        
        .vchannels(vchannels)
        
        //.in_fifo_length(in_fifo_length),
        //.out_fifo_length(out_fifo_length)
    ) AXI_NIC_2
    (
        .s00_axi_aclk(s02_axi_aclk),
        .s00_axi_aresetn(s02_axi_aresetn),
        .s00_axi_awid(s02_axi_awid),
        .s00_axi_awaddr(s02_axi_awaddr),
        .s00_axi_awlen(s02_axi_awlen),
        .s00_axi_awsize(s02_axi_awsize),
        .s00_axi_awburst(s02_axi_awburst),
        .s00_axi_awlock(s02_axi_awlock),
        .s00_axi_awcache(s02_axi_awcache),
        .s00_axi_awprot(s02_axi_awprot),
        .s00_axi_awqos(s02_axi_awqos),
        .s00_axi_awregion(s02_axi_awregion),
        .s00_axi_awuser(s02_axi_awuser),
        .s00_axi_awvalid(s02_axi_awvalid),
        .s00_axi_awready(s02_axi_awready),
        .s00_axi_wdata(s02_axi_wdata),
        .s00_axi_wstrb(s02_axi_wstrb),
        .s00_axi_wlast(s02_axi_wlast),
        .s00_axi_wuser(s02_axi_wuser),
        .s00_axi_wvalid(s02_axi_wvalid),
        .s00_axi_wready(s02_axi_wready),
        .s00_axi_bid(s02_axi_bid),
        .s00_axi_bresp(s02_axi_bresp),
        .s00_axi_buser(s02_axi_buser),
        .s00_axi_bvalid(s02_axi_bvalid),
        .s00_axi_bready(s02_axi_bready),
        .s00_axi_arid(s02_axi_arid),
        .s00_axi_araddr(s02_axi_araddr),
        .s00_axi_arlen(s02_axi_arlen),
        .s00_axi_arsize(s02_axi_arsize),
        .s00_axi_arburst(s02_axi_arburst),
        .s00_axi_arlock(s02_axi_arlock),
        .s00_axi_arcache(s02_axi_arcache),
        .s00_axi_arprot(s02_axi_arprot),
        .s00_axi_arqos(s02_axi_arqos),
        .s00_axi_arregion(s02_axi_arregion),
        .s00_axi_aruser(s02_axi_aruser),
        .s00_axi_arvalid(s02_axi_arvalid),
        .s00_axi_arready(s02_axi_arready),
        .s00_axi_rid(s02_axi_rid),
        .s00_axi_rdata(s02_axi_rdata),
        .s00_axi_rresp(s02_axi_rresp),
        .s00_axi_rlast(s02_axi_rlast),
        .s00_axi_ruser(s02_axi_ruser),
        .s00_axi_rvalid(s02_axi_rvalid),
        .s00_axi_rready(s02_axi_rready),
    
        .clk(clk),
        .rst(rst),
    
        .in_flit_i(link2_out_flit_o), 
        .in_valid_i(link2_out_valid_o), 
        .in_ready_o(link2_out_ready_i),
        .out_flit_o(link2_in_flit_i), 
        .out_valid_o(link2_in_valid_i), 
        .out_ready_i(link2_in_ready_o)
    );
    
    //Network interface controler to be connected to router 3
    lisnoc_mp_interface
    #(
    
        .C_S00_AXI_ID_WIDTH(C_S03_AXI_ID_WIDTH),
        .C_S00_AXI_DATA_WIDTH(C_S03_AXI_DATA_WIDTH),
        .C_S00_AXI_ADDR_WIDTH(C_S03_AXI_ADDR_WIDTH),
        .C_S00_AXI_AWUSER_WIDTH(C_S03_AXI_AWUSER_WIDTH),
        .C_S00_AXI_ARUSER_WIDTH(C_S03_AXI_ARUSER_WIDTH),
        .C_S00_AXI_WUSER_WIDTH(C_S03_AXI_WUSER_WIDTH),
        .C_S00_AXI_RUSER_WIDTH(C_S03_AXI_RUSER_WIDTH),
        .C_S00_AXI_BUSER_WIDTH(C_S03_AXI_BUSER_WIDTH),    
    
        .flit_data_width(flit_data_width),
        .flit_type_width(flit_type_width),
        .flit_address_width(ph_dest_width),
        
        .vchannels(vchannels)
        
        //.in_fifo_length(in_fifo_length),
        //.out_fifo_length(out_fifo_length)
    ) AXI_NIC_3
    (
        .s00_axi_aclk(s03_axi_aclk),
        .s00_axi_aresetn(s03_axi_aresetn),
        .s00_axi_awid(s03_axi_awid),
        .s00_axi_awaddr(s03_axi_awaddr),
        .s00_axi_awlen(s03_axi_awlen),
        .s00_axi_awsize(s03_axi_awsize),
        .s00_axi_awburst(s03_axi_awburst),
        .s00_axi_awlock(s03_axi_awlock),
        .s00_axi_awcache(s03_axi_awcache),
        .s00_axi_awprot(s03_axi_awprot),
        .s00_axi_awqos(s03_axi_awqos),
        .s00_axi_awregion(s03_axi_awregion),
        .s00_axi_awuser(s03_axi_awuser),
        .s00_axi_awvalid(s03_axi_awvalid),
        .s00_axi_awready(s03_axi_awready),
        .s00_axi_wdata(s03_axi_wdata),
        .s00_axi_wstrb(s03_axi_wstrb),
        .s00_axi_wlast(s03_axi_wlast),
        .s00_axi_wuser(s03_axi_wuser),
        .s00_axi_wvalid(s03_axi_wvalid),
        .s00_axi_wready(s03_axi_wready),
        .s00_axi_bid(s03_axi_bid),
        .s00_axi_bresp(s03_axi_bresp),
        .s00_axi_buser(s03_axi_buser),
        .s00_axi_bvalid(s03_axi_bvalid),
        .s00_axi_bready(s03_axi_bready),
        .s00_axi_arid(s03_axi_arid),
        .s00_axi_araddr(s03_axi_araddr),
        .s00_axi_arlen(s03_axi_arlen),
        .s00_axi_arsize(s03_axi_arsize),
        .s00_axi_arburst(s03_axi_arburst),
        .s00_axi_arlock(s03_axi_arlock),
        .s00_axi_arcache(s03_axi_arcache),
        .s00_axi_arprot(s03_axi_arprot),
        .s00_axi_arqos(s03_axi_arqos),
        .s00_axi_arregion(s03_axi_arregion),
        .s00_axi_aruser(s03_axi_aruser),
        .s00_axi_arvalid(s03_axi_arvalid),
        .s00_axi_arready(s03_axi_arready),
        .s00_axi_rid(s03_axi_rid),
        .s00_axi_rdata(s03_axi_rdata),
        .s00_axi_rresp(s03_axi_rresp),
        .s00_axi_rlast(s03_axi_rlast),
        .s00_axi_ruser(s03_axi_ruser),
        .s00_axi_rvalid(s03_axi_rvalid),
        .s00_axi_rready(s03_axi_rready),
    
        .clk(clk),
        .rst(rst),
    
     
        .in_flit_i(link3_out_flit_o), 
        .in_valid_i(link3_out_valid_o), 
        .in_ready_o(link3_out_ready_i),
        .out_flit_o(link3_in_flit_i), 
        .out_valid_o(link3_in_valid_i), 
        .out_ready_i(link3_in_ready_o)
    );
    
    
    
    /*
     * Mesh of routers to be connected to the MP interfaces, thus, the actual network
     */
    lisnoc_mesh2x2
    #(
         .flit_data_width(flit_data_width),
         .flit_type_width(flit_type_width),
         .ph_dest_width(ph_dest_width),
        
         .vchannels(vchannels),
        
         .in_fifo_length(in_fifo_length),
         .out_fifo_length(out_fifo_length)
     )
     network(
        .clk(clk),
        .rst(rst),
     
        .link0_in_flit_i(link0_in_flit_i), 
        .link0_in_valid_i(link0_in_valid_i), 
        .link0_in_ready_o(link0_in_ready_o),
        .link0_out_flit_o(link0_out_flit_o), 
        .link0_out_valid_o(link0_out_valid_o), 
        .link0_out_ready_i(link0_out_ready_i),
        
        .link1_in_flit_i(link1_in_flit_i), 
        .link1_in_valid_i(link1_in_valid_i), 
        .link1_in_ready_o(link1_in_ready_o),
        .link1_out_flit_o(link1_out_flit_o), 
        .link1_out_valid_o(link1_out_valid_o), 
        .link1_out_ready_i(link1_out_ready_i),
    
        .link2_in_flit_i(link2_in_flit_i), 
        .link2_in_valid_i(link2_in_valid_i), 
        .link2_in_ready_o(link2_in_ready_o),
        .link2_out_flit_o(link2_out_flit_o), 
        .link2_out_valid_o(link2_out_valid_o), 
        .link2_out_ready_i(link2_out_ready_i),
    
        .link3_in_flit_i(link3_in_flit_i), 
        .link3_in_valid_i(link3_in_valid_i), 
        .link3_in_ready_o(link3_in_ready_o),
        .link3_out_flit_o(link3_out_flit_o), 
        .link3_out_valid_o(link3_out_valid_o), 
        .link3_out_ready_i(link3_out_ready_i)
    );

endmodule
