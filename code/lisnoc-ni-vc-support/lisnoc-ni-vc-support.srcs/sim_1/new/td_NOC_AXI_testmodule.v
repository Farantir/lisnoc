`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/26/2019 11:04:17 AM
// Design Name: 
// Module Name: td_NOC_AXI_testmodule
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
/*
 * this testbench is used and adapted to find errors before systesis and to recreate observed errors in 
 * systhesised hardware. It uses a simple, mostly aouto genereated AXI Master to connect to the NICs of the network module 
 */
/*
 Testbench
+----------------------------------------------------+
|                                                    |
|  AXI Master for testing       NOC AXI Tesmodule    |
| +----------------------+     +------------------+  |
| |                      | AXI |                  |  |
| |                      +----->                  |  |
| |                      <-----+                  |  |
| |                      |     |                  |  |
| +----------------------+     |                  |  |
|                              |                  |  |
|  AXI Master for testing      |                  |  |
| +----------------------+     |                  |  |
| |                      | AXI |                  |  |
| |                      +----->                  |  |
| |                      <-----+                  |  |
| |                      |     |                  |  |
| +----------------------+     |                  |  |
|                              |                  |  |
|  AXI Master for testing      |                  |  |
| +----------------------+     |                  |  |
| |                      | AXI |                  |  |
| |                      +----->                  |  |
| |                      <-----+                  |  |
| |                      |     |                  |  |
| +----------------------+     |                  |  |
|                              |                  |  |
|  AXI Master for testing      |                  |  |
| +----------------------+     |                  |  |
| |                      | AXI |                  |  |
| |                      +----->                  |  |
| |                      <-----+                  |  |
| |                      |     |                  |  |
| +----------------------+     +------------------+  |
|                                                    |
+----------------------------------------------------+

*/
module td_NOC_AXI_testmodule(


    );
    /*
     * Configuration parameters of the lisnoc interface
     */
    parameter flit_data_width = 32;
    parameter flit_type_width = 2;
    localparam flit_width = flit_data_width+flit_type_width;
    parameter ph_dest_width = 4;
    
    //first bit of the axi adderess (counted from the left) that is part of the packets priority
    parameter vchannel_select_lefoffset = 0;
    //amount of bits ini the axi address, that are part of the packets priority
    parameter vchannel_select_bits = 2;
    parameter vchannels = 4;
    
    parameter in_fifo_length = 4;
    parameter out_fifo_length = 4;


        // Parameters of Axi Master Bus Interface M00_AXI
    parameter integer C_M00_AXI_BURST_LEN    = 8;
    parameter integer C_M00_AXI_ID_WIDTH    = 12;
    parameter integer C_M00_AXI_ADDR_WIDTH    = 16;
    parameter integer C_M00_AXI_DATA_WIDTH    = 32;
    parameter integer C_M00_AXI_AWUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_ARUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_WUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_RUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_BUSER_WIDTH    = 0;


    // Users to add ports here

    // User ports ends
    // Do not modify the ports beyond this line


    // Ports of Axi Master Bus Interface M00_AXI
    reg  m00_axi_init_axi_txn;
    wire  m00_axi_txn_done;
    wire  m00_axi_error;
    reg  m00_axi_aclk;
    reg  m00_axi_aresetn;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_awid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_awaddr;
    wire [7 : 0] m00_axi_awlen;
    wire [2 : 0] m00_axi_awsize;
    wire [1 : 0] m00_axi_awburst;
    wire  m00_axi_awlock;
    wire [3 : 0] m00_axi_awcache;
    wire [2 : 0] m00_axi_awprot;
    wire [3 : 0] m00_axi_awqos;
    wire [C_M00_AXI_AWUSER_WIDTH-1 : 0] m00_axi_awuser;
    wire  m00_axi_awvalid;
    wire  m00_axi_awready;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_wdata;
    wire [C_M00_AXI_DATA_WIDTH/8-1 : 0] m00_axi_wstrb;
    wire  m00_axi_wlast;
    wire [C_M00_AXI_WUSER_WIDTH-1 : 0] m00_axi_wuser;
    wire  m00_axi_wvalid;
    wire  m00_axi_wready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_bid;
    wire [1 : 0] m00_axi_bresp;
    wire [C_M00_AXI_BUSER_WIDTH-1 : 0] m00_axi_buser;
    wire  m00_axi_bvalid;
    wire  m00_axi_bready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_arid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_araddr;
    wire [7 : 0] m00_axi_arlen;
    wire [2 : 0] m00_axi_arsize;
    wire [1 : 0] m00_axi_arburst;
    wire  m00_axi_arlock;
    wire [3 : 0] m00_axi_arcache;
    wire [2 : 0] m00_axi_arprot;
    wire [3 : 0] m00_axi_arqos;
    wire [C_M00_AXI_ARUSER_WIDTH-1 : 0] m00_axi_aruser;
    wire  m00_axi_arvalid;
    wire  m00_axi_arready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_rid;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_rdata;
    wire [1 : 0] m00_axi_rresp;
    wire  m00_axi_rlast;
    wire [C_M00_AXI_RUSER_WIDTH-1 : 0] m00_axi_ruser;
    wire  m00_axi_rvalid;
    wire  m00_axi_rready;
    
    // Ports of Axi Master Bus Interface M01_AXI
    reg  m01_axi_init_axi_txn;
    wire  m01_axi_txn_done;
    wire  m01_axi_error;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m01_axi_awid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m01_axi_awaddr;
    wire [7 : 0] m01_axi_awlen;
    wire [2 : 0] m01_axi_awsize;
    wire [1 : 0] m01_axi_awburst;
    wire  m01_axi_awlock;
    wire [3 : 0] m01_axi_awcache;
    wire [2 : 0] m01_axi_awprot;
    wire [3 : 0] m01_axi_awqos;
    wire [C_M00_AXI_AWUSER_WIDTH-1 : 0] m01_axi_awuser;
    wire  m01_axi_awvalid;
    wire  m01_axi_awready;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m01_axi_wdata;
    wire [C_M00_AXI_DATA_WIDTH/8-1 : 0] m01_axi_wstrb;
    wire  m01_axi_wlast;
    wire [C_M00_AXI_WUSER_WIDTH-1 : 0] m01_axi_wuser;
    wire  m01_axi_wvalid;
    wire  m01_axi_wready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m01_axi_bid;
    wire [1 : 0] m01_axi_bresp;
    wire [C_M00_AXI_BUSER_WIDTH-1 : 0] m01_axi_buser;
    wire  m01_axi_bvalid;
    wire  m01_axi_bready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m01_axi_arid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m01_axi_araddr;
    wire [7 : 0] m01_axi_arlen;
    wire [2 : 0] m01_axi_arsize;
    wire [1 : 0] m01_axi_arburst;
    wire  m01_axi_arlock;
    wire [3 : 0] m01_axi_arcache;
    wire [2 : 0] m01_axi_arprot;
    wire [3 : 0] m01_axi_arqos;
    wire [C_M00_AXI_ARUSER_WIDTH-1 : 0] m01_axi_aruser;
    wire  m01_axi_arvalid;
    wire  m01_axi_arready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m01_axi_rid;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m01_axi_rdata;
    wire [1 : 0] m01_axi_rresp;
    wire  m01_axi_rlast;
    wire [C_M00_AXI_RUSER_WIDTH-1 : 0] m01_axi_ruser;
    wire  m01_axi_rvalid;
    wire  m01_axi_rready;
    
    
        // Ports of Axi Master Bus Interface M02_AXI
    reg  m02_axi_init_axi_txn;
    wire  m02_axi_txn_done;
    wire  m02_axi_error;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m02_axi_awid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m02_axi_awaddr;
    wire [7 : 0] m02_axi_awlen;
    wire [2 : 0] m02_axi_awsize;
    wire [1 : 0] m02_axi_awburst;
    wire  m02_axi_awlock;
    wire [3 : 0] m02_axi_awcache;
    wire [2 : 0] m02_axi_awprot;
    wire [3 : 0] m02_axi_awqos;
    wire [C_M00_AXI_AWUSER_WIDTH-1 : 0] m02_axi_awuser;
    wire  m02_axi_awvalid;
    wire  m02_axi_awready;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m02_axi_wdata;
    wire [C_M00_AXI_DATA_WIDTH/8-1 : 0] m02_axi_wstrb;
    wire  m02_axi_wlast;
    wire [C_M00_AXI_WUSER_WIDTH-1 : 0] m02_axi_wuser;
    wire  m02_axi_wvalid;
    wire  m02_axi_wready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m02_axi_bid;
    wire [1 : 0] m02_axi_bresp;
    wire [C_M00_AXI_BUSER_WIDTH-1 : 0] m02_axi_buser;
    wire  m02_axi_bvalid;
    wire  m02_axi_bready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m02_axi_arid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m02_axi_araddr;
    wire [7 : 0] m02_axi_arlen;
    wire [2 : 0] m02_axi_arsize;
    wire [1 : 0] m02_axi_arburst;
    wire  m02_axi_arlock;
    wire [3 : 0] m02_axi_arcache;
    wire [2 : 0] m02_axi_arprot;
    wire [3 : 0] m02_axi_arqos;
    wire [C_M00_AXI_ARUSER_WIDTH-1 : 0] m02_axi_aruser;
    wire  m02_axi_arvalid;
    wire  m02_axi_arready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m02_axi_rid;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m02_axi_rdata;
    wire [1 : 0] m02_axi_rresp;
    wire  m02_axi_rlast;
    wire [C_M00_AXI_RUSER_WIDTH-1 : 0] m02_axi_ruser;
    wire  m02_axi_rvalid;
    wire  m02_axi_rready;
    
        // Ports of Axi Master Bus Interface M03_AXI
    reg  m03_axi_init_axi_txn;
    wire  m03_axi_txn_done;
    wire  m03_axi_error;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m03_axi_awid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m03_axi_awaddr;
    wire [7 : 0] m03_axi_awlen;
    wire [2 : 0] m03_axi_awsize;
    wire [1 : 0] m03_axi_awburst;
    wire  m03_axi_awlock;
    wire [3 : 0] m03_axi_awcache;
    wire [2 : 0] m03_axi_awprot;
    wire [3 : 0] m03_axi_awqos;
    wire [C_M00_AXI_AWUSER_WIDTH-1 : 0] m03_axi_awuser;
    wire  m03_axi_awvalid;
    wire  m03_axi_awready;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m03_axi_wdata;
    wire [C_M00_AXI_DATA_WIDTH/8-1 : 0] m03_axi_wstrb;
    wire  m03_axi_wlast;
    wire [C_M00_AXI_WUSER_WIDTH-1 : 0] m03_axi_wuser;
    wire  m03_axi_wvalid;
    wire  m03_axi_wready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m03_axi_bid;
    wire [1 : 0] m03_axi_bresp;
    wire [C_M00_AXI_BUSER_WIDTH-1 : 0] m03_axi_buser;
    wire  m03_axi_bvalid;
    wire  m03_axi_bready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m03_axi_arid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m03_axi_araddr;
    wire [7 : 0] m03_axi_arlen;
    wire [2 : 0] m03_axi_arsize;
    wire [1 : 0] m03_axi_arburst;
    wire  m03_axi_arlock;
    wire [3 : 0] m03_axi_arcache;
    wire [2 : 0] m03_axi_arprot;
    wire [3 : 0] m03_axi_arqos;
    wire [C_M00_AXI_ARUSER_WIDTH-1 : 0] m03_axi_aruser;
    wire  m03_axi_arvalid;
    wire  m03_axi_arready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m03_axi_rid;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m03_axi_rdata;
    wire [1 : 0] m03_axi_rresp;
    wire  m03_axi_rlast;
    wire [C_M00_AXI_RUSER_WIDTH-1 : 0] m03_axi_ruser;
    wire  m03_axi_rvalid;
    wire  m03_axi_rready;
    /*
     * resetting the whole setup
     */
    
    always #2  m00_axi_aclk = ~m00_axi_aclk;
    initial
    begin
    m00_axi_aclk = 0;
    m00_axi_aresetn = 1;
    #1 m00_axi_aresetn = 0;
    
    #0 m01_axi_init_axi_txn = 1;
    #0 m00_axi_init_axi_txn = 1;
    #0 m02_axi_init_axi_txn = 1;
    #0 m03_axi_init_axi_txn = 1;
    #20 m00_axi_aresetn = 1;
    end
    
    wire [0:31] latency;
    wire [0:31] throughput;
    wire [0:3]packet_int;
    //probe for performance testing
    lisnoc_probe probe_inst
    (
        //signals from the sending axi master
        .axiwvalid_i(m00_axi_awvalid),
        .axiwready_i(m00_axi_awready),
        //signal from the recieving lisnoc node
        .packet_arived_i(packet_int),
        .latency_o(latency),
        .throughput_o(throughput),
        //control signals
        .clk(m00_axi_aclk),
        .axi_reset(m00_axi_aresetn),
        .reset(0)
    );
    
    // Instantiation of Axi Bus Interface M00_AXI
    axi_master_for_testing_v1_0_M00_AXI # ( 
        .C_M_TARGET_SLAVE_BASE_ADDR(32'h1000),
        .C_M_AXI_BURST_LEN(C_M00_AXI_BURST_LEN),
        .C_M_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
        .C_M_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
        .C_M_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
        .C_M_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
        .C_M_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
        .C_M_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
        .C_M_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
        .C_M_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH),
        .axi_start_data(1),
        .write_count(100),
        .start_delay(0)
    ) axi_master_for_testing_v1_0_M00_AXI_inst (
        .INIT_AXI_TXN(m00_axi_init_axi_txn),
        .TXN_DONE(m00_axi_txn_done),
        .ERROR(m00_axi_error),
        .M_AXI_ACLK(m00_axi_aclk),
        .M_AXI_ARESETN(m00_axi_aresetn),
        .M_AXI_AWID(m00_axi_awid),
        .M_AXI_AWADDR(m00_axi_awaddr),
        .M_AXI_AWLEN(m00_axi_awlen),
        .M_AXI_AWSIZE(m00_axi_awsize),
        .M_AXI_AWBURST(m00_axi_awburst),
        .M_AXI_AWLOCK(m00_axi_awlock),
        .M_AXI_AWCACHE(m00_axi_awcache),
        .M_AXI_AWPROT(m00_axi_awprot),
        .M_AXI_AWQOS(m00_axi_awqos),
        .M_AXI_AWUSER(m00_axi_awuser),
        .M_AXI_AWVALID(m00_axi_awvalid),
        .M_AXI_AWREADY(m00_axi_awready),
        .M_AXI_WDATA(m00_axi_wdata),
        .M_AXI_WSTRB(m00_axi_wstrb),
        .M_AXI_WLAST(m00_axi_wlast),
        .M_AXI_WUSER(m00_axi_wuser),
        .M_AXI_WVALID(m00_axi_wvalid),
        .M_AXI_WREADY(m00_axi_wready),
        .M_AXI_BID(m00_axi_bid),
        .M_AXI_BRESP(m00_axi_bresp),
        .M_AXI_BUSER(m00_axi_buser),
        .M_AXI_BVALID(m00_axi_bvalid),
        .M_AXI_BREADY(m00_axi_bready),
        .M_AXI_ARID(m00_axi_arid),
        .M_AXI_ARADDR(m00_axi_araddr),
        .M_AXI_ARLEN(m00_axi_arlen),
        .M_AXI_ARSIZE(m00_axi_arsize),
        .M_AXI_ARBURST(m00_axi_arburst),
        .M_AXI_ARLOCK(m00_axi_arlock),
        .M_AXI_ARCACHE(m00_axi_arcache),
        .M_AXI_ARPROT(m00_axi_arprot),
        .M_AXI_ARQOS(m00_axi_arqos),
        .M_AXI_ARUSER(m00_axi_aruser),
        .M_AXI_ARVALID(m00_axi_arvalid),
        .M_AXI_ARREADY(m00_axi_arready),
        .M_AXI_RID(m00_axi_rid),
        .M_AXI_RDATA(m00_axi_rdata),
        .M_AXI_RRESP(m00_axi_rresp),
        .M_AXI_RLAST(m00_axi_rlast),
        .M_AXI_RUSER(m00_axi_ruser),
        .M_AXI_RVALID(m00_axi_rvalid),
        .M_AXI_RREADY(m00_axi_rready)
    );
    
        // Instantiation of Axi Bus Interface M01_AXI
    axi_master_for_testing_v1_0_M00_AXI # ( 
        .C_M_TARGET_SLAVE_BASE_ADDR(32'h0000),
        .C_M_AXI_BURST_LEN(C_M00_AXI_BURST_LEN),
        .C_M_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
        .C_M_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
        .C_M_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
        .C_M_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
        .C_M_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
        .C_M_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
        .C_M_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
        .C_M_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH),
        .axi_start_data(32),
        .write_count(0)
    ) axi_master_for_testing_v1_0_M01_AXI_inst (           
        .INIT_AXI_TXN(m01_axi_init_axi_txn),
        .TXN_DONE(m01_axi_txn_done),
        .ERROR(m01_axi_error),
        .M_AXI_ACLK(m00_axi_aclk),
        .M_AXI_ARESETN(m00_axi_aresetn),
        .M_AXI_AWID(m01_axi_awid),
        .M_AXI_AWADDR(m01_axi_awaddr),
        .M_AXI_AWLEN(m01_axi_awlen),
        .M_AXI_AWSIZE(m01_axi_awsize),
        .M_AXI_AWBURST(m01_axi_awburst),
        .M_AXI_AWLOCK(m01_axi_awlock),
        .M_AXI_AWCACHE(m01_axi_awcache),
        .M_AXI_AWPROT(m01_axi_awprot),
        .M_AXI_AWQOS(m01_axi_awqos),
        .M_AXI_AWUSER(m01_axi_awuser),
        .M_AXI_AWVALID(m01_axi_awvalid),
        .M_AXI_AWREADY(m01_axi_awready),
        .M_AXI_WDATA(m01_axi_wdata),
        .M_AXI_WSTRB(m01_axi_wstrb),
        .M_AXI_WLAST(m01_axi_wlast),
        .M_AXI_WUSER(m01_axi_wuser),
        .M_AXI_WVALID(m01_axi_wvalid),
        .M_AXI_WREADY(m01_axi_wready),
        .M_AXI_BID(m01_axi_bid),
        .M_AXI_BRESP(m01_axi_bresp),
        .M_AXI_BUSER(m01_axi_buser),
        .M_AXI_BVALID(m01_axi_bvalid),
        .M_AXI_BREADY(m01_axi_bready),
        .M_AXI_ARID(m01_axi_arid),
        .M_AXI_ARADDR(m01_axi_araddr),
        .M_AXI_ARLEN(m01_axi_arlen),
        .M_AXI_ARSIZE(m01_axi_arsize),
        .M_AXI_ARBURST(m01_axi_arburst),
        .M_AXI_ARLOCK(m01_axi_arlock),
        .M_AXI_ARCACHE(m01_axi_arcache),
        .M_AXI_ARPROT(m01_axi_arprot),
        .M_AXI_ARQOS(m01_axi_arqos),
        .M_AXI_ARUSER(m01_axi_aruser),
        .M_AXI_ARVALID(m01_axi_arvalid),
        .M_AXI_ARREADY(m01_axi_arready),
        .M_AXI_RID(m01_axi_rid),
        .M_AXI_RDATA(m01_axi_rdata),
        .M_AXI_RRESP(m01_axi_rresp),
        .M_AXI_RLAST(m01_axi_rlast),
        .M_AXI_RUSER(m01_axi_ruser),
        .M_AXI_RVALID(m01_axi_rvalid),
        .M_AXI_RREADY(m01_axi_rready)
    );
    
            wire [C_M00_AXI_ADDR_WIDTH-1 : 0] axi_awaddr_i;
            wire [31:0]write_row_counter_o;
            reg [C_M00_AXI_ADDR_WIDTH-1 : 0] custom_write_address [31:0];
            initial
            begin
                for(integer x = 0; x<2000; x = x+4)
                begin
                    custom_write_address[0+x] = 32'h3000;
                    custom_write_address[1+x] = 32'h7000;
                    custom_write_address[2+x] = 32'hb000;
                    custom_write_address[3+x] = 32'hf000;
                end
            end
    
            assign axi_awaddr_i = custom_write_address[write_row_counter_o];
    
            // Instantiation of Axi Bus Interface M02_AXI
axi_master_for_testing_v1_0_M00_AXI # ( 
    .C_M_TARGET_SLAVE_BASE_ADDR(32'h4000),
    .C_M_AXI_BURST_LEN(C_M00_AXI_BURST_LEN),
    .C_M_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
    .C_M_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
    .C_M_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
    .C_M_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
    .C_M_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
    .C_M_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
    .C_M_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
    .C_M_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH),
    .axi_start_data(64),
    
    .use_custom_write_address(1'b0),
    
    .write_count(20)
) axi_master_for_testing_v1_0_M02_AXI_inst (
    .INIT_AXI_TXN(m02_axi_init_axi_txn),
    .TXN_DONE(m02_axi_txn_done),
    .ERROR(m02_axi_error),
    .axi_awaddr_i(axi_awaddr_i),
    .write_row_counter_o(write_row_counter_o),
    .M_AXI_ACLK(m00_axi_aclk),
    .M_AXI_ARESETN(m00_axi_aresetn),
    .M_AXI_AWID(m02_axi_awid),
    .M_AXI_AWADDR(m02_axi_awaddr),
    .M_AXI_AWLEN(m02_axi_awlen),
    .M_AXI_AWSIZE(m02_axi_awsize),
    .M_AXI_AWBURST(m02_axi_awburst),
    .M_AXI_AWLOCK(m02_axi_awlock),
    .M_AXI_AWCACHE(m02_axi_awcache),
    .M_AXI_AWPROT(m02_axi_awprot),
    .M_AXI_AWQOS(m02_axi_awqos),
    .M_AXI_AWUSER(m02_axi_awuser),
    .M_AXI_AWVALID(m02_axi_awvalid),
    .M_AXI_AWREADY(m02_axi_awready),
    .M_AXI_WDATA(m02_axi_wdata),
    .M_AXI_WSTRB(m02_axi_wstrb),
    .M_AXI_WLAST(m02_axi_wlast),
    .M_AXI_WUSER(m02_axi_wuser),
    .M_AXI_WVALID(m02_axi_wvalid),
    .M_AXI_WREADY(m02_axi_wready),
    .M_AXI_BID(m02_axi_bid),
    .M_AXI_BRESP(m02_axi_bresp),
    .M_AXI_BUSER(m02_axi_buser),
    .M_AXI_BVALID(m02_axi_bvalid),
    .M_AXI_BREADY(m02_axi_bready),
    .M_AXI_ARID(m02_axi_arid),
    .M_AXI_ARADDR(m02_axi_araddr),
    .M_AXI_ARLEN(m02_axi_arlen),
    .M_AXI_ARSIZE(m02_axi_arsize),
    .M_AXI_ARBURST(m02_axi_arburst),
    .M_AXI_ARLOCK(m02_axi_arlock),
    .M_AXI_ARCACHE(m02_axi_arcache),
    .M_AXI_ARPROT(m02_axi_arprot),
    .M_AXI_ARQOS(m02_axi_arqos),
    .M_AXI_ARUSER(m02_axi_aruser),
    .M_AXI_ARVALID(m02_axi_arvalid),
    .M_AXI_ARREADY(m02_axi_arready),
    .M_AXI_RID(m02_axi_rid),
    .M_AXI_RDATA(m02_axi_rdata),
    .M_AXI_RRESP(m02_axi_rresp),
    .M_AXI_RLAST(m02_axi_rlast),
    .M_AXI_RUSER(m02_axi_ruser),
    .M_AXI_RVALID(m02_axi_rvalid),
    .M_AXI_RREADY(m02_axi_rready)
);


            wire [C_M00_AXI_ADDR_WIDTH-1 : 0] axi_araddr_i;
            wire [31:0]read_row_counter_o;
            reg [C_M00_AXI_ADDR_WIDTH-1 : 0] custom_read_address [31:0];
            initial
            begin
                for(integer x = 0; x<2000; x = x+4)
                begin
                    custom_read_address[3+x] = 32'h0000;
                    custom_read_address[2+x] = 32'h4000;
                    custom_read_address[1+x] = 32'h8000;
                    custom_read_address[0+x] = 32'hc000;
                end
            end
    
            assign axi_araddr_i = custom_read_address[read_row_counter_o];
            

        // Instantiation of Axi Bus Interface M03_AXI
    axi_master_for_testing_v1_0_M00_AXI # ( 
        .C_M_TARGET_SLAVE_BASE_ADDR(32'h8000),
        .C_M_AXI_BURST_LEN(C_M00_AXI_BURST_LEN),
        .C_M_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
        .C_M_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
        .C_M_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
        .C_M_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
        .C_M_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
        .C_M_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
        .C_M_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
        .C_M_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH),
        .axi_start_data(128),
        .use_custom_read_address(1'b0),
        .write_count(20)
    ) axi_master_for_testing_v1_0_M03_AXI_inst (
        .axi_araddr_i(axi_araddr_i),
        .read_row_counter_o(read_row_counter_o),
        
        .INIT_AXI_TXN(m03_axi_init_axi_txn),
        .TXN_DONE(m03_axi_txn_done),
        .ERROR(m03_axi_error),
        .M_AXI_ACLK(m00_axi_aclk),
        .M_AXI_ARESETN(m00_axi_aresetn),
        .M_AXI_AWID(m03_axi_awid),
        .M_AXI_AWADDR(m03_axi_awaddr),
        .M_AXI_AWLEN(m03_axi_awlen),
        .M_AXI_AWSIZE(m03_axi_awsize),
        .M_AXI_AWBURST(m03_axi_awburst),
        .M_AXI_AWLOCK(m03_axi_awlock),
        .M_AXI_AWCACHE(m03_axi_awcache),
        .M_AXI_AWPROT(m03_axi_awprot),
        .M_AXI_AWQOS(m03_axi_awqos),
        .M_AXI_AWUSER(m03_axi_awuser),
        .M_AXI_AWVALID(m03_axi_awvalid),
        .M_AXI_AWREADY(m03_axi_awready),
        .M_AXI_WDATA(m03_axi_wdata),
        .M_AXI_WSTRB(m03_axi_wstrb),
        .M_AXI_WLAST(m03_axi_wlast),
        .M_AXI_WUSER(m03_axi_wuser),
        .M_AXI_WVALID(m03_axi_wvalid),
        .M_AXI_WREADY(m03_axi_wready),
        .M_AXI_BID(m03_axi_bid),
        .M_AXI_BRESP(m03_axi_bresp),
        .M_AXI_BUSER(m03_axi_buser),
        .M_AXI_BVALID(m03_axi_bvalid),
        .M_AXI_BREADY(m03_axi_bready),
        .M_AXI_ARID(m03_axi_arid),
        .M_AXI_ARADDR(m03_axi_araddr),
        .M_AXI_ARLEN(m03_axi_arlen),
        .M_AXI_ARSIZE(m03_axi_arsize),
        .M_AXI_ARBURST(m03_axi_arburst),
        .M_AXI_ARLOCK(m03_axi_arlock),
        .M_AXI_ARCACHE(m03_axi_arcache),
        .M_AXI_ARPROT(m03_axi_arprot),
        .M_AXI_ARQOS(m03_axi_arqos),
        .M_AXI_ARUSER(m03_axi_aruser),
        .M_AXI_ARVALID(m03_axi_arvalid),
        .M_AXI_ARREADY(m03_axi_arready),
        .M_AXI_RID(m03_axi_rid),
        .M_AXI_RDATA(m03_axi_rdata),
        .M_AXI_RRESP(m03_axi_rresp),
        .M_AXI_RLAST(m03_axi_rlast),
        .M_AXI_RUSER(m03_axi_ruser),
        .M_AXI_RVALID(m03_axi_rvalid),
        .M_AXI_RREADY(m03_axi_rready)
    );
    
    /*MP interface connected to router 0*/
           // Instantiation of Axi Bus Interface S00_AXI
            NOC_AXI_testmodule # ( 
                            
                .C_S00_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
                .C_S00_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
                .C_S00_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
                .C_S00_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
                .C_S00_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
                .C_S00_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
                .C_S00_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
                .C_S00_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH),
                
                
                .C_S01_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
                .C_S01_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
                .C_S01_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
                .C_S01_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
                .C_S01_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
                .C_S01_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
                .C_S01_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
                .C_S01_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH),
                
                .C_S02_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
                .C_S02_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
                .C_S02_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
                .C_S02_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
                .C_S02_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
                .C_S02_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
                .C_S02_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
                .C_S02_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH),
                
                .C_S03_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
                .C_S03_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
                .C_S03_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
                .C_S03_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
                .C_S03_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
                .C_S03_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
                .C_S03_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
                .C_S03_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH),
               
                
                .flit_data_width(flit_data_width),
                .flit_type_width(flit_type_width),
                .flit_address_width(ph_dest_width),
                
                .vchannel_select_bits(vchannel_select_bits),
                .vchannel_select_lefoffset(vchannel_select_lefoffset),
                .vchannels(vchannels)
                
            ) NOC (
                .S1_packet_arrived_o(packet_int),
            
                .s00_axi_aclk(m00_axi_aclk),
                .s00_axi_aresetn(m00_axi_aresetn),
                .s00_axi_awid(m00_axi_awid),
                .s00_axi_awaddr(m00_axi_awaddr),
                .s00_axi_awlen(m00_axi_awlen),
                .s00_axi_awsize(m00_axi_awsize),
                .s00_axi_awburst(m00_axi_awburst),
                .s00_axi_awlock(m00_axi_awlock),
                .s00_axi_awcache(m00_axi_awcache),
                .s00_axi_awprot(m00_axi_awprot),
                .s00_axi_awqos(m00_axi_awqos),
                .s00_axi_awregion(m00_axi_awregion),
                .s00_axi_awuser(m00_axi_awuser),
                .s00_axi_awvalid(m00_axi_awvalid),
                .s00_axi_awready(m00_axi_awready),
                .s00_axi_wdata(m00_axi_wdata),
                .s00_axi_wstrb(m00_axi_wstrb),
                .s00_axi_wlast(m00_axi_wlast),
                .s00_axi_wuser(m00_axi_wuser),
                .s00_axi_wvalid(m00_axi_wvalid),
                .s00_axi_wready(m00_axi_wready),
                .s00_axi_bid(m00_axi_bid),
                .s00_axi_bresp(m00_axi_bresp),
                .s00_axi_buser(m00_axi_buser),
                .s00_axi_bvalid(m00_axi_bvalid),
                .s00_axi_bready(m00_axi_bready),
                .s00_axi_arid(m00_axi_arid),
                .s00_axi_araddr(m00_axi_araddr),
                .s00_axi_arlen(m00_axi_arlen),
                .s00_axi_arsize(m00_axi_arsize),
                .s00_axi_arburst(m00_axi_arburst),
                .s00_axi_arlock(m00_axi_arlock),
                .s00_axi_arcache(m00_axi_arcache),
                .s00_axi_arprot(m00_axi_arprot),
                .s00_axi_arqos(m00_axi_arqos),
                .s00_axi_arregion(m00_axi_arregion),
                .s00_axi_aruser(m00_axi_aruser),
                .s00_axi_arvalid(m00_axi_arvalid),
                .s00_axi_arready(m00_axi_arready),
                .s00_axi_rid(m00_axi_rid),
                .s00_axi_rdata(m00_axi_rdata),
                .s00_axi_rresp(m00_axi_rresp),
                .s00_axi_rlast(m00_axi_rlast),
                .s00_axi_ruser(m00_axi_ruser),
                .s00_axi_rvalid(m00_axi_rvalid),
                .s00_axi_rready(m00_axi_rready),
               
                .s01_axi_aclk(m00_axi_aclk),
                .s01_axi_aresetn(m00_axi_aresetn),
                .s01_axi_awid(m01_axi_awid),
                .s01_axi_awaddr(m01_axi_awaddr),
                .s01_axi_awlen(m01_axi_awlen),
                .s01_axi_awsize(m01_axi_awsize),
                .s01_axi_awburst(m01_axi_awburst),
                .s01_axi_awlock(m01_axi_awlock),
                .s01_axi_awcache(m01_axi_awcache),
                .s01_axi_awprot(m01_axi_awprot),
                .s01_axi_awqos(m01_axi_awqos),
                .s01_axi_awregion(m01_axi_awregion),
                .s01_axi_awuser(m01_axi_awuser),
                .s01_axi_awvalid(m01_axi_awvalid),
                .s01_axi_awready(m01_axi_awready),
                .s01_axi_wdata(m01_axi_wdata),
                .s01_axi_wstrb(m01_axi_wstrb),
                .s01_axi_wlast(m01_axi_wlast),
                .s01_axi_wuser(m01_axi_wuser),
                .s01_axi_wvalid(m01_axi_wvalid),
                .s01_axi_wready(m01_axi_wready),
                .s01_axi_bid(m01_axi_bid),
                .s01_axi_bresp(m01_axi_bresp),
                .s01_axi_buser(m01_axi_buser),
                .s01_axi_bvalid(m01_axi_bvalid),
                .s01_axi_bready(m01_axi_bready),
                .s01_axi_arid(m01_axi_arid),
                .s01_axi_araddr(m01_axi_araddr),
                .s01_axi_arlen(m01_axi_arlen),
                .s01_axi_arsize(m01_axi_arsize),
                .s01_axi_arburst(m01_axi_arburst),
                .s01_axi_arlock(m01_axi_arlock),
                .s01_axi_arcache(m01_axi_arcache),
                .s01_axi_arprot(m01_axi_arprot),
                .s01_axi_arqos(m01_axi_arqos),
                .s01_axi_arregion(m01_axi_arregion),
                .s01_axi_aruser(m01_axi_aruser),
                .s01_axi_arvalid(m01_axi_arvalid),
                .s01_axi_arready(m01_axi_arready),
                .s01_axi_rid(m01_axi_rid),
                .s01_axi_rdata(m01_axi_rdata),
                .s01_axi_rresp(m01_axi_rresp),
                .s01_axi_rlast(m01_axi_rlast),
                .s01_axi_ruser(m01_axi_ruser),
                .s01_axi_rvalid(m01_axi_rvalid),
                .s01_axi_rready(m01_axi_rready),
                
                .s02_axi_aclk(m00_axi_aclk),
                .s02_axi_aresetn(m00_axi_aresetn),
                .s02_axi_awid(m02_axi_awid),
                .s02_axi_awaddr(m02_axi_awaddr),
                .s02_axi_awlen(m02_axi_awlen),
                .s02_axi_awsize(m02_axi_awsize),
                .s02_axi_awburst(m02_axi_awburst),
                .s02_axi_awlock(m02_axi_awlock),
                .s02_axi_awcache(m02_axi_awcache),
                .s02_axi_awprot(m02_axi_awprot),
                .s02_axi_awqos(m02_axi_awqos),
                .s02_axi_awregion(m02_axi_awregion),
                .s02_axi_awuser(m02_axi_awuser),
                .s02_axi_awvalid(m02_axi_awvalid),
                .s02_axi_awready(m02_axi_awready),
                .s02_axi_wdata(m02_axi_wdata),
                .s02_axi_wstrb(m02_axi_wstrb),
                .s02_axi_wlast(m02_axi_wlast),
                .s02_axi_wuser(m02_axi_wuser),
                .s02_axi_wvalid(m02_axi_wvalid),
                .s02_axi_wready(m02_axi_wready),
                .s02_axi_bid(m02_axi_bid),
                .s02_axi_bresp(m02_axi_bresp),
                .s02_axi_buser(m02_axi_buser),
                .s02_axi_bvalid(m02_axi_bvalid),
                .s02_axi_bready(m02_axi_bready),
                .s02_axi_arid(m02_axi_arid),
                .s02_axi_araddr(m02_axi_araddr),
                .s02_axi_arlen(m02_axi_arlen),
                .s02_axi_arsize(m02_axi_arsize),
                .s02_axi_arburst(m02_axi_arburst),
                .s02_axi_arlock(m02_axi_arlock),
                .s02_axi_arcache(m02_axi_arcache),
                .s02_axi_arprot(m02_axi_arprot),
                .s02_axi_arqos(m02_axi_arqos),
                .s02_axi_arregion(m02_axi_arregion),
                .s02_axi_aruser(m02_axi_aruser),
                .s02_axi_arvalid(m02_axi_arvalid),
                .s02_axi_arready(m02_axi_arready),
                .s02_axi_rid(m02_axi_rid),
                .s02_axi_rdata(m02_axi_rdata),
                .s02_axi_rresp(m02_axi_rresp),
                .s02_axi_rlast(m02_axi_rlast),
                .s02_axi_ruser(m02_axi_ruser),
                .s02_axi_rvalid(m02_axi_rvalid),
                .s02_axi_rready(m02_axi_rready),
                
                .s03_axi_aclk(m00_axi_aclk),
                .s03_axi_aresetn(m00_axi_aresetn),
                .s03_axi_awid(m03_axi_awid),
                .s03_axi_awaddr(m03_axi_awaddr),
                .s03_axi_awlen(m03_axi_awlen),
                .s03_axi_awsize(m03_axi_awsize),
                .s03_axi_awburst(m03_axi_awburst),
                .s03_axi_awlock(m03_axi_awlock),
                .s03_axi_awcache(m03_axi_awcache),
                .s03_axi_awprot(m03_axi_awprot),
                .s03_axi_awqos(m03_axi_awqos),
                .s03_axi_awregion(m03_axi_awregion),
                .s03_axi_awuser(m03_axi_awuser),
                .s03_axi_awvalid(m03_axi_awvalid),
                .s03_axi_awready(m03_axi_awready),
                .s03_axi_wdata(m03_axi_wdata),
                .s03_axi_wstrb(m03_axi_wstrb),
                .s03_axi_wlast(m03_axi_wlast),
                .s03_axi_wuser(m03_axi_wuser),
                .s03_axi_wvalid(m03_axi_wvalid),
                .s03_axi_wready(m03_axi_wready),
                .s03_axi_bid(m03_axi_bid),
                .s03_axi_bresp(m03_axi_bresp),
                .s03_axi_buser(m03_axi_buser),
                .s03_axi_bvalid(m03_axi_bvalid),
                .s03_axi_bready(m03_axi_bready),
                .s03_axi_arid(m03_axi_arid),
                .s03_axi_araddr(m03_axi_araddr),
                .s03_axi_arlen(m03_axi_arlen),
                .s03_axi_arsize(m03_axi_arsize),
                .s03_axi_arburst(m03_axi_arburst),
                .s03_axi_arlock(m03_axi_arlock),
                .s03_axi_arcache(m03_axi_arcache),
                .s03_axi_arprot(m03_axi_arprot),
                .s03_axi_arqos(m03_axi_arqos),
                .s03_axi_arregion(m03_axi_arregion),
                .s03_axi_aruser(m03_axi_aruser),
                .s03_axi_arvalid(m03_axi_arvalid),
                .s03_axi_arready(m03_axi_arready),
                .s03_axi_rid(m03_axi_rid),
                .s03_axi_rdata(m03_axi_rdata),
                .s03_axi_rresp(m03_axi_rresp),
                .s03_axi_rlast(m03_axi_rlast),
                .s03_axi_ruser(m03_axi_ruser),
                .s03_axi_rvalid(m03_axi_rvalid),
                .s03_axi_rready(m03_axi_rready)
            );
endmodule
