`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/16/2019 02:12:03 PM
// Design Name: 
// Module Name: tb_lisnoc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
/////////////////////////////////////////////////////////////////////////////////
/*
 * Testbench for the Lisnoc 2x2 mech and the lisnoc_mp_interface created by jakob Hippe
 */
module tb_lisnoc();

/*
 * Configuration parameters of the lisnoc interface
 */
parameter flit_data_width = 32;
parameter flit_type_width = 2;
localparam flit_width = flit_data_width+flit_type_width;
parameter ph_dest_width = 4;

parameter vchannels = 1;

parameter in_fifo_length = 4;
parameter out_fifo_length = 4;


//connections to router 0 
wire [flit_width-1:0] link0_in_flit_i;
wire [vchannels-1:0] link0_in_valid_i;
wire [vchannels-1:0] link0_in_ready_o;
wire [flit_width-1:0] link0_out_flit_o;
wire [vchannels-1:0] link0_out_valid_o;
wire [vchannels-1:0] link0_out_ready_i;

//connections to router 1
wire [flit_width-1:0] link1_in_flit_i;
wire [vchannels-1:0] link1_in_valid_i;
wire [vchannels-1:0] link1_in_ready_o;
wire [flit_width-1:0] link1_out_flit_o;
wire [vchannels-1:0] link1_out_valid_o;
wire [vchannels-1:0] link1_out_ready_i;

//connections to router 2 
wire [flit_width-1:0] link2_in_flit_i;
wire [vchannels-1:0] link2_in_valid_i;
wire [vchannels-1:0] link2_in_ready_o;
wire [flit_width-1:0] link2_out_flit_o;
wire [vchannels-1:0] link2_out_valid_o;
wire [vchannels-1:0] link2_out_ready_i;

//connections to router 3 
wire [flit_width-1:0] link3_in_flit_i;
wire [vchannels-1:0] link3_in_valid_i;
wire [vchannels-1:0] link3_in_ready_o;
wire [flit_width-1:0] link3_out_flit_o;
wire [vchannels-1:0] link3_out_valid_o;
wire [vchannels-1:0] link3_out_ready_i;

    // Parameters of Axi Master Bus Interface M00_AXI
    parameter  C_M00_AXI_TARGET_SLAVE_BASE_ADDR    = 32'h40000000;
    parameter integer C_M00_AXI_BURST_LEN    = 16;
    parameter integer C_M00_AXI_ID_WIDTH    = 1;
    parameter integer C_M00_AXI_ADDR_WIDTH    = 32;
    parameter integer C_M00_AXI_DATA_WIDTH    = 32;
    parameter integer C_M00_AXI_AWUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_ARUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_WUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_RUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_BUSER_WIDTH    = 0;


    // Users to add ports here

    // User ports ends
    // Do not modify the ports beyond this line


    // Ports of Axi Master Bus Interface M00_AXI
    reg  m00_axi_init_axi_txn;
    wire  m00_axi_txn_done;
    wire  m00_axi_error;
    reg  m00_axi_aclk;
    reg  m00_axi_aresetn;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_awid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_awaddr;
    wire [7 : 0] m00_axi_awlen;
    wire [2 : 0] m00_axi_awsize;
    wire [1 : 0] m00_axi_awburst;
    wire  m00_axi_awlock;
    wire [3 : 0] m00_axi_awcache;
    wire [2 : 0] m00_axi_awprot;
    wire [3 : 0] m00_axi_awqos;
    wire [C_M00_AXI_AWUSER_WIDTH-1 : 0] m00_axi_awuser;
    wire  m00_axi_awvalid;
    wire  m00_axi_awready;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_wdata;
    wire [C_M00_AXI_DATA_WIDTH/8-1 : 0] m00_axi_wstrb;
    wire  m00_axi_wlast;
    wire [C_M00_AXI_WUSER_WIDTH-1 : 0] m00_axi_wuser;
    wire  m00_axi_wvalid;
    wire  m00_axi_wready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_bid;
    wire [1 : 0] m00_axi_bresp;
    wire [C_M00_AXI_BUSER_WIDTH-1 : 0] m00_axi_buser;
    wire  m00_axi_bvalid;
    wire  m00_axi_bready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_arid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_araddr;
    wire [7 : 0] m00_axi_arlen;
    wire [2 : 0] m00_axi_arsize;
    wire [1 : 0] m00_axi_arburst;
    wire  m00_axi_arlock;
    wire [3 : 0] m00_axi_arcache;
    wire [2 : 0] m00_axi_arprot;
    wire [3 : 0] m00_axi_arqos;
    wire [C_M00_AXI_ARUSER_WIDTH-1 : 0] m00_axi_aruser;
    wire  m00_axi_arvalid;
    wire  m00_axi_arready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_rid;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_rdata;
    wire [1 : 0] m00_axi_rresp;
    wire  m00_axi_rlast;
    wire [C_M00_AXI_RUSER_WIDTH-1 : 0] m00_axi_ruser;
    wire  m00_axi_rvalid;
    wire  m00_axi_rready;

reg clk;
reg rst;

/*
 * resetting the whole setup
 */
initial
begin
    clk = 1'b0;
    rst = 1'b0;
    #2 rst = 1'b1;
    #5 rst = 1'b0;
end

always #2  m00_axi_aclk = ~m00_axi_aclk;
initial
begin
m00_axi_aclk = 0;
m00_axi_aresetn = 1;
#1 m00_axi_aresetn = 0;

#2 m00_axi_init_axi_txn = 1;
#20 m00_axi_aresetn = 1;
end


// Instantiation of Axi Bus Interface M00_AXI
axi_master_for_testing_v1_0_M00_AXI # ( 
    .C_M_TARGET_SLAVE_BASE_ADDR(C_M00_AXI_TARGET_SLAVE_BASE_ADDR),
    .C_M_AXI_BURST_LEN(C_M00_AXI_BURST_LEN),
    .C_M_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
    .C_M_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
    .C_M_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
    .C_M_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
    .C_M_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
    .C_M_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
    .C_M_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
    .C_M_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH)
) axi_master_for_testing_v1_0_M00_AXI_inst (
    .INIT_AXI_TXN(m00_axi_init_axi_txn),
    .TXN_DONE(m00_axi_txn_done),
    .ERROR(m00_axi_error),
    .M_AXI_ACLK(m00_axi_aclk),
    .M_AXI_ARESETN(m00_axi_aresetn),
    .M_AXI_AWID(m00_axi_awid),
    .M_AXI_AWADDR(m00_axi_awaddr),
    .M_AXI_AWLEN(m00_axi_awlen),
    .M_AXI_AWSIZE(m00_axi_awsize),
    .M_AXI_AWBURST(m00_axi_awburst),
    .M_AXI_AWLOCK(m00_axi_awlock),
    .M_AXI_AWCACHE(m00_axi_awcache),
    .M_AXI_AWPROT(m00_axi_awprot),
    .M_AXI_AWQOS(m00_axi_awqos),
    .M_AXI_AWUSER(m00_axi_awuser),
    .M_AXI_AWVALID(m00_axi_awvalid),
    .M_AXI_AWREADY(m00_axi_awready),
    .M_AXI_WDATA(m00_axi_wdata),
    .M_AXI_WSTRB(m00_axi_wstrb),
    .M_AXI_WLAST(m00_axi_wlast),
    .M_AXI_WUSER(m00_axi_wuser),
    .M_AXI_WVALID(m00_axi_wvalid),
    .M_AXI_WREADY(m00_axi_wready),
    .M_AXI_BID(m00_axi_bid),
    .M_AXI_BRESP(m00_axi_bresp),
    .M_AXI_BUSER(m00_axi_buser),
    .M_AXI_BVALID(m00_axi_bvalid),
    .M_AXI_BREADY(m00_axi_bready),
    .M_AXI_ARID(m00_axi_arid),
    .M_AXI_ARADDR(m00_axi_araddr),
    .M_AXI_ARLEN(m00_axi_arlen),
    .M_AXI_ARSIZE(m00_axi_arsize),
    .M_AXI_ARBURST(m00_axi_arburst),
    .M_AXI_ARLOCK(m00_axi_arlock),
    .M_AXI_ARCACHE(m00_axi_arcache),
    .M_AXI_ARPROT(m00_axi_arprot),
    .M_AXI_ARQOS(m00_axi_arqos),
    .M_AXI_ARUSER(m00_axi_aruser),
    .M_AXI_ARVALID(m00_axi_arvalid),
    .M_AXI_ARREADY(m00_axi_arready),
    .M_AXI_RID(m00_axi_rid),
    .M_AXI_RDATA(m00_axi_rdata),
    .M_AXI_RRESP(m00_axi_rresp),
    .M_AXI_RLAST(m00_axi_rlast),
    .M_AXI_RUSER(m00_axi_ruser),
    .M_AXI_RVALID(m00_axi_rvalid),
    .M_AXI_RREADY(m00_axi_rready)
);

/*
 * alternating the clock every 5 ms
 */
always #5 clk = ~clk;

/*MP interface connected to router 0*/
       // Instantiation of Axi Bus Interface S00_AXI
        lisnoc_mp_interface # ( 
            .C_S_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
            .C_S_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
            .C_S_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
            .C_S_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
            .C_S_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
            .C_S_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
            .C_S_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
            .C_S_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH),
            
            .flit_data_width(flit_data_width),
            .flit_type_width(flit_type_width),
            .flit_address_width(ph_dest_width),
            
            .vchannels(vchannels)
            
        ) core_0 (
            .s00_axi_aclk(m00_axi_aclk),
            .s00_axi_aresetn(m00_axi_aresetn),
            .s00_axi_awid(m00_axi_awid),
            .s00_axi_awaddr(m00_axi_awaddr),
            .s00_axi_awlen(m00_axi_awlen),
            .s00_axi_awsize(m00_axi_awsize),
            .s00_axi_awburst(m00_axi_awburst),
            .s00_axi_awlock(m00_axi_awlock),
            .s00_axi_awcache(m00_axi_awcache),
            .s00_axi_awprot(m00_axi_awprot),
            .s00_axi_awqos(m00_axi_awqos),
            .s00_axi_awregion(m00_axi_awregion),
            .s00_axi_awuser(m00_axi_awuser),
            .s00_axi_awvalid(m00_axi_awvalid),
            .s00_axi_awready(m00_axi_awready),
            .s00_axi_wdata(m00_axi_wdata),
            .s00_axi_wstrb(m00_axi_wstrb),
            .s00_axi_wlast(m00_axi_wlast),
            .s00_axi_wuser(m00_axi_wuser),
            .s00_axi_wvalid(m00_axi_wvalid),
            .s00_axi_wready(m00_axi_wready),
            .s00_axi_bid(m00_axi_bid),
            .s00_axi_bresp(m00_axi_bresp),
            .s00_axi_buser(m00_axi_buser),
            .s00_axi_bvalid(m00_axi_bvalid),
            .s00_axi_bready(m00_axi_bready),
            .s00_axi_arid(m00_axi_arid),
            .s00_axi_araddr(m00_axi_araddr),
            .s00_axi_arlen(m00_axi_arlen),
            .s00_axi_arsize(m00_axi_arsize),
            .s00_axi_arburst(m00_axi_arburst),
            .s00_axi_arlock(m00_axi_arlock),
            .s00_axi_arcache(m00_axi_arcache),
            .s00_axi_arprot(m00_axi_arprot),
            .s00_axi_arqos(m00_axi_arqos),
            .s00_axi_arregion(m00_axi_arregion),
            .s00_axi_aruser(m00_axi_aruser),
            .s00_axi_arvalid(m00_axi_arvalid),
            .s00_axi_arready(m00_axi_arready),
            .s00_axi_rid(m00_axi_rid),
            .s00_axi_rdata(m00_axi_rdata),
            .s00_axi_rresp(m00_axi_rresp),
            .s00_axi_rlast(m00_axi_rlast),
            .s00_axi_ruser(m00_axi_ruser),
            .s00_axi_rvalid(m00_axi_rvalid),
            .s00_axi_rready(m00_axi_rready),
            
            .clk(clk),
            .rst(rst),
         
            .in_flit_i(link0_out_flit_o), 
            .in_valid_i(link0_out_valid_o), 
            .in_ready_o(link0_out_ready_i),
            .out_flit_o(link0_in_flit_i), 
            .out_valid_o(link0_in_valid_i), 
            .out_ready_i(link0_in_ready_o)
        );

/*MP interface connected to router 1*/

lisnoc_mp_interface
#(
    .flit_data_width(flit_data_width),
    .flit_type_width(flit_type_width),
    .flit_address_width(ph_dest_width),
    
    .vchannels(vchannels)
    
    //.in_fifo_length(in_fifo_length),
    //.out_fifo_length(out_fifo_length)
) core_1
(

    .clk(clk),
    .rst(rst),

 
    .in_flit_i(link1_out_flit_o), 
    .in_valid_i(link1_out_valid_o), 
    .in_ready_o(link1_out_ready_i),
    .out_flit_o(link1_in_flit_i), 
    .out_valid_o(link1_in_valid_i), 
    .out_ready_i(link1_in_ready_o)
);

//MP interface connected to router 2
lisnoc_mp_interface
#(
    .flit_data_width(flit_data_width),
    .flit_type_width(flit_type_width),
    .flit_address_width(ph_dest_width),
    
    .vchannels(vchannels)
    
    //.in_fifo_length(in_fifo_length),
    //.out_fifo_length(out_fifo_length)
) core_2
(

    .clk(clk),
    .rst(rst),

    .in_flit_i(link2_out_flit_o), 
    .in_valid_i(link2_out_valid_o), 
    .in_ready_o(link2_out_ready_i),
    .out_flit_o(link2_in_flit_i), 
    .out_valid_o(link2_in_valid_i), 
    .out_ready_i(link2_in_ready_o)
);

//MP interface connected to router 3
lisnoc_mp_interface
#(
    .flit_data_width(flit_data_width),
    .flit_type_width(flit_type_width),
    .flit_address_width(ph_dest_width),
    
    .vchannels(vchannels)
    
    //.in_fifo_length(in_fifo_length),
    //.out_fifo_length(out_fifo_length)
) core_3
(

    .clk(clk),
    .rst(rst),

 
    .in_flit_i(link3_out_flit_o), 
    .in_valid_i(link3_out_valid_o), 
    .in_ready_o(link3_out_ready_i),
    .out_flit_o(link3_in_flit_i), 
    .out_valid_o(link3_in_valid_i), 
    .out_ready_i(link3_in_ready_o)
);



/*
 * Mesh of routers to be connected to the MP interfaces
 */
lisnoc_mesh2x2
#(
     .flit_data_width(flit_data_width),
     .flit_type_width(flit_type_width),
     .ph_dest_width(ph_dest_width),
    
     .vchannels(vchannels),
    
     .in_fifo_length(in_fifo_length),
     .out_fifo_length(out_fifo_length)
 )
 network(
    .clk(clk),
    .rst(rst),
 
    .link0_in_flit_i(link0_in_flit_i), 
    .link0_in_valid_i(link0_in_valid_i), 
    .link0_in_ready_o(link0_in_ready_o),
    .link0_out_flit_o(link0_out_flit_o), 
    .link0_out_valid_o(link0_out_valid_o), 
    .link0_out_ready_i(link0_out_ready_i),
    
    .link1_in_flit_i(link1_in_flit_i), 
    .link1_in_valid_i(link1_in_valid_i), 
    .link1_in_ready_o(link1_in_ready_o),
    .link1_out_flit_o(link1_out_flit_o), 
    .link1_out_valid_o(link1_out_valid_o), 
    .link1_out_ready_i(link1_out_ready_i),

    .link2_in_flit_i(link2_in_flit_i), 
    .link2_in_valid_i(link2_in_valid_i), 
    .link2_in_ready_o(link2_in_ready_o),
    .link2_out_flit_o(link2_out_flit_o), 
    .link2_out_valid_o(link2_out_valid_o), 
    .link2_out_ready_i(link2_out_ready_i),

    .link3_in_flit_i(link3_in_flit_i), 
    .link3_in_valid_i(link3_in_valid_i), 
    .link3_in_ready_o(link3_in_ready_o),
    .link3_out_flit_o(link3_out_flit_o), 
    .link3_out_valid_o(link3_out_valid_o), 
    .link3_out_ready_i(link3_out_ready_i)
);

endmodule
