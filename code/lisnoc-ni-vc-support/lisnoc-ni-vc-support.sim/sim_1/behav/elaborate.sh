#!/bin/bash -f
xv_path="/opt/Xilinx/Vivado/2016.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto 88258c7ccb6f4903a91edfda98e49235 -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip --snapshot td_NOC_AXI_testmodule_behav xil_defaultlib.td_NOC_AXI_testmodule xil_defaultlib.glbl -log elaborate.log
