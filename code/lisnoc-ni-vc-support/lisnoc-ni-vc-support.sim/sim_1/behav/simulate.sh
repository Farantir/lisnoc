#!/bin/bash -f
xv_path="/opt/Xilinx/Vivado/2016.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim td_NOC_AXI_testmodule_behav -key {Behavioral:sim_1:Functional:td_NOC_AXI_testmodule} -tclbatch td_NOC_AXI_testmodule.tcl -log simulate.log
