`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/03/2018 12:48:03 PM
// Design Name: 
// Module Name: lisnoc_soc_axi
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lisnoc_soc_axi(

    //Outputs == Inputs von wb
	o_axi_awready_0, o_axi_wready_0, o_axi_bresp_0, o_axi_bvalid_0, 
    o_axi_arready_0, o_axi_rresp_0, o_axi_rvalid_0, o_axi_rdata_0,

	o_axi_awready_1, o_axi_wready_1, o_axi_bresp_1, o_axi_bvalid_1, 
    o_axi_arready_1, o_axi_rresp_1, o_axi_rvalid_1, o_axi_rdata_1,

	o_axi_awready_2, o_axi_wready_2, o_axi_bresp_2, o_axi_bvalid_2, 
    o_axi_arready_2, o_axi_rresp_2, o_axi_rvalid_2, o_axi_rdata_2,

	o_axi_awready_3, o_axi_wready_3, o_axi_bresp_3, o_axi_bvalid_3, 
    o_axi_arready_3, o_axi_rresp_3, o_axi_rvalid_3, o_axi_rdata_3,
    
    //Inputs == Output von core
    i_axi_awaddr_0, i_axi_awcache_0, i_axi_awprot_0, i_axi_awvalid_0,
    i_axi_wdata_0, i_axi_wstrb_0, i_axi_wvalid_0, i_axi_bready_0, 
    i_axi_araddr_0, i_axi_arcache_0, i_axi_arprot_0, i_axi_arvalid_0, 
    i_axi_rready_0, 
    i_axi_clk_0, 
    i_axi_reset_n_0,    

    i_axi_awaddr_1, i_axi_awcache_1, i_axi_awprot_1, i_axi_awvalid_1,
    i_axi_wdata_1, i_axi_wstrb_1, i_axi_wvalid_1, i_axi_bready_1, 
    i_axi_araddr_1, i_axi_arcache_1, i_axi_arprot_1, i_axi_arvalid_1, 
    i_axi_rready_1, 
    i_axi_clk_1, 
    i_axi_reset_n_1,    
    
    i_axi_awaddr_2, i_axi_awcache_2, i_axi_awprot_2, i_axi_awvalid_2,
    i_axi_wdata_2, i_axi_wstrb_2, i_axi_wvalid_2, i_axi_bready_2, 
    i_axi_araddr_2, i_axi_arcache_2, i_axi_arprot_2, i_axi_arvalid_2, 
    i_axi_rready_2, 
    i_axi_clk_2, 
    i_axi_reset_n_2,    
    
    i_axi_awaddr_3, i_axi_awcache_3, i_axi_awprot_3, i_axi_awvalid_3,
    i_axi_wdata_3, i_axi_wstrb_3, i_axi_wvalid_3, i_axi_bready_3, 
    i_axi_araddr_3, i_axi_arcache_3, i_axi_arprot_3, i_axi_arvalid_3, 
    i_axi_rready_3, 
    i_axi_clk_3, 
    i_axi_reset_n_3    

    
    );
    
    localparam C_AXI_DATA_WIDTH	= 32;// Width of the AXI R&W data
    parameter C_AXI_ADDR_WIDTH    = 28;    // AXI Address width
    localparam DW = C_AXI_DATA_WIDTH;
    localparam AW = C_AXI_ADDR_WIDTH-2;
    parameter    LGFIFO = 4;
    parameter        F_MAXSTALL = 3;
    parameter        F_MAXDELAY = 3;
    parameter    [0:0]    OPT_READONLY  = 1'b1;
    parameter    [0:0]    OPT_WRITEONLY = 1'b0;
    
    input wire	i_axi_clk_0;
    input wire    i_axi_reset_n_0;
    output wire o_axi_awready_0; 
    input wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_awaddr_0;    
    input wire    [3:0]    i_axi_awcache_0;
    input wire    [2:0]    i_axi_awprot_0;
    input wire    i_axi_awvalid_0;
    output wire    o_axi_wready_0;
    input wire    [DW-1:0]    i_axi_wdata_0;    
    input wire    [DW/8-1:0] i_axi_wstrb_0;
    input wire    i_axi_wvalid_0;    
    output wire    [1:0]    o_axi_bresp_0;    
    output wire o_axi_bvalid_0; 
    input wire    i_axi_bready_0; 
    output wire    o_axi_arready_0;    
    input wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_araddr_0;    
    input wire    [3:0]    i_axi_arcache_0;    
    input wire    [2:0]    i_axi_arprot_0;    
    input wire    i_axi_arvalid_0;    
    output wire    [1:0]    o_axi_rresp_0;
    output wire    o_axi_rvalid_0;
    output wire    [C_AXI_DATA_WIDTH-1:0] o_axi_rdata_0;
    input wire    i_axi_rready_0;

    input wire	i_axi_clk_1;
    input wire    i_axi_reset_n_1;
    output wire o_axi_awready_1; 
    input wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_awaddr_1;    
    input wire    [3:0]    i_axi_awcache_1;
    input wire    [2:0]    i_axi_awprot_1;
    input wire    i_axi_awvalid_1;
    output wire    o_axi_wready_1;
    input wire    [DW-1:0]    i_axi_wdata_1;    
    input wire    [DW/8-1:0] i_axi_wstrb_1;
    input wire    i_axi_wvalid_1;    
    output wire    [1:0]    o_axi_bresp_1;    
    output wire o_axi_bvalid_1; 
    input wire    i_axi_bready_1; 
    output wire    o_axi_arready_1;    
    input wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_araddr_1;    
    input wire    [3:0]    i_axi_arcache_1;    
    input wire    [2:0]    i_axi_arprot_1;    
    input wire    i_axi_arvalid_1;    
    output wire    [1:0]    o_axi_rresp_1;
    output wire    o_axi_rvalid_1;
    output wire    [C_AXI_DATA_WIDTH-1:0] o_axi_rdata_1;
    input wire    i_axi_rready_1;
    
    input wire	i_axi_clk_2;
    input wire    i_axi_reset_n_2;
    output wire o_axi_awready_2; 
    input wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_awaddr_2;    
    input wire    [3:0]    i_axi_awcache_2;
    input wire    [2:0]    i_axi_awprot_2;
    input wire    i_axi_awvalid_2;
    output wire    o_axi_wready_2;
    input wire    [DW-1:0]    i_axi_wdata_2;    
    input wire    [DW/8-1:0] i_axi_wstrb_2;
    input wire    i_axi_wvalid_2;    
    output wire    [1:0]    o_axi_bresp_2;    
    output wire o_axi_bvalid_2; 
    input wire    i_axi_bready_2; 
    output wire    o_axi_arready_2;    
    input wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_araddr_2;    
    input wire    [3:0]    i_axi_arcache_2;    
    input wire    [2:0]    i_axi_arprot_2;    
    input wire    i_axi_arvalid_2;    
    output wire    [1:0]    o_axi_rresp_2;
    output wire    o_axi_rvalid_2;
    output wire    [C_AXI_DATA_WIDTH-1:0] o_axi_rdata_2;
    input wire    i_axi_rready_2;
    
    input wire	i_axi_clk_3;
    input wire    i_axi_reset_n_3;
    output wire o_axi_awready_3; 
    input wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_awaddr_3;    
    input wire    [3:0]    i_axi_awcache_3;
    input wire    [2:0]    i_axi_awprot_3;
    input wire    i_axi_awvalid_3;
    output wire    o_axi_wready_3;
    input wire    [DW-1:0]    i_axi_wdata_3;    
    input wire    [DW/8-1:0] i_axi_wstrb_3;
    input wire    i_axi_wvalid_3;    
    output wire    [1:0]    o_axi_bresp_3;    
    output wire o_axi_bvalid_3; 
    input wire    i_axi_bready_3; 
    output wire    o_axi_arready_3;    
    input wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_araddr_3;    
    input wire    [3:0]    i_axi_arcache_3;    
    input wire    [2:0]    i_axi_arprot_3;    
    input wire    i_axi_arvalid_3;    
    output wire    [1:0]    o_axi_rresp_3;
    output wire    o_axi_rvalid_3;
    output wire    [C_AXI_DATA_WIDTH-1:0] o_axi_rdata_3;
    input wire    i_axi_rready_3;


    wire clk; 
    wire	wb_rst_0;
    wire	wb_cyc_0;
    wire	wb_stb_0;
    wire	wb_we_0;
    wire [(AW-1):0]	wb_adr_0;
    wire [(DW-1):0]	wb_data_0;
    wire [(DW/8-1):0]	wb_sel_0;
    wire	wb_ack_0;
    wire	wb_stall_0;
//    wire [(DW-1):0]	wb_data_0;
    wire	wb_err_0;
    // wb1
    wire	wb_rst_1;
    wire    wb_cyc_1;
    wire    wb_stb_1;
    wire    wb_we_1;
    wire [(AW-1):0]    wb_adr_1;
    wire [(DW-1):0]    wb_data_1;
    wire [(DW/8-1):0]    wb_sel_1;
    wire    wb_ack_1;
    wire    wb_stall_1;
//    wire [(DW-1):0]    wb_data_1;
    wire    wb_err_1;
    //
    wire    wb_rst_2;
    wire    wb_cyc_2;
    wire    wb_stb_2;
    wire    wb_we_2;
    wire [(AW-1):0]	wb_adr_2;
    wire [(DW-1):0]    wb_data_2;
    wire [(DW/8-1):0]    wb_sel_2;
    wire    wb_ack_2;
    wire    wb_stall_2;
//    wire [(DW-1):0]    wb_data_2;
    wire    wb_err_2;
    wire    wb_rst_3;
    wire    wb_cyc_3;
    wire    wb_stb_3;
    wire    wb_we_3;
    wire [(AW-1):0]    wb_adr_3;
    wire [(DW-1):0]    wb_data_3;
    wire [(DW/8-1):0]    wb_sel_3;
    wire    wb_ack_3;
    wire    wb_stall_3;
//    wire [(DW-1):0]    wb_data_3;
    wire    wb_err_3;    

    axlite2wbsp
      u_axilite2wb_0 (
    .i_clk (i_axi_clk_0), 
    .i_axi_reset_n (i_axi_rstn_0),
	.o_axi_awready (o_axi_awready_0), 
	.i_axi_awaddr (i_axi_awaddr_0), 
    .i_axi_awcache (i_axi_awcache_0), 
    .i_axi_awprot (i_axi_awprot_0), 
    .i_axi_awvalid (i_axi_awvalid_0),
    .o_axi_wready (o_axi_wready_0), 
    .i_axi_wdata (i_axi_wdata_0), 
    .i_axi_wstrb (i_axi_wstrb_0), 
    .i_axi_wvalid (i_axi_wvalid_0),
    .o_axi_bresp (o_axi_bresp_0), 
    .o_axi_bvalid (o_axi_bvalid_0), 
    .i_axi_bready (i_axi_bready_0),
    .o_axi_arready (o_axi_arready_0), 
    .i_axi_araddr (i_axi_araddr_0), 
    .i_axi_arcache (i_axi_arcache_0), 
    .i_axi_arprot (i_axi_arprot_0), 
    .i_axi_arvalid (i_axi_arvalid_0),
    .o_axi_rresp (o_axi_rresp_0), 
    .o_axi_rvalid (o_axi_rvalid_0), 
    .o_axi_rdata (o_axi_rdata_0), 
    .i_axi_rready (i_axi_rready_0),
    .o_reset (wb_rst_0), 
    .o_wb_cyc (wb_cyc_0), 
    .o_wb_stb (wb_stb_0), 
    .o_wb_we (wb_we_0), 
    .o_wb_addr (wb_addr_0), 
    .o_wb_data (wb_data_0), 
    .o_wb_sel (wb_sel_0),
    .i_wb_ack (wb_ack_0), 
    .i_wb_stall (wb_stall_0), 
    .i_wb_data (wb_data_0), 
    .i_wb_err (wb_err_0));

    axlite2wbsp
      u_axilite2wb_1 (
  .i_clk (i_axi_clk_1), 
  .i_axi_reset_n (i_axi_rstn_1),
  .o_axi_awready (o_axi_awready_1), 
  .i_axi_awaddr (i_axi_awaddr_1), 
  .i_axi_awcache (i_axi_awcache_1), 
  .i_axi_awprot (i_axi_awprot_1), 
  .i_axi_awvalid (i_axi_awvalid_1),
  .o_axi_wready (o_axi_wready_1), 
  .i_axi_wdata (i_axi_wdata_1), 
  .i_axi_wstrb (i_axi_wstrb_1), 
  .i_axi_wvalid (i_axi_wvalid_1),
  .o_axi_bresp (o_axi_bresp_1), 
  .o_axi_bvalid (o_axi_bvalid_1), 
  .i_axi_bready (i_axi_bready_1),
  .o_axi_arready (o_axi_arready_1), 
  .i_axi_araddr (i_axi_araddr_1), 
  .i_axi_arcache (i_axi_arcache_1), 
  .i_axi_arprot (i_axi_arprot_1), 
  .i_axi_arvalid (i_axi_arvalid_1),
  .o_axi_rresp (o_axi_rresp_1), 
  .o_axi_rvalid (o_axi_rvalid_1), 
  .o_axi_rdata (o_axi_rdata_1), 
  .i_axi_rready (i_axi_rready_1),
  .o_reset (wb_rst_1), 
  .o_wb_cyc (wb_cyc_1), 
  .o_wb_stb (wb_stb_1), 
  .o_wb_we (wb_we_1), 
  .o_wb_addr (wb_addr_1), 
  .o_wb_data (wb_data_1), 
  .o_wb_sel (wb_sel_1),
  .i_wb_ack (wb_ack_1), 
  .i_wb_stall (wb_stall_1), 
  .i_wb_data (wb_data_1), 
  .i_wb_err (wb_err_1));

    axlite2wbsp
      u_axilite2wb_2 (
  .i_clk (i_axi_clk_2), 
  .i_axi_reset_n (i_axi_rstn_2),
  .o_axi_awready (o_axi_awready_2), 
  .i_axi_awaddr (i_axi_awaddr_2), 
  .i_axi_awcache (i_axi_awcache_2), 
  .i_axi_awprot (i_axi_awprot_2), 
  .i_axi_awvalid (i_axi_awvalid_2),
  .o_axi_wready (o_axi_wready_2), 
  .i_axi_wdata (i_axi_wdata_2), 
  .i_axi_wstrb (i_axi_wstrb_2), 
  .i_axi_wvalid (i_axi_wvalid_2),
  .o_axi_bresp (o_axi_bresp_2), 
  .o_axi_bvalid (o_axi_bvalid_2), 
  .i_axi_bready (i_axi_bready_2),
  .o_axi_arready (o_axi_arready_2), 
  .i_axi_araddr (i_axi_araddr_2), 
  .i_axi_arcache (i_axi_arcache_2), 
  .i_axi_arprot (i_axi_arprot_2), 
  .i_axi_arvalid (i_axi_arvalid_2),
  .o_axi_rresp (o_axi_rresp_2), 
  .o_axi_rvalid (o_axi_rvalid_2), 
  .o_axi_rdata (o_axi_rdata_2), 
  .i_axi_rready (i_axi_rready_2),
  .o_reset (wb_rst_2), 
  .o_wb_cyc (wb_cyc_2), 
  .o_wb_stb (wb_stb_2), 
  .o_wb_we (wb_we_2), 
  .o_wb_addr (wb_addr_2), 
  .o_wb_data (wb_data_2), 
  .o_wb_sel (wb_sel_2),
  .i_wb_ack (wb_ack_2), 
  .i_wb_stall (wb_stall_2), 
  .i_wb_data (wb_data_2), 
  .i_wb_err (wb_err_2));

    axlite2wbsp
      u_axilite2wb_3 (
  .i_clk (i_axi_clk_3), 
  .i_axi_reset_n (i_axi_rstn_3),
  .o_axi_awready (o_axi_awready_3), 
  .i_axi_awaddr (i_axi_awaddr_3), 
  .i_axi_awcache (i_axi_awcache_3), 
  .i_axi_awprot (i_axi_awprot_3), 
  .i_axi_awvalid (i_axi_awvalid_3),
  .o_axi_wready (o_axi_wready_3), 
  .i_axi_wdata (i_axi_wdata_3), 
  .i_axi_wstrb (i_axi_wstrb_3), 
  .i_axi_wvalid (i_axi_wvalid_3),
  .o_axi_bresp (o_axi_bresp_3), 
  .o_axi_bvalid (o_axi_bvalid_3), 
  .i_axi_bready (i_axi_bready_3),
  .o_axi_arready (o_axi_arready_3), 
  .i_axi_araddr (i_axi_araddr_3), 
  .i_axi_arcache (i_axi_arcache_3), 
  .i_axi_arprot (i_axi_arprot_3), 
  .i_axi_arvalid (i_axi_arvalid_3),
  .o_axi_rresp (o_axi_rresp_3), 
  .o_axi_rvalid (o_axi_rvalid_3), 
  .o_axi_rdata (o_axi_rdata_3), 
  .i_axi_rready (i_axi_rready_3),
  .o_reset (wb_rst_3), 
  .o_wb_cyc (wb_cyc_3), 
  .o_wb_stb (wb_stb_3), 
  .o_wb_we (wb_we_3), 
  .o_wb_addr (wb_addr_3), 
  .o_wb_data (wb_data_3), 
  .o_wb_sel (wb_sel_3),
  .i_wb_ack (wb_ack_3), 
  .i_wb_stall (wb_stall_3), 
  .i_wb_data (wb_data_3), 
  .i_wb_err (wb_err_3));



    lisnoc_soc
        u_lisnoc_soc (
        .clk (clk), 
        .rst (wb_rst_0), 
        .wb_dat_o_0 (wb_dat_0), 
        .wb_ack_o_0 (wb_ack_0), 
        .irq_0 (irq_0),
        .wb_dat_o_1 (wb_dat_1), 
        .wb_ack_o_1 (wb_ack_1), 
        .irq_1 (irq_1),
        .wb_dat_o_2 (wb_dat_2), 
        .wb_ack_o_2 (wb_ack_2), 
        .irq_2 (irq_2),
        .wb_dat_o_3 (wb_dat_3), 
        .wb_ack_o_3 (wb_ack_3), 
        .irq_3 (irq_3),
        .wb_adr_i_0 (wb_adr_0), 
        .wb_we_i_0 (wb_we_0), 
        .wb_cyc_i_0 (wb_cyc_0), 
        .wb_stb_i_0 (wb_stb_0), 
        .wb_dat_i_0 (wb_dat_0),
        .wb_adr_i_1 (wb_adr_1), 
        .wb_we_i_1 (wb_we_1), 
        .wb_cyc_i_1 (wb_cyc_1), 
        .wb_stb_i_1 (wb_stb_1), 
        .wb_dat_i_1 (wb_dat_1),
        .wb_adr_i_2 (wb_adr_2), 
        .wb_we_i_2 (wb_we_2), 
        .wb_cyc_i_2 (wb_cyc_2), 
        .wb_stb_i_2 (wb_stb_2), 
        .wb_dat_i_2 (wb_dat_2),
        .wb_adr_i_3 (wb_adr_3), 
        .wb_we_i_3 (wb_we_3), 
        .wb_cyc_i_3 (wb_cyc_3), 
        .wb_stb_i_3 (wb_stb_3), 
        .wb_dat_i_3 (wb_dat_3)
        
     );
    
endmodule







