`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/14/2019 12:21:50 PM
// Design Name: 
// Module Name: tb_axi_lisnoc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_axi_lisnoc();

    localparam C_AXI_DATA_WIDTH	= 32;// Width of the AXI R&W data
    localparam C_AXI_ADDR_WIDTH    = 28;    // AXI Address width
    localparam DW = C_AXI_DATA_WIDTH;
    localparam AW = C_AXI_ADDR_WIDTH-2;

    reg clk, rstn; 
    

    wire	i_axi_clk_0;
    wire    i_axi_reset_n_0;
    wire    o_axi_awready_0; 
    wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_awaddr_0;    
    wire    [3:0]    i_axi_awcache_0;
    wire    [2:0]    i_axi_awprot_0;
    wire    i_axi_awvalid_0;
    wire    o_axi_wready_0;
    wire    [DW-1:0]    i_axi_wdata_0;    
    wire    [DW/8-1:0] i_axi_wstrb_0;
    wire    i_axi_wvalid_0;    
    wire    [1:0]    o_axi_bresp_0;    
    wire    o_axi_bvalid_0; 
    wire    i_axi_bready_0; 
    wire    o_axi_arready_0;    
    wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_araddr_0;    
    wire    [3:0]    i_axi_arcache_0;    
    wire    [2:0]    i_axi_arprot_0;    
    wire    i_axi_arvalid_0;    
    wire    [1:0]    o_axi_rresp_0;
    wire    o_axi_rvalid_0;
    wire    [C_AXI_DATA_WIDTH-1:0] o_axi_rdata_0;
    wire    i_axi_rready_0;
    
    wire	i_axi_clk_1;
    wire    i_axi_reset_n_1;
    wire    o_axi_awready_1; 
    wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_awaddr_1;    
    wire    [3:0]    i_axi_awcache_1;
    wire    [2:0]    i_axi_awprot_1;
    wire    i_axi_awvalid_1;
    wire    o_axi_wready_1;
    wire    [DW-1:0]    i_axi_wdata_1;    
    wire    [DW/8-1:0] i_axi_wstrb_1;
    wire    i_axi_wvalid_1;    
    wire    [1:0]    o_axi_bresp_1;    
    wire    o_axi_bvalid_1; 
    wire    i_axi_bready_1; 
    wire    o_axi_arready_1;    
    wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_araddr_1;    
    wire    [3:0]    i_axi_arcache_1;    
    wire    [2:0]    i_axi_arprot_1;    
    wire    i_axi_arvalid_1;    
    wire    [1:0]    o_axi_rresp_1;
    wire    o_axi_rvalid_1;
    wire    [C_AXI_DATA_WIDTH-1:0] o_axi_rdata_1;
    wire    i_axi_rready_1;
    
    wire	i_axi_clk_2;
    wire    i_axi_reset_n_2;
    wire    o_axi_awready_2; 
    wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_awaddr_2;    
    wire    [3:0]    i_axi_awcache_2;
    wire    [2:0]    i_axi_awprot_2;
    wire    i_axi_awvalid_2;
    wire    o_axi_wready_2;
    wire    [DW-1:0]    i_axi_wdata_2;    
    wire    [DW/8-1:0] i_axi_wstrb_2;
    wire    i_axi_wvalid_2;    
    wire    [1:0]    o_axi_bresp_2;    
    wire    o_axi_bvalid_2; 
    wire    i_axi_bready_2; 
    wire    o_axi_arready_2;    
    wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_araddr_2;    
    wire    [3:0]    i_axi_arcache_2;    
    wire    [2:0]    i_axi_arprot_2;    
    wire    i_axi_arvalid_2;    
    wire    [1:0]    o_axi_rresp_2;
    wire    o_axi_rvalid_2;
    wire    [C_AXI_DATA_WIDTH-1:0] o_axi_rdata_2;
    wire    i_axi_rready_2;
    
    wire	i_axi_clk_3;
    wire    i_axi_reset_n_3;
    wire    o_axi_awready_3; 
    wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_awaddr_3;    
    wire    [3:0]    i_axi_awcache_3;
    wire    [2:0]    i_axi_awprot_3;
    wire    i_axi_awvalid_3;
    wire    o_axi_wready_3;
    wire    [DW-1:0]    i_axi_wdata_3;    
    wire    [DW/8-1:0] i_axi_wstrb_3;
    wire    i_axi_wvalid_3;    
    wire    [1:0]    o_axi_bresp_3;    
    wire    o_axi_bvalid_3; 
    wire    i_axi_bready_3; 
    wire    o_axi_arready_3;    
    wire    [C_AXI_ADDR_WIDTH-1:0]    i_axi_araddr_3;    
    wire    [3:0]    i_axi_arcache_3;    
    wire    [2:0]    i_axi_arprot_3;    
    wire    i_axi_arvalid_3;    
    wire    [1:0]    o_axi_rresp_3;
    wire    o_axi_rvalid_3;
    wire    [C_AXI_DATA_WIDTH-1:0] o_axi_rdata_3;
    wire    i_axi_rready_3;

    initial
    begin
        clk = 0; 
        rstn = 0; 
        #3 rstn = 1; 
    end

    always #5 clk = ~clk; 
    
    always i_axi_clk_0 = clk; 
    always i_axi_clk_1 = clk; 
    always i_axi_clk_2 = clk; 
    always i_axi_clk_3 = clk; 
    always i_axi_rstn_0 = rstn; 
    always i_axi_rstn_1 = rstn; 
    always i_axi_rstn_2 = rstn; 
    always i_axi_rstn_3 = rstn; 

        
    lisnoc_soc_axi uut (
        .i_axi_clk_0 (i_axi_clk_0), 
        .i_axi_reset_n_0 (i_axi_rstn_0),
        .o_axi_awready_0 (o_axi_awready_0), 
        .i_axi_awaddr_0 (i_axi_awaddr_0), 
        .i_axi_awcache_0 (i_axi_awcache_0), 
        .i_axi_awprot_0 (i_axi_awprot_0), 
        .i_axi_awvalid_0 (i_axi_awvalid_0),
        .o_axi_wready_0 (o_axi_wready_0), 
        .i_axi_wdata_0 (i_axi_wdata_0), 
        .i_axi_wstrb_0 (i_axi_wstrb_0), 
        .i_axi_wvalid_0 (i_axi_wvalid_0),
        .o_axi_bresp_0 (o_axi_bresp_0), 
        .o_axi_bvalid_0 (o_axi_bvalid_0), 
        .i_axi_bready_0 (i_axi_bready_0),
        .o_axi_arready_0 (o_axi_arready_0), 
        .i_axi_araddr_0 (i_axi_araddr_0), 
        .i_axi_arcache_0 (i_axi_arcache_0), 
        .i_axi_arprot_0 (i_axi_arprot_0), 
        .i_axi_arvalid_0 (i_axi_arvalid_0),
        .o_axi_rresp_0 (o_axi_rresp_0), 
        .o_axi_rvalid_0 (o_axi_rvalid_0), 
        .o_axi_rdata_0 (o_axi_rdata_0), 
        .i_axi_rready_0 (i_axi_rready_0),
        
        .i_axi_clk_1 (i_axi_clk_1), 
        .i_axi_reset_n_1 (i_axi_rstn_1),
        .o_axi_awready_1 (o_axi_awready_1), 
        .i_axi_awaddr_1 (i_axi_awaddr_1), 
        .i_axi_awcache_1 (i_axi_awcache_1), 
        .i_axi_awprot_1 (i_axi_awprot_1), 
        .i_axi_awvalid_1 (i_axi_awvalid_1),
        .o_axi_wready_1 (o_axi_wready_1), 
        .i_axi_wdata_1 (i_axi_wdata_1), 
        .i_axi_wstrb_1 (i_axi_wstrb_1), 
        .i_axi_wvalid_1 (i_axi_wvalid_1),
        .o_axi_bresp_1 (o_axi_bresp_1), 
        .o_axi_bvalid_1 (o_axi_bvalid_1), 
        .i_axi_bready_1 (i_axi_bready_1),
        .o_axi_arready_1 (o_axi_arready_1), 
        .i_axi_araddr_1 (i_axi_araddr_1), 
        .i_axi_arcache_1 (i_axi_arcache_1), 
        .i_axi_arprot_1 (i_axi_arprot_1), 
        .i_axi_arvalid_1 (i_axi_arvalid_1),
        .o_axi_rresp_1 (o_axi_rresp_1), 
        .o_axi_rvalid_1 (o_axi_rvalid_1), 
        .o_axi_rdata_1 (o_axi_rdata_1), 
        .i_axi_rready_1 (i_axi_rready_1),
        
        .i_axi_clk_2 (i_axi_clk_2), 
        .i_axi_reset_n_2 (i_axi_rstn_2),
        .o_axi_awready_2 (o_axi_awready_2), 
        .i_axi_awaddr_2 (i_axi_awaddr_2), 
        .i_axi_awcache_2 (i_axi_awcache_2), 
        .i_axi_awprot_2 (i_axi_awprot_2), 
        .i_axi_awvalid_2 (i_axi_awvalid_2),
        .o_axi_wready_2 (o_axi_wready_2), 
        .i_axi_wdata_2 (i_axi_wdata_2), 
        .i_axi_wstrb_2 (i_axi_wstrb_2), 
        .i_axi_wvalid_2 (i_axi_wvalid_2),
        .o_axi_bresp_2 (o_axi_bresp_2), 
        .o_axi_bvalid_2 (o_axi_bvalid_2), 
        .i_axi_bready_2 (i_axi_bready_2),
        .o_axi_arready_2 (o_axi_arready_2), 
        .i_axi_araddr_2 (i_axi_araddr_2), 
        .i_axi_arcache_2 (i_axi_arcache_2), 
        .i_axi_arprot_2 (i_axi_arprot_2), 
        .i_axi_arvalid_2 (i_axi_arvalid_2),
        .o_axi_rresp_2 (o_axi_rresp_2), 
        .o_axi_rvalid_2 (o_axi_rvalid_2), 
        .o_axi_rdata_2 (o_axi_rdata_2), 
        .i_axi_rready_2 (i_axi_rready_2),
        
        .i_axi_clk_3 (i_axi_clk_3), 
        .i_axi_reset_n_3 (i_axi_rstn_3),
        .o_axi_awready_3 (o_axi_awready_3), 
        .i_axi_awaddr_3 (i_axi_awaddr_3), 
        .i_axi_awcache_3 (i_axi_awcache_3), 
        .i_axi_awprot_3 (i_axi_awprot_3), 
        .i_axi_awvalid_3 (i_axi_awvalid_3),
        .o_axi_wready_3 (o_axi_wready_3), 
        .i_axi_wdata_3 (i_axi_wdata_3), 
        .i_axi_wstrb_3 (i_axi_wstrb_3), 
        .i_axi_wvalid_3 (i_axi_wvalid_3),
        .o_axi_bresp_3 (o_axi_bresp_3), 
        .o_axi_bvalid_3 (o_axi_bvalid_3), 
        .i_axi_bready_3 (i_axi_bready_3),
        .o_axi_arready_3 (o_axi_arready_3), 
        .i_axi_araddr_3 (i_axi_araddr_3), 
        .i_axi_arcache_3 (i_axi_arcache_3), 
        .i_axi_arprot_3 (i_axi_arprot_3), 
        .i_axi_arvalid_3 (i_axi_arvalid_3),
        .o_axi_rresp_3 (o_axi_rresp_3), 
        .o_axi_rvalid_3 (o_axi_rvalid_3), 
        .o_axi_rdata_3 (o_axi_rdata_3), 
        .i_axi_rready_3 (i_axi_rready_3)); 
        
        core_init core_0_0 (
        .INIT_AXI_TXN (0), 
        .PORT_ID_WR (8'b00000000), 
        .MSG_LENGTH_WR (8'b00000000), 
        .INIT_AXI_RXN (0), 
        .PORT_ID_RD (8'b00000000), 
        .MSG_LENGTH_RD (8'b00000000), 
    
        .M_AXI_ACLK (i_axi_clk_0), 
        .M_AXI_ARESETN (i_axi_rstn_0), 
        .M_AXI_AWADDR (i_axi_awaddr_0), 
        .M_AXI_AWVALID (i_axi_awvalid_0), 
        .M_AXI_AWREADY (o_axi_awready_0), 
        .M_AXI_AWPROT(i_axi_awprot_0),xx 
        .M_AXI_WDATA (i_axi_wdata_0), 
        .M_AXI_WSTRB (i_axi_wstrb_0), 
        .M_AXI_AWCACHE (i_axi_awcache_0),  
        .M_AXI_WVALID (i_axi_wvalid_0), 
        .M_AXI_WREADY (o_axi_wready_0), 
        .M_AXI_BRESP (o_axi_bresp_0), 
        .M_AXI_BVALID (o_axi_bvalid_0), 
        .M_AXI_BREADY (i_axi_bready_0), 
        .M_AXI_ARADDR (i_axi_araddr_0), 
        .M_AXI_ARCACHE (i_axi_arcache_0), 
        .M_AXI_ARPROT (i_axi_arprot_0), 
        .M_AXI_ARVALID (i_axi_arvalid_0), 
        .M_AXI_ARREADY (o_axi_arready_0), 
        .M_AXI_RDATA (o_axi_rdata_0), 
        .M_AXI_RRESP (o_axi_rresp_0), 
        .M_AXI_RVALID (o_axi_rvalid_0), 
        .M_AXI_RREADY (i_axi_rready_0)); 
    


endmodule
