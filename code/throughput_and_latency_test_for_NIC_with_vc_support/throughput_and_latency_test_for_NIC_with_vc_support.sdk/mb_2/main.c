/*
 * main.c
 *
 *  Created on: Aug 7, 2019
 *      Author: jhippe
 */

#include <stdio.h>
#include "xil_io.h"


int main()
{
	//recieve 500 messages from MB 0 as soon as they arrive
	for(int i = 0; i<500 ;i++)
	{
		Xil_In32(0x44a00000);
	}
	//notify the PS that all messages have arrived -
	//and the test sequence is thus completed
	Xil_Out32(0x44a03000, 300);

    return 0;
}
