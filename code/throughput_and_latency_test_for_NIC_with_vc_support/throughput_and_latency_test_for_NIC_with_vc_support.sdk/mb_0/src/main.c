/*
 * main.c
 *
 *  Created on: Aug 7, 2019
 *      Author: jhippe
 */
#include <stdio.h>
#include "xil_io.h"


int main()
{

	//wait until the PS has signaled to start of the Test sequence
	Xil_In32(0x44a00000);
	//sending 500 messages through the netork
	for(int i = 0; i<500 ;i++)
	{
		//send a message to MB 1
		Xil_Out32(0x44a02000, i);
	}
    return 0;
}

