
�
Command: %s
53*	vivadotcl2{
gsynth_design -top design_1_NOC_AXI_testmodule_vc_support_0_0 -part xc7z020clg484-1 -mode out_of_context2default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7z0202default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7z0202default:defaultZ17-349h px� 
�
Pparameter declaration becomes local in %s with formal parameter declaration list2507*oasys2%
input_virtualiser2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/input_virtualiser.v2default:default2
1232default:default8@Z8-2507h px� 
�
%s*synth2�
�Starting RTL Elaboration : Time (s): cpu = 00:00:07 ; elapsed = 00:00:07 . Memory (MB): peak = 1106.668 ; gain = 169.301 ; free physical = 1308 ; free virtual = 16182
2default:defaulth px� 
�
synthesizing module '%s'638*oasys2>
*design_1_NOC_AXI_testmodule_vc_support_0_02default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
562default:default8@Z8-638h px� 
�
synthesizing module '%s'638*oasys2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/NOC_AXI_testmodule.v2default:default2
742default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter in_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter out_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter vchannel_select_lefoffset bound to: 4 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter vchannel_select_bits bound to: 2 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_S00_AXI_ID_WIDTH bound to: 12 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_ADDR_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_S00_AXI_AWUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_S00_AXI_ARUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_WUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_RUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_BUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_S01_AXI_ID_WIDTH bound to: 12 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S01_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S01_AXI_ADDR_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_S01_AXI_AWUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_S01_AXI_ARUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S01_AXI_WUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S01_AXI_RUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S01_AXI_BUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_S02_AXI_ID_WIDTH bound to: 12 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S02_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S02_AXI_ADDR_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_S02_AXI_AWUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_S02_AXI_ARUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S02_AXI_WUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S02_AXI_RUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S02_AXI_BUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_S03_AXI_ID_WIDTH bound to: 12 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S03_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S03_AXI_ADDR_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_S03_AXI_AWUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_S03_AXI_ARUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S03_AXI_WUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S03_AXI_RUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S03_AXI_BUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2'
lisnoc_mp_interface2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/new/lisnoc_mp_interface.v2default:default2
572default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter buffer_size bound to: 10 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter flit_address_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter vchannel_select_lefoffset bound to: 4 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter vchannel_select_bits bound to: 2 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter max_packet_size bound to: 10 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_S00_AXI_ID_WIDTH bound to: 12 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_ADDR_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_S00_AXI_AWUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_S00_AXI_ARUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_WUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_RUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_BUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/hdl/lisnoc_axi_slave_v1_0_S00_AXI.v2default:default2
812default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter ramsize_in_flit bound to: 10 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_S_AXI_ID_WIDTH bound to: 12 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_S_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_S_AXI_ADDR_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_S_AXI_AWUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_S_AXI_ARUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_S_AXI_WUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_S_AXI_RUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_S_AXI_BUSER_WIDTH bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter ADDR_LSB bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2
12default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/hdl/lisnoc_axi_slave_v1_0_S00_AXI.v2default:default2
812default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2&
output_virtualiser2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/output_virtualiser.v2default:default2
632default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter flit_address_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter buffer_size bound to: 10 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter max_packet_size bound to: 10 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter max_packet_size_bits bound to: 320 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter packet_size_bits bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter packet_address_lenght bound to: 16 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_ADDR_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter vchannel_select_lefoffset bound to: 4 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter vchannel_select_bits bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2#
stream_to_flits2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/stream_to_flits.v2default:default2
692default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter flit_address_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter max_packet_size bound to: 10 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter max_packet_size_bits bound to: 320 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter packet_size_bits bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter packet_address_lenght bound to: 16 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter lookup_bit_left_offset bound to: 2 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter lookup_bit_num bound to: 2 - type: integer 
2default:defaulth p
x
� 
~
%s
*synth2f
R	Parameter remaining_flit_header_data bound to: 28'b0000000000000000000000000000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2#
stream_to_flits2default:default2
22default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/stream_to_flits.v2default:default2
692default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2
lisnoc_fifo2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter LENGTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
lisnoc_fifo2default:default2
32default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
0Net %s in module/entity %s does not have driver.3422*oasys2C
/output_selector_packet_buffer_read_address_o[4]2default:default2&
output_virtualiser2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/output_virtualiser.v2default:default2
1332default:default8@Z8-3848h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2&
output_virtualiser2default:default2
42default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/output_virtualiser.v2default:default2
632default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2%
input_virtualiser2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/input_virtualiser.v2default:default2
652default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter buffer_lenght bound to: 10 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter flit_address_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter vchannel_select_lefoffset bound to: 4 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter vchannel_select_bits bound to: 2 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter max_packet_size bound to: 10 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S00_AXI_ADDR_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter packet_size_bits bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2#
flits_to_stream2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/flits_to_stream.v2default:default2
562default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter flit_address_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter max_packet_size bound to: 10 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter max_packet_size_bits bound to: 320 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter packet_size_bits bound to: 9 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2#
flits_to_stream2default:default2
52default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/flits_to_stream.v2default:default2
562default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2%
input_virtualiser2default:default2
62default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/input_virtualiser.v2default:default2
652default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2'
lisnoc_mp_interface2default:default2
72default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/new/lisnoc_mp_interface.v2default:default2
572default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2"
lisnoc_mesh2x22default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/meshs/lisnoc_mesh2x2.v2default:default2
32default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter in_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter out_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2(
lisnoc_router_2dgrid2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_2dgrid.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter use_prio bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_prio_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter in_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter out_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b10000000100010000100 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2!
lisnoc_router2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router.v2default:default2
332default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter use_prio bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_prio_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter input_ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter output_ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter in_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter out_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b10000000100010000100 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter FLIT_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter FLIT_TYPE_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FLIT_WIDTH bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter NUM_DESTS bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PH_DEST_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter USE_PRIO bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PH_PRIO_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter VCHANNELS bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter INPUT_PORTS bound to: 5 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter OUTPUT_PORTS bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter IN_FIFO_LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter OUT_FIFO_LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter LOOKUP bound to: 20'b10000000100010000100 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2'
lisnoc_router_input2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b10000000100010000100 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2/
lisnoc_fifo__parameterized02default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
lisnoc_fifo__parameterized02default:default2
72default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2-
lisnoc_router_input_route2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b10000000100010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2-
lisnoc_router_input_route2default:default2
82default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2/
lisnoc_fifo__parameterized12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
lisnoc_fifo__parameterized12default:default2
82default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2/
lisnoc_fifo__parameterized22default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
lisnoc_fifo__parameterized22default:default2
82default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2/
lisnoc_fifo__parameterized32default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
lisnoc_fifo__parameterized32default:default2
82default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2'
lisnoc_router_input2default:default2
92default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2(
lisnoc_router_output2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_output.v2default:default2
342default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter use_prio bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_prio_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter ph_prio_offset bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
lisnoc_router_arbiter2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/router/lisnoc_router_arbiter.sv2default:default2
332default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ports_width bound to: 3 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2!
lisnoc_arb_rr2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/lisnoc_arb_rr.v2default:default2
322default:default8@Z8-638h px� 
V
%s
*synth2>
*	Parameter N bound to: 5 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2!
lisnoc_arb_rr2default:default2
102default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/lisnoc_arb_rr.v2default:default2
322default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
lisnoc_router_arbiter2default:default2
112default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/router/lisnoc_router_arbiter.sv2default:default2
332default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2/
lisnoc_fifo__parameterized42default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
lisnoc_fifo__parameterized42default:default2
112default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2/
lisnoc_fifo__parameterized52default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
lisnoc_fifo__parameterized52default:default2
112default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2/
lisnoc_fifo__parameterized62default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
lisnoc_fifo__parameterized62default:default2
112default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2/
lisnoc_fifo__parameterized72default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
lisnoc_fifo__parameterized72default:default2
112default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_router_output_arbiter2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/router/lisnoc_router_output_arbiter.sv2default:default2
362default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter CHANNEL_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_router_output_arbiter2default:default2
122default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/router/lisnoc_router_output_arbiter.sv2default:default2
362default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2(
lisnoc_router_output2default:default2
132default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_output.v2default:default2
342default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2!
lisnoc_router2default:default2
142default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router.v2default:default2
332default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2(
lisnoc_router_2dgrid2default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_2dgrid.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys28
$lisnoc_router_2dgrid__parameterized02default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_2dgrid.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter use_prio bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_prio_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter in_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter out_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys21
lisnoc_router__parameterized02default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router.v2default:default2
332default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter use_prio bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_prio_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter input_ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter output_ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter in_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter out_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter FLIT_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter FLIT_TYPE_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FLIT_WIDTH bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter NUM_DESTS bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PH_DEST_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter USE_PRIO bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PH_PRIO_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter VCHANNELS bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter INPUT_PORTS bound to: 5 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter OUTPUT_PORTS bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter IN_FIFO_LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter OUT_FIFO_LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter LOOKUP bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys27
#lisnoc_router_input__parameterized02default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2/
lisnoc_fifo__parameterized82default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
lisnoc_fifo__parameterized82default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)lisnoc_router_input_route__parameterized02default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)lisnoc_router_input_route__parameterized02default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2/
lisnoc_fifo__parameterized92default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
lisnoc_fifo__parameterized92default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)lisnoc_router_input_route__parameterized12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)lisnoc_router_input_route__parameterized12default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized102default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized102default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)lisnoc_router_input_route__parameterized22default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)lisnoc_router_input_route__parameterized22default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized112default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized112default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)lisnoc_router_input_route__parameterized32default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)lisnoc_router_input_route__parameterized32default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys27
#lisnoc_router_input__parameterized02default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys27
#lisnoc_router_input__parameterized12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized122default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized122default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)lisnoc_router_input_route__parameterized42default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)lisnoc_router_input_route__parameterized42default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized132default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized132default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)lisnoc_router_input_route__parameterized52default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)lisnoc_router_input_route__parameterized52default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized142default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized142default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)lisnoc_router_input_route__parameterized62default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)lisnoc_router_input_route__parameterized62default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized152default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized152default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)lisnoc_router_input_route__parameterized72default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)lisnoc_router_input_route__parameterized72default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys27
#lisnoc_router_input__parameterized12default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys27
#lisnoc_router_input__parameterized22default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized162default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized162default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)lisnoc_router_input_route__parameterized82default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)lisnoc_router_input_route__parameterized82default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized172default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized172default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)lisnoc_router_input_route__parameterized92default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)lisnoc_router_input_route__parameterized92default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized182default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized182default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized102default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized102default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized192default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized192default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized112default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized112default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys27
#lisnoc_router_input__parameterized22default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys27
#lisnoc_router_input__parameterized32default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized202default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized202default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized122default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized122default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized212default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized212default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized132default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized132default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized222default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized222default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized142default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized142default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized232default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized232default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized152default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized152default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys27
#lisnoc_router_input__parameterized32default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys27
#lisnoc_router_input__parameterized42default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized242default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized242default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized162default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized162default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized252default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized252default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized172default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized172default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized262default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized262default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized182default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized182default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized272default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized272default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized192default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b01000100000010000100 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized192default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys27
#lisnoc_router_input__parameterized42default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys21
lisnoc_router__parameterized02default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router.v2default:default2
332default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys28
$lisnoc_router_2dgrid__parameterized02default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_2dgrid.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys28
$lisnoc_router_2dgrid__parameterized12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_2dgrid.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter use_prio bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_prio_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter in_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter out_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys21
lisnoc_router__parameterized12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router.v2default:default2
332default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter use_prio bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_prio_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter input_ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter output_ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter in_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter out_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter FLIT_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter FLIT_TYPE_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FLIT_WIDTH bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter NUM_DESTS bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PH_DEST_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter USE_PRIO bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PH_PRIO_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter VCHANNELS bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter INPUT_PORTS bound to: 5 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter OUTPUT_PORTS bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter IN_FIFO_LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter OUT_FIFO_LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter LOOKUP bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys27
#lisnoc_router_input__parameterized52default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized282default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized282default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized202default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized202default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized292default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized292default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized212default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized212default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized302default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized302default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized222default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized222default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized312default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized312default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized232default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized232default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys27
#lisnoc_router_input__parameterized52default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys27
#lisnoc_router_input__parameterized62default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized322default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized322default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized242default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized242default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized332default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized332default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized252default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized252default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized342default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized342default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized262default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized262default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized352default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized352default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized272default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized272default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys27
#lisnoc_router_input__parameterized62default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys27
#lisnoc_router_input__parameterized72default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized362default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized362default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized282default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized282default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized372default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized372default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized292default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized292default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized382default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized382default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized302default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized302default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized392default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized392default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized312default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized312default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys27
#lisnoc_router_input__parameterized72default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys27
#lisnoc_router_input__parameterized82default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized402default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized402default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized322default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized322default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized412default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized412default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized332default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized332default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized422default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized422default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized342default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized342default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized432default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized432default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized352default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized352default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys27
#lisnoc_router_input__parameterized82default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys27
#lisnoc_router_input__parameterized92default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized442default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized442default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized362default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized362default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized452default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized452default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized372default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized372default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized462default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized462default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized382default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized382default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized472default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized472default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized392default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000011000000010 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized392default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys27
#lisnoc_router_input__parameterized92default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys21
lisnoc_router__parameterized12default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router.v2default:default2
332default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys28
$lisnoc_router_2dgrid__parameterized12default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_2dgrid.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys28
$lisnoc_router_2dgrid__parameterized22default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_2dgrid.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter use_prio bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_prio_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter in_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter out_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys21
lisnoc_router__parameterized22default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router.v2default:default2
332default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter use_prio bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_prio_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter input_ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter output_ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter in_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter out_fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter FLIT_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter FLIT_TYPE_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FLIT_WIDTH bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter NUM_DESTS bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PH_DEST_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter USE_PRIO bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PH_PRIO_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter VCHANNELS bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter INPUT_PORTS bound to: 5 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter OUTPUT_PORTS bound to: 5 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter IN_FIFO_LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter OUT_FIFO_LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter LOOKUP bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys28
$lisnoc_router_input__parameterized102default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized482default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized482default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized402default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized402default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized492default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized492default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized412default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized412default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized502default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized502default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized422default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized422default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized512default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized512default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized432default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized432default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys28
$lisnoc_router_input__parameterized102default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys28
$lisnoc_router_input__parameterized112default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized522default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized522default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized442default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized442default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized532default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized532default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized452default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized452default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized542default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized542default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized462default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized462default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized552default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized552default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized472default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized472default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys28
$lisnoc_router_input__parameterized112default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys28
$lisnoc_router_input__parameterized122default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized562default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized562default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized482default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized482default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized572default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized572default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized492default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized492default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized582default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized582default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized502default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized502default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized592default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized592default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized512default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized512default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys28
$lisnoc_router_input__parameterized122default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys28
$lisnoc_router_input__parameterized132default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized602default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized602default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized522default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized522default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized612default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized612default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized532default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized532default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized622default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized622default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized542default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized542default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized632default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized632default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized552default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized552default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys28
$lisnoc_router_input__parameterized132default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys28
$lisnoc_router_input__parameterized142default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter vchannels bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ports bound to: 5 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter fifo_length bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized642default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized642default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized562default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized562default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized652default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized652default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized572default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized572default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized662default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized662default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized582default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized582default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
lisnoc_fifo__parameterized672default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter packet_length bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter LENGTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
lisnoc_fifo__parameterized672default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v2default:default2
312default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2>
*lisnoc_router_input_route__parameterized592default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter flit_data_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter flit_type_width bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter flit_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ph_dest_width bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter num_dests bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter directions bound to: 5 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter lookup bound to: 20'b00001000010100010000 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*lisnoc_router_input_route__parameterized592default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys28
$lisnoc_router_input__parameterized142default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v2default:default2
352default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys21
lisnoc_router__parameterized22default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router.v2default:default2
332default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys28
$lisnoc_router_2dgrid__parameterized22default:default2
152default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_2dgrid.v2default:default2
312default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2"
lisnoc_mesh2x22default:default2
162default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/meshs/lisnoc_mesh2x2.v2default:default2
32default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2&
NOC_AXI_testmodule2default:default2
172default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/new/NOC_AXI_testmodule.v2default:default2
742default:default8@Z8-256h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2"
s00_axi_awuser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
6782default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s00_axi_wuser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
6842default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s00_axi_buser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
6892default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2"
s00_axi_aruser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7022default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s00_axi_ruser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7092default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2"
s01_axi_awuser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7242default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s01_axi_wuser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7302default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s01_axi_buser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7352default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2"
s01_axi_aruser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7482default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s01_axi_ruser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7552default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2"
s02_axi_awuser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7702default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s02_axi_wuser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7762default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s02_axi_buser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7812default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2"
s02_axi_aruser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
7942default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s02_axi_ruser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
8012default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2"
s03_axi_awuser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
8162default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s03_axi_wuser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
8222default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s03_axi_buser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
8272default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2"
s03_axi_aruser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
8402default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
12default:default2!
s03_axi_ruser2default:default2
22default:default2&
NOC_AXI_testmodule2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
8472default:default8@Z8-689h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2>
*design_1_NOC_AXI_testmodule_vc_support_0_02default:default2
182default:default2
12default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_NOC_AXI_testmodule_vc_support_0_0/synth/design_1_NOC_AXI_testmodule_vc_support_0_0.v2default:default2
562default:default8@Z8-256h px� 
�
!design %s has unconnected port %s3331*oasys2#
flits_to_stream2default:default23
packet_buffer_read_address_i[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2"
axi_araddr[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2"
axi_araddr[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2"
axi_araddr[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2"
axi_araddr[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2*
packet_target_addr[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2*
packet_target_addr[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2*
packet_target_addr[11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2*
packet_target_addr[10]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2)
packet_target_addr[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2)
packet_target_addr[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2)
packet_target_addr[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2)
packet_target_addr[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2)
packet_target_addr[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2)
packet_target_addr[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2)
packet_target_addr[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2)
packet_target_addr[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2)
packet_target_addr[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2)
packet_target_addr[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default23
packet_buffer_read_address_i[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_AWSIZE[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_AWSIZE[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_AWSIZE[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2 
S_AXI_AWLOCK2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2$
S_AXI_AWCACHE[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2$
S_AXI_AWCACHE[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2$
S_AXI_AWCACHE[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2$
S_AXI_AWCACHE[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_AWPROT[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_AWPROT[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_AWPROT[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_AWQOS[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_AWQOS[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_AWQOS[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_AWQOS[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2%
S_AXI_AWREGION[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2%
S_AXI_AWREGION[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2%
S_AXI_AWREGION[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2%
S_AXI_AWREGION[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2$
S_AXI_AWUSER[-1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_AWUSER[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_WSTRB[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_WSTRB[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_WSTRB[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_WSTRB[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_WUSER[-1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_WUSER[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_ARSIZE[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_ARSIZE[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_ARSIZE[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2 
S_AXI_ARLOCK2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2$
S_AXI_ARCACHE[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2$
S_AXI_ARCACHE[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2$
S_AXI_ARCACHE[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2$
S_AXI_ARCACHE[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_ARPROT[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_ARPROT[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_ARPROT[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_ARQOS[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_ARQOS[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_ARQOS[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2"
S_AXI_ARQOS[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2%
S_AXI_ARREGION[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2%
S_AXI_ARREGION[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2%
S_AXI_ARREGION[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2%
S_AXI_ARREGION[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2$
S_AXI_ARUSER[-1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys21
lisnoc_axi_slave_v1_0_S00_AXI2default:default2#
S_AXI_ARUSER[0]2default:defaultZ8-3331h px� 
�
%s*synth2�
�Finished RTL Elaboration : Time (s): cpu = 00:00:10 ; elapsed = 00:00:11 . Memory (MB): peak = 1195.926 ; gain = 258.559 ; free physical = 838 ; free virtual = 15710
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:11 ; elapsed = 00:00:11 . Memory (MB): peak = 1195.926 ; gain = 258.559 ; free physical = 787 ; free virtual = 15659
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
Loading part %s157*device2#
xc7z020clg484-12default:defaultZ21-403h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common24
 Constraint Validation Runtime : 2default:default2
00:00:022default:default2
00:00:00.992default:default2
1673.5552default:default2
30.0042default:default2
8842default:default2
157592default:defaultZ17-722h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Constraint Validation : Time (s): cpu = 00:00:28 ; elapsed = 00:00:27 . Memory (MB): peak = 1673.555 ; gain = 736.188 ; free physical = 1058 ; free virtual = 15931
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Loading part: xc7z020clg484-1
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:28 ; elapsed = 00:00:27 . Memory (MB): peak = 1673.555 ; gain = 736.188 ; free physical = 1058 ; free virtual = 15930
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:28 ; elapsed = 00:00:27 . Memory (MB): peak = 1673.555 ; gain = 736.188 ; free physical = 1056 ; free virtual = 15928
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2&
packet_target_addr2default:defaultZ8-5546h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2(
address_lookup_table2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2(
address_lookup_table2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2#
nxt_activeroute2default:default2
22default:default2
52default:defaultZ8-5544h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:34 ; elapsed = 00:00:33 . Memory (MB): peak = 1673.555 ; gain = 736.188 ; free physical = 1659 ; free virtual = 16533
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
s
%s
*synth2[
G|      |RTL Partition                        |Replication |Instances |
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
s
%s
*synth2[
G|1     |lisnoc_mp_interface                  |           4|     13526|
2default:defaulth p
x
� 
s
%s
*synth2[
G|2     |lisnoc_router_2dgrid__parameterized1 |           1|     27685|
2default:defaulth p
x
� 
s
%s
*synth2[
G|3     |lisnoc_router_2dgrid__parameterized0 |           1|     27645|
2default:defaulth p
x
� 
s
%s
*synth2[
G|4     |lisnoc_router_2dgrid                 |           1|     27645|
2default:defaulth p
x
� 
s
%s
*synth2[
G|5     |lisnoc_router_2dgrid__parameterized2 |           1|     27685|
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     16 Bit       Adders := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     15 Bit       Adders := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     14 Bit       Adders := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit       Adders := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit       Adders := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      7 Bit       Adders := 36    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 44    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 60    
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1040  
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               16 Bit    Registers := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 32    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                9 Bit    Registers := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 24    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 440   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 36    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 268   
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              320 Bit         RAMs := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               80 Bit         RAMs := 16    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 2352  
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 20    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     32 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     16 Bit        Muxes := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     16 Bit        Muxes := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 32    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 744   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      5 Bit        Muxes := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 164   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      4 Bit        Muxes := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 20    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 160   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 460   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1412  
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 8     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_axi_slave_v1_0_S00_AXI 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     16 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     15 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     14 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      7 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               16 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 10    
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               80 Bit         RAMs := 4     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     16 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     16 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 26    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
G
%s
*synth2/
Module stream_to_flits__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      7 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 15    
2default:defaulth p
x
� 
C
%s
*synth2+
Module lisnoc_fifo__5 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 27    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
G
%s
*synth2/
Module stream_to_flits__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      7 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 15    
2default:defaulth p
x
� 
C
%s
*synth2+
Module lisnoc_fifo__6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 27    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
G
%s
*synth2/
Module stream_to_flits__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      7 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 15    
2default:defaulth p
x
� 
C
%s
*synth2+
Module lisnoc_fifo__7 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 27    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
D
%s
*synth2,
Module stream_to_flits 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      7 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 15    
2default:defaulth p
x
� 
@
%s
*synth2(
Module lisnoc_fifo 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 27    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
G
%s
*synth2/
Module output_virtualiser 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
G
%s
*synth2/
Module flits_to_stream__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                9 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              320 Bit         RAMs := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 7     
2default:defaulth p
x
� 
C
%s
*synth2+
Module lisnoc_fifo__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 27    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
G
%s
*synth2/
Module flits_to_stream__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                9 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              320 Bit         RAMs := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 7     
2default:defaulth p
x
� 
C
%s
*synth2+
Module lisnoc_fifo__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 27    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
G
%s
*synth2/
Module flits_to_stream__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                9 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              320 Bit         RAMs := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 7     
2default:defaulth p
x
� 
C
%s
*synth2+
Module lisnoc_fifo__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 27    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
D
%s
*synth2,
Module flits_to_stream 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                9 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              320 Bit         RAMs := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 7     
2default:defaulth p
x
� 
C
%s
*synth2+
Module lisnoc_fifo__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 27    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
F
%s
*synth2.
Module input_virtualiser 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized0__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_router_input_route__7 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized1__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_router_input_route__6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized2__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_router_input_route__5 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized3__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_router_input_route__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized0__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_router_input_route__11 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized1__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_router_input_route__10 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized2__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_router_input_route__9 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized3__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_router_input_route__8 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized0__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_router_input_route__15 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized1__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_router_input_route__14 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized2__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_router_input_route__13 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized3__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_router_input_route__12 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized0__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_router_input_route__19 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized1__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_router_input_route__18 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized2__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_router_input_route__17 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized3__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module lisnoc_router_input_route__16 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
P
%s
*synth28
$Module lisnoc_fifo__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_router_input_route__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
P
%s
*synth28
$Module lisnoc_fifo__parameterized1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_router_input_route__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
P
%s
*synth28
$Module lisnoc_fifo__parameterized2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_router_input_route__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
P
%s
*synth28
$Module lisnoc_fifo__parameterized3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_input_route 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module lisnoc_router_output_arbiter__11 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__47 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized4__11 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__46 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized5__11 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__45 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized6__11 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__44 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized7__11 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module lisnoc_router_output_arbiter__12 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__51 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized4__12 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__50 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized5__12 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__49 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized6__12 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__48 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized7__12 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module lisnoc_router_output_arbiter__13 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__55 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized4__13 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__54 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized5__13 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__53 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized6__13 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__52 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized7__13 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module lisnoc_router_output_arbiter__14 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__59 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized4__14 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__58 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized5__14 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__57 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized6__14 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__56 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized7__14 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module lisnoc_router_output_arbiter__15 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__41 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized4__15 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__42 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized5__15 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__43 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized6__15 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__60 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized7__15 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
P
%s
*synth28
$Module lisnoc_fifo__parameterized8 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module lisnoc_router_input_route__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
P
%s
*synth28
$Module lisnoc_fifo__parameterized9 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module lisnoc_router_input_route__parameterized1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized10 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module lisnoc_router_input_route__parameterized2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized11 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module lisnoc_router_input_route__parameterized3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized12 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module lisnoc_router_input_route__parameterized4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized13 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module lisnoc_router_input_route__parameterized5 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized14 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module lisnoc_router_input_route__parameterized6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized15 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module lisnoc_router_input_route__parameterized7 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized16 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module lisnoc_router_input_route__parameterized8 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized17 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module lisnoc_router_input_route__parameterized9 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized18 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized10 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized19 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized11 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized20 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized12 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized21 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized13 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized22 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized14 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized23 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized15 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized24 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized16 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized25 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized17 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized26 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized18 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized27 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized19 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_router_output_arbiter__6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__27 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized4__6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__26 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized5__6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__25 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized6__6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__24 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized7__6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_router_output_arbiter__7 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__31 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized4__7 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__30 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized5__7 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__29 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized6__7 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__28 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized7__7 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_router_output_arbiter__8 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__35 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized4__8 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__34 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized5__8 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__33 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized6__8 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__32 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized7__8 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_router_output_arbiter__9 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__39 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized4__9 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__38 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized5__9 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__37 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized6__9 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__36 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized7__9 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module lisnoc_router_output_arbiter__10 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__21 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized4__10 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__22 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized5__10 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__23 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized6__10 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__40 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized7__10 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized28 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized20 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized29 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized21 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized30 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized22 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized31 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized23 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized32 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized24 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized33 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized25 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized34 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized26 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized35 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized27 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized36 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized28 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized37 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized29 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized38 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized30 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized39 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized31 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized40 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized32 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized41 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized33 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized42 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized34 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized43 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized35 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized44 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized36 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized45 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized37 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized46 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized38 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized47 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized39 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_router_output_arbiter__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
M
%s
*synth25
!Module lisnoc_router_arbiter__7 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized4__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
M
%s
*synth25
!Module lisnoc_router_arbiter__6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized5__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
M
%s
*synth25
!Module lisnoc_router_arbiter__5 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized6__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
M
%s
*synth25
!Module lisnoc_router_arbiter__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized7__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_router_output_arbiter__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__11 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized4__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__10 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized5__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
M
%s
*synth25
!Module lisnoc_router_arbiter__9 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized6__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
M
%s
*synth25
!Module lisnoc_router_arbiter__8 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized7__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_router_output_arbiter__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__15 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized4__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__14 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized5__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__13 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized6__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__12 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized7__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_router_output_arbiter__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__19 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized4__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__18 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized5__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__17 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized6__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__16 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized7__4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_router_output_arbiter__5 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
M
%s
*synth25
!Module lisnoc_router_arbiter__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized4__5 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
M
%s
*synth25
!Module lisnoc_router_arbiter__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized5__5 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
M
%s
*synth25
!Module lisnoc_router_arbiter__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized6__5 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__20 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module lisnoc_fifo__parameterized7__5 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized48 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized40 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized49 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized41 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized50 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized42 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized51 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized43 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized52 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized44 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized53 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized45 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized54 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized46 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized55 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized47 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized56 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized48 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized57 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized49 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized58 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized50 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized59 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized51 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized60 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized52 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized61 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized53 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized62 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized54 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized63 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized55 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized64 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized56 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized65 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized57 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized66 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized58 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_fifo__parameterized67 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
_
%s
*synth2G
3Module lisnoc_router_input_route__parameterized59 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module lisnoc_router_output_arbiter__16 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__67 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized4__16 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__66 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized5__16 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__65 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized6__16 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__64 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized7__16 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module lisnoc_router_output_arbiter__17 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__71 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized4__17 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__70 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized5__17 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__69 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized6__17 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__68 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized7__17 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module lisnoc_router_output_arbiter__18 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__75 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized4__18 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__74 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized5__18 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__73 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized6__18 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__72 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized7__18 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module lisnoc_router_output_arbiter__19 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__79 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized4__19 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__78 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized5__19 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__77 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized6__19 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__76 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module lisnoc_fifo__parameterized7__19 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module lisnoc_router_output_arbiter 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     34 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__61 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
P
%s
*synth28
$Module lisnoc_fifo__parameterized4 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__62 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
P
%s
*synth28
$Module lisnoc_fifo__parameterized5 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module lisnoc_router_arbiter__63 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
P
%s
*synth28
$Module lisnoc_fifo__parameterized6 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
J
%s
*synth22
Module lisnoc_router_arbiter 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
P
%s
*synth28
$Module lisnoc_fifo__parameterized7 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               34 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     34 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2k
WPart Resources:
DSPs: 220 (col length:60)
BRAMs: 280 (col length: RAMB18 60 RAMB36 30)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
HMultithreading enabled for synth_design using a maximum of %s processes.4031*oasys2
42default:defaultZ8-5580h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
merging register '%s' into '%s'3619*oasys2I
5lisnoc_axi_slave_v1_0_S00_AXI_inst/axi_awlen_reg[7:0]2default:default2I
5lisnoc_axi_slave_v1_0_S00_AXI_inst/axi_awlen_reg[7:0]2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/hdl/lisnoc_axi_slave_v1_0_S00_AXI.v2default:default2
3322default:default8@Z8-4471h px� 
�
merging register '%s' into '%s'3619*oasys2I
5lisnoc_axi_slave_v1_0_S00_AXI_inst/axi_arlen_reg[7:0]2default:default2I
5lisnoc_axi_slave_v1_0_S00_AXI_inst/axi_arlen_reg[7:0]2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ipshared/e073/sources_1/imports/hdl/lisnoc_axi_slave_v1_0_S00_AXI.v2default:default2
5552default:default8@Z8-4471h px� 
�
+design %s has port %s driven by constant %s3447*oasys2'
lisnoc_mp_interface2default:default2%
s00_axi_buser[-1]2default:default2
02default:defaultZ8-3917h px� 
�
+design %s has port %s driven by constant %s3447*oasys2'
lisnoc_mp_interface2default:default2$
s00_axi_buser[0]2default:default2
02default:defaultZ8-3917h px� 
�
+design %s has port %s driven by constant %s3447*oasys2'
lisnoc_mp_interface2default:default2%
s00_axi_ruser[-1]2default:default2
02default:defaultZ8-3917h px� 
�
+design %s has port %s driven by constant %s3447*oasys2'
lisnoc_mp_interface2default:default2$
s00_axi_ruser[0]2default:default2
02default:defaultZ8-3917h px� 
�
!design %s has unconnected port %s3331*oasys2#
flits_to_stream2default:default23
packet_buffer_read_address_i[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2&
flits_to_stream__32default:default23
packet_buffer_read_address_i[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2&
flits_to_stream__22default:default23
packet_buffer_read_address_i[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2&
flits_to_stream__12default:default23
packet_buffer_read_address_i[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default23
packet_buffer_read_address_i[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2"
axi_araddr[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2"
axi_araddr[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2"
axi_araddr[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2"
axi_araddr[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
input_virtualiser2default:default2!
axi_araddr[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2*
packet_target_addr[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2*
packet_target_addr[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2#
stream_to_flits2default:default2*
packet_target_addr[11]2default:defaultZ8-3331h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33312default:default2
1002default:defaultZ17-14h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33312default:default2
1002default:defaultZ17-14h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2G
3lisnoc_axi_slave_v1_0_S00_AXI_inst/axi_rresp_reg[0]2default:default2
FDRE2default:default2G
3lisnoc_axi_slave_v1_0_S00_AXI_inst/axi_rresp_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2G
3lisnoc_axi_slave_v1_0_S00_AXI_inst/axi_bresp_reg[0]2default:default2
FDRE2default:default2G
3lisnoc_axi_slave_v1_0_S00_AXI_inst/axi_bresp_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[2].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[2].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[2].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[2].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[2].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[1].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[1].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[1].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[1].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[1].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[0].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[0].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[0].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[0].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[0].route/switch_request_reg[3]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[2].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[2].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[2].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[2].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[2].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[2].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[1].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[2].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[1].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[2].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[1].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[2].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[1].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[2].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[1].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[1].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[0].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[1].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[0].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[1].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[0].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[1].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[0].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[1].route/switch_request_reg[2]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[0].route/switch_request_reg[2]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\inputs[0].inputs /\vchannel[0].route/switch_request_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\inputs[1].inputs /\vchannel[0].route/switch_request_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\inputs[2].inputs /\vchannel[0].route/switch_request_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\inputs[4].inputs /\vchannel[0].route/switch_request_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\inputs[3].inputs /\vchannel[0].route/switch_request_reg[2] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[3].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[3].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[2].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[2].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[1].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[1].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[0].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[0].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[3].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[3].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[3].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[2].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[2].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[2].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[1].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[1].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[1].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[0].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[0].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[0].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[3].fifo/fifo_write_ptr_reg[2]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[3].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[2].fifo/fifo_write_ptr_reg[2]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[2].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[1].fifo/fifo_write_ptr_reg[2]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[1].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[0].fifo/fifo_write_ptr_reg[2]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[0].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[3].fifo/fifo_write_ptr_reg[2]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[3].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[2].fifo/fifo_write_ptr_reg[2]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[2].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[1].fifo/fifo_write_ptr_reg[2]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[1].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[0].fifo/fifo_write_ptr_reg[2]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[0].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[3].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[2].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[1].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[0].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[3].fifo/fifo_write_ptr_reg[1]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[3].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[2].fifo/fifo_write_ptr_reg[1]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[2].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[1].fifo/fifo_write_ptr_reg[1]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[1].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[3].outputs/vchannel[0].fifo/fifo_write_ptr_reg[1]2default:default2
FDRE2default:default2V
Bu_router/outputs[3].outputs/vchannel[0].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[3].fifo/fifo_write_ptr_reg[1]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[3].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[2].fifo/fifo_write_ptr_reg[1]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[2].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[1].fifo/fifo_write_ptr_reg[1]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[1].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[2].outputs/vchannel[0].fifo/fifo_write_ptr_reg[1]2default:default2
FDRE2default:default2V
Bu_router/outputs[2].outputs/vchannel[0].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[3].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[2].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[1].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Z
Fu_router/\outputs[2].outputs/vchannel[0].fifo /\fifo_write_ptr_reg[3] 2default:defaultZ8-3333h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[0].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized52default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[0].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized52default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[1].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized52default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[1].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized52default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[2].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized52default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[2].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized52default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[3].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized52default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[3].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized52default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2;
'vchannel[0].route/switch_request_reg[2]2default:default27
#lisnoc_router_input__parameterized52default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[0].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized62default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[0].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized62default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[1].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized62default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[1].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized62default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[2].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized62default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[2].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized62default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[3].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized62default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[3].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized62default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2;
'vchannel[0].route/switch_request_reg[2]2default:default27
#lisnoc_router_input__parameterized62default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[0].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized72default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[0].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized72default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[1].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized72default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[1].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized72default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[2].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized72default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[2].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized72default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[3].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized72default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[3].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized72default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2;
'vchannel[0].route/switch_request_reg[2]2default:default27
#lisnoc_router_input__parameterized72default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[0].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized82default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[0].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized82default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[1].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized82default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[1].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized82default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[2].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized82default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[2].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized82default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[3].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized82default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[3].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized82default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2;
'vchannel[0].route/switch_request_reg[2]2default:default27
#lisnoc_router_input__parameterized82default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[0].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized92default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[0].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized92default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[1].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized92default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[1].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized92default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[2].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized92default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[2].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized92default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[3].route/cur_select_reg[3]2default:default27
#lisnoc_router_input__parameterized92default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys27
#vchannel[3].route/cur_select_reg[2]2default:default27
#lisnoc_router_input__parameterized92default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2;
'vchannel[0].route/switch_request_reg[2]2default:default27
#lisnoc_router_input__parameterized92default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[4]2default:default2-
lisnoc_router_arbiter__152default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[3]2default:default2-
lisnoc_router_arbiter__152default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[2]2default:default2-
lisnoc_router_arbiter__152default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[1]2default:default2-
lisnoc_router_arbiter__152default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[0]2default:default2-
lisnoc_router_arbiter__152default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2#
activeroute_reg2default:default2-
lisnoc_router_arbiter__152default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2)
fifo_write_ptr_reg[3]2default:default22
lisnoc_fifo__parameterized4__32default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[4]2default:default2-
lisnoc_router_arbiter__142default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[3]2default:default2-
lisnoc_router_arbiter__142default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[2]2default:default2-
lisnoc_router_arbiter__142default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[1]2default:default2-
lisnoc_router_arbiter__142default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[0]2default:default2-
lisnoc_router_arbiter__142default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2#
activeroute_reg2default:default2-
lisnoc_router_arbiter__142default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2)
fifo_write_ptr_reg[3]2default:default22
lisnoc_fifo__parameterized5__32default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[4]2default:default2-
lisnoc_router_arbiter__132default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[3]2default:default2-
lisnoc_router_arbiter__132default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[2]2default:default2-
lisnoc_router_arbiter__132default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[1]2default:default2-
lisnoc_router_arbiter__132default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[0]2default:default2-
lisnoc_router_arbiter__132default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2#
activeroute_reg2default:default2-
lisnoc_router_arbiter__132default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2)
fifo_write_ptr_reg[3]2default:default22
lisnoc_fifo__parameterized6__32default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[4]2default:default2-
lisnoc_router_arbiter__122default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[3]2default:default2-
lisnoc_router_arbiter__122default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[2]2default:default2-
lisnoc_router_arbiter__122default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[1]2default:default2-
lisnoc_router_arbiter__122default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[0]2default:default2-
lisnoc_router_arbiter__122default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2#
activeroute_reg2default:default2-
lisnoc_router_arbiter__122default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2)
fifo_write_ptr_reg[3]2default:default22
lisnoc_fifo__parameterized7__32default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[4]2default:default2-
lisnoc_router_arbiter__192default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[3]2default:default2-
lisnoc_router_arbiter__192default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[2]2default:default2-
lisnoc_router_arbiter__192default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[1]2default:default2-
lisnoc_router_arbiter__192default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[0]2default:default2-
lisnoc_router_arbiter__192default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2#
activeroute_reg2default:default2-
lisnoc_router_arbiter__192default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2)
fifo_write_ptr_reg[3]2default:default22
lisnoc_fifo__parameterized4__42default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[4]2default:default2-
lisnoc_router_arbiter__182default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[3]2default:default2-
lisnoc_router_arbiter__182default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[2]2default:default2-
lisnoc_router_arbiter__182default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[1]2default:default2-
lisnoc_router_arbiter__182default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[0]2default:default2-
lisnoc_router_arbiter__182default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2#
activeroute_reg2default:default2-
lisnoc_router_arbiter__182default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2)
fifo_write_ptr_reg[3]2default:default22
lisnoc_fifo__parameterized5__42default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[4]2default:default2-
lisnoc_router_arbiter__172default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[3]2default:default2-
lisnoc_router_arbiter__172default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[2]2default:default2-
lisnoc_router_arbiter__172default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[1]2default:default2-
lisnoc_router_arbiter__172default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[0]2default:default2-
lisnoc_router_arbiter__172default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2#
activeroute_reg2default:default2-
lisnoc_router_arbiter__172default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2)
fifo_write_ptr_reg[3]2default:default22
lisnoc_fifo__parameterized6__42default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[4]2default:default2-
lisnoc_router_arbiter__162default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[3]2default:default2-
lisnoc_router_arbiter__162default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[2]2default:default2-
lisnoc_router_arbiter__162default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[1]2default:default2-
lisnoc_router_arbiter__162default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2%
activeport_reg[0]2default:default2-
lisnoc_router_arbiter__162default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2#
activeroute_reg2default:default2-
lisnoc_router_arbiter__162default:defaultZ8-3332h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33322default:default2
1002default:defaultZ17-14h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33322default:default2
1002default:defaultZ17-14h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][5] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][5] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][5] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][5] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][6] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][6] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][6] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][6] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][7] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][7] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][7] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][7] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][8] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][8] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][8] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][8] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2X
Du_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][10] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][10] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][10] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][10] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][12] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][12] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][12] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][12] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][13] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][13] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][13] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][13] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][14] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][14] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][14] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][14] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][15] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][15] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][15] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][15] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][16] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][16] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][16] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[0].fifo /\fifo_data_reg[3][16] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[3].fifo /\fifo_data_reg[3][17] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[1].fifo /\fifo_data_reg[3][17] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
Eu_router/\outputs[3].outputs/vchannel[2].fifo /\fifo_data_reg[3][17] 2default:defaultZ8-3333h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33332default:default2
1002default:defaultZ17-14h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33332default:default2
1002default:defaultZ17-14h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[2].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[2].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[2].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[2].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[2].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[1].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[1].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[1].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[1].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[1].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[0].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[0].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[0].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[0].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[0].route/switch_request_reg[1]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[3].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[2].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[3].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[2].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[3].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[2].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[3].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[2].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[3].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[2].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[2].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[1].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[2].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[1].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[2].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[1].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[2].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[1].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[2].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[1].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[0].inputs/vchannel[1].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[0].inputs/vchannel[0].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[1].inputs/vchannel[1].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[1].inputs/vchannel[0].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[2].inputs/vchannel[1].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[2].inputs/vchannel[0].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[4].inputs/vchannel[1].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[4].inputs/vchannel[0].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2U
Au_router/inputs[3].inputs/vchannel[1].route/switch_request_reg[0]2default:default2
FD2default:default2U
Au_router/inputs[3].inputs/vchannel[0].route/switch_request_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[1].outputs/vchannel[3].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[1].outputs/vchannel[3].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[1].outputs/vchannel[2].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[1].outputs/vchannel[2].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[1].outputs/vchannel[1].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[1].outputs/vchannel[1].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2V
Bu_router/outputs[1].outputs/vchannel[0].fifo/fifo_write_ptr_reg[4]2default:default2
FDRE2default:default2V
Bu_router/outputs[1].outputs/vchannel[0].fifo/fifo_write_ptr_reg[3]2default:defaultZ8-3886h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-38862default:default2
1002default:defaultZ17-14h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33322default:default2
1002default:defaultZ17-14h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33332default:default2
1002default:defaultZ17-14h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33322default:default2
1002default:defaultZ17-14h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33332default:default2
1002default:defaultZ17-14h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33322default:default2
1002default:defaultZ17-14h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33332default:default2
1002default:defaultZ17-14h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:51 ; elapsed = 00:01:44 . Memory (MB): peak = 1673.555 ; gain = 736.188 ; free physical = 377 ; free virtual = 14058
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
k
%s*synth2S
?
Distributed RAM: Preliminary Mapping  Report (see note below)
2default:defaulth px� 
�
%s*synth2�
�+--------------------+------------------------------------------------------------------+-----------+----------------------+--------------+
2default:defaulth px� 
�
%s*synth2�
�|Module Name         | RTL Object                                                       | Inference | Size (Depth x Width) | Primitives   | 
2default:defaulth px� 
�
%s*synth2�
�+--------------------+------------------------------------------------------------------+-----------+----------------------+--------------+
2default:defaulth px� 
�
%s*synth2�
�|lisnoc_mp_interface | lisnoc_axi_slave_v1_0_S00_AXI_inst/BYTE_BRAM_GEN[0].byte_ram_reg | Implied   | 16 x 8               | RAM32M x 2   | 
2default:defaulth px� 
�
%s*synth2�
�|lisnoc_mp_interface | lisnoc_axi_slave_v1_0_S00_AXI_inst/BYTE_BRAM_GEN[1].byte_ram_reg | Implied   | 16 x 8               | RAM32M x 2   | 
2default:defaulth px� 
�
%s*synth2�
�|lisnoc_mp_interface | lisnoc_axi_slave_v1_0_S00_AXI_inst/BYTE_BRAM_GEN[2].byte_ram_reg | Implied   | 16 x 8               | RAM32M x 2   | 
2default:defaulth px� 
�
%s*synth2�
�|lisnoc_mp_interface | lisnoc_axi_slave_v1_0_S00_AXI_inst/BYTE_BRAM_GEN[3].byte_ram_reg | Implied   | 16 x 8               | RAM32M x 2   | 
2default:defaulth px� 
�
%s*synth2�
�|flits_to_stream     | recieving_packet_buffer_reg                                      | Implied   | 16 x 32              | RAM32M x 6   | 
2default:defaulth px� 
�
%s*synth2�
�|flits_to_stream     | recieving_packet_buffer_reg                                      | Implied   | 16 x 32              | RAM32M x 6   | 
2default:defaulth px� 
�
%s*synth2�
�|flits_to_stream     | recieving_packet_buffer_reg                                      | Implied   | 16 x 32              | RAM32M x 6   | 
2default:defaulth px� 
�
%s*synth2�
�|flits_to_stream     | recieving_packet_buffer_reg                                      | Implied   | 16 x 32              | RAM32M x 6   | 
2default:defaulth px� 
�
%s*synth2�
�+--------------------+------------------------------------------------------------------+-----------+----------------------+--------------+

2default:defaulth px� 
�
%s*synth2�
�Note: The table above is a preliminary report that shows the Distributed RAMs at the current stage of the synthesis flow. Some Distributed RAMs may be reimplemented as non Distributed RAM primitives later in the synthesis flow. Multiple instantiated RAMs are reported only once.
2default:defaulth px� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
s
%s
*synth2[
G|      |RTL Partition                        |Replication |Instances |
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
s
%s
*synth2[
G|1     |lisnoc_mp_interface                  |           4|     20792|
2default:defaulth p
x
� 
s
%s
*synth2[
G|2     |lisnoc_router_2dgrid__parameterized1 |           1|     33277|
2default:defaulth p
x
� 
s
%s
*synth2[
G|3     |lisnoc_router_2dgrid__parameterized0 |           1|     33257|
2default:defaulth p
x
� 
s
%s
*synth2[
G|4     |lisnoc_router_2dgrid                 |           1|     33277|
2default:defaulth p
x
� 
s
%s
*synth2[
G|5     |lisnoc_router_2dgrid__parameterized2 |           1|     33277|
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:00:58 ; elapsed = 00:01:52 . Memory (MB): peak = 1689.535 ; gain = 752.168 ; free physical = 140 ; free virtual = 13909
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Timing Optimization : Time (s): cpu = 00:01:15 ; elapsed = 00:02:09 . Memory (MB): peak = 1723.535 ; gain = 786.168 ; free physical = 798 ; free virtual = 14605
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
s
%s
*synth2[
G|      |RTL Partition                        |Replication |Instances |
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
s
%s
*synth2[
G|1     |lisnoc_mp_interface                  |           1|     20792|
2default:defaulth p
x
� 
s
%s
*synth2[
G|2     |lisnoc_router_2dgrid__parameterized1 |           1|     30053|
2default:defaulth p
x
� 
s
%s
*synth2[
G|3     |lisnoc_router_2dgrid__parameterized0 |           1|     30033|
2default:defaulth p
x
� 
s
%s
*synth2[
G|4     |lisnoc_router_2dgrid                 |           1|     30053|
2default:defaulth p
x
� 
s
%s
*synth2[
G|5     |lisnoc_router_2dgrid__parameterized2 |           1|     30053|
2default:defaulth p
x
� 
s
%s
*synth2[
G|6     |lisnoc_mp_interface__1               |           3|     20792|
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Technology Mapping : Time (s): cpu = 00:01:22 ; elapsed = 00:02:37 . Memory (MB): peak = 1743.473 ; gain = 806.105 ; free physical = 153 ; free virtual = 13171
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
s
%s
*synth2[
G|      |RTL Partition                        |Replication |Instances |
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
s
%s
*synth2[
G|1     |lisnoc_mp_interface                  |           1|      6719|
2default:defaulth p
x
� 
s
%s
*synth2[
G|2     |lisnoc_router_2dgrid__parameterized1 |           1|      8423|
2default:defaulth p
x
� 
s
%s
*synth2[
G|3     |lisnoc_router_2dgrid__parameterized0 |           1|      8487|
2default:defaulth p
x
� 
s
%s
*synth2[
G|4     |lisnoc_router_2dgrid                 |           1|      8487|
2default:defaulth p
x
� 
s
%s
*synth2[
G|5     |lisnoc_router_2dgrid__parameterized2 |           1|      8487|
2default:defaulth p
x
� 
s
%s
*synth2[
G|6     |lisnoc_mp_interface__1               |           3|      6719|
2default:defaulth p
x
� 
s
%s
*synth2[
G+------+-------------------------------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished IO Insertion : Time (s): cpu = 00:01:28 ; elapsed = 00:02:45 . Memory (MB): peak = 1775.777 ; gain = 838.410 ; free physical = 168 ; free virtual = 13140
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:01:28 ; elapsed = 00:02:45 . Memory (MB): peak = 1775.777 ; gain = 838.410 ; free physical = 160 ; free virtual = 13136
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:01:32 ; elapsed = 00:02:50 . Memory (MB): peak = 1775.777 ; gain = 838.410 ; free physical = 136 ; free virtual = 13137
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:01:32 ; elapsed = 00:02:50 . Memory (MB): peak = 1775.777 ; gain = 838.410 ; free physical = 121 ; free virtual = 13136
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:01:40 ; elapsed = 00:02:59 . Memory (MB): peak = 1775.777 ; gain = 838.410 ; free physical = 123 ; free virtual = 12911
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:01:41 ; elapsed = 00:02:59 . Memory (MB): peak = 1775.777 ; gain = 838.410 ; free physical = 121 ; free virtual = 12910
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
| |BlackBox name |Instances |
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
D
%s*synth2,
|      |Cell   |Count |
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
D
%s*synth2,
|1     |CARRY4 |   112|
2default:defaulth px� 
D
%s*synth2,
|2     |LUT1   |   384|
2default:defaulth px� 
D
%s*synth2,
|3     |LUT2   |  1080|
2default:defaulth px� 
D
%s*synth2,
|4     |LUT3   |  1064|
2default:defaulth px� 
D
%s*synth2,
|5     |LUT4   |  4992|
2default:defaulth px� 
D
%s*synth2,
|6     |LUT5   |  1632|
2default:defaulth px� 
D
%s*synth2,
|7     |LUT6   | 23720|
2default:defaulth px� 
D
%s*synth2,
|8     |RAM32M |   128|
2default:defaulth px� 
D
%s*synth2,
|9     |FDRE   | 27444|
2default:defaulth px� 
D
%s*synth2,
|10    |FDSE   |   176|
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
�
%s
*synth2z
f+------+-----------------------------------------+-------------------------------------------+------+
2default:defaulth p
x
� 
�
%s
*synth2z
f|      |Instance                                 |Module                                     |Cells |
2default:defaulth p
x
� 
�
%s
*synth2z
f+------+-----------------------------------------+-------------------------------------------+------+
2default:defaulth p
x
� 
�
%s
*synth2z
f|1     |top                                      |                                           | 60732|
2default:defaulth p
x
� 
�
%s
*synth2z
f|2     |  inst                                   |NOC_AXI_testmodule                         | 60732|
2default:defaulth p
x
� 
�
%s
*synth2z
f|3     |    AXI_NIC_0                            |lisnoc_mp_interface                        |  6722|
2default:defaulth p
x
� 
�
%s
*synth2z
f|4     |      lisnoc_axi_slave_v1_0_S00_AXI_inst |lisnoc_axi_slave_v1_0_S00_AXI_188          |   517|
2default:defaulth p
x
� 
�
%s
*synth2z
f|5     |      multiplexed_input                  |input_virtualiser_189                      |  3102|
2default:defaulth p
x
� 
�
%s
*synth2z
f|6     |        \genblk1[0].input_to_stream      |flits_to_stream_199                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|7     |        \genblk1[0].recieving_fifo       |lisnoc_fifo_200                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|8     |        \genblk1[1].input_to_stream      |flits_to_stream_201                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|9     |        \genblk1[1].recieving_fifo       |lisnoc_fifo_202                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|10    |        \genblk1[2].input_to_stream      |flits_to_stream_203                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|11    |        \genblk1[2].recieving_fifo       |lisnoc_fifo_204                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|12    |        \genblk1[3].input_to_stream      |flits_to_stream_205                        |    66|
2default:defaulth p
x
� 
�
%s
*synth2z
f|13    |        \genblk1[3].recieving_fifo       |lisnoc_fifo_206                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|14    |      multiplexed_output                 |output_virtualiser_190                     |  2952|
2default:defaulth p
x
� 
�
%s
*synth2z
f|15    |        \genblk1[0].output_to_flits      |stream_to_flits_191                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|16    |        \genblk1[0].sending_fifo         |lisnoc_fifo_192                            |   728|
2default:defaulth p
x
� 
�
%s
*synth2z
f|17    |        \genblk1[1].output_to_flits      |stream_to_flits_193                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|18    |        \genblk1[1].sending_fifo         |lisnoc_fifo_194                            |   728|
2default:defaulth p
x
� 
�
%s
*synth2z
f|19    |        \genblk1[2].output_to_flits      |stream_to_flits_195                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|20    |        \genblk1[2].sending_fifo         |lisnoc_fifo_196                            |   745|
2default:defaulth p
x
� 
�
%s
*synth2z
f|21    |        \genblk1[3].output_to_flits      |stream_to_flits_197                        |    13|
2default:defaulth p
x
� 
�
%s
*synth2z
f|22    |        \genblk1[3].sending_fifo         |lisnoc_fifo_198                            |   712|
2default:defaulth p
x
� 
�
%s
*synth2z
f|23    |    AXI_NIC_1                            |lisnoc_mp_interface_0                      |  6723|
2default:defaulth p
x
� 
�
%s
*synth2z
f|24    |      lisnoc_axi_slave_v1_0_S00_AXI_inst |lisnoc_axi_slave_v1_0_S00_AXI_169          |   518|
2default:defaulth p
x
� 
�
%s
*synth2z
f|25    |      multiplexed_input                  |input_virtualiser_170                      |  3102|
2default:defaulth p
x
� 
�
%s
*synth2z
f|26    |        \genblk1[0].input_to_stream      |flits_to_stream_180                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|27    |        \genblk1[0].recieving_fifo       |lisnoc_fifo_181                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|28    |        \genblk1[1].input_to_stream      |flits_to_stream_182                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|29    |        \genblk1[1].recieving_fifo       |lisnoc_fifo_183                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|30    |        \genblk1[2].input_to_stream      |flits_to_stream_184                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|31    |        \genblk1[2].recieving_fifo       |lisnoc_fifo_185                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|32    |        \genblk1[3].input_to_stream      |flits_to_stream_186                        |    66|
2default:defaulth p
x
� 
�
%s
*synth2z
f|33    |        \genblk1[3].recieving_fifo       |lisnoc_fifo_187                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|34    |      multiplexed_output                 |output_virtualiser_171                     |  2952|
2default:defaulth p
x
� 
�
%s
*synth2z
f|35    |        \genblk1[0].output_to_flits      |stream_to_flits_172                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|36    |        \genblk1[0].sending_fifo         |lisnoc_fifo_173                            |   728|
2default:defaulth p
x
� 
�
%s
*synth2z
f|37    |        \genblk1[1].output_to_flits      |stream_to_flits_174                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|38    |        \genblk1[1].sending_fifo         |lisnoc_fifo_175                            |   728|
2default:defaulth p
x
� 
�
%s
*synth2z
f|39    |        \genblk1[2].output_to_flits      |stream_to_flits_176                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|40    |        \genblk1[2].sending_fifo         |lisnoc_fifo_177                            |   745|
2default:defaulth p
x
� 
�
%s
*synth2z
f|41    |        \genblk1[3].output_to_flits      |stream_to_flits_178                        |    13|
2default:defaulth p
x
� 
�
%s
*synth2z
f|42    |        \genblk1[3].sending_fifo         |lisnoc_fifo_179                            |   712|
2default:defaulth p
x
� 
�
%s
*synth2z
f|43    |    AXI_NIC_2                            |lisnoc_mp_interface_1                      |  6723|
2default:defaulth p
x
� 
�
%s
*synth2z
f|44    |      lisnoc_axi_slave_v1_0_S00_AXI_inst |lisnoc_axi_slave_v1_0_S00_AXI_150          |   518|
2default:defaulth p
x
� 
�
%s
*synth2z
f|45    |      multiplexed_input                  |input_virtualiser_151                      |  3102|
2default:defaulth p
x
� 
�
%s
*synth2z
f|46    |        \genblk1[0].input_to_stream      |flits_to_stream_161                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|47    |        \genblk1[0].recieving_fifo       |lisnoc_fifo_162                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|48    |        \genblk1[1].input_to_stream      |flits_to_stream_163                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|49    |        \genblk1[1].recieving_fifo       |lisnoc_fifo_164                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|50    |        \genblk1[2].input_to_stream      |flits_to_stream_165                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|51    |        \genblk1[2].recieving_fifo       |lisnoc_fifo_166                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|52    |        \genblk1[3].input_to_stream      |flits_to_stream_167                        |    66|
2default:defaulth p
x
� 
�
%s
*synth2z
f|53    |        \genblk1[3].recieving_fifo       |lisnoc_fifo_168                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|54    |      multiplexed_output                 |output_virtualiser_152                     |  2952|
2default:defaulth p
x
� 
�
%s
*synth2z
f|55    |        \genblk1[0].output_to_flits      |stream_to_flits_153                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|56    |        \genblk1[0].sending_fifo         |lisnoc_fifo_154                            |   728|
2default:defaulth p
x
� 
�
%s
*synth2z
f|57    |        \genblk1[1].output_to_flits      |stream_to_flits_155                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|58    |        \genblk1[1].sending_fifo         |lisnoc_fifo_156                            |   728|
2default:defaulth p
x
� 
�
%s
*synth2z
f|59    |        \genblk1[2].output_to_flits      |stream_to_flits_157                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|60    |        \genblk1[2].sending_fifo         |lisnoc_fifo_158                            |   745|
2default:defaulth p
x
� 
�
%s
*synth2z
f|61    |        \genblk1[3].output_to_flits      |stream_to_flits_159                        |    13|
2default:defaulth p
x
� 
�
%s
*synth2z
f|62    |        \genblk1[3].sending_fifo         |lisnoc_fifo_160                            |   712|
2default:defaulth p
x
� 
�
%s
*synth2z
f|63    |    AXI_NIC_3                            |lisnoc_mp_interface_2                      |  6723|
2default:defaulth p
x
� 
�
%s
*synth2z
f|64    |      lisnoc_axi_slave_v1_0_S00_AXI_inst |lisnoc_axi_slave_v1_0_S00_AXI              |   518|
2default:defaulth p
x
� 
�
%s
*synth2z
f|65    |      multiplexed_input                  |input_virtualiser                          |  3102|
2default:defaulth p
x
� 
�
%s
*synth2z
f|66    |        \genblk1[0].input_to_stream      |flits_to_stream                            |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|67    |        \genblk1[0].recieving_fifo       |lisnoc_fifo_143                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|68    |        \genblk1[1].input_to_stream      |flits_to_stream_144                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|69    |        \genblk1[1].recieving_fifo       |lisnoc_fifo_145                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|70    |        \genblk1[2].input_to_stream      |flits_to_stream_146                        |    28|
2default:defaulth p
x
� 
�
%s
*synth2z
f|71    |        \genblk1[2].recieving_fifo       |lisnoc_fifo_147                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|72    |        \genblk1[3].input_to_stream      |flits_to_stream_148                        |    66|
2default:defaulth p
x
� 
�
%s
*synth2z
f|73    |        \genblk1[3].recieving_fifo       |lisnoc_fifo_149                            |   738|
2default:defaulth p
x
� 
�
%s
*synth2z
f|74    |      multiplexed_output                 |output_virtualiser                         |  2952|
2default:defaulth p
x
� 
�
%s
*synth2z
f|75    |        \genblk1[0].output_to_flits      |stream_to_flits                            |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|76    |        \genblk1[0].sending_fifo         |lisnoc_fifo                                |   728|
2default:defaulth p
x
� 
�
%s
*synth2z
f|77    |        \genblk1[1].output_to_flits      |stream_to_flits_137                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|78    |        \genblk1[1].sending_fifo         |lisnoc_fifo_138                            |   728|
2default:defaulth p
x
� 
�
%s
*synth2z
f|79    |        \genblk1[2].output_to_flits      |stream_to_flits_139                        |     8|
2default:defaulth p
x
� 
�
%s
*synth2z
f|80    |        \genblk1[2].sending_fifo         |lisnoc_fifo_140                            |   745|
2default:defaulth p
x
� 
�
%s
*synth2z
f|81    |        \genblk1[3].output_to_flits      |stream_to_flits_141                        |    13|
2default:defaulth p
x
� 
�
%s
*synth2z
f|82    |        \genblk1[3].sending_fifo         |lisnoc_fifo_142                            |   712|
2default:defaulth p
x
� 
�
%s
*synth2z
f|83    |    network                              |lisnoc_mesh2x2                             | 33841|
2default:defaulth p
x
� 
�
%s
*synth2z
f|84    |      u_router_0_0                       |lisnoc_router_2dgrid                       |  8478|
2default:defaulth p
x
� 
�
%s
*synth2z
f|85    |        u_router                         |lisnoc_router                              |  7980|
2default:defaulth p
x
� 
�
%s
*synth2z
f|86    |          \inputs[1].inputs              |lisnoc_router_input                        |  1324|
2default:defaulth p
x
� 
�
%s
*synth2z
f|87    |            \vchannel[0].fifo            |lisnoc_fifo__parameterized0_129            |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|88    |            \vchannel[0].route           |lisnoc_router_input_route_130              |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|89    |            \vchannel[1].fifo            |lisnoc_fifo__parameterized1_131            |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|90    |            \vchannel[1].route           |lisnoc_router_input_route_132              |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|91    |            \vchannel[2].fifo            |lisnoc_fifo__parameterized2_133            |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|92    |            \vchannel[2].route           |lisnoc_router_input_route_134              |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|93    |            \vchannel[3].fifo            |lisnoc_fifo__parameterized3_135            |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|94    |            \vchannel[3].route           |lisnoc_router_input_route_136              |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|95    |          \inputs[2].inputs              |lisnoc_router_input_86                     |  1324|
2default:defaulth p
x
� 
�
%s
*synth2z
f|96    |            \vchannel[0].fifo            |lisnoc_fifo__parameterized0_121            |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|97    |            \vchannel[0].route           |lisnoc_router_input_route_122              |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|98    |            \vchannel[1].fifo            |lisnoc_fifo__parameterized1_123            |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|99    |            \vchannel[1].route           |lisnoc_router_input_route_124              |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|100   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized2_125            |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|101   |            \vchannel[2].route           |lisnoc_router_input_route_126              |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|102   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized3_127            |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|103   |            \vchannel[3].route           |lisnoc_router_input_route_128              |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|104   |          \inputs[4].inputs              |lisnoc_router_input_87                     |  1323|
2default:defaulth p
x
� 
�
%s
*synth2z
f|105   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized0                |   293|
2default:defaulth p
x
� 
�
%s
*synth2z
f|106   |            \vchannel[0].route           |lisnoc_router_input_route                  |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|107   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized1                |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|108   |            \vchannel[1].route           |lisnoc_router_input_route_118              |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|109   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized2                |   290|
2default:defaulth p
x
� 
�
%s
*synth2z
f|110   |            \vchannel[2].route           |lisnoc_router_input_route_119              |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|111   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized3                |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|112   |            \vchannel[3].route           |lisnoc_router_input_route_120              |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|113   |          \outputs[1].outputs            |lisnoc_router_output_88                    |  1283|
2default:defaulth p
x
� 
�
%s
*synth2z
f|114   |            output_arbiter               |lisnoc_router_output_arbiter_109           |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|115   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_110                  |   161|
2default:defaulth p
x
� 
�
%s
*synth2z
f|116   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_111            |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|117   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_112                  |   161|
2default:defaulth p
x
� 
�
%s
*synth2z
f|118   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_113            |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|119   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_114                  |   161|
2default:defaulth p
x
� 
�
%s
*synth2z
f|120   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_115            |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|121   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_116                  |    59|
2default:defaulth p
x
� 
�
%s
*synth2z
f|122   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_117            |   252|
2default:defaulth p
x
� 
�
%s
*synth2z
f|123   |          \outputs[2].outputs            |lisnoc_router_output_89                    |  1283|
2default:defaulth p
x
� 
�
%s
*synth2z
f|124   |            output_arbiter               |lisnoc_router_output_arbiter_100           |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|125   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_101                  |   161|
2default:defaulth p
x
� 
�
%s
*synth2z
f|126   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_102            |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|127   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_103                  |   161|
2default:defaulth p
x
� 
�
%s
*synth2z
f|128   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_104            |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|129   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_105                  |   161|
2default:defaulth p
x
� 
�
%s
*synth2z
f|130   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_106            |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|131   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_107                  |    59|
2default:defaulth p
x
� 
�
%s
*synth2z
f|132   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_108            |   252|
2default:defaulth p
x
� 
�
%s
*synth2z
f|133   |          \outputs[4].outputs            |lisnoc_router_output_90                    |  1443|
2default:defaulth p
x
� 
�
%s
*synth2z
f|134   |            output_arbiter               |lisnoc_router_output_arbiter_91            |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|135   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_92                   |   201|
2default:defaulth p
x
� 
�
%s
*synth2z
f|136   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_93             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|137   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_94                   |   201|
2default:defaulth p
x
� 
�
%s
*synth2z
f|138   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_95             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|139   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_96                   |   201|
2default:defaulth p
x
� 
�
%s
*synth2z
f|140   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_97             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|141   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_98                   |    99|
2default:defaulth p
x
� 
�
%s
*synth2z
f|142   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_99             |   252|
2default:defaulth p
x
� 
�
%s
*synth2z
f|143   |      u_router_0_1                       |lisnoc_router_2dgrid__parameterized0       |  8471|
2default:defaulth p
x
� 
�
%s
*synth2z
f|144   |        u_router                         |lisnoc_router__parameterized0              |  7829|
2default:defaulth p
x
� 
�
%s
*synth2z
f|145   |          \inputs[2].inputs              |lisnoc_router_input__parameterized2        |  1324|
2default:defaulth p
x
� 
�
%s
*synth2z
f|146   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized16               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|147   |            \vchannel[0].route           |lisnoc_router_input_route__parameterized8  |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|148   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized17               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|149   |            \vchannel[1].route           |lisnoc_router_input_route__parameterized9  |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|150   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized18               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|151   |            \vchannel[2].route           |lisnoc_router_input_route__parameterized10 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|152   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized19               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|153   |            \vchannel[3].route           |lisnoc_router_input_route__parameterized11 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|154   |          \inputs[3].inputs              |lisnoc_router_input__parameterized3        |  1324|
2default:defaulth p
x
� 
�
%s
*synth2z
f|155   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized20               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|156   |            \vchannel[0].route           |lisnoc_router_input_route__parameterized12 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|157   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized21               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|158   |            \vchannel[1].route           |lisnoc_router_input_route__parameterized13 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|159   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized22               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|160   |            \vchannel[2].route           |lisnoc_router_input_route__parameterized14 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|161   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized23               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|162   |            \vchannel[3].route           |lisnoc_router_input_route__parameterized15 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|163   |          \inputs[4].inputs              |lisnoc_router_input__parameterized4        |  1323|
2default:defaulth p
x
� 
�
%s
*synth2z
f|164   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized24               |   293|
2default:defaulth p
x
� 
�
%s
*synth2z
f|165   |            \vchannel[0].route           |lisnoc_router_input_route__parameterized16 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|166   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized25               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|167   |            \vchannel[1].route           |lisnoc_router_input_route__parameterized17 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|168   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized26               |   290|
2default:defaulth p
x
� 
�
%s
*synth2z
f|169   |            \vchannel[2].route           |lisnoc_router_input_route__parameterized18 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|170   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized27               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|171   |            \vchannel[3].route           |lisnoc_router_input_route__parameterized19 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|172   |          \outputs[2].outputs            |lisnoc_router_output_56                    |  1295|
2default:defaulth p
x
� 
�
%s
*synth2z
f|173   |            output_arbiter               |lisnoc_router_output_arbiter_77            |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|174   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_78                   |   164|
2default:defaulth p
x
� 
�
%s
*synth2z
f|175   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_79             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|176   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_80                   |   164|
2default:defaulth p
x
� 
�
%s
*synth2z
f|177   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_81             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|178   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_82                   |   164|
2default:defaulth p
x
� 
�
%s
*synth2z
f|179   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_83             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|180   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_84                   |    62|
2default:defaulth p
x
� 
�
%s
*synth2z
f|181   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_85             |   252|
2default:defaulth p
x
� 
�
%s
*synth2z
f|182   |          \outputs[3].outputs            |lisnoc_router_output_57                    |  1279|
2default:defaulth p
x
� 
�
%s
*synth2z
f|183   |            output_arbiter               |lisnoc_router_output_arbiter_68            |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|184   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_69                   |   160|
2default:defaulth p
x
� 
�
%s
*synth2z
f|185   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_70             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|186   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_71                   |   160|
2default:defaulth p
x
� 
�
%s
*synth2z
f|187   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_72             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|188   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_73                   |   160|
2default:defaulth p
x
� 
�
%s
*synth2z
f|189   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_74             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|190   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_75                   |    58|
2default:defaulth p
x
� 
�
%s
*synth2z
f|191   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_76             |   252|
2default:defaulth p
x
� 
�
%s
*synth2z
f|192   |          \outputs[4].outputs            |lisnoc_router_output_58                    |  1284|
2default:defaulth p
x
� 
�
%s
*synth2z
f|193   |            output_arbiter               |lisnoc_router_output_arbiter_59            |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|194   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_60                   |   161|
2default:defaulth p
x
� 
�
%s
*synth2z
f|195   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_61             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|196   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_62                   |   161|
2default:defaulth p
x
� 
�
%s
*synth2z
f|197   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_63             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|198   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_64                   |   161|
2default:defaulth p
x
� 
�
%s
*synth2z
f|199   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_65             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|200   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_66                   |    59|
2default:defaulth p
x
� 
�
%s
*synth2z
f|201   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_67             |   253|
2default:defaulth p
x
� 
�
%s
*synth2z
f|202   |      u_router_1_0                       |lisnoc_router_2dgrid__parameterized1       |  8414|
2default:defaulth p
x
� 
�
%s
*synth2z
f|203   |        u_router                         |lisnoc_router__parameterized1              |  8184|
2default:defaulth p
x
� 
�
%s
*synth2z
f|204   |          \inputs[0].inputs              |lisnoc_router_input__parameterized5        |  1324|
2default:defaulth p
x
� 
�
%s
*synth2z
f|205   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized28               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|206   |            \vchannel[0].route           |lisnoc_router_input_route__parameterized20 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|207   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized29               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|208   |            \vchannel[1].route           |lisnoc_router_input_route__parameterized21 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|209   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized30               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|210   |            \vchannel[2].route           |lisnoc_router_input_route__parameterized22 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|211   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized31               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|212   |            \vchannel[3].route           |lisnoc_router_input_route__parameterized23 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|213   |          \inputs[1].inputs              |lisnoc_router_input__parameterized6        |  1324|
2default:defaulth p
x
� 
�
%s
*synth2z
f|214   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized32               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|215   |            \vchannel[0].route           |lisnoc_router_input_route__parameterized24 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|216   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized33               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|217   |            \vchannel[1].route           |lisnoc_router_input_route__parameterized25 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|218   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized34               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|219   |            \vchannel[2].route           |lisnoc_router_input_route__parameterized26 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|220   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized35               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|221   |            \vchannel[3].route           |lisnoc_router_input_route__parameterized27 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|222   |          \inputs[4].inputs              |lisnoc_router_input__parameterized9        |  1323|
2default:defaulth p
x
� 
�
%s
*synth2z
f|223   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized44               |   293|
2default:defaulth p
x
� 
�
%s
*synth2z
f|224   |            \vchannel[0].route           |lisnoc_router_input_route__parameterized36 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|225   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized45               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|226   |            \vchannel[1].route           |lisnoc_router_input_route__parameterized37 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|227   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized46               |   290|
2default:defaulth p
x
� 
�
%s
*synth2z
f|228   |            \vchannel[2].route           |lisnoc_router_input_route__parameterized38 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|229   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized47               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|230   |            \vchannel[3].route           |lisnoc_router_input_route__parameterized39 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|231   |          \outputs[0].outputs            |lisnoc_router_output_26                    |  1403|
2default:defaulth p
x
� 
�
%s
*synth2z
f|232   |            output_arbiter               |lisnoc_router_output_arbiter_47            |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|233   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_48                   |   191|
2default:defaulth p
x
� 
�
%s
*synth2z
f|234   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_49             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|235   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_50                   |   191|
2default:defaulth p
x
� 
�
%s
*synth2z
f|236   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_51             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|237   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_52                   |   191|
2default:defaulth p
x
� 
�
%s
*synth2z
f|238   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_53             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|239   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_54                   |    89|
2default:defaulth p
x
� 
�
%s
*synth2z
f|240   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_55             |   252|
2default:defaulth p
x
� 
�
%s
*synth2z
f|241   |          \outputs[1].outputs            |lisnoc_router_output_27                    |  1399|
2default:defaulth p
x
� 
�
%s
*synth2z
f|242   |            output_arbiter               |lisnoc_router_output_arbiter_38            |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|243   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_39                   |   190|
2default:defaulth p
x
� 
�
%s
*synth2z
f|244   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_40             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|245   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_41                   |   190|
2default:defaulth p
x
� 
�
%s
*synth2z
f|246   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_42             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|247   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_43                   |   190|
2default:defaulth p
x
� 
�
%s
*synth2z
f|248   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_44             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|249   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_45                   |    88|
2default:defaulth p
x
� 
�
%s
*synth2z
f|250   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_46             |   252|
2default:defaulth p
x
� 
�
%s
*synth2z
f|251   |          \outputs[4].outputs            |lisnoc_router_output_28                    |  1411|
2default:defaulth p
x
� 
�
%s
*synth2z
f|252   |            output_arbiter               |lisnoc_router_output_arbiter_29            |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|253   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_30                   |   193|
2default:defaulth p
x
� 
�
%s
*synth2z
f|254   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_31             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|255   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_32                   |   193|
2default:defaulth p
x
� 
�
%s
*synth2z
f|256   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_33             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|257   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_34                   |   193|
2default:defaulth p
x
� 
�
%s
*synth2z
f|258   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_35             |   147|
2default:defaulth p
x
� 
�
%s
*synth2z
f|259   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_36                   |    91|
2default:defaulth p
x
� 
�
%s
*synth2z
f|260   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_37             |   252|
2default:defaulth p
x
� 
�
%s
*synth2z
f|261   |      u_router_1_1                       |lisnoc_router_2dgrid__parameterized2       |  8478|
2default:defaulth p
x
� 
�
%s
*synth2z
f|262   |        u_router                         |lisnoc_router__parameterized2              |  8256|
2default:defaulth p
x
� 
�
%s
*synth2z
f|263   |          \inputs[0].inputs              |lisnoc_router_input__parameterized10       |  1324|
2default:defaulth p
x
� 
�
%s
*synth2z
f|264   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized48               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|265   |            \vchannel[0].route           |lisnoc_router_input_route__parameterized40 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|266   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized49               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|267   |            \vchannel[1].route           |lisnoc_router_input_route__parameterized41 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|268   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized50               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|269   |            \vchannel[2].route           |lisnoc_router_input_route__parameterized42 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|270   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized51               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|271   |            \vchannel[3].route           |lisnoc_router_input_route__parameterized43 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|272   |          \inputs[3].inputs              |lisnoc_router_input__parameterized13       |  1324|
2default:defaulth p
x
� 
�
%s
*synth2z
f|273   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized60               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|274   |            \vchannel[0].route           |lisnoc_router_input_route__parameterized52 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|275   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized61               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|276   |            \vchannel[1].route           |lisnoc_router_input_route__parameterized53 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|277   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized62               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|278   |            \vchannel[2].route           |lisnoc_router_input_route__parameterized54 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|279   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized63               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|280   |            \vchannel[3].route           |lisnoc_router_input_route__parameterized55 |    43|
2default:defaulth p
x
� 
�
%s
*synth2z
f|281   |          \inputs[4].inputs              |lisnoc_router_input__parameterized14       |  1323|
2default:defaulth p
x
� 
�
%s
*synth2z
f|282   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized64               |   293|
2default:defaulth p
x
� 
�
%s
*synth2z
f|283   |            \vchannel[0].route           |lisnoc_router_input_route__parameterized56 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|284   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized65               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|285   |            \vchannel[1].route           |lisnoc_router_input_route__parameterized57 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|286   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized66               |   290|
2default:defaulth p
x
� 
�
%s
*synth2z
f|287   |            \vchannel[2].route           |lisnoc_router_input_route__parameterized58 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|288   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized67               |   288|
2default:defaulth p
x
� 
�
%s
*synth2z
f|289   |            \vchannel[3].route           |lisnoc_router_input_route__parameterized59 |    41|
2default:defaulth p
x
� 
�
%s
*synth2z
f|290   |          \outputs[0].outputs            |lisnoc_router_output                       |  1435|
2default:defaulth p
x
� 
�
%s
*synth2z
f|291   |            output_arbiter               |lisnoc_router_output_arbiter_17            |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|292   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_18                   |   198|
2default:defaulth p
x
� 
�
%s
*synth2z
f|293   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_19             |   148|
2default:defaulth p
x
� 
�
%s
*synth2z
f|294   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_20                   |   198|
2default:defaulth p
x
� 
�
%s
*synth2z
f|295   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_21             |   148|
2default:defaulth p
x
� 
�
%s
*synth2z
f|296   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_22                   |   198|
2default:defaulth p
x
� 
�
%s
*synth2z
f|297   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_23             |   148|
2default:defaulth p
x
� 
�
%s
*synth2z
f|298   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_24                   |    96|
2default:defaulth p
x
� 
�
%s
*synth2z
f|299   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_25             |   253|
2default:defaulth p
x
� 
�
%s
*synth2z
f|300   |          \outputs[3].outputs            |lisnoc_router_output_3                     |  1423|
2default:defaulth p
x
� 
�
%s
*synth2z
f|301   |            output_arbiter               |lisnoc_router_output_arbiter_8             |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|302   |            \vchannel[0].arbiter         |lisnoc_router_arbiter_9                    |   195|
2default:defaulth p
x
� 
�
%s
*synth2z
f|303   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4_10             |   148|
2default:defaulth p
x
� 
�
%s
*synth2z
f|304   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_11                   |   195|
2default:defaulth p
x
� 
�
%s
*synth2z
f|305   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5_12             |   148|
2default:defaulth p
x
� 
�
%s
*synth2z
f|306   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_13                   |   195|
2default:defaulth p
x
� 
�
%s
*synth2z
f|307   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6_14             |   148|
2default:defaulth p
x
� 
�
%s
*synth2z
f|308   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_15                   |    93|
2default:defaulth p
x
� 
�
%s
*synth2z
f|309   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7_16             |   253|
2default:defaulth p
x
� 
�
%s
*synth2z
f|310   |          \outputs[4].outputs            |lisnoc_router_output_4                     |  1427|
2default:defaulth p
x
� 
�
%s
*synth2z
f|311   |            output_arbiter               |lisnoc_router_output_arbiter               |    44|
2default:defaulth p
x
� 
�
%s
*synth2z
f|312   |            \vchannel[0].arbiter         |lisnoc_router_arbiter                      |   196|
2default:defaulth p
x
� 
�
%s
*synth2z
f|313   |            \vchannel[0].fifo            |lisnoc_fifo__parameterized4                |   148|
2default:defaulth p
x
� 
�
%s
*synth2z
f|314   |            \vchannel[1].arbiter         |lisnoc_router_arbiter_5                    |   196|
2default:defaulth p
x
� 
�
%s
*synth2z
f|315   |            \vchannel[1].fifo            |lisnoc_fifo__parameterized5                |   148|
2default:defaulth p
x
� 
�
%s
*synth2z
f|316   |            \vchannel[2].arbiter         |lisnoc_router_arbiter_6                    |   196|
2default:defaulth p
x
� 
�
%s
*synth2z
f|317   |            \vchannel[2].fifo            |lisnoc_fifo__parameterized6                |   148|
2default:defaulth p
x
� 
�
%s
*synth2z
f|318   |            \vchannel[3].arbiter         |lisnoc_router_arbiter_7                    |    94|
2default:defaulth p
x
� 
�
%s
*synth2z
f|319   |            \vchannel[3].fifo            |lisnoc_fifo__parameterized7                |   253|
2default:defaulth p
x
� 
�
%s
*synth2z
f+------+-----------------------------------------+-------------------------------------------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:01:41 ; elapsed = 00:02:59 . Memory (MB): peak = 1775.777 ; gain = 838.410 ; free physical = 120 ; free virtual = 12910
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
t
%s
*synth2\
HSynthesis finished with 0 errors, 0 critical warnings and 536 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Runtime : Time (s): cpu = 00:01:25 ; elapsed = 00:03:12 . Memory (MB): peak = 1779.684 ; gain = 275.598 ; free physical = 2880 ; free virtual = 15577
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Complete : Time (s): cpu = 00:01:41 ; elapsed = 00:03:27 . Memory (MB): peak = 1779.684 ; gain = 842.316 ; free physical = 2830 ; free virtual = 15533
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
g
-Analyzing %s Unisim elements for replacement
17*netlist2
2402default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
�
!Unisim Transformation Summary:
%s111*project2�
�  A total of 128 instances were transformed.
  RAM32M => RAM32M (RAMD32, RAMD32, RAMD32, RAMD32, RAMD32, RAMD32, RAMS32, RAMS32): 128 instances
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
5652default:default2
2262default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2"
synth_design: 2default:default2
00:01:462default:default2
00:03:362default:default2
1823.8012default:default2
809.9302default:default2
46742default:default2
174652default:defaultZ17-722h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.runs/design_1_NOC_AXI_testmodule_vc_support_0_0_synth_1/design_1_NOC_AXI_testmodule_vc_support_0_0.dcp2default:defaultZ17-1381h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2&
write_checkpoint: 2default:default2
00:00:072default:default2
00:00:062default:default2
1847.8122default:default2
24.0122default:default2
45162default:default2
173452default:defaultZ17-722h px� 
R
Renamed %s cell refs.
330*coretcl2
3182default:defaultZ2-1174h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2�
�/home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.runs/design_1_NOC_AXI_testmodule_vc_support_0_0_synth_1/design_1_NOC_AXI_testmodule_vc_support_0_0.dcp2default:defaultZ17-1381h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2&
write_checkpoint: 2default:default2
00:00:062default:default2
00:00:062default:default2
1847.8122default:default2
0.0002default:default2
43302default:default2
172922default:defaultZ17-722h px� 
�
�report_utilization: Time (s): cpu = 00:00:00.29 ; elapsed = 00:00:00.34 . Memory (MB): peak = 1847.812 ; gain = 0.000 ; free physical = 4322 ; free virtual = 17285
*commonh px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Wed Aug  7 10:22:38 20192default:defaultZ17-206h px� 


End Record