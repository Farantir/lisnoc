-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (lin64) Build 1733598 Wed Dec 14 22:35:42 MST 2016
-- Date        : Wed Aug  7 10:19:49 2019
-- Host        : zynq running 64-bit Ubuntu 14.04.5 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_lisnoc_probe_0_0_sim_netlist.vhdl
-- Design      : design_1_lisnoc_probe_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lisnoc_probe is
  port (
    latency_o : out STD_LOGIC_VECTOR ( 0 to 31 );
    throughput_o : out STD_LOGIC_VECTOR ( 0 to 31 );
    clk : in STD_LOGIC;
    axiwvalid_i : in STD_LOGIC;
    axiwready_i : in STD_LOGIC;
    reset : in STD_LOGIC;
    axi_reset : in STD_LOGIC;
    packet_arived_i : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lisnoc_probe;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lisnoc_probe is
  signal axi_transaction_completet : STD_LOGIC;
  signal counting_latency : STD_LOGIC;
  signal counting_latency3 : STD_LOGIC;
  signal counting_latency_i_1_n_0 : STD_LOGIC;
  signal counting_latency_i_3_n_0 : STD_LOGIC;
  signal \current_latnecy[11]_i_2_n_0\ : STD_LOGIC;
  signal \current_latnecy[11]_i_3_n_0\ : STD_LOGIC;
  signal \current_latnecy[11]_i_4_n_0\ : STD_LOGIC;
  signal \current_latnecy[11]_i_5_n_0\ : STD_LOGIC;
  signal \current_latnecy[15]_i_2_n_0\ : STD_LOGIC;
  signal \current_latnecy[15]_i_3_n_0\ : STD_LOGIC;
  signal \current_latnecy[15]_i_4_n_0\ : STD_LOGIC;
  signal \current_latnecy[15]_i_5_n_0\ : STD_LOGIC;
  signal \current_latnecy[19]_i_2_n_0\ : STD_LOGIC;
  signal \current_latnecy[19]_i_3_n_0\ : STD_LOGIC;
  signal \current_latnecy[19]_i_4_n_0\ : STD_LOGIC;
  signal \current_latnecy[19]_i_5_n_0\ : STD_LOGIC;
  signal \current_latnecy[23]_i_2_n_0\ : STD_LOGIC;
  signal \current_latnecy[23]_i_3_n_0\ : STD_LOGIC;
  signal \current_latnecy[23]_i_4_n_0\ : STD_LOGIC;
  signal \current_latnecy[23]_i_5_n_0\ : STD_LOGIC;
  signal \current_latnecy[27]_i_2_n_0\ : STD_LOGIC;
  signal \current_latnecy[27]_i_3_n_0\ : STD_LOGIC;
  signal \current_latnecy[27]_i_4_n_0\ : STD_LOGIC;
  signal \current_latnecy[27]_i_5_n_0\ : STD_LOGIC;
  signal \current_latnecy[31]_i_2_n_0\ : STD_LOGIC;
  signal \current_latnecy[31]_i_3_n_0\ : STD_LOGIC;
  signal \current_latnecy[31]_i_4_n_0\ : STD_LOGIC;
  signal \current_latnecy[3]_i_2_n_0\ : STD_LOGIC;
  signal \current_latnecy[3]_i_3_n_0\ : STD_LOGIC;
  signal \current_latnecy[3]_i_4_n_0\ : STD_LOGIC;
  signal \current_latnecy[3]_i_5_n_0\ : STD_LOGIC;
  signal \current_latnecy[7]_i_2_n_0\ : STD_LOGIC;
  signal \current_latnecy[7]_i_3_n_0\ : STD_LOGIC;
  signal \current_latnecy[7]_i_4_n_0\ : STD_LOGIC;
  signal \current_latnecy[7]_i_5_n_0\ : STD_LOGIC;
  signal current_latnecy_reg : STD_LOGIC_VECTOR ( 0 to 31 );
  signal \current_latnecy_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \current_latnecy_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \current_latnecy_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \current_latnecy_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \current_latnecy_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \current_latnecy_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \current_latnecy_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \current_latnecy_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \current_latnecy_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \current_latnecy_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \current_latnecy_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \current_latnecy_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \current_latnecy_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \current_latnecy_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \current_latnecy_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \current_latnecy_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \current_latnecy_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \current_latnecy_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \current_latnecy_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \current_latnecy_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \current_latnecy_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \current_latnecy_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \current_latnecy_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \current_latnecy_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \current_latnecy_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \current_latnecy_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \current_latnecy_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \current_latnecy_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \current_latnecy_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \current_latnecy_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \current_latnecy_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \current_latnecy_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \current_latnecy_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \current_latnecy_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \current_latnecy_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \current_latnecy_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \current_latnecy_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \current_latnecy_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \current_latnecy_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \current_latnecy_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \current_latnecy_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \current_latnecy_reg[31]_i_1_n_1\ : STD_LOGIC;
  signal \current_latnecy_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \current_latnecy_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \current_latnecy_reg[31]_i_1_n_4\ : STD_LOGIC;
  signal \current_latnecy_reg[31]_i_1_n_5\ : STD_LOGIC;
  signal \current_latnecy_reg[31]_i_1_n_6\ : STD_LOGIC;
  signal \current_latnecy_reg[31]_i_1_n_7\ : STD_LOGIC;
  signal \current_latnecy_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \current_latnecy_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \current_latnecy_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \current_latnecy_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \current_latnecy_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \current_latnecy_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \current_latnecy_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \current_latnecy_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \current_latnecy_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \current_latnecy_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \current_latnecy_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \current_latnecy_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \current_latnecy_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \current_latnecy_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \current_latnecy_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal current_troughput : STD_LOGIC_VECTOR ( 0 to 31 );
  signal \current_troughput[11]_i_2_n_0\ : STD_LOGIC;
  signal \current_troughput[11]_i_3_n_0\ : STD_LOGIC;
  signal \current_troughput[11]_i_4_n_0\ : STD_LOGIC;
  signal \current_troughput[11]_i_5_n_0\ : STD_LOGIC;
  signal \current_troughput[15]_i_2_n_0\ : STD_LOGIC;
  signal \current_troughput[15]_i_3_n_0\ : STD_LOGIC;
  signal \current_troughput[15]_i_4_n_0\ : STD_LOGIC;
  signal \current_troughput[15]_i_5_n_0\ : STD_LOGIC;
  signal \current_troughput[19]_i_2_n_0\ : STD_LOGIC;
  signal \current_troughput[19]_i_3_n_0\ : STD_LOGIC;
  signal \current_troughput[19]_i_4_n_0\ : STD_LOGIC;
  signal \current_troughput[19]_i_5_n_0\ : STD_LOGIC;
  signal \current_troughput[23]_i_2_n_0\ : STD_LOGIC;
  signal \current_troughput[23]_i_3_n_0\ : STD_LOGIC;
  signal \current_troughput[23]_i_4_n_0\ : STD_LOGIC;
  signal \current_troughput[23]_i_5_n_0\ : STD_LOGIC;
  signal \current_troughput[27]_i_2_n_0\ : STD_LOGIC;
  signal \current_troughput[27]_i_3_n_0\ : STD_LOGIC;
  signal \current_troughput[27]_i_4_n_0\ : STD_LOGIC;
  signal \current_troughput[27]_i_5_n_0\ : STD_LOGIC;
  signal \current_troughput[31]_i_2_n_0\ : STD_LOGIC;
  signal \current_troughput[31]_i_3_n_0\ : STD_LOGIC;
  signal \current_troughput[31]_i_4_n_0\ : STD_LOGIC;
  signal \current_troughput[31]_i_5_n_0\ : STD_LOGIC;
  signal \current_troughput[3]_i_2_n_0\ : STD_LOGIC;
  signal \current_troughput[3]_i_3_n_0\ : STD_LOGIC;
  signal \current_troughput[3]_i_4_n_0\ : STD_LOGIC;
  signal \current_troughput[3]_i_5_n_0\ : STD_LOGIC;
  signal \current_troughput[7]_i_2_n_0\ : STD_LOGIC;
  signal \current_troughput[7]_i_3_n_0\ : STD_LOGIC;
  signal \current_troughput[7]_i_4_n_0\ : STD_LOGIC;
  signal \current_troughput[7]_i_5_n_0\ : STD_LOGIC;
  signal current_troughput_reg : STD_LOGIC_VECTOR ( 0 to 31 );
  signal \current_troughput_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \current_troughput_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \current_troughput_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \current_troughput_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \current_troughput_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \current_troughput_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \current_troughput_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \current_troughput_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \current_troughput_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \current_troughput_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \current_troughput_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \current_troughput_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \current_troughput_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \current_troughput_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \current_troughput_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \current_troughput_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \current_troughput_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \current_troughput_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \current_troughput_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \current_troughput_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \current_troughput_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \current_troughput_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \current_troughput_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \current_troughput_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \current_troughput_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \current_troughput_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \current_troughput_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \current_troughput_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \current_troughput_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \current_troughput_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \current_troughput_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \current_troughput_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \current_troughput_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \current_troughput_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \current_troughput_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \current_troughput_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \current_troughput_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \current_troughput_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \current_troughput_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \current_troughput_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \current_troughput_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \current_troughput_reg[31]_i_1_n_1\ : STD_LOGIC;
  signal \current_troughput_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \current_troughput_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \current_troughput_reg[31]_i_1_n_4\ : STD_LOGIC;
  signal \current_troughput_reg[31]_i_1_n_5\ : STD_LOGIC;
  signal \current_troughput_reg[31]_i_1_n_6\ : STD_LOGIC;
  signal \current_troughput_reg[31]_i_1_n_7\ : STD_LOGIC;
  signal \current_troughput_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \current_troughput_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \current_troughput_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \current_troughput_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \current_troughput_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \current_troughput_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \current_troughput_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \current_troughput_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \current_troughput_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \current_troughput_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \current_troughput_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \current_troughput_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \current_troughput_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \current_troughput_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \current_troughput_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal latency_measured : STD_LOGIC;
  signal latency_measured_reg_C_n_0 : STD_LOGIC;
  signal latency_measured_reg_LDC_n_0 : STD_LOGIC;
  signal latency_measured_reg_P_n_0 : STD_LOGIC;
  signal \^latency_o\ : STD_LOGIC_VECTOR ( 0 to 31 );
  signal latency_o2 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \latency_o_reg[0]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[0]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[0]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[0]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[0]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[10]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[10]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[10]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[10]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[10]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[11]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[11]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[11]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[11]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[11]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[12]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[12]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[12]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[12]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[12]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[13]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[13]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[13]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[13]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[13]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[14]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[14]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[14]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[14]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[14]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[15]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[15]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[15]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[15]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[15]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[16]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[16]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[16]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[16]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[16]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[17]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[17]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[17]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[17]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[17]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[18]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[18]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[18]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[18]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[18]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[19]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[19]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[19]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[19]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[19]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[1]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[1]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[1]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[1]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[1]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[20]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[20]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[20]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[20]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[20]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[21]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[21]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[21]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[21]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[21]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[22]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[22]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[22]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[22]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[22]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[23]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[23]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[23]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[23]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[23]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[24]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[24]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[24]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[24]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[24]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[25]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[25]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[25]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[25]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[25]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[26]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[26]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[26]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[26]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[26]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[27]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[27]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[27]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[27]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[27]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[28]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[28]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[28]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[28]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[28]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[29]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[29]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[29]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[29]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[29]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[2]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[2]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[2]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[2]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[2]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[30]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[30]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[30]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[30]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[30]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[31]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[31]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[31]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[31]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[31]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[3]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[3]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[3]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[3]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[3]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[4]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[4]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[4]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[4]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[4]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[5]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[5]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[5]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[5]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[5]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[6]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[6]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[6]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[6]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[6]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[7]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[7]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[7]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[7]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[7]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[8]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[8]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[8]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[8]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[8]_P_n_0\ : STD_LOGIC;
  signal \latency_o_reg[9]_C_n_0\ : STD_LOGIC;
  signal \latency_o_reg[9]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \latency_o_reg[9]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \latency_o_reg[9]_LDC_n_0\ : STD_LOGIC;
  signal \latency_o_reg[9]_P_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 31 downto 2 );
  signal p_3_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal packet_arived_last : STD_LOGIC;
  signal packet_arived_last_i_1_n_0 : STD_LOGIC;
  signal rst : STD_LOGIC;
  signal \throughput_o[0]_i_12_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_13_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_17_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_18_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_19_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_1_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_20_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_23_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_24_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_25_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_26_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_27_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_28_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_29_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_30_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_32_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_33_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_34_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_35_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_36_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_37_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_38_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_39_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_40_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_41_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_42_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_43_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_44_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_45_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_46_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_47_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_48_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_49_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_50_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_51_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_52_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_53_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_54_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_6_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_8_n_0\ : STD_LOGIC;
  signal \throughput_o[0]_i_9_n_0\ : STD_LOGIC;
  signal \throughput_o[12]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_o[12]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_o[12]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_o[12]_i_6_n_0\ : STD_LOGIC;
  signal \throughput_o[16]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_o[16]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_o[16]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_o[16]_i_6_n_0\ : STD_LOGIC;
  signal \throughput_o[20]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_o[20]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_o[20]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_o[20]_i_6_n_0\ : STD_LOGIC;
  signal \throughput_o[24]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_o[24]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_o[24]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_o[24]_i_6_n_0\ : STD_LOGIC;
  signal \throughput_o[28]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_o[28]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_o[28]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_o[28]_i_6_n_0\ : STD_LOGIC;
  signal \throughput_o[4]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_o[4]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_o[4]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_o[4]_i_6_n_0\ : STD_LOGIC;
  signal \throughput_o[8]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_o[8]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_o[8]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_o[8]_i_6_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_10_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_10_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_10_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_10_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_11_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_11_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_11_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_11_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_14_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_14_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_15_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_15_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_15_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_15_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_16_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_16_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_16_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_16_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_21_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_21_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_21_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_21_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_22_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_22_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_22_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_22_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_31_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_31_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_31_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_31_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_7_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_7_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[0]_i_7_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[12]_i_2_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[12]_i_2_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[12]_i_2_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[16]_i_2_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[16]_i_2_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[16]_i_2_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[20]_i_2_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[20]_i_2_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[20]_i_2_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[24]_i_2_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[24]_i_2_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[24]_i_2_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[28]_i_2_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[28]_i_2_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[28]_i_2_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal \throughput_o_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_o_reg[8]_i_2_n_1\ : STD_LOGIC;
  signal \throughput_o_reg[8]_i_2_n_2\ : STD_LOGIC;
  signal \throughput_o_reg[8]_i_2_n_3\ : STD_LOGIC;
  signal throughput_timer : STD_LOGIC;
  signal \throughput_timer[11]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_timer[11]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_timer[11]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_timer[11]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_timer[15]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_timer[15]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_timer[15]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_timer[15]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_timer[19]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_timer[19]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_timer[19]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_timer[19]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_timer[23]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_timer[23]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_timer[23]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_timer[23]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_timer[27]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_timer[27]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_timer[27]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_timer[27]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_timer[31]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_timer[31]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_timer[31]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_timer[31]_i_6_n_0\ : STD_LOGIC;
  signal \throughput_timer[3]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_timer[3]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_timer[3]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_timer[3]_i_5_n_0\ : STD_LOGIC;
  signal \throughput_timer[7]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_timer[7]_i_3_n_0\ : STD_LOGIC;
  signal \throughput_timer[7]_i_4_n_0\ : STD_LOGIC;
  signal \throughput_timer[7]_i_5_n_0\ : STD_LOGIC;
  signal throughput_timer_reg : STD_LOGIC_VECTOR ( 0 to 31 );
  signal \throughput_timer_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \throughput_timer_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \throughput_timer_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \throughput_timer_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \throughput_timer_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \throughput_timer_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \throughput_timer_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \throughput_timer_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \throughput_timer_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \throughput_timer_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \throughput_timer_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \throughput_timer_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \throughput_timer_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \throughput_timer_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \throughput_timer_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \throughput_timer_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \throughput_timer_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \throughput_timer_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \throughput_timer_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \throughput_timer_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \throughput_timer_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \throughput_timer_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \throughput_timer_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \throughput_timer_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \throughput_timer_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \throughput_timer_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \throughput_timer_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \throughput_timer_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \throughput_timer_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \throughput_timer_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \throughput_timer_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \throughput_timer_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \throughput_timer_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \throughput_timer_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \throughput_timer_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \throughput_timer_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \throughput_timer_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \throughput_timer_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \throughput_timer_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \throughput_timer_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \throughput_timer_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \throughput_timer_reg[31]_i_2_n_1\ : STD_LOGIC;
  signal \throughput_timer_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \throughput_timer_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \throughput_timer_reg[31]_i_2_n_4\ : STD_LOGIC;
  signal \throughput_timer_reg[31]_i_2_n_5\ : STD_LOGIC;
  signal \throughput_timer_reg[31]_i_2_n_6\ : STD_LOGIC;
  signal \throughput_timer_reg[31]_i_2_n_7\ : STD_LOGIC;
  signal \throughput_timer_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \throughput_timer_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \throughput_timer_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \throughput_timer_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \throughput_timer_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \throughput_timer_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \throughput_timer_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \throughput_timer_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \throughput_timer_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \throughput_timer_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \throughput_timer_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \throughput_timer_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \throughput_timer_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \throughput_timer_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \throughput_timer_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \troughput_state[0]_C_i_1_n_0\ : STD_LOGIC;
  signal \troughput_state[0]_C_i_2_n_0\ : STD_LOGIC;
  signal \troughput_state[0]_C_i_3_n_0\ : STD_LOGIC;
  signal \troughput_state[1]_C_i_1_n_0\ : STD_LOGIC;
  signal \troughput_state[1]_C_i_2_n_0\ : STD_LOGIC;
  signal \troughput_state[1]_C_i_3_n_0\ : STD_LOGIC;
  signal \troughput_state[1]_C_i_4_n_0\ : STD_LOGIC;
  signal \troughput_state[1]_C_i_5_n_0\ : STD_LOGIC;
  signal \troughput_state[1]_C_i_6_n_0\ : STD_LOGIC;
  signal \troughput_state_reg[0]_C_n_0\ : STD_LOGIC;
  signal \troughput_state_reg[1]_C_n_0\ : STD_LOGIC;
  signal \NLW_current_latnecy_reg[3]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_current_troughput_reg[3]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_throughput_o_reg[0]_i_14_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_throughput_o_reg[0]_i_14_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_throughput_o_reg[0]_i_31_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_throughput_o_reg[0]_i_7_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_throughput_timer_reg[3]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of counting_latency_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of latency_measured_C_i_1 : label is "soft_lutpair1";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of latency_measured_reg_LDC : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[0]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[10]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[11]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[12]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[13]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[14]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[15]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[16]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[17]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[18]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[19]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[1]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[20]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[21]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[22]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[23]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[24]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[25]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[26]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[27]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[28]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[29]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[2]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[30]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[31]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[3]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[4]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[5]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[6]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[7]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[8]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \latency_o_reg[9]_LDC\ : label is "LDC";
  attribute SOFT_HLUTNM of \troughput_state[0]_C_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \troughput_state[1]_C_i_4\ : label is "soft_lutpair0";
begin
  latency_o(0 to 31) <= \^latency_o\(0 to 31);
counting_latency_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF47"
    )
        port map (
      I0 => latency_measured_reg_P_n_0,
      I1 => latency_measured_reg_LDC_n_0,
      I2 => latency_measured_reg_C_n_0,
      I3 => counting_latency,
      O => counting_latency_i_1_n_0
    );
counting_latency_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => axiwvalid_i,
      I1 => axiwready_i,
      O => axi_transaction_completet
    );
counting_latency_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF8F"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => axi_reset,
      I3 => reset,
      O => counting_latency_i_3_n_0
    );
counting_latency_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => counting_latency_i_3_n_0,
      D => counting_latency_i_1_n_0,
      Q => counting_latency
    );
\current_latnecy[11]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(8),
      O => \current_latnecy[11]_i_2_n_0\
    );
\current_latnecy[11]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(9),
      O => \current_latnecy[11]_i_3_n_0\
    );
\current_latnecy[11]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(10),
      O => \current_latnecy[11]_i_4_n_0\
    );
\current_latnecy[11]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(11),
      O => \current_latnecy[11]_i_5_n_0\
    );
\current_latnecy[15]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(12),
      O => \current_latnecy[15]_i_2_n_0\
    );
\current_latnecy[15]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(13),
      O => \current_latnecy[15]_i_3_n_0\
    );
\current_latnecy[15]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(14),
      O => \current_latnecy[15]_i_4_n_0\
    );
\current_latnecy[15]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(15),
      O => \current_latnecy[15]_i_5_n_0\
    );
\current_latnecy[19]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(16),
      O => \current_latnecy[19]_i_2_n_0\
    );
\current_latnecy[19]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(17),
      O => \current_latnecy[19]_i_3_n_0\
    );
\current_latnecy[19]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(18),
      O => \current_latnecy[19]_i_4_n_0\
    );
\current_latnecy[19]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(19),
      O => \current_latnecy[19]_i_5_n_0\
    );
\current_latnecy[23]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(20),
      O => \current_latnecy[23]_i_2_n_0\
    );
\current_latnecy[23]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(21),
      O => \current_latnecy[23]_i_3_n_0\
    );
\current_latnecy[23]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(22),
      O => \current_latnecy[23]_i_4_n_0\
    );
\current_latnecy[23]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(23),
      O => \current_latnecy[23]_i_5_n_0\
    );
\current_latnecy[27]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(24),
      O => \current_latnecy[27]_i_2_n_0\
    );
\current_latnecy[27]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(25),
      O => \current_latnecy[27]_i_3_n_0\
    );
\current_latnecy[27]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(26),
      O => \current_latnecy[27]_i_4_n_0\
    );
\current_latnecy[27]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(27),
      O => \current_latnecy[27]_i_5_n_0\
    );
\current_latnecy[31]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(28),
      O => \current_latnecy[31]_i_2_n_0\
    );
\current_latnecy[31]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(29),
      O => \current_latnecy[31]_i_3_n_0\
    );
\current_latnecy[31]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(30),
      O => \current_latnecy[31]_i_4_n_0\
    );
\current_latnecy[31]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => current_latnecy_reg(31),
      O => latency_o2(0)
    );
\current_latnecy[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(0),
      O => \current_latnecy[3]_i_2_n_0\
    );
\current_latnecy[3]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(1),
      O => \current_latnecy[3]_i_3_n_0\
    );
\current_latnecy[3]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(2),
      O => \current_latnecy[3]_i_4_n_0\
    );
\current_latnecy[3]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(3),
      O => \current_latnecy[3]_i_5_n_0\
    );
\current_latnecy[7]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(4),
      O => \current_latnecy[7]_i_2_n_0\
    );
\current_latnecy[7]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(5),
      O => \current_latnecy[7]_i_3_n_0\
    );
\current_latnecy[7]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(6),
      O => \current_latnecy[7]_i_4_n_0\
    );
\current_latnecy[7]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_latnecy_reg(7),
      O => \current_latnecy[7]_i_5_n_0\
    );
\current_latnecy_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[3]_i_1_n_4\,
      Q => current_latnecy_reg(0)
    );
\current_latnecy_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[11]_i_1_n_6\,
      Q => current_latnecy_reg(10)
    );
\current_latnecy_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[11]_i_1_n_7\,
      Q => current_latnecy_reg(11)
    );
\current_latnecy_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_latnecy_reg[15]_i_1_n_0\,
      CO(3) => \current_latnecy_reg[11]_i_1_n_0\,
      CO(2) => \current_latnecy_reg[11]_i_1_n_1\,
      CO(1) => \current_latnecy_reg[11]_i_1_n_2\,
      CO(0) => \current_latnecy_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_latnecy_reg[11]_i_1_n_4\,
      O(2) => \current_latnecy_reg[11]_i_1_n_5\,
      O(1) => \current_latnecy_reg[11]_i_1_n_6\,
      O(0) => \current_latnecy_reg[11]_i_1_n_7\,
      S(3) => \current_latnecy[11]_i_2_n_0\,
      S(2) => \current_latnecy[11]_i_3_n_0\,
      S(1) => \current_latnecy[11]_i_4_n_0\,
      S(0) => \current_latnecy[11]_i_5_n_0\
    );
\current_latnecy_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[15]_i_1_n_4\,
      Q => current_latnecy_reg(12)
    );
\current_latnecy_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[15]_i_1_n_5\,
      Q => current_latnecy_reg(13)
    );
\current_latnecy_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[15]_i_1_n_6\,
      Q => current_latnecy_reg(14)
    );
\current_latnecy_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[15]_i_1_n_7\,
      Q => current_latnecy_reg(15)
    );
\current_latnecy_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_latnecy_reg[19]_i_1_n_0\,
      CO(3) => \current_latnecy_reg[15]_i_1_n_0\,
      CO(2) => \current_latnecy_reg[15]_i_1_n_1\,
      CO(1) => \current_latnecy_reg[15]_i_1_n_2\,
      CO(0) => \current_latnecy_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_latnecy_reg[15]_i_1_n_4\,
      O(2) => \current_latnecy_reg[15]_i_1_n_5\,
      O(1) => \current_latnecy_reg[15]_i_1_n_6\,
      O(0) => \current_latnecy_reg[15]_i_1_n_7\,
      S(3) => \current_latnecy[15]_i_2_n_0\,
      S(2) => \current_latnecy[15]_i_3_n_0\,
      S(1) => \current_latnecy[15]_i_4_n_0\,
      S(0) => \current_latnecy[15]_i_5_n_0\
    );
\current_latnecy_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[19]_i_1_n_4\,
      Q => current_latnecy_reg(16)
    );
\current_latnecy_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[19]_i_1_n_5\,
      Q => current_latnecy_reg(17)
    );
\current_latnecy_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[19]_i_1_n_6\,
      Q => current_latnecy_reg(18)
    );
\current_latnecy_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[19]_i_1_n_7\,
      Q => current_latnecy_reg(19)
    );
\current_latnecy_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_latnecy_reg[23]_i_1_n_0\,
      CO(3) => \current_latnecy_reg[19]_i_1_n_0\,
      CO(2) => \current_latnecy_reg[19]_i_1_n_1\,
      CO(1) => \current_latnecy_reg[19]_i_1_n_2\,
      CO(0) => \current_latnecy_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_latnecy_reg[19]_i_1_n_4\,
      O(2) => \current_latnecy_reg[19]_i_1_n_5\,
      O(1) => \current_latnecy_reg[19]_i_1_n_6\,
      O(0) => \current_latnecy_reg[19]_i_1_n_7\,
      S(3) => \current_latnecy[19]_i_2_n_0\,
      S(2) => \current_latnecy[19]_i_3_n_0\,
      S(1) => \current_latnecy[19]_i_4_n_0\,
      S(0) => \current_latnecy[19]_i_5_n_0\
    );
\current_latnecy_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[3]_i_1_n_5\,
      Q => current_latnecy_reg(1)
    );
\current_latnecy_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[23]_i_1_n_4\,
      Q => current_latnecy_reg(20)
    );
\current_latnecy_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[23]_i_1_n_5\,
      Q => current_latnecy_reg(21)
    );
\current_latnecy_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[23]_i_1_n_6\,
      Q => current_latnecy_reg(22)
    );
\current_latnecy_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[23]_i_1_n_7\,
      Q => current_latnecy_reg(23)
    );
\current_latnecy_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_latnecy_reg[27]_i_1_n_0\,
      CO(3) => \current_latnecy_reg[23]_i_1_n_0\,
      CO(2) => \current_latnecy_reg[23]_i_1_n_1\,
      CO(1) => \current_latnecy_reg[23]_i_1_n_2\,
      CO(0) => \current_latnecy_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_latnecy_reg[23]_i_1_n_4\,
      O(2) => \current_latnecy_reg[23]_i_1_n_5\,
      O(1) => \current_latnecy_reg[23]_i_1_n_6\,
      O(0) => \current_latnecy_reg[23]_i_1_n_7\,
      S(3) => \current_latnecy[23]_i_2_n_0\,
      S(2) => \current_latnecy[23]_i_3_n_0\,
      S(1) => \current_latnecy[23]_i_4_n_0\,
      S(0) => \current_latnecy[23]_i_5_n_0\
    );
\current_latnecy_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[27]_i_1_n_4\,
      Q => current_latnecy_reg(24)
    );
\current_latnecy_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[27]_i_1_n_5\,
      Q => current_latnecy_reg(25)
    );
\current_latnecy_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[27]_i_1_n_6\,
      Q => current_latnecy_reg(26)
    );
\current_latnecy_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[27]_i_1_n_7\,
      Q => current_latnecy_reg(27)
    );
\current_latnecy_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_latnecy_reg[31]_i_1_n_0\,
      CO(3) => \current_latnecy_reg[27]_i_1_n_0\,
      CO(2) => \current_latnecy_reg[27]_i_1_n_1\,
      CO(1) => \current_latnecy_reg[27]_i_1_n_2\,
      CO(0) => \current_latnecy_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_latnecy_reg[27]_i_1_n_4\,
      O(2) => \current_latnecy_reg[27]_i_1_n_5\,
      O(1) => \current_latnecy_reg[27]_i_1_n_6\,
      O(0) => \current_latnecy_reg[27]_i_1_n_7\,
      S(3) => \current_latnecy[27]_i_2_n_0\,
      S(2) => \current_latnecy[27]_i_3_n_0\,
      S(1) => \current_latnecy[27]_i_4_n_0\,
      S(0) => \current_latnecy[27]_i_5_n_0\
    );
\current_latnecy_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[31]_i_1_n_4\,
      Q => current_latnecy_reg(28)
    );
\current_latnecy_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[31]_i_1_n_5\,
      Q => current_latnecy_reg(29)
    );
\current_latnecy_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[3]_i_1_n_6\,
      Q => current_latnecy_reg(2)
    );
\current_latnecy_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[31]_i_1_n_6\,
      Q => current_latnecy_reg(30)
    );
\current_latnecy_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[31]_i_1_n_7\,
      Q => current_latnecy_reg(31)
    );
\current_latnecy_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \current_latnecy_reg[31]_i_1_n_0\,
      CO(2) => \current_latnecy_reg[31]_i_1_n_1\,
      CO(1) => \current_latnecy_reg[31]_i_1_n_2\,
      CO(0) => \current_latnecy_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \current_latnecy_reg[31]_i_1_n_4\,
      O(2) => \current_latnecy_reg[31]_i_1_n_5\,
      O(1) => \current_latnecy_reg[31]_i_1_n_6\,
      O(0) => \current_latnecy_reg[31]_i_1_n_7\,
      S(3) => \current_latnecy[31]_i_2_n_0\,
      S(2) => \current_latnecy[31]_i_3_n_0\,
      S(1) => \current_latnecy[31]_i_4_n_0\,
      S(0) => latency_o2(0)
    );
\current_latnecy_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[3]_i_1_n_7\,
      Q => current_latnecy_reg(3)
    );
\current_latnecy_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_latnecy_reg[7]_i_1_n_0\,
      CO(3) => \NLW_current_latnecy_reg[3]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \current_latnecy_reg[3]_i_1_n_1\,
      CO(1) => \current_latnecy_reg[3]_i_1_n_2\,
      CO(0) => \current_latnecy_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_latnecy_reg[3]_i_1_n_4\,
      O(2) => \current_latnecy_reg[3]_i_1_n_5\,
      O(1) => \current_latnecy_reg[3]_i_1_n_6\,
      O(0) => \current_latnecy_reg[3]_i_1_n_7\,
      S(3) => \current_latnecy[3]_i_2_n_0\,
      S(2) => \current_latnecy[3]_i_3_n_0\,
      S(1) => \current_latnecy[3]_i_4_n_0\,
      S(0) => \current_latnecy[3]_i_5_n_0\
    );
\current_latnecy_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[7]_i_1_n_4\,
      Q => current_latnecy_reg(4)
    );
\current_latnecy_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[7]_i_1_n_5\,
      Q => current_latnecy_reg(5)
    );
\current_latnecy_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[7]_i_1_n_6\,
      Q => current_latnecy_reg(6)
    );
\current_latnecy_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[7]_i_1_n_7\,
      Q => current_latnecy_reg(7)
    );
\current_latnecy_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_latnecy_reg[11]_i_1_n_0\,
      CO(3) => \current_latnecy_reg[7]_i_1_n_0\,
      CO(2) => \current_latnecy_reg[7]_i_1_n_1\,
      CO(1) => \current_latnecy_reg[7]_i_1_n_2\,
      CO(0) => \current_latnecy_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_latnecy_reg[7]_i_1_n_4\,
      O(2) => \current_latnecy_reg[7]_i_1_n_5\,
      O(1) => \current_latnecy_reg[7]_i_1_n_6\,
      O(0) => \current_latnecy_reg[7]_i_1_n_7\,
      S(3) => \current_latnecy[7]_i_2_n_0\,
      S(2) => \current_latnecy[7]_i_3_n_0\,
      S(1) => \current_latnecy[7]_i_4_n_0\,
      S(0) => \current_latnecy[7]_i_5_n_0\
    );
\current_latnecy_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[11]_i_1_n_4\,
      Q => current_latnecy_reg(8)
    );
\current_latnecy_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => counting_latency,
      CLR => rst,
      D => \current_latnecy_reg[11]_i_1_n_5\,
      Q => current_latnecy_reg(9)
    );
\current_troughput[11]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(8),
      O => \current_troughput[11]_i_2_n_0\
    );
\current_troughput[11]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(9),
      O => \current_troughput[11]_i_3_n_0\
    );
\current_troughput[11]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(10),
      O => \current_troughput[11]_i_4_n_0\
    );
\current_troughput[11]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(11),
      O => \current_troughput[11]_i_5_n_0\
    );
\current_troughput[15]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(12),
      O => \current_troughput[15]_i_2_n_0\
    );
\current_troughput[15]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(13),
      O => \current_troughput[15]_i_3_n_0\
    );
\current_troughput[15]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(14),
      O => \current_troughput[15]_i_4_n_0\
    );
\current_troughput[15]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(15),
      O => \current_troughput[15]_i_5_n_0\
    );
\current_troughput[19]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(16),
      O => \current_troughput[19]_i_2_n_0\
    );
\current_troughput[19]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(17),
      O => \current_troughput[19]_i_3_n_0\
    );
\current_troughput[19]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(18),
      O => \current_troughput[19]_i_4_n_0\
    );
\current_troughput[19]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(19),
      O => \current_troughput[19]_i_5_n_0\
    );
\current_troughput[23]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(20),
      O => \current_troughput[23]_i_2_n_0\
    );
\current_troughput[23]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(21),
      O => \current_troughput[23]_i_3_n_0\
    );
\current_troughput[23]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(22),
      O => \current_troughput[23]_i_4_n_0\
    );
\current_troughput[23]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(23),
      O => \current_troughput[23]_i_5_n_0\
    );
\current_troughput[27]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(24),
      O => \current_troughput[27]_i_2_n_0\
    );
\current_troughput[27]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(25),
      O => \current_troughput[27]_i_3_n_0\
    );
\current_troughput[27]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(26),
      O => \current_troughput[27]_i_4_n_0\
    );
\current_troughput[27]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(27),
      O => \current_troughput[27]_i_5_n_0\
    );
\current_troughput[31]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(28),
      O => \current_troughput[31]_i_2_n_0\
    );
\current_troughput[31]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(29),
      O => \current_troughput[31]_i_3_n_0\
    );
\current_troughput[31]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(30),
      O => \current_troughput[31]_i_4_n_0\
    );
\current_troughput[31]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => packet_arived_last,
      I1 => packet_arived_i,
      I2 => current_troughput_reg(31),
      O => \current_troughput[31]_i_5_n_0\
    );
\current_troughput[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(0),
      O => \current_troughput[3]_i_2_n_0\
    );
\current_troughput[3]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(1),
      O => \current_troughput[3]_i_3_n_0\
    );
\current_troughput[3]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(2),
      O => \current_troughput[3]_i_4_n_0\
    );
\current_troughput[3]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(3),
      O => \current_troughput[3]_i_5_n_0\
    );
\current_troughput[7]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(4),
      O => \current_troughput[7]_i_2_n_0\
    );
\current_troughput[7]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(5),
      O => \current_troughput[7]_i_3_n_0\
    );
\current_troughput[7]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(6),
      O => \current_troughput[7]_i_4_n_0\
    );
\current_troughput[7]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(7),
      O => \current_troughput[7]_i_5_n_0\
    );
\current_troughput_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[3]_i_1_n_4\,
      Q => current_troughput_reg(0)
    );
\current_troughput_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[11]_i_1_n_6\,
      Q => current_troughput_reg(10)
    );
\current_troughput_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[11]_i_1_n_7\,
      Q => current_troughput_reg(11)
    );
\current_troughput_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_troughput_reg[15]_i_1_n_0\,
      CO(3) => \current_troughput_reg[11]_i_1_n_0\,
      CO(2) => \current_troughput_reg[11]_i_1_n_1\,
      CO(1) => \current_troughput_reg[11]_i_1_n_2\,
      CO(0) => \current_troughput_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_troughput_reg[11]_i_1_n_4\,
      O(2) => \current_troughput_reg[11]_i_1_n_5\,
      O(1) => \current_troughput_reg[11]_i_1_n_6\,
      O(0) => \current_troughput_reg[11]_i_1_n_7\,
      S(3) => \current_troughput[11]_i_2_n_0\,
      S(2) => \current_troughput[11]_i_3_n_0\,
      S(1) => \current_troughput[11]_i_4_n_0\,
      S(0) => \current_troughput[11]_i_5_n_0\
    );
\current_troughput_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[15]_i_1_n_4\,
      Q => current_troughput_reg(12)
    );
\current_troughput_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[15]_i_1_n_5\,
      Q => current_troughput_reg(13)
    );
\current_troughput_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[15]_i_1_n_6\,
      Q => current_troughput_reg(14)
    );
\current_troughput_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[15]_i_1_n_7\,
      Q => current_troughput_reg(15)
    );
\current_troughput_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_troughput_reg[19]_i_1_n_0\,
      CO(3) => \current_troughput_reg[15]_i_1_n_0\,
      CO(2) => \current_troughput_reg[15]_i_1_n_1\,
      CO(1) => \current_troughput_reg[15]_i_1_n_2\,
      CO(0) => \current_troughput_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_troughput_reg[15]_i_1_n_4\,
      O(2) => \current_troughput_reg[15]_i_1_n_5\,
      O(1) => \current_troughput_reg[15]_i_1_n_6\,
      O(0) => \current_troughput_reg[15]_i_1_n_7\,
      S(3) => \current_troughput[15]_i_2_n_0\,
      S(2) => \current_troughput[15]_i_3_n_0\,
      S(1) => \current_troughput[15]_i_4_n_0\,
      S(0) => \current_troughput[15]_i_5_n_0\
    );
\current_troughput_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[19]_i_1_n_4\,
      Q => current_troughput_reg(16)
    );
\current_troughput_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[19]_i_1_n_5\,
      Q => current_troughput_reg(17)
    );
\current_troughput_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[19]_i_1_n_6\,
      Q => current_troughput_reg(18)
    );
\current_troughput_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[19]_i_1_n_7\,
      Q => current_troughput_reg(19)
    );
\current_troughput_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_troughput_reg[23]_i_1_n_0\,
      CO(3) => \current_troughput_reg[19]_i_1_n_0\,
      CO(2) => \current_troughput_reg[19]_i_1_n_1\,
      CO(1) => \current_troughput_reg[19]_i_1_n_2\,
      CO(0) => \current_troughput_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_troughput_reg[19]_i_1_n_4\,
      O(2) => \current_troughput_reg[19]_i_1_n_5\,
      O(1) => \current_troughput_reg[19]_i_1_n_6\,
      O(0) => \current_troughput_reg[19]_i_1_n_7\,
      S(3) => \current_troughput[19]_i_2_n_0\,
      S(2) => \current_troughput[19]_i_3_n_0\,
      S(1) => \current_troughput[19]_i_4_n_0\,
      S(0) => \current_troughput[19]_i_5_n_0\
    );
\current_troughput_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[3]_i_1_n_5\,
      Q => current_troughput_reg(1)
    );
\current_troughput_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[23]_i_1_n_4\,
      Q => current_troughput_reg(20)
    );
\current_troughput_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[23]_i_1_n_5\,
      Q => current_troughput_reg(21)
    );
\current_troughput_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[23]_i_1_n_6\,
      Q => current_troughput_reg(22)
    );
\current_troughput_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[23]_i_1_n_7\,
      Q => current_troughput_reg(23)
    );
\current_troughput_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_troughput_reg[27]_i_1_n_0\,
      CO(3) => \current_troughput_reg[23]_i_1_n_0\,
      CO(2) => \current_troughput_reg[23]_i_1_n_1\,
      CO(1) => \current_troughput_reg[23]_i_1_n_2\,
      CO(0) => \current_troughput_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_troughput_reg[23]_i_1_n_4\,
      O(2) => \current_troughput_reg[23]_i_1_n_5\,
      O(1) => \current_troughput_reg[23]_i_1_n_6\,
      O(0) => \current_troughput_reg[23]_i_1_n_7\,
      S(3) => \current_troughput[23]_i_2_n_0\,
      S(2) => \current_troughput[23]_i_3_n_0\,
      S(1) => \current_troughput[23]_i_4_n_0\,
      S(0) => \current_troughput[23]_i_5_n_0\
    );
\current_troughput_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[27]_i_1_n_4\,
      Q => current_troughput_reg(24)
    );
\current_troughput_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[27]_i_1_n_5\,
      Q => current_troughput_reg(25)
    );
\current_troughput_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[27]_i_1_n_6\,
      Q => current_troughput_reg(26)
    );
\current_troughput_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[27]_i_1_n_7\,
      Q => current_troughput_reg(27)
    );
\current_troughput_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_troughput_reg[31]_i_1_n_0\,
      CO(3) => \current_troughput_reg[27]_i_1_n_0\,
      CO(2) => \current_troughput_reg[27]_i_1_n_1\,
      CO(1) => \current_troughput_reg[27]_i_1_n_2\,
      CO(0) => \current_troughput_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_troughput_reg[27]_i_1_n_4\,
      O(2) => \current_troughput_reg[27]_i_1_n_5\,
      O(1) => \current_troughput_reg[27]_i_1_n_6\,
      O(0) => \current_troughput_reg[27]_i_1_n_7\,
      S(3) => \current_troughput[27]_i_2_n_0\,
      S(2) => \current_troughput[27]_i_3_n_0\,
      S(1) => \current_troughput[27]_i_4_n_0\,
      S(0) => \current_troughput[27]_i_5_n_0\
    );
\current_troughput_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[31]_i_1_n_4\,
      Q => current_troughput_reg(28)
    );
\current_troughput_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[31]_i_1_n_5\,
      Q => current_troughput_reg(29)
    );
\current_troughput_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[3]_i_1_n_6\,
      Q => current_troughput_reg(2)
    );
\current_troughput_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[31]_i_1_n_6\,
      Q => current_troughput_reg(30)
    );
\current_troughput_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[31]_i_1_n_7\,
      Q => current_troughput_reg(31)
    );
\current_troughput_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \current_troughput_reg[31]_i_1_n_0\,
      CO(2) => \current_troughput_reg[31]_i_1_n_1\,
      CO(1) => \current_troughput_reg[31]_i_1_n_2\,
      CO(0) => \current_troughput_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => current_troughput_reg(31),
      O(3) => \current_troughput_reg[31]_i_1_n_4\,
      O(2) => \current_troughput_reg[31]_i_1_n_5\,
      O(1) => \current_troughput_reg[31]_i_1_n_6\,
      O(0) => \current_troughput_reg[31]_i_1_n_7\,
      S(3) => \current_troughput[31]_i_2_n_0\,
      S(2) => \current_troughput[31]_i_3_n_0\,
      S(1) => \current_troughput[31]_i_4_n_0\,
      S(0) => \current_troughput[31]_i_5_n_0\
    );
\current_troughput_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[3]_i_1_n_7\,
      Q => current_troughput_reg(3)
    );
\current_troughput_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_troughput_reg[7]_i_1_n_0\,
      CO(3) => \NLW_current_troughput_reg[3]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \current_troughput_reg[3]_i_1_n_1\,
      CO(1) => \current_troughput_reg[3]_i_1_n_2\,
      CO(0) => \current_troughput_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_troughput_reg[3]_i_1_n_4\,
      O(2) => \current_troughput_reg[3]_i_1_n_5\,
      O(1) => \current_troughput_reg[3]_i_1_n_6\,
      O(0) => \current_troughput_reg[3]_i_1_n_7\,
      S(3) => \current_troughput[3]_i_2_n_0\,
      S(2) => \current_troughput[3]_i_3_n_0\,
      S(1) => \current_troughput[3]_i_4_n_0\,
      S(0) => \current_troughput[3]_i_5_n_0\
    );
\current_troughput_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[7]_i_1_n_4\,
      Q => current_troughput_reg(4)
    );
\current_troughput_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[7]_i_1_n_5\,
      Q => current_troughput_reg(5)
    );
\current_troughput_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[7]_i_1_n_6\,
      Q => current_troughput_reg(6)
    );
\current_troughput_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[7]_i_1_n_7\,
      Q => current_troughput_reg(7)
    );
\current_troughput_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \current_troughput_reg[11]_i_1_n_0\,
      CO(3) => \current_troughput_reg[7]_i_1_n_0\,
      CO(2) => \current_troughput_reg[7]_i_1_n_1\,
      CO(1) => \current_troughput_reg[7]_i_1_n_2\,
      CO(0) => \current_troughput_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \current_troughput_reg[7]_i_1_n_4\,
      O(2) => \current_troughput_reg[7]_i_1_n_5\,
      O(1) => \current_troughput_reg[7]_i_1_n_6\,
      O(0) => \current_troughput_reg[7]_i_1_n_7\,
      S(3) => \current_troughput[7]_i_2_n_0\,
      S(2) => \current_troughput[7]_i_3_n_0\,
      S(1) => \current_troughput[7]_i_4_n_0\,
      S(0) => \current_troughput[7]_i_5_n_0\
    );
\current_troughput_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[11]_i_1_n_4\,
      Q => current_troughput_reg(8)
    );
\current_troughput_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \current_troughput_reg[11]_i_1_n_5\,
      Q => current_troughput_reg(9)
    );
latency_measured_C_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => latency_measured_reg_P_n_0,
      I1 => latency_measured_reg_LDC_n_0,
      I2 => latency_measured_reg_C_n_0,
      O => latency_measured
    );
latency_measured_reg_C: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => rst,
      D => latency_measured,
      Q => latency_measured_reg_C_n_0
    );
latency_measured_reg_LDC: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => rst,
      D => '1',
      G => counting_latency3,
      GE => '1',
      Q => latency_measured_reg_LDC_n_0
    );
latency_measured_reg_LDC_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      O => counting_latency3
    );
latency_measured_reg_P: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => latency_measured,
      PRE => counting_latency3,
      Q => latency_measured_reg_P_n_0
    );
\latency_o[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[0]_P_n_0\,
      I1 => \latency_o_reg[0]_LDC_n_0\,
      I2 => \latency_o_reg[0]_C_n_0\,
      O => \^latency_o\(0)
    );
\latency_o[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[10]_P_n_0\,
      I1 => \latency_o_reg[10]_LDC_n_0\,
      I2 => \latency_o_reg[10]_C_n_0\,
      O => \^latency_o\(10)
    );
\latency_o[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[11]_P_n_0\,
      I1 => \latency_o_reg[11]_LDC_n_0\,
      I2 => \latency_o_reg[11]_C_n_0\,
      O => \^latency_o\(11)
    );
\latency_o[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[12]_P_n_0\,
      I1 => \latency_o_reg[12]_LDC_n_0\,
      I2 => \latency_o_reg[12]_C_n_0\,
      O => \^latency_o\(12)
    );
\latency_o[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[13]_P_n_0\,
      I1 => \latency_o_reg[13]_LDC_n_0\,
      I2 => \latency_o_reg[13]_C_n_0\,
      O => \^latency_o\(13)
    );
\latency_o[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[14]_P_n_0\,
      I1 => \latency_o_reg[14]_LDC_n_0\,
      I2 => \latency_o_reg[14]_C_n_0\,
      O => \^latency_o\(14)
    );
\latency_o[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[15]_P_n_0\,
      I1 => \latency_o_reg[15]_LDC_n_0\,
      I2 => \latency_o_reg[15]_C_n_0\,
      O => \^latency_o\(15)
    );
\latency_o[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[16]_P_n_0\,
      I1 => \latency_o_reg[16]_LDC_n_0\,
      I2 => \latency_o_reg[16]_C_n_0\,
      O => \^latency_o\(16)
    );
\latency_o[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[17]_P_n_0\,
      I1 => \latency_o_reg[17]_LDC_n_0\,
      I2 => \latency_o_reg[17]_C_n_0\,
      O => \^latency_o\(17)
    );
\latency_o[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[18]_P_n_0\,
      I1 => \latency_o_reg[18]_LDC_n_0\,
      I2 => \latency_o_reg[18]_C_n_0\,
      O => \^latency_o\(18)
    );
\latency_o[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[19]_P_n_0\,
      I1 => \latency_o_reg[19]_LDC_n_0\,
      I2 => \latency_o_reg[19]_C_n_0\,
      O => \^latency_o\(19)
    );
\latency_o[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[1]_P_n_0\,
      I1 => \latency_o_reg[1]_LDC_n_0\,
      I2 => \latency_o_reg[1]_C_n_0\,
      O => \^latency_o\(1)
    );
\latency_o[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[20]_P_n_0\,
      I1 => \latency_o_reg[20]_LDC_n_0\,
      I2 => \latency_o_reg[20]_C_n_0\,
      O => \^latency_o\(20)
    );
\latency_o[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[21]_P_n_0\,
      I1 => \latency_o_reg[21]_LDC_n_0\,
      I2 => \latency_o_reg[21]_C_n_0\,
      O => \^latency_o\(21)
    );
\latency_o[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[22]_P_n_0\,
      I1 => \latency_o_reg[22]_LDC_n_0\,
      I2 => \latency_o_reg[22]_C_n_0\,
      O => \^latency_o\(22)
    );
\latency_o[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[23]_P_n_0\,
      I1 => \latency_o_reg[23]_LDC_n_0\,
      I2 => \latency_o_reg[23]_C_n_0\,
      O => \^latency_o\(23)
    );
\latency_o[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[24]_P_n_0\,
      I1 => \latency_o_reg[24]_LDC_n_0\,
      I2 => \latency_o_reg[24]_C_n_0\,
      O => \^latency_o\(24)
    );
\latency_o[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[25]_P_n_0\,
      I1 => \latency_o_reg[25]_LDC_n_0\,
      I2 => \latency_o_reg[25]_C_n_0\,
      O => \^latency_o\(25)
    );
\latency_o[26]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[26]_P_n_0\,
      I1 => \latency_o_reg[26]_LDC_n_0\,
      I2 => \latency_o_reg[26]_C_n_0\,
      O => \^latency_o\(26)
    );
\latency_o[27]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[27]_P_n_0\,
      I1 => \latency_o_reg[27]_LDC_n_0\,
      I2 => \latency_o_reg[27]_C_n_0\,
      O => \^latency_o\(27)
    );
\latency_o[28]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[28]_P_n_0\,
      I1 => \latency_o_reg[28]_LDC_n_0\,
      I2 => \latency_o_reg[28]_C_n_0\,
      O => \^latency_o\(28)
    );
\latency_o[29]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[29]_P_n_0\,
      I1 => \latency_o_reg[29]_LDC_n_0\,
      I2 => \latency_o_reg[29]_C_n_0\,
      O => \^latency_o\(29)
    );
\latency_o[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[2]_P_n_0\,
      I1 => \latency_o_reg[2]_LDC_n_0\,
      I2 => \latency_o_reg[2]_C_n_0\,
      O => \^latency_o\(2)
    );
\latency_o[30]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[30]_P_n_0\,
      I1 => \latency_o_reg[30]_LDC_n_0\,
      I2 => \latency_o_reg[30]_C_n_0\,
      O => \^latency_o\(30)
    );
\latency_o[31]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[31]_P_n_0\,
      I1 => \latency_o_reg[31]_LDC_n_0\,
      I2 => \latency_o_reg[31]_C_n_0\,
      O => \^latency_o\(31)
    );
\latency_o[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[3]_P_n_0\,
      I1 => \latency_o_reg[3]_LDC_n_0\,
      I2 => \latency_o_reg[3]_C_n_0\,
      O => \^latency_o\(3)
    );
\latency_o[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[4]_P_n_0\,
      I1 => \latency_o_reg[4]_LDC_n_0\,
      I2 => \latency_o_reg[4]_C_n_0\,
      O => \^latency_o\(4)
    );
\latency_o[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[5]_P_n_0\,
      I1 => \latency_o_reg[5]_LDC_n_0\,
      I2 => \latency_o_reg[5]_C_n_0\,
      O => \^latency_o\(5)
    );
\latency_o[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[6]_P_n_0\,
      I1 => \latency_o_reg[6]_LDC_n_0\,
      I2 => \latency_o_reg[6]_C_n_0\,
      O => \^latency_o\(6)
    );
\latency_o[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[7]_P_n_0\,
      I1 => \latency_o_reg[7]_LDC_n_0\,
      I2 => \latency_o_reg[7]_C_n_0\,
      O => \^latency_o\(7)
    );
\latency_o[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[8]_P_n_0\,
      I1 => \latency_o_reg[8]_LDC_n_0\,
      I2 => \latency_o_reg[8]_C_n_0\,
      O => \^latency_o\(8)
    );
\latency_o[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \latency_o_reg[9]_P_n_0\,
      I1 => \latency_o_reg[9]_LDC_n_0\,
      I2 => \latency_o_reg[9]_C_n_0\,
      O => \^latency_o\(9)
    );
\latency_o_reg[0]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[0]_LDC_i_2_n_0\,
      D => \^latency_o\(0),
      Q => \latency_o_reg[0]_C_n_0\
    );
\latency_o_reg[0]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[0]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[0]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[0]_LDC_n_0\
    );
\latency_o_reg[0]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(0),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[0]_LDC_i_1_n_0\
    );
\latency_o_reg[0]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(0),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[0]_LDC_i_2_n_0\
    );
\latency_o_reg[0]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(0),
      PRE => \latency_o_reg[0]_LDC_i_1_n_0\,
      Q => \latency_o_reg[0]_P_n_0\
    );
\latency_o_reg[10]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[10]_LDC_i_2_n_0\,
      D => \^latency_o\(10),
      Q => \latency_o_reg[10]_C_n_0\
    );
\latency_o_reg[10]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[10]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[10]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[10]_LDC_n_0\
    );
\latency_o_reg[10]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(10),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[10]_LDC_i_1_n_0\
    );
\latency_o_reg[10]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(10),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[10]_LDC_i_2_n_0\
    );
\latency_o_reg[10]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(10),
      PRE => \latency_o_reg[10]_LDC_i_1_n_0\,
      Q => \latency_o_reg[10]_P_n_0\
    );
\latency_o_reg[11]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[11]_LDC_i_2_n_0\,
      D => \^latency_o\(11),
      Q => \latency_o_reg[11]_C_n_0\
    );
\latency_o_reg[11]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[11]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[11]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[11]_LDC_n_0\
    );
\latency_o_reg[11]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(11),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[11]_LDC_i_1_n_0\
    );
\latency_o_reg[11]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(11),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[11]_LDC_i_2_n_0\
    );
\latency_o_reg[11]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(11),
      PRE => \latency_o_reg[11]_LDC_i_1_n_0\,
      Q => \latency_o_reg[11]_P_n_0\
    );
\latency_o_reg[12]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[12]_LDC_i_2_n_0\,
      D => \^latency_o\(12),
      Q => \latency_o_reg[12]_C_n_0\
    );
\latency_o_reg[12]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[12]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[12]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[12]_LDC_n_0\
    );
\latency_o_reg[12]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(12),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[12]_LDC_i_1_n_0\
    );
\latency_o_reg[12]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(12),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[12]_LDC_i_2_n_0\
    );
\latency_o_reg[12]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(12),
      PRE => \latency_o_reg[12]_LDC_i_1_n_0\,
      Q => \latency_o_reg[12]_P_n_0\
    );
\latency_o_reg[13]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[13]_LDC_i_2_n_0\,
      D => \^latency_o\(13),
      Q => \latency_o_reg[13]_C_n_0\
    );
\latency_o_reg[13]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[13]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[13]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[13]_LDC_n_0\
    );
\latency_o_reg[13]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(13),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[13]_LDC_i_1_n_0\
    );
\latency_o_reg[13]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(13),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[13]_LDC_i_2_n_0\
    );
\latency_o_reg[13]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(13),
      PRE => \latency_o_reg[13]_LDC_i_1_n_0\,
      Q => \latency_o_reg[13]_P_n_0\
    );
\latency_o_reg[14]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[14]_LDC_i_2_n_0\,
      D => \^latency_o\(14),
      Q => \latency_o_reg[14]_C_n_0\
    );
\latency_o_reg[14]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[14]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[14]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[14]_LDC_n_0\
    );
\latency_o_reg[14]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(14),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[14]_LDC_i_1_n_0\
    );
\latency_o_reg[14]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(14),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[14]_LDC_i_2_n_0\
    );
\latency_o_reg[14]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(14),
      PRE => \latency_o_reg[14]_LDC_i_1_n_0\,
      Q => \latency_o_reg[14]_P_n_0\
    );
\latency_o_reg[15]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[15]_LDC_i_2_n_0\,
      D => \^latency_o\(15),
      Q => \latency_o_reg[15]_C_n_0\
    );
\latency_o_reg[15]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[15]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[15]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[15]_LDC_n_0\
    );
\latency_o_reg[15]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(15),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[15]_LDC_i_1_n_0\
    );
\latency_o_reg[15]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(15),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[15]_LDC_i_2_n_0\
    );
\latency_o_reg[15]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(15),
      PRE => \latency_o_reg[15]_LDC_i_1_n_0\,
      Q => \latency_o_reg[15]_P_n_0\
    );
\latency_o_reg[16]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[16]_LDC_i_2_n_0\,
      D => \^latency_o\(16),
      Q => \latency_o_reg[16]_C_n_0\
    );
\latency_o_reg[16]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[16]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[16]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[16]_LDC_n_0\
    );
\latency_o_reg[16]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(16),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[16]_LDC_i_1_n_0\
    );
\latency_o_reg[16]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(16),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[16]_LDC_i_2_n_0\
    );
\latency_o_reg[16]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(16),
      PRE => \latency_o_reg[16]_LDC_i_1_n_0\,
      Q => \latency_o_reg[16]_P_n_0\
    );
\latency_o_reg[17]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[17]_LDC_i_2_n_0\,
      D => \^latency_o\(17),
      Q => \latency_o_reg[17]_C_n_0\
    );
\latency_o_reg[17]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[17]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[17]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[17]_LDC_n_0\
    );
\latency_o_reg[17]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(17),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[17]_LDC_i_1_n_0\
    );
\latency_o_reg[17]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(17),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[17]_LDC_i_2_n_0\
    );
\latency_o_reg[17]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(17),
      PRE => \latency_o_reg[17]_LDC_i_1_n_0\,
      Q => \latency_o_reg[17]_P_n_0\
    );
\latency_o_reg[18]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[18]_LDC_i_2_n_0\,
      D => \^latency_o\(18),
      Q => \latency_o_reg[18]_C_n_0\
    );
\latency_o_reg[18]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[18]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[18]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[18]_LDC_n_0\
    );
\latency_o_reg[18]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(18),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[18]_LDC_i_1_n_0\
    );
\latency_o_reg[18]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(18),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[18]_LDC_i_2_n_0\
    );
\latency_o_reg[18]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(18),
      PRE => \latency_o_reg[18]_LDC_i_1_n_0\,
      Q => \latency_o_reg[18]_P_n_0\
    );
\latency_o_reg[19]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[19]_LDC_i_2_n_0\,
      D => \^latency_o\(19),
      Q => \latency_o_reg[19]_C_n_0\
    );
\latency_o_reg[19]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[19]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[19]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[19]_LDC_n_0\
    );
\latency_o_reg[19]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(19),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[19]_LDC_i_1_n_0\
    );
\latency_o_reg[19]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(19),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[19]_LDC_i_2_n_0\
    );
\latency_o_reg[19]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(19),
      PRE => \latency_o_reg[19]_LDC_i_1_n_0\,
      Q => \latency_o_reg[19]_P_n_0\
    );
\latency_o_reg[1]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[1]_LDC_i_2_n_0\,
      D => \^latency_o\(1),
      Q => \latency_o_reg[1]_C_n_0\
    );
\latency_o_reg[1]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[1]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[1]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[1]_LDC_n_0\
    );
\latency_o_reg[1]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(1),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[1]_LDC_i_1_n_0\
    );
\latency_o_reg[1]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(1),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[1]_LDC_i_2_n_0\
    );
\latency_o_reg[1]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(1),
      PRE => \latency_o_reg[1]_LDC_i_1_n_0\,
      Q => \latency_o_reg[1]_P_n_0\
    );
\latency_o_reg[20]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[20]_LDC_i_2_n_0\,
      D => \^latency_o\(20),
      Q => \latency_o_reg[20]_C_n_0\
    );
\latency_o_reg[20]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[20]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[20]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[20]_LDC_n_0\
    );
\latency_o_reg[20]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(20),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[20]_LDC_i_1_n_0\
    );
\latency_o_reg[20]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(20),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[20]_LDC_i_2_n_0\
    );
\latency_o_reg[20]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(20),
      PRE => \latency_o_reg[20]_LDC_i_1_n_0\,
      Q => \latency_o_reg[20]_P_n_0\
    );
\latency_o_reg[21]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[21]_LDC_i_2_n_0\,
      D => \^latency_o\(21),
      Q => \latency_o_reg[21]_C_n_0\
    );
\latency_o_reg[21]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[21]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[21]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[21]_LDC_n_0\
    );
\latency_o_reg[21]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(21),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[21]_LDC_i_1_n_0\
    );
\latency_o_reg[21]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(21),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[21]_LDC_i_2_n_0\
    );
\latency_o_reg[21]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(21),
      PRE => \latency_o_reg[21]_LDC_i_1_n_0\,
      Q => \latency_o_reg[21]_P_n_0\
    );
\latency_o_reg[22]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[22]_LDC_i_2_n_0\,
      D => \^latency_o\(22),
      Q => \latency_o_reg[22]_C_n_0\
    );
\latency_o_reg[22]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[22]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[22]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[22]_LDC_n_0\
    );
\latency_o_reg[22]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(22),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[22]_LDC_i_1_n_0\
    );
\latency_o_reg[22]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(22),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[22]_LDC_i_2_n_0\
    );
\latency_o_reg[22]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(22),
      PRE => \latency_o_reg[22]_LDC_i_1_n_0\,
      Q => \latency_o_reg[22]_P_n_0\
    );
\latency_o_reg[23]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[23]_LDC_i_2_n_0\,
      D => \^latency_o\(23),
      Q => \latency_o_reg[23]_C_n_0\
    );
\latency_o_reg[23]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[23]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[23]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[23]_LDC_n_0\
    );
\latency_o_reg[23]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(23),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[23]_LDC_i_1_n_0\
    );
\latency_o_reg[23]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(23),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[23]_LDC_i_2_n_0\
    );
\latency_o_reg[23]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(23),
      PRE => \latency_o_reg[23]_LDC_i_1_n_0\,
      Q => \latency_o_reg[23]_P_n_0\
    );
\latency_o_reg[24]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[24]_LDC_i_2_n_0\,
      D => \^latency_o\(24),
      Q => \latency_o_reg[24]_C_n_0\
    );
\latency_o_reg[24]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[24]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[24]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[24]_LDC_n_0\
    );
\latency_o_reg[24]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(24),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[24]_LDC_i_1_n_0\
    );
\latency_o_reg[24]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(24),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[24]_LDC_i_2_n_0\
    );
\latency_o_reg[24]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(24),
      PRE => \latency_o_reg[24]_LDC_i_1_n_0\,
      Q => \latency_o_reg[24]_P_n_0\
    );
\latency_o_reg[25]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[25]_LDC_i_2_n_0\,
      D => \^latency_o\(25),
      Q => \latency_o_reg[25]_C_n_0\
    );
\latency_o_reg[25]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[25]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[25]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[25]_LDC_n_0\
    );
\latency_o_reg[25]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(25),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[25]_LDC_i_1_n_0\
    );
\latency_o_reg[25]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(25),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[25]_LDC_i_2_n_0\
    );
\latency_o_reg[25]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(25),
      PRE => \latency_o_reg[25]_LDC_i_1_n_0\,
      Q => \latency_o_reg[25]_P_n_0\
    );
\latency_o_reg[26]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[26]_LDC_i_2_n_0\,
      D => \^latency_o\(26),
      Q => \latency_o_reg[26]_C_n_0\
    );
\latency_o_reg[26]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[26]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[26]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[26]_LDC_n_0\
    );
\latency_o_reg[26]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(26),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[26]_LDC_i_1_n_0\
    );
\latency_o_reg[26]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(26),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[26]_LDC_i_2_n_0\
    );
\latency_o_reg[26]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(26),
      PRE => \latency_o_reg[26]_LDC_i_1_n_0\,
      Q => \latency_o_reg[26]_P_n_0\
    );
\latency_o_reg[27]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[27]_LDC_i_2_n_0\,
      D => \^latency_o\(27),
      Q => \latency_o_reg[27]_C_n_0\
    );
\latency_o_reg[27]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[27]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[27]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[27]_LDC_n_0\
    );
\latency_o_reg[27]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(27),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[27]_LDC_i_1_n_0\
    );
\latency_o_reg[27]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(27),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[27]_LDC_i_2_n_0\
    );
\latency_o_reg[27]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(27),
      PRE => \latency_o_reg[27]_LDC_i_1_n_0\,
      Q => \latency_o_reg[27]_P_n_0\
    );
\latency_o_reg[28]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[28]_LDC_i_2_n_0\,
      D => \^latency_o\(28),
      Q => \latency_o_reg[28]_C_n_0\
    );
\latency_o_reg[28]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[28]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[28]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[28]_LDC_n_0\
    );
\latency_o_reg[28]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(28),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[28]_LDC_i_1_n_0\
    );
\latency_o_reg[28]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(28),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[28]_LDC_i_2_n_0\
    );
\latency_o_reg[28]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(28),
      PRE => \latency_o_reg[28]_LDC_i_1_n_0\,
      Q => \latency_o_reg[28]_P_n_0\
    );
\latency_o_reg[29]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[29]_LDC_i_2_n_0\,
      D => \^latency_o\(29),
      Q => \latency_o_reg[29]_C_n_0\
    );
\latency_o_reg[29]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[29]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[29]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[29]_LDC_n_0\
    );
\latency_o_reg[29]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(29),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[29]_LDC_i_1_n_0\
    );
\latency_o_reg[29]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(29),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[29]_LDC_i_2_n_0\
    );
\latency_o_reg[29]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(29),
      PRE => \latency_o_reg[29]_LDC_i_1_n_0\,
      Q => \latency_o_reg[29]_P_n_0\
    );
\latency_o_reg[2]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[2]_LDC_i_2_n_0\,
      D => \^latency_o\(2),
      Q => \latency_o_reg[2]_C_n_0\
    );
\latency_o_reg[2]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[2]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[2]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[2]_LDC_n_0\
    );
\latency_o_reg[2]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(2),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[2]_LDC_i_1_n_0\
    );
\latency_o_reg[2]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(2),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[2]_LDC_i_2_n_0\
    );
\latency_o_reg[2]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(2),
      PRE => \latency_o_reg[2]_LDC_i_1_n_0\,
      Q => \latency_o_reg[2]_P_n_0\
    );
\latency_o_reg[30]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[30]_LDC_i_2_n_0\,
      D => \^latency_o\(30),
      Q => \latency_o_reg[30]_C_n_0\
    );
\latency_o_reg[30]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[30]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[30]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[30]_LDC_n_0\
    );
\latency_o_reg[30]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(30),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[30]_LDC_i_1_n_0\
    );
\latency_o_reg[30]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(30),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[30]_LDC_i_2_n_0\
    );
\latency_o_reg[30]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(30),
      PRE => \latency_o_reg[30]_LDC_i_1_n_0\,
      Q => \latency_o_reg[30]_P_n_0\
    );
\latency_o_reg[31]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[31]_LDC_i_2_n_0\,
      D => \^latency_o\(31),
      Q => \latency_o_reg[31]_C_n_0\
    );
\latency_o_reg[31]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[31]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[31]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[31]_LDC_n_0\
    );
\latency_o_reg[31]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(31),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[31]_LDC_i_1_n_0\
    );
\latency_o_reg[31]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(31),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[31]_LDC_i_2_n_0\
    );
\latency_o_reg[31]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(31),
      PRE => \latency_o_reg[31]_LDC_i_1_n_0\,
      Q => \latency_o_reg[31]_P_n_0\
    );
\latency_o_reg[3]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[3]_LDC_i_2_n_0\,
      D => \^latency_o\(3),
      Q => \latency_o_reg[3]_C_n_0\
    );
\latency_o_reg[3]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[3]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[3]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[3]_LDC_n_0\
    );
\latency_o_reg[3]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(3),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[3]_LDC_i_1_n_0\
    );
\latency_o_reg[3]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(3),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[3]_LDC_i_2_n_0\
    );
\latency_o_reg[3]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(3),
      PRE => \latency_o_reg[3]_LDC_i_1_n_0\,
      Q => \latency_o_reg[3]_P_n_0\
    );
\latency_o_reg[4]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[4]_LDC_i_2_n_0\,
      D => \^latency_o\(4),
      Q => \latency_o_reg[4]_C_n_0\
    );
\latency_o_reg[4]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[4]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[4]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[4]_LDC_n_0\
    );
\latency_o_reg[4]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(4),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[4]_LDC_i_1_n_0\
    );
\latency_o_reg[4]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(4),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[4]_LDC_i_2_n_0\
    );
\latency_o_reg[4]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(4),
      PRE => \latency_o_reg[4]_LDC_i_1_n_0\,
      Q => \latency_o_reg[4]_P_n_0\
    );
\latency_o_reg[5]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[5]_LDC_i_2_n_0\,
      D => \^latency_o\(5),
      Q => \latency_o_reg[5]_C_n_0\
    );
\latency_o_reg[5]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[5]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[5]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[5]_LDC_n_0\
    );
\latency_o_reg[5]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(5),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[5]_LDC_i_1_n_0\
    );
\latency_o_reg[5]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(5),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[5]_LDC_i_2_n_0\
    );
\latency_o_reg[5]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(5),
      PRE => \latency_o_reg[5]_LDC_i_1_n_0\,
      Q => \latency_o_reg[5]_P_n_0\
    );
\latency_o_reg[6]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[6]_LDC_i_2_n_0\,
      D => \^latency_o\(6),
      Q => \latency_o_reg[6]_C_n_0\
    );
\latency_o_reg[6]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[6]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[6]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[6]_LDC_n_0\
    );
\latency_o_reg[6]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(6),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[6]_LDC_i_1_n_0\
    );
\latency_o_reg[6]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(6),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[6]_LDC_i_2_n_0\
    );
\latency_o_reg[6]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(6),
      PRE => \latency_o_reg[6]_LDC_i_1_n_0\,
      Q => \latency_o_reg[6]_P_n_0\
    );
\latency_o_reg[7]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[7]_LDC_i_2_n_0\,
      D => \^latency_o\(7),
      Q => \latency_o_reg[7]_C_n_0\
    );
\latency_o_reg[7]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[7]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[7]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[7]_LDC_n_0\
    );
\latency_o_reg[7]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(7),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[7]_LDC_i_1_n_0\
    );
\latency_o_reg[7]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(7),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[7]_LDC_i_2_n_0\
    );
\latency_o_reg[7]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(7),
      PRE => \latency_o_reg[7]_LDC_i_1_n_0\,
      Q => \latency_o_reg[7]_P_n_0\
    );
\latency_o_reg[8]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[8]_LDC_i_2_n_0\,
      D => \^latency_o\(8),
      Q => \latency_o_reg[8]_C_n_0\
    );
\latency_o_reg[8]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[8]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[8]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[8]_LDC_n_0\
    );
\latency_o_reg[8]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(8),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[8]_LDC_i_1_n_0\
    );
\latency_o_reg[8]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(8),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[8]_LDC_i_2_n_0\
    );
\latency_o_reg[8]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(8),
      PRE => \latency_o_reg[8]_LDC_i_1_n_0\,
      Q => \latency_o_reg[8]_P_n_0\
    );
\latency_o_reg[9]_C\: unisim.vcomponents.FDCE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      CLR => \latency_o_reg[9]_LDC_i_2_n_0\,
      D => \^latency_o\(9),
      Q => \latency_o_reg[9]_C_n_0\
    );
\latency_o_reg[9]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \latency_o_reg[9]_LDC_i_2_n_0\,
      D => '1',
      G => \latency_o_reg[9]_LDC_i_1_n_0\,
      GE => '1',
      Q => \latency_o_reg[9]_LDC_n_0\
    );
\latency_o_reg[9]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => current_latnecy_reg(9),
      I1 => axi_reset,
      I2 => reset,
      I3 => counting_latency,
      I4 => packet_arived_i,
      O => \latency_o_reg[9]_LDC_i_1_n_0\
    );
\latency_o_reg[9]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF08FF"
    )
        port map (
      I0 => counting_latency,
      I1 => packet_arived_i,
      I2 => current_latnecy_reg(9),
      I3 => axi_reset,
      I4 => reset,
      O => \latency_o_reg[9]_LDC_i_2_n_0\
    );
\latency_o_reg[9]_P\: unisim.vcomponents.FDPE
     port map (
      C => axi_transaction_completet,
      CE => '1',
      D => \^latency_o\(9),
      PRE => \latency_o_reg[9]_LDC_i_1_n_0\,
      Q => \latency_o_reg[9]_P_n_0\
    );
packet_arived_last_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F4B0"
    )
        port map (
      I0 => \troughput_state_reg[0]_C_n_0\,
      I1 => \troughput_state_reg[1]_C_n_0\,
      I2 => packet_arived_last,
      I3 => packet_arived_i,
      O => packet_arived_last_i_1_n_0
    );
packet_arived_last_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => rst,
      D => packet_arived_last_i_1_n_0,
      Q => packet_arived_last
    );
\throughput_o[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44444440"
    )
        port map (
      I0 => \troughput_state_reg[0]_C_n_0\,
      I1 => \troughput_state_reg[1]_C_n_0\,
      I2 => \throughput_o[0]_i_4_n_0\,
      I3 => \throughput_o[0]_i_5_n_0\,
      I4 => \throughput_o[0]_i_6_n_0\,
      O => \throughput_o[0]_i_1_n_0\
    );
\throughput_o[0]_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88888880"
    )
        port map (
      I0 => p_0_in(6),
      I1 => p_0_in(5),
      I2 => p_0_in(4),
      I3 => p_0_in(3),
      I4 => p_0_in(2),
      O => \throughput_o[0]_i_12_n_0\
    );
\throughput_o[0]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => p_0_in(24),
      I1 => p_0_in(11),
      I2 => p_0_in(28),
      I3 => p_0_in(22),
      O => \throughput_o[0]_i_13_n_0\
    );
\throughput_o[0]_i_17\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(0),
      O => \throughput_o[0]_i_17_n_0\
    );
\throughput_o[0]_i_18\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(1),
      O => \throughput_o[0]_i_18_n_0\
    );
\throughput_o[0]_i_19\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(2),
      O => \throughput_o[0]_i_19_n_0\
    );
\throughput_o[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(0),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(31)
    );
\throughput_o[0]_i_20\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(3),
      O => \throughput_o[0]_i_20_n_0\
    );
\throughput_o[0]_i_23\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(15),
      O => \throughput_o[0]_i_23_n_0\
    );
\throughput_o[0]_i_24\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(16),
      O => \throughput_o[0]_i_24_n_0\
    );
\throughput_o[0]_i_25\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(17),
      O => \throughput_o[0]_i_25_n_0\
    );
\throughput_o[0]_i_26\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(18),
      O => \throughput_o[0]_i_26_n_0\
    );
\throughput_o[0]_i_27\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(11),
      O => \throughput_o[0]_i_27_n_0\
    );
\throughput_o[0]_i_28\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(12),
      O => \throughput_o[0]_i_28_n_0\
    );
\throughput_o[0]_i_29\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(13),
      O => \throughput_o[0]_i_29_n_0\
    );
\throughput_o[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset,
      I1 => axi_reset,
      O => rst
    );
\throughput_o[0]_i_30\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(14),
      O => \throughput_o[0]_i_30_n_0\
    );
\throughput_o[0]_i_32\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(0),
      O => \throughput_o[0]_i_32_n_0\
    );
\throughput_o[0]_i_33\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(1),
      O => \throughput_o[0]_i_33_n_0\
    );
\throughput_o[0]_i_34\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(2),
      O => \throughput_o[0]_i_34_n_0\
    );
\throughput_o[0]_i_35\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(3),
      O => \throughput_o[0]_i_35_n_0\
    );
\throughput_o[0]_i_36\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(4),
      O => \throughput_o[0]_i_36_n_0\
    );
\throughput_o[0]_i_37\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(5),
      O => \throughput_o[0]_i_37_n_0\
    );
\throughput_o[0]_i_38\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(6),
      O => \throughput_o[0]_i_38_n_0\
    );
\throughput_o[0]_i_39\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(7),
      O => \throughput_o[0]_i_39_n_0\
    );
\throughput_o[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \throughput_o[0]_i_8_n_0\,
      I1 => \throughput_o[0]_i_9_n_0\,
      I2 => p_0_in(14),
      I3 => p_0_in(19),
      I4 => p_0_in(15),
      O => \throughput_o[0]_i_4_n_0\
    );
\throughput_o[0]_i_40\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(8),
      O => \throughput_o[0]_i_40_n_0\
    );
\throughput_o[0]_i_41\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(9),
      O => \throughput_o[0]_i_41_n_0\
    );
\throughput_o[0]_i_42\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(10),
      O => \throughput_o[0]_i_42_n_0\
    );
\throughput_o[0]_i_43\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(19),
      O => \throughput_o[0]_i_43_n_0\
    );
\throughput_o[0]_i_44\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(20),
      O => \throughput_o[0]_i_44_n_0\
    );
\throughput_o[0]_i_45\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(21),
      O => \throughput_o[0]_i_45_n_0\
    );
\throughput_o[0]_i_46\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(22),
      O => \throughput_o[0]_i_46_n_0\
    );
\throughput_o[0]_i_47\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(23),
      O => \throughput_o[0]_i_47_n_0\
    );
\throughput_o[0]_i_48\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(24),
      O => \throughput_o[0]_i_48_n_0\
    );
\throughput_o[0]_i_49\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(25),
      O => \throughput_o[0]_i_49_n_0\
    );
\throughput_o[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \throughput_o[0]_i_12_n_0\,
      I1 => \throughput_o[0]_i_13_n_0\,
      I2 => p_0_in(29),
      I3 => p_0_in(16),
      I4 => p_0_in(30),
      I5 => p_0_in(27),
      O => \throughput_o[0]_i_5_n_0\
    );
\throughput_o[0]_i_50\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(26),
      O => \throughput_o[0]_i_50_n_0\
    );
\throughput_o[0]_i_51\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(27),
      O => \throughput_o[0]_i_51_n_0\
    );
\throughput_o[0]_i_52\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(28),
      O => \throughput_o[0]_i_52_n_0\
    );
\throughput_o[0]_i_53\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(29),
      O => \throughput_o[0]_i_53_n_0\
    );
\throughput_o[0]_i_54\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(30),
      O => \throughput_o[0]_i_54_n_0\
    );
\throughput_o[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => p_0_in(23),
      I1 => p_0_in(25),
      I2 => p_0_in(21),
      I3 => p_0_in(20),
      I4 => p_0_in(31),
      I5 => p_0_in(26),
      O => \throughput_o[0]_i_6_n_0\
    );
\throughput_o[0]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => p_0_in(9),
      I1 => p_0_in(18),
      I2 => p_0_in(12),
      I3 => p_0_in(7),
      O => \throughput_o[0]_i_8_n_0\
    );
\throughput_o[0]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => p_0_in(10),
      I1 => p_0_in(17),
      I2 => p_0_in(13),
      I3 => p_0_in(8),
      O => \throughput_o[0]_i_9_n_0\
    );
\throughput_o[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(10),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(21)
    );
\throughput_o[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(11),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(20)
    );
\throughput_o[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(12),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(19)
    );
\throughput_o[12]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(12),
      O => \throughput_o[12]_i_3_n_0\
    );
\throughput_o[12]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(13),
      O => \throughput_o[12]_i_4_n_0\
    );
\throughput_o[12]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(14),
      O => \throughput_o[12]_i_5_n_0\
    );
\throughput_o[12]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(15),
      O => \throughput_o[12]_i_6_n_0\
    );
\throughput_o[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(13),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(18)
    );
\throughput_o[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(14),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(17)
    );
\throughput_o[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(15),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(16)
    );
\throughput_o[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(16),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(15)
    );
\throughput_o[16]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(16),
      O => \throughput_o[16]_i_3_n_0\
    );
\throughput_o[16]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(17),
      O => \throughput_o[16]_i_4_n_0\
    );
\throughput_o[16]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(18),
      O => \throughput_o[16]_i_5_n_0\
    );
\throughput_o[16]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(19),
      O => \throughput_o[16]_i_6_n_0\
    );
\throughput_o[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(17),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(14)
    );
\throughput_o[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(18),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(13)
    );
\throughput_o[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(19),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(12)
    );
\throughput_o[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(1),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(30)
    );
\throughput_o[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(20),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(11)
    );
\throughput_o[20]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(20),
      O => \throughput_o[20]_i_3_n_0\
    );
\throughput_o[20]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(21),
      O => \throughput_o[20]_i_4_n_0\
    );
\throughput_o[20]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(22),
      O => \throughput_o[20]_i_5_n_0\
    );
\throughput_o[20]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(23),
      O => \throughput_o[20]_i_6_n_0\
    );
\throughput_o[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(21),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(10)
    );
\throughput_o[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(22),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(9)
    );
\throughput_o[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(23),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(8)
    );
\throughput_o[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(24),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(7)
    );
\throughput_o[24]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(24),
      O => \throughput_o[24]_i_3_n_0\
    );
\throughput_o[24]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(25),
      O => \throughput_o[24]_i_4_n_0\
    );
\throughput_o[24]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(26),
      O => \throughput_o[24]_i_5_n_0\
    );
\throughput_o[24]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(27),
      O => \throughput_o[24]_i_6_n_0\
    );
\throughput_o[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(25),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(6)
    );
\throughput_o[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(26),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(5)
    );
\throughput_o[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(27),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(4)
    );
\throughput_o[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(28),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(3)
    );
\throughput_o[28]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(28),
      O => \throughput_o[28]_i_3_n_0\
    );
\throughput_o[28]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(29),
      O => \throughput_o[28]_i_4_n_0\
    );
\throughput_o[28]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(30),
      O => \throughput_o[28]_i_5_n_0\
    );
\throughput_o[28]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"9A"
    )
        port map (
      I0 => current_troughput_reg(31),
      I1 => packet_arived_last,
      I2 => packet_arived_i,
      O => \throughput_o[28]_i_6_n_0\
    );
\throughput_o[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(29),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(2)
    );
\throughput_o[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(2),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(29)
    );
\throughput_o[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(30),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(1)
    );
\throughput_o[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(31),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(0)
    );
\throughput_o[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(3),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(28)
    );
\throughput_o[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(4),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(27)
    );
\throughput_o[4]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(4),
      O => \throughput_o[4]_i_3_n_0\
    );
\throughput_o[4]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(5),
      O => \throughput_o[4]_i_4_n_0\
    );
\throughput_o[4]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(6),
      O => \throughput_o[4]_i_5_n_0\
    );
\throughput_o[4]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(7),
      O => \throughput_o[4]_i_6_n_0\
    );
\throughput_o[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(5),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(26)
    );
\throughput_o[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(6),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(25)
    );
\throughput_o[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(7),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(24)
    );
\throughput_o[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(8),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(23)
    );
\throughput_o[8]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(8),
      O => \throughput_o[8]_i_3_n_0\
    );
\throughput_o[8]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(9),
      O => \throughput_o[8]_i_4_n_0\
    );
\throughput_o[8]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(10),
      O => \throughput_o[8]_i_5_n_0\
    );
\throughput_o[8]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => current_troughput_reg(11),
      O => \throughput_o[8]_i_6_n_0\
    );
\throughput_o[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2020202020202000"
    )
        port map (
      I0 => current_troughput(9),
      I1 => \troughput_state_reg[0]_C_n_0\,
      I2 => \troughput_state_reg[1]_C_n_0\,
      I3 => \throughput_o[0]_i_4_n_0\,
      I4 => \throughput_o[0]_i_5_n_0\,
      I5 => \throughput_o[0]_i_6_n_0\,
      O => p_3_in(22)
    );
\throughput_o_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(31),
      Q => throughput_o(0)
    );
\throughput_o_reg[0]_i_10\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[0]_i_21_n_0\,
      CO(3) => \throughput_o_reg[0]_i_10_n_0\,
      CO(2) => \throughput_o_reg[0]_i_10_n_1\,
      CO(1) => \throughput_o_reg[0]_i_10_n_2\,
      CO(0) => \throughput_o_reg[0]_i_10_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_0_in(16 downto 13),
      S(3) => \throughput_o[0]_i_23_n_0\,
      S(2) => \throughput_o[0]_i_24_n_0\,
      S(1) => \throughput_o[0]_i_25_n_0\,
      S(0) => \throughput_o[0]_i_26_n_0\
    );
\throughput_o_reg[0]_i_11\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[0]_i_10_n_0\,
      CO(3) => \throughput_o_reg[0]_i_11_n_0\,
      CO(2) => \throughput_o_reg[0]_i_11_n_1\,
      CO(1) => \throughput_o_reg[0]_i_11_n_2\,
      CO(0) => \throughput_o_reg[0]_i_11_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_0_in(20 downto 17),
      S(3) => \throughput_o[0]_i_27_n_0\,
      S(2) => \throughput_o[0]_i_28_n_0\,
      S(1) => \throughput_o[0]_i_29_n_0\,
      S(0) => \throughput_o[0]_i_30_n_0\
    );
\throughput_o_reg[0]_i_14\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[0]_i_15_n_0\,
      CO(3 downto 2) => \NLW_throughput_o_reg[0]_i_14_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \throughput_o_reg[0]_i_14_n_2\,
      CO(0) => \throughput_o_reg[0]_i_14_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_throughput_o_reg[0]_i_14_O_UNCONNECTED\(3),
      O(2 downto 0) => p_0_in(31 downto 29),
      S(3) => '0',
      S(2) => \throughput_o[0]_i_32_n_0\,
      S(1) => \throughput_o[0]_i_33_n_0\,
      S(0) => \throughput_o[0]_i_34_n_0\
    );
\throughput_o_reg[0]_i_15\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[0]_i_16_n_0\,
      CO(3) => \throughput_o_reg[0]_i_15_n_0\,
      CO(2) => \throughput_o_reg[0]_i_15_n_1\,
      CO(1) => \throughput_o_reg[0]_i_15_n_2\,
      CO(0) => \throughput_o_reg[0]_i_15_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_0_in(28 downto 25),
      S(3) => \throughput_o[0]_i_35_n_0\,
      S(2) => \throughput_o[0]_i_36_n_0\,
      S(1) => \throughput_o[0]_i_37_n_0\,
      S(0) => \throughput_o[0]_i_38_n_0\
    );
\throughput_o_reg[0]_i_16\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[0]_i_11_n_0\,
      CO(3) => \throughput_o_reg[0]_i_16_n_0\,
      CO(2) => \throughput_o_reg[0]_i_16_n_1\,
      CO(1) => \throughput_o_reg[0]_i_16_n_2\,
      CO(0) => \throughput_o_reg[0]_i_16_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_0_in(24 downto 21),
      S(3) => \throughput_o[0]_i_39_n_0\,
      S(2) => \throughput_o[0]_i_40_n_0\,
      S(1) => \throughput_o[0]_i_41_n_0\,
      S(0) => \throughput_o[0]_i_42_n_0\
    );
\throughput_o_reg[0]_i_21\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[0]_i_22_n_0\,
      CO(3) => \throughput_o_reg[0]_i_21_n_0\,
      CO(2) => \throughput_o_reg[0]_i_21_n_1\,
      CO(1) => \throughput_o_reg[0]_i_21_n_2\,
      CO(0) => \throughput_o_reg[0]_i_21_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_0_in(12 downto 9),
      S(3) => \throughput_o[0]_i_43_n_0\,
      S(2) => \throughput_o[0]_i_44_n_0\,
      S(1) => \throughput_o[0]_i_45_n_0\,
      S(0) => \throughput_o[0]_i_46_n_0\
    );
\throughput_o_reg[0]_i_22\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[0]_i_31_n_0\,
      CO(3) => \throughput_o_reg[0]_i_22_n_0\,
      CO(2) => \throughput_o_reg[0]_i_22_n_1\,
      CO(1) => \throughput_o_reg[0]_i_22_n_2\,
      CO(0) => \throughput_o_reg[0]_i_22_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_0_in(8 downto 5),
      S(3) => \throughput_o[0]_i_47_n_0\,
      S(2) => \throughput_o[0]_i_48_n_0\,
      S(1) => \throughput_o[0]_i_49_n_0\,
      S(0) => \throughput_o[0]_i_50_n_0\
    );
\throughput_o_reg[0]_i_31\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \throughput_o_reg[0]_i_31_n_0\,
      CO(2) => \throughput_o_reg[0]_i_31_n_1\,
      CO(1) => \throughput_o_reg[0]_i_31_n_2\,
      CO(0) => \throughput_o_reg[0]_i_31_n_3\,
      CYINIT => throughput_timer_reg(31),
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => p_0_in(4 downto 2),
      O(0) => \NLW_throughput_o_reg[0]_i_31_O_UNCONNECTED\(0),
      S(3) => \throughput_o[0]_i_51_n_0\,
      S(2) => \throughput_o[0]_i_52_n_0\,
      S(1) => \throughput_o[0]_i_53_n_0\,
      S(0) => \throughput_o[0]_i_54_n_0\
    );
\throughput_o_reg[0]_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[4]_i_2_n_0\,
      CO(3) => \NLW_throughput_o_reg[0]_i_7_CO_UNCONNECTED\(3),
      CO(2) => \throughput_o_reg[0]_i_7_n_1\,
      CO(1) => \throughput_o_reg[0]_i_7_n_2\,
      CO(0) => \throughput_o_reg[0]_i_7_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => current_troughput(0),
      O(2) => current_troughput(1),
      O(1) => current_troughput(2),
      O(0) => current_troughput(3),
      S(3) => \throughput_o[0]_i_17_n_0\,
      S(2) => \throughput_o[0]_i_18_n_0\,
      S(1) => \throughput_o[0]_i_19_n_0\,
      S(0) => \throughput_o[0]_i_20_n_0\
    );
\throughput_o_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(21),
      Q => throughput_o(10)
    );
\throughput_o_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(20),
      Q => throughput_o(11)
    );
\throughput_o_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(19),
      Q => throughput_o(12)
    );
\throughput_o_reg[12]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[16]_i_2_n_0\,
      CO(3) => \throughput_o_reg[12]_i_2_n_0\,
      CO(2) => \throughput_o_reg[12]_i_2_n_1\,
      CO(1) => \throughput_o_reg[12]_i_2_n_2\,
      CO(0) => \throughput_o_reg[12]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => current_troughput(12),
      O(2) => current_troughput(13),
      O(1) => current_troughput(14),
      O(0) => current_troughput(15),
      S(3) => \throughput_o[12]_i_3_n_0\,
      S(2) => \throughput_o[12]_i_4_n_0\,
      S(1) => \throughput_o[12]_i_5_n_0\,
      S(0) => \throughput_o[12]_i_6_n_0\
    );
\throughput_o_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(18),
      Q => throughput_o(13)
    );
\throughput_o_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(17),
      Q => throughput_o(14)
    );
\throughput_o_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(16),
      Q => throughput_o(15)
    );
\throughput_o_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(15),
      Q => throughput_o(16)
    );
\throughput_o_reg[16]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[20]_i_2_n_0\,
      CO(3) => \throughput_o_reg[16]_i_2_n_0\,
      CO(2) => \throughput_o_reg[16]_i_2_n_1\,
      CO(1) => \throughput_o_reg[16]_i_2_n_2\,
      CO(0) => \throughput_o_reg[16]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => current_troughput(16),
      O(2) => current_troughput(17),
      O(1) => current_troughput(18),
      O(0) => current_troughput(19),
      S(3) => \throughput_o[16]_i_3_n_0\,
      S(2) => \throughput_o[16]_i_4_n_0\,
      S(1) => \throughput_o[16]_i_5_n_0\,
      S(0) => \throughput_o[16]_i_6_n_0\
    );
\throughput_o_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(14),
      Q => throughput_o(17)
    );
\throughput_o_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(13),
      Q => throughput_o(18)
    );
\throughput_o_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(12),
      Q => throughput_o(19)
    );
\throughput_o_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(30),
      Q => throughput_o(1)
    );
\throughput_o_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(11),
      Q => throughput_o(20)
    );
\throughput_o_reg[20]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[24]_i_2_n_0\,
      CO(3) => \throughput_o_reg[20]_i_2_n_0\,
      CO(2) => \throughput_o_reg[20]_i_2_n_1\,
      CO(1) => \throughput_o_reg[20]_i_2_n_2\,
      CO(0) => \throughput_o_reg[20]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => current_troughput(20),
      O(2) => current_troughput(21),
      O(1) => current_troughput(22),
      O(0) => current_troughput(23),
      S(3) => \throughput_o[20]_i_3_n_0\,
      S(2) => \throughput_o[20]_i_4_n_0\,
      S(1) => \throughput_o[20]_i_5_n_0\,
      S(0) => \throughput_o[20]_i_6_n_0\
    );
\throughput_o_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(10),
      Q => throughput_o(21)
    );
\throughput_o_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(9),
      Q => throughput_o(22)
    );
\throughput_o_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(8),
      Q => throughput_o(23)
    );
\throughput_o_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(7),
      Q => throughput_o(24)
    );
\throughput_o_reg[24]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[28]_i_2_n_0\,
      CO(3) => \throughput_o_reg[24]_i_2_n_0\,
      CO(2) => \throughput_o_reg[24]_i_2_n_1\,
      CO(1) => \throughput_o_reg[24]_i_2_n_2\,
      CO(0) => \throughput_o_reg[24]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => current_troughput(24),
      O(2) => current_troughput(25),
      O(1) => current_troughput(26),
      O(0) => current_troughput(27),
      S(3) => \throughput_o[24]_i_3_n_0\,
      S(2) => \throughput_o[24]_i_4_n_0\,
      S(1) => \throughput_o[24]_i_5_n_0\,
      S(0) => \throughput_o[24]_i_6_n_0\
    );
\throughput_o_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(6),
      Q => throughput_o(25)
    );
\throughput_o_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(5),
      Q => throughput_o(26)
    );
\throughput_o_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(4),
      Q => throughput_o(27)
    );
\throughput_o_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(3),
      Q => throughput_o(28)
    );
\throughput_o_reg[28]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \throughput_o_reg[28]_i_2_n_0\,
      CO(2) => \throughput_o_reg[28]_i_2_n_1\,
      CO(1) => \throughput_o_reg[28]_i_2_n_2\,
      CO(0) => \throughput_o_reg[28]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => current_troughput_reg(31),
      O(3) => current_troughput(28),
      O(2) => current_troughput(29),
      O(1) => current_troughput(30),
      O(0) => current_troughput(31),
      S(3) => \throughput_o[28]_i_3_n_0\,
      S(2) => \throughput_o[28]_i_4_n_0\,
      S(1) => \throughput_o[28]_i_5_n_0\,
      S(0) => \throughput_o[28]_i_6_n_0\
    );
\throughput_o_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(2),
      Q => throughput_o(29)
    );
\throughput_o_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(29),
      Q => throughput_o(2)
    );
\throughput_o_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(1),
      Q => throughput_o(30)
    );
\throughput_o_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(0),
      Q => throughput_o(31)
    );
\throughput_o_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(28),
      Q => throughput_o(3)
    );
\throughput_o_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(27),
      Q => throughput_o(4)
    );
\throughput_o_reg[4]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[8]_i_2_n_0\,
      CO(3) => \throughput_o_reg[4]_i_2_n_0\,
      CO(2) => \throughput_o_reg[4]_i_2_n_1\,
      CO(1) => \throughput_o_reg[4]_i_2_n_2\,
      CO(0) => \throughput_o_reg[4]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => current_troughput(4),
      O(2) => current_troughput(5),
      O(1) => current_troughput(6),
      O(0) => current_troughput(7),
      S(3) => \throughput_o[4]_i_3_n_0\,
      S(2) => \throughput_o[4]_i_4_n_0\,
      S(1) => \throughput_o[4]_i_5_n_0\,
      S(0) => \throughput_o[4]_i_6_n_0\
    );
\throughput_o_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(26),
      Q => throughput_o(5)
    );
\throughput_o_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(25),
      Q => throughput_o(6)
    );
\throughput_o_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(24),
      Q => throughput_o(7)
    );
\throughput_o_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(23),
      Q => throughput_o(8)
    );
\throughput_o_reg[8]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_o_reg[12]_i_2_n_0\,
      CO(3) => \throughput_o_reg[8]_i_2_n_0\,
      CO(2) => \throughput_o_reg[8]_i_2_n_1\,
      CO(1) => \throughput_o_reg[8]_i_2_n_2\,
      CO(0) => \throughput_o_reg[8]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => current_troughput(8),
      O(2) => current_troughput(9),
      O(1) => current_troughput(10),
      O(0) => current_troughput(11),
      S(3) => \throughput_o[8]_i_3_n_0\,
      S(2) => \throughput_o[8]_i_4_n_0\,
      S(1) => \throughput_o[8]_i_5_n_0\,
      S(0) => \throughput_o[8]_i_6_n_0\
    );
\throughput_o_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \throughput_o[0]_i_1_n_0\,
      CLR => rst,
      D => p_3_in(22),
      Q => throughput_o(9)
    );
\throughput_timer[11]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(8),
      O => \throughput_timer[11]_i_2_n_0\
    );
\throughput_timer[11]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(9),
      O => \throughput_timer[11]_i_3_n_0\
    );
\throughput_timer[11]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(10),
      O => \throughput_timer[11]_i_4_n_0\
    );
\throughput_timer[11]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(11),
      O => \throughput_timer[11]_i_5_n_0\
    );
\throughput_timer[15]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(12),
      O => \throughput_timer[15]_i_2_n_0\
    );
\throughput_timer[15]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(13),
      O => \throughput_timer[15]_i_3_n_0\
    );
\throughput_timer[15]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(14),
      O => \throughput_timer[15]_i_4_n_0\
    );
\throughput_timer[15]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(15),
      O => \throughput_timer[15]_i_5_n_0\
    );
\throughput_timer[19]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(16),
      O => \throughput_timer[19]_i_2_n_0\
    );
\throughput_timer[19]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(17),
      O => \throughput_timer[19]_i_3_n_0\
    );
\throughput_timer[19]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(18),
      O => \throughput_timer[19]_i_4_n_0\
    );
\throughput_timer[19]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(19),
      O => \throughput_timer[19]_i_5_n_0\
    );
\throughput_timer[23]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(20),
      O => \throughput_timer[23]_i_2_n_0\
    );
\throughput_timer[23]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(21),
      O => \throughput_timer[23]_i_3_n_0\
    );
\throughput_timer[23]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(22),
      O => \throughput_timer[23]_i_4_n_0\
    );
\throughput_timer[23]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(23),
      O => \throughput_timer[23]_i_5_n_0\
    );
\throughput_timer[27]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(24),
      O => \throughput_timer[27]_i_2_n_0\
    );
\throughput_timer[27]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(25),
      O => \throughput_timer[27]_i_3_n_0\
    );
\throughput_timer[27]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(26),
      O => \throughput_timer[27]_i_4_n_0\
    );
\throughput_timer[27]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(27),
      O => \throughput_timer[27]_i_5_n_0\
    );
\throughput_timer[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \troughput_state_reg[1]_C_n_0\,
      I1 => \troughput_state_reg[0]_C_n_0\,
      O => throughput_timer
    );
\throughput_timer[31]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(28),
      O => \throughput_timer[31]_i_3_n_0\
    );
\throughput_timer[31]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(29),
      O => \throughput_timer[31]_i_4_n_0\
    );
\throughput_timer[31]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(30),
      O => \throughput_timer[31]_i_5_n_0\
    );
\throughput_timer[31]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => throughput_timer_reg(31),
      O => \throughput_timer[31]_i_6_n_0\
    );
\throughput_timer[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(0),
      O => \throughput_timer[3]_i_2_n_0\
    );
\throughput_timer[3]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(1),
      O => \throughput_timer[3]_i_3_n_0\
    );
\throughput_timer[3]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(2),
      O => \throughput_timer[3]_i_4_n_0\
    );
\throughput_timer[3]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(3),
      O => \throughput_timer[3]_i_5_n_0\
    );
\throughput_timer[7]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(4),
      O => \throughput_timer[7]_i_2_n_0\
    );
\throughput_timer[7]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(5),
      O => \throughput_timer[7]_i_3_n_0\
    );
\throughput_timer[7]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(6),
      O => \throughput_timer[7]_i_4_n_0\
    );
\throughput_timer[7]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => throughput_timer_reg(7),
      O => \throughput_timer[7]_i_5_n_0\
    );
\throughput_timer_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[3]_i_1_n_4\,
      Q => throughput_timer_reg(0)
    );
\throughput_timer_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[11]_i_1_n_6\,
      Q => throughput_timer_reg(10)
    );
\throughput_timer_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[11]_i_1_n_7\,
      Q => throughput_timer_reg(11)
    );
\throughput_timer_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_timer_reg[15]_i_1_n_0\,
      CO(3) => \throughput_timer_reg[11]_i_1_n_0\,
      CO(2) => \throughput_timer_reg[11]_i_1_n_1\,
      CO(1) => \throughput_timer_reg[11]_i_1_n_2\,
      CO(0) => \throughput_timer_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \throughput_timer_reg[11]_i_1_n_4\,
      O(2) => \throughput_timer_reg[11]_i_1_n_5\,
      O(1) => \throughput_timer_reg[11]_i_1_n_6\,
      O(0) => \throughput_timer_reg[11]_i_1_n_7\,
      S(3) => \throughput_timer[11]_i_2_n_0\,
      S(2) => \throughput_timer[11]_i_3_n_0\,
      S(1) => \throughput_timer[11]_i_4_n_0\,
      S(0) => \throughput_timer[11]_i_5_n_0\
    );
\throughput_timer_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[15]_i_1_n_4\,
      Q => throughput_timer_reg(12)
    );
\throughput_timer_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[15]_i_1_n_5\,
      Q => throughput_timer_reg(13)
    );
\throughput_timer_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[15]_i_1_n_6\,
      Q => throughput_timer_reg(14)
    );
\throughput_timer_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[15]_i_1_n_7\,
      Q => throughput_timer_reg(15)
    );
\throughput_timer_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_timer_reg[19]_i_1_n_0\,
      CO(3) => \throughput_timer_reg[15]_i_1_n_0\,
      CO(2) => \throughput_timer_reg[15]_i_1_n_1\,
      CO(1) => \throughput_timer_reg[15]_i_1_n_2\,
      CO(0) => \throughput_timer_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \throughput_timer_reg[15]_i_1_n_4\,
      O(2) => \throughput_timer_reg[15]_i_1_n_5\,
      O(1) => \throughput_timer_reg[15]_i_1_n_6\,
      O(0) => \throughput_timer_reg[15]_i_1_n_7\,
      S(3) => \throughput_timer[15]_i_2_n_0\,
      S(2) => \throughput_timer[15]_i_3_n_0\,
      S(1) => \throughput_timer[15]_i_4_n_0\,
      S(0) => \throughput_timer[15]_i_5_n_0\
    );
\throughput_timer_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[19]_i_1_n_4\,
      Q => throughput_timer_reg(16)
    );
\throughput_timer_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[19]_i_1_n_5\,
      Q => throughput_timer_reg(17)
    );
\throughput_timer_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[19]_i_1_n_6\,
      Q => throughput_timer_reg(18)
    );
\throughput_timer_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[19]_i_1_n_7\,
      Q => throughput_timer_reg(19)
    );
\throughput_timer_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_timer_reg[23]_i_1_n_0\,
      CO(3) => \throughput_timer_reg[19]_i_1_n_0\,
      CO(2) => \throughput_timer_reg[19]_i_1_n_1\,
      CO(1) => \throughput_timer_reg[19]_i_1_n_2\,
      CO(0) => \throughput_timer_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \throughput_timer_reg[19]_i_1_n_4\,
      O(2) => \throughput_timer_reg[19]_i_1_n_5\,
      O(1) => \throughput_timer_reg[19]_i_1_n_6\,
      O(0) => \throughput_timer_reg[19]_i_1_n_7\,
      S(3) => \throughput_timer[19]_i_2_n_0\,
      S(2) => \throughput_timer[19]_i_3_n_0\,
      S(1) => \throughput_timer[19]_i_4_n_0\,
      S(0) => \throughput_timer[19]_i_5_n_0\
    );
\throughput_timer_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[3]_i_1_n_5\,
      Q => throughput_timer_reg(1)
    );
\throughput_timer_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[23]_i_1_n_4\,
      Q => throughput_timer_reg(20)
    );
\throughput_timer_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[23]_i_1_n_5\,
      Q => throughput_timer_reg(21)
    );
\throughput_timer_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[23]_i_1_n_6\,
      Q => throughput_timer_reg(22)
    );
\throughput_timer_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[23]_i_1_n_7\,
      Q => throughput_timer_reg(23)
    );
\throughput_timer_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_timer_reg[27]_i_1_n_0\,
      CO(3) => \throughput_timer_reg[23]_i_1_n_0\,
      CO(2) => \throughput_timer_reg[23]_i_1_n_1\,
      CO(1) => \throughput_timer_reg[23]_i_1_n_2\,
      CO(0) => \throughput_timer_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \throughput_timer_reg[23]_i_1_n_4\,
      O(2) => \throughput_timer_reg[23]_i_1_n_5\,
      O(1) => \throughput_timer_reg[23]_i_1_n_6\,
      O(0) => \throughput_timer_reg[23]_i_1_n_7\,
      S(3) => \throughput_timer[23]_i_2_n_0\,
      S(2) => \throughput_timer[23]_i_3_n_0\,
      S(1) => \throughput_timer[23]_i_4_n_0\,
      S(0) => \throughput_timer[23]_i_5_n_0\
    );
\throughput_timer_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[27]_i_1_n_4\,
      Q => throughput_timer_reg(24)
    );
\throughput_timer_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[27]_i_1_n_5\,
      Q => throughput_timer_reg(25)
    );
\throughput_timer_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[27]_i_1_n_6\,
      Q => throughput_timer_reg(26)
    );
\throughput_timer_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[27]_i_1_n_7\,
      Q => throughput_timer_reg(27)
    );
\throughput_timer_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_timer_reg[31]_i_2_n_0\,
      CO(3) => \throughput_timer_reg[27]_i_1_n_0\,
      CO(2) => \throughput_timer_reg[27]_i_1_n_1\,
      CO(1) => \throughput_timer_reg[27]_i_1_n_2\,
      CO(0) => \throughput_timer_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \throughput_timer_reg[27]_i_1_n_4\,
      O(2) => \throughput_timer_reg[27]_i_1_n_5\,
      O(1) => \throughput_timer_reg[27]_i_1_n_6\,
      O(0) => \throughput_timer_reg[27]_i_1_n_7\,
      S(3) => \throughput_timer[27]_i_2_n_0\,
      S(2) => \throughput_timer[27]_i_3_n_0\,
      S(1) => \throughput_timer[27]_i_4_n_0\,
      S(0) => \throughput_timer[27]_i_5_n_0\
    );
\throughput_timer_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[31]_i_2_n_4\,
      Q => throughput_timer_reg(28)
    );
\throughput_timer_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[31]_i_2_n_5\,
      Q => throughput_timer_reg(29)
    );
\throughput_timer_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[3]_i_1_n_6\,
      Q => throughput_timer_reg(2)
    );
\throughput_timer_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[31]_i_2_n_6\,
      Q => throughput_timer_reg(30)
    );
\throughput_timer_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[31]_i_2_n_7\,
      Q => throughput_timer_reg(31)
    );
\throughput_timer_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \throughput_timer_reg[31]_i_2_n_0\,
      CO(2) => \throughput_timer_reg[31]_i_2_n_1\,
      CO(1) => \throughput_timer_reg[31]_i_2_n_2\,
      CO(0) => \throughput_timer_reg[31]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \throughput_timer_reg[31]_i_2_n_4\,
      O(2) => \throughput_timer_reg[31]_i_2_n_5\,
      O(1) => \throughput_timer_reg[31]_i_2_n_6\,
      O(0) => \throughput_timer_reg[31]_i_2_n_7\,
      S(3) => \throughput_timer[31]_i_3_n_0\,
      S(2) => \throughput_timer[31]_i_4_n_0\,
      S(1) => \throughput_timer[31]_i_5_n_0\,
      S(0) => \throughput_timer[31]_i_6_n_0\
    );
\throughput_timer_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[3]_i_1_n_7\,
      Q => throughput_timer_reg(3)
    );
\throughput_timer_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_timer_reg[7]_i_1_n_0\,
      CO(3) => \NLW_throughput_timer_reg[3]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \throughput_timer_reg[3]_i_1_n_1\,
      CO(1) => \throughput_timer_reg[3]_i_1_n_2\,
      CO(0) => \throughput_timer_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \throughput_timer_reg[3]_i_1_n_4\,
      O(2) => \throughput_timer_reg[3]_i_1_n_5\,
      O(1) => \throughput_timer_reg[3]_i_1_n_6\,
      O(0) => \throughput_timer_reg[3]_i_1_n_7\,
      S(3) => \throughput_timer[3]_i_2_n_0\,
      S(2) => \throughput_timer[3]_i_3_n_0\,
      S(1) => \throughput_timer[3]_i_4_n_0\,
      S(0) => \throughput_timer[3]_i_5_n_0\
    );
\throughput_timer_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[7]_i_1_n_4\,
      Q => throughput_timer_reg(4)
    );
\throughput_timer_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[7]_i_1_n_5\,
      Q => throughput_timer_reg(5)
    );
\throughput_timer_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[7]_i_1_n_6\,
      Q => throughput_timer_reg(6)
    );
\throughput_timer_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[7]_i_1_n_7\,
      Q => throughput_timer_reg(7)
    );
\throughput_timer_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \throughput_timer_reg[11]_i_1_n_0\,
      CO(3) => \throughput_timer_reg[7]_i_1_n_0\,
      CO(2) => \throughput_timer_reg[7]_i_1_n_1\,
      CO(1) => \throughput_timer_reg[7]_i_1_n_2\,
      CO(0) => \throughput_timer_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \throughput_timer_reg[7]_i_1_n_4\,
      O(2) => \throughput_timer_reg[7]_i_1_n_5\,
      O(1) => \throughput_timer_reg[7]_i_1_n_6\,
      O(0) => \throughput_timer_reg[7]_i_1_n_7\,
      S(3) => \throughput_timer[7]_i_2_n_0\,
      S(2) => \throughput_timer[7]_i_3_n_0\,
      S(1) => \throughput_timer[7]_i_4_n_0\,
      S(0) => \throughput_timer[7]_i_5_n_0\
    );
\throughput_timer_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[11]_i_1_n_4\,
      Q => throughput_timer_reg(8)
    );
\throughput_timer_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => throughput_timer,
      CLR => rst,
      D => \throughput_timer_reg[11]_i_1_n_5\,
      Q => throughput_timer_reg(9)
    );
\troughput_state[0]_C_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FEFFFF00FE0000"
    )
        port map (
      I0 => \throughput_o[0]_i_4_n_0\,
      I1 => \throughput_o[0]_i_5_n_0\,
      I2 => \throughput_o[0]_i_6_n_0\,
      I3 => \troughput_state[0]_C_i_2_n_0\,
      I4 => \troughput_state[0]_C_i_3_n_0\,
      I5 => \troughput_state_reg[0]_C_n_0\,
      O => \troughput_state[0]_C_i_1_n_0\
    );
\troughput_state[0]_C_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0040FFFFFFFF"
    )
        port map (
      I0 => \troughput_state_reg[0]_C_n_0\,
      I1 => axiwvalid_i,
      I2 => axiwready_i,
      I3 => \troughput_state_reg[1]_C_n_0\,
      I4 => reset,
      I5 => axi_reset,
      O => \troughput_state[0]_C_i_2_n_0\
    );
\troughput_state[0]_C_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00EA"
    )
        port map (
      I0 => \troughput_state_reg[1]_C_n_0\,
      I1 => axiwready_i,
      I2 => axiwvalid_i,
      I3 => \troughput_state_reg[0]_C_n_0\,
      O => \troughput_state[0]_C_i_3_n_0\
    );
\troughput_state[1]_C_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF0200000202FFFF"
    )
        port map (
      I0 => \troughput_state[1]_C_i_2_n_0\,
      I1 => \throughput_o[0]_i_6_n_0\,
      I2 => \troughput_state[1]_C_i_3_n_0\,
      I3 => \troughput_state_reg[0]_C_n_0\,
      I4 => \troughput_state_reg[1]_C_n_0\,
      I5 => \troughput_state[1]_C_i_4_n_0\,
      O => \troughput_state[1]_C_i_1_n_0\
    );
\troughput_state[1]_C_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => axi_reset,
      I1 => reset,
      O => \troughput_state[1]_C_i_2_n_0\
    );
\troughput_state[1]_C_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \troughput_state[1]_C_i_5_n_0\,
      I1 => \throughput_o[0]_i_9_n_0\,
      I2 => \throughput_o[0]_i_8_n_0\,
      I3 => \troughput_state[1]_C_i_6_n_0\,
      I4 => \throughput_o[0]_i_13_n_0\,
      I5 => \throughput_o[0]_i_12_n_0\,
      O => \troughput_state[1]_C_i_3_n_0\
    );
\troughput_state[1]_C_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \troughput_state_reg[0]_C_n_0\,
      I1 => axiwvalid_i,
      I2 => axiwready_i,
      O => \troughput_state[1]_C_i_4_n_0\
    );
\troughput_state[1]_C_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => p_0_in(14),
      I1 => p_0_in(19),
      I2 => p_0_in(15),
      O => \troughput_state[1]_C_i_5_n_0\
    );
\troughput_state[1]_C_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => p_0_in(29),
      I1 => p_0_in(16),
      I2 => p_0_in(30),
      I3 => p_0_in(27),
      O => \troughput_state[1]_C_i_6_n_0\
    );
\troughput_state_reg[0]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => rst,
      D => \troughput_state[0]_C_i_1_n_0\,
      Q => \troughput_state_reg[0]_C_n_0\
    );
\troughput_state_reg[1]_C\: unisim.vcomponents.FDCE
     port map (
      C => clk,
      CE => '1',
      CLR => rst,
      D => \troughput_state[1]_C_i_1_n_0\,
      Q => \troughput_state_reg[1]_C_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    axiwvalid_i : in STD_LOGIC;
    axiwready_i : in STD_LOGIC;
    packet_arived_i : in STD_LOGIC;
    latency_o : out STD_LOGIC_VECTOR ( 0 to 31 );
    throughput_o : out STD_LOGIC_VECTOR ( 0 to 31 );
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    axi_reset : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_lisnoc_probe_0_0,lisnoc_probe,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "lisnoc_probe,Vivado 2016.4";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_lisnoc_probe
     port map (
      axi_reset => axi_reset,
      axiwready_i => axiwready_i,
      axiwvalid_i => axiwvalid_i,
      clk => clk,
      latency_o(0 to 31) => latency_o(0 to 31),
      packet_arived_i => packet_arived_i,
      reset => reset,
      throughput_o(0 to 31) => throughput_o(0 to 31)
    );
end STRUCTURE;
