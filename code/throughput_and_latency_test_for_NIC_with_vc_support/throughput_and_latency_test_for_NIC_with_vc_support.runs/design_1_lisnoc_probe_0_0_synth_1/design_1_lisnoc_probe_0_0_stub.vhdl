-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (lin64) Build 1733598 Wed Dec 14 22:35:42 MST 2016
-- Date        : Wed Aug  7 10:19:48 2019
-- Host        : zynq running 64-bit Ubuntu 14.04.5 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_lisnoc_probe_0_0_stub.vhdl
-- Design      : design_1_lisnoc_probe_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    axiwvalid_i : in STD_LOGIC;
    axiwready_i : in STD_LOGIC;
    packet_arived_i : in STD_LOGIC;
    latency_o : out STD_LOGIC_VECTOR ( 0 to 31 );
    throughput_o : out STD_LOGIC_VECTOR ( 0 to 31 );
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    axi_reset : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "axiwvalid_i,axiwready_i,packet_arived_i,latency_o[0:31],throughput_o[0:31],clk,reset,axi_reset";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "lisnoc_probe,Vivado 2016.4";
begin
end;
