// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1733598 Wed Dec 14 22:35:42 MST 2016
// Date        : Wed Aug  7 10:19:46 2019
// Host        : zynq running 64-bit Ubuntu 14.04.5 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_lisnoc_probe_0_0_stub.v
// Design      : design_1_lisnoc_probe_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "lisnoc_probe,Vivado 2016.4" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(axiwvalid_i, axiwready_i, packet_arived_i, 
  latency_o, throughput_o, clk, reset, axi_reset)
/* synthesis syn_black_box black_box_pad_pin="axiwvalid_i,axiwready_i,packet_arived_i,latency_o[0:31],throughput_o[0:31],clk,reset,axi_reset" */;
  input axiwvalid_i;
  input axiwready_i;
  input packet_arived_i;
  output [0:31]latency_o;
  output [0:31]throughput_o;
  input clk;
  input reset;
  input axi_reset;
endmodule
