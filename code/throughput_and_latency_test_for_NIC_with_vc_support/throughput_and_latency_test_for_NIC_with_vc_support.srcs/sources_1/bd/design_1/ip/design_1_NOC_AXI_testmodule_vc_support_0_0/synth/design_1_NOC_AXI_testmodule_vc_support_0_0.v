// (c) Copyright 1995-2019 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: USiegen:LISNoC:NOC_AXI_testmodule_vc_support:1.0
// IP Revision: 2

(* X_CORE_INFO = "NOC_AXI_testmodule,Vivado 2016.4" *)
(* CHECK_LICENSE_TYPE = "design_1_NOC_AXI_testmodule_vc_support_0_0,NOC_AXI_testmodule,{}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_1_NOC_AXI_testmodule_vc_support_0_0 (
  S0_packet_arrived_o,
  S1_packet_arrived_o,
  S2_packet_arrived_o,
  S3_packet_arrived_o,
  s00_axi_aclk,
  s00_axi_aresetn,
  s00_axi_awid,
  s00_axi_awaddr,
  s00_axi_awlen,
  s00_axi_awsize,
  s00_axi_awburst,
  s00_axi_awlock,
  s00_axi_awcache,
  s00_axi_awprot,
  s00_axi_awqos,
  s00_axi_awregion,
  s00_axi_awuser,
  s00_axi_awvalid,
  s00_axi_awready,
  s00_axi_wdata,
  s00_axi_wstrb,
  s00_axi_wlast,
  s00_axi_wuser,
  s00_axi_wvalid,
  s00_axi_wready,
  s00_axi_bid,
  s00_axi_bresp,
  s00_axi_buser,
  s00_axi_bvalid,
  s00_axi_bready,
  s00_axi_arid,
  s00_axi_araddr,
  s00_axi_arlen,
  s00_axi_arsize,
  s00_axi_arburst,
  s00_axi_arlock,
  s00_axi_arcache,
  s00_axi_arprot,
  s00_axi_arqos,
  s00_axi_arregion,
  s00_axi_aruser,
  s00_axi_arvalid,
  s00_axi_arready,
  s00_axi_rid,
  s00_axi_rdata,
  s00_axi_rresp,
  s00_axi_rlast,
  s00_axi_ruser,
  s00_axi_rvalid,
  s00_axi_rready,
  s01_axi_aclk,
  s01_axi_aresetn,
  s01_axi_awid,
  s01_axi_awaddr,
  s01_axi_awlen,
  s01_axi_awsize,
  s01_axi_awburst,
  s01_axi_awlock,
  s01_axi_awcache,
  s01_axi_awprot,
  s01_axi_awqos,
  s01_axi_awregion,
  s01_axi_awuser,
  s01_axi_awvalid,
  s01_axi_awready,
  s01_axi_wdata,
  s01_axi_wstrb,
  s01_axi_wlast,
  s01_axi_wuser,
  s01_axi_wvalid,
  s01_axi_wready,
  s01_axi_bid,
  s01_axi_bresp,
  s01_axi_buser,
  s01_axi_bvalid,
  s01_axi_bready,
  s01_axi_arid,
  s01_axi_araddr,
  s01_axi_arlen,
  s01_axi_arsize,
  s01_axi_arburst,
  s01_axi_arlock,
  s01_axi_arcache,
  s01_axi_arprot,
  s01_axi_arqos,
  s01_axi_arregion,
  s01_axi_aruser,
  s01_axi_arvalid,
  s01_axi_arready,
  s01_axi_rid,
  s01_axi_rdata,
  s01_axi_rresp,
  s01_axi_rlast,
  s01_axi_ruser,
  s01_axi_rvalid,
  s01_axi_rready,
  s02_axi_aclk,
  s02_axi_aresetn,
  s02_axi_awid,
  s02_axi_awaddr,
  s02_axi_awlen,
  s02_axi_awsize,
  s02_axi_awburst,
  s02_axi_awlock,
  s02_axi_awcache,
  s02_axi_awprot,
  s02_axi_awqos,
  s02_axi_awregion,
  s02_axi_awuser,
  s02_axi_awvalid,
  s02_axi_awready,
  s02_axi_wdata,
  s02_axi_wstrb,
  s02_axi_wlast,
  s02_axi_wuser,
  s02_axi_wvalid,
  s02_axi_wready,
  s02_axi_bid,
  s02_axi_bresp,
  s02_axi_buser,
  s02_axi_bvalid,
  s02_axi_bready,
  s02_axi_arid,
  s02_axi_araddr,
  s02_axi_arlen,
  s02_axi_arsize,
  s02_axi_arburst,
  s02_axi_arlock,
  s02_axi_arcache,
  s02_axi_arprot,
  s02_axi_arqos,
  s02_axi_arregion,
  s02_axi_aruser,
  s02_axi_arvalid,
  s02_axi_arready,
  s02_axi_rid,
  s02_axi_rdata,
  s02_axi_rresp,
  s02_axi_rlast,
  s02_axi_ruser,
  s02_axi_rvalid,
  s02_axi_rready,
  s03_axi_aclk,
  s03_axi_aresetn,
  s03_axi_awid,
  s03_axi_awaddr,
  s03_axi_awlen,
  s03_axi_awsize,
  s03_axi_awburst,
  s03_axi_awlock,
  s03_axi_awcache,
  s03_axi_awprot,
  s03_axi_awqos,
  s03_axi_awregion,
  s03_axi_awuser,
  s03_axi_awvalid,
  s03_axi_awready,
  s03_axi_wdata,
  s03_axi_wstrb,
  s03_axi_wlast,
  s03_axi_wuser,
  s03_axi_wvalid,
  s03_axi_wready,
  s03_axi_bid,
  s03_axi_bresp,
  s03_axi_buser,
  s03_axi_bvalid,
  s03_axi_bready,
  s03_axi_arid,
  s03_axi_araddr,
  s03_axi_arlen,
  s03_axi_arsize,
  s03_axi_arburst,
  s03_axi_arlock,
  s03_axi_arcache,
  s03_axi_arprot,
  s03_axi_arqos,
  s03_axi_arregion,
  s03_axi_aruser,
  s03_axi_arvalid,
  s03_axi_arready,
  s03_axi_rid,
  s03_axi_rdata,
  s03_axi_rresp,
  s03_axi_rlast,
  s03_axi_ruser,
  s03_axi_rvalid,
  s03_axi_rready
);

output wire [3 : 0] S0_packet_arrived_o;
output wire [3 : 0] S1_packet_arrived_o;
output wire [3 : 0] S2_packet_arrived_o;
output wire [3 : 0] S3_packet_arrived_o;
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s00_axi_aclk CLK" *)
input wire s00_axi_aclk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST" *)
input wire s00_axi_aresetn;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWID" *)
input wire [11 : 0] s00_axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWADDR" *)
input wire [15 : 0] s00_axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWLEN" *)
input wire [7 : 0] s00_axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWSIZE" *)
input wire [2 : 0] s00_axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWBURST" *)
input wire [1 : 0] s00_axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWLOCK" *)
input wire s00_axi_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWCACHE" *)
input wire [3 : 0] s00_axi_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWPROT" *)
input wire [2 : 0] s00_axi_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWQOS" *)
input wire [3 : 0] s00_axi_awqos;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWREGION" *)
input wire [3 : 0] s00_axi_awregion;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWUSER" *)
input wire [0 : 0] s00_axi_awuser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWVALID" *)
input wire s00_axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi AWREADY" *)
output wire s00_axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi WDATA" *)
input wire [31 : 0] s00_axi_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi WSTRB" *)
input wire [3 : 0] s00_axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi WLAST" *)
input wire s00_axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi WUSER" *)
input wire [0 : 0] s00_axi_wuser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi WVALID" *)
input wire s00_axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi WREADY" *)
output wire s00_axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi BID" *)
output wire [11 : 0] s00_axi_bid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi BRESP" *)
output wire [1 : 0] s00_axi_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi BUSER" *)
output wire [0 : 0] s00_axi_buser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi BVALID" *)
output wire s00_axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi BREADY" *)
input wire s00_axi_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARID" *)
input wire [11 : 0] s00_axi_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARADDR" *)
input wire [15 : 0] s00_axi_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARLEN" *)
input wire [7 : 0] s00_axi_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARSIZE" *)
input wire [2 : 0] s00_axi_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARBURST" *)
input wire [1 : 0] s00_axi_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARLOCK" *)
input wire s00_axi_arlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARCACHE" *)
input wire [3 : 0] s00_axi_arcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARPROT" *)
input wire [2 : 0] s00_axi_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARQOS" *)
input wire [3 : 0] s00_axi_arqos;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARREGION" *)
input wire [3 : 0] s00_axi_arregion;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARUSER" *)
input wire [0 : 0] s00_axi_aruser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARVALID" *)
input wire s00_axi_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi ARREADY" *)
output wire s00_axi_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi RID" *)
output wire [11 : 0] s00_axi_rid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi RDATA" *)
output wire [31 : 0] s00_axi_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi RRESP" *)
output wire [1 : 0] s00_axi_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi RLAST" *)
output wire s00_axi_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi RUSER" *)
output wire [0 : 0] s00_axi_ruser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi RVALID" *)
output wire s00_axi_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s00_axi RREADY" *)
input wire s00_axi_rready;
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s01_axi_aclk CLK" *)
input wire s01_axi_aclk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s01_axi_aresetn RST" *)
input wire s01_axi_aresetn;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWID" *)
input wire [11 : 0] s01_axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWADDR" *)
input wire [15 : 0] s01_axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWLEN" *)
input wire [7 : 0] s01_axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWSIZE" *)
input wire [2 : 0] s01_axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWBURST" *)
input wire [1 : 0] s01_axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWLOCK" *)
input wire s01_axi_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWCACHE" *)
input wire [3 : 0] s01_axi_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWPROT" *)
input wire [2 : 0] s01_axi_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWQOS" *)
input wire [3 : 0] s01_axi_awqos;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWREGION" *)
input wire [3 : 0] s01_axi_awregion;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWUSER" *)
input wire [0 : 0] s01_axi_awuser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWVALID" *)
input wire s01_axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi AWREADY" *)
output wire s01_axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi WDATA" *)
input wire [31 : 0] s01_axi_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi WSTRB" *)
input wire [3 : 0] s01_axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi WLAST" *)
input wire s01_axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi WUSER" *)
input wire [0 : 0] s01_axi_wuser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi WVALID" *)
input wire s01_axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi WREADY" *)
output wire s01_axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi BID" *)
output wire [11 : 0] s01_axi_bid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi BRESP" *)
output wire [1 : 0] s01_axi_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi BUSER" *)
output wire [0 : 0] s01_axi_buser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi BVALID" *)
output wire s01_axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi BREADY" *)
input wire s01_axi_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARID" *)
input wire [11 : 0] s01_axi_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARADDR" *)
input wire [15 : 0] s01_axi_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARLEN" *)
input wire [7 : 0] s01_axi_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARSIZE" *)
input wire [2 : 0] s01_axi_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARBURST" *)
input wire [1 : 0] s01_axi_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARLOCK" *)
input wire s01_axi_arlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARCACHE" *)
input wire [3 : 0] s01_axi_arcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARPROT" *)
input wire [2 : 0] s01_axi_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARQOS" *)
input wire [3 : 0] s01_axi_arqos;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARREGION" *)
input wire [3 : 0] s01_axi_arregion;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARUSER" *)
input wire [0 : 0] s01_axi_aruser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARVALID" *)
input wire s01_axi_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi ARREADY" *)
output wire s01_axi_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi RID" *)
output wire [11 : 0] s01_axi_rid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi RDATA" *)
output wire [31 : 0] s01_axi_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi RRESP" *)
output wire [1 : 0] s01_axi_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi RLAST" *)
output wire s01_axi_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi RUSER" *)
output wire [0 : 0] s01_axi_ruser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi RVALID" *)
output wire s01_axi_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s01_axi RREADY" *)
input wire s01_axi_rready;
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s02_axi_aclk CLK" *)
input wire s02_axi_aclk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s02_axi_aresetn RST" *)
input wire s02_axi_aresetn;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWID" *)
input wire [11 : 0] s02_axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWADDR" *)
input wire [15 : 0] s02_axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWLEN" *)
input wire [7 : 0] s02_axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWSIZE" *)
input wire [2 : 0] s02_axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWBURST" *)
input wire [1 : 0] s02_axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWLOCK" *)
input wire s02_axi_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWCACHE" *)
input wire [3 : 0] s02_axi_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWPROT" *)
input wire [2 : 0] s02_axi_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWQOS" *)
input wire [3 : 0] s02_axi_awqos;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWREGION" *)
input wire [3 : 0] s02_axi_awregion;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWUSER" *)
input wire [0 : 0] s02_axi_awuser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWVALID" *)
input wire s02_axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi AWREADY" *)
output wire s02_axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi WDATA" *)
input wire [31 : 0] s02_axi_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi WSTRB" *)
input wire [3 : 0] s02_axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi WLAST" *)
input wire s02_axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi WUSER" *)
input wire [0 : 0] s02_axi_wuser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi WVALID" *)
input wire s02_axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi WREADY" *)
output wire s02_axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi BID" *)
output wire [11 : 0] s02_axi_bid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi BRESP" *)
output wire [1 : 0] s02_axi_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi BUSER" *)
output wire [0 : 0] s02_axi_buser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi BVALID" *)
output wire s02_axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi BREADY" *)
input wire s02_axi_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARID" *)
input wire [11 : 0] s02_axi_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARADDR" *)
input wire [15 : 0] s02_axi_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARLEN" *)
input wire [7 : 0] s02_axi_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARSIZE" *)
input wire [2 : 0] s02_axi_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARBURST" *)
input wire [1 : 0] s02_axi_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARLOCK" *)
input wire s02_axi_arlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARCACHE" *)
input wire [3 : 0] s02_axi_arcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARPROT" *)
input wire [2 : 0] s02_axi_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARQOS" *)
input wire [3 : 0] s02_axi_arqos;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARREGION" *)
input wire [3 : 0] s02_axi_arregion;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARUSER" *)
input wire [0 : 0] s02_axi_aruser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARVALID" *)
input wire s02_axi_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi ARREADY" *)
output wire s02_axi_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi RID" *)
output wire [11 : 0] s02_axi_rid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi RDATA" *)
output wire [31 : 0] s02_axi_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi RRESP" *)
output wire [1 : 0] s02_axi_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi RLAST" *)
output wire s02_axi_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi RUSER" *)
output wire [0 : 0] s02_axi_ruser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi RVALID" *)
output wire s02_axi_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s02_axi RREADY" *)
input wire s02_axi_rready;
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s03_axi_aclk CLK" *)
input wire s03_axi_aclk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s03_axi_aresetn RST" *)
input wire s03_axi_aresetn;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWID" *)
input wire [11 : 0] s03_axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWADDR" *)
input wire [15 : 0] s03_axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWLEN" *)
input wire [7 : 0] s03_axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWSIZE" *)
input wire [2 : 0] s03_axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWBURST" *)
input wire [1 : 0] s03_axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWLOCK" *)
input wire s03_axi_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWCACHE" *)
input wire [3 : 0] s03_axi_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWPROT" *)
input wire [2 : 0] s03_axi_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWQOS" *)
input wire [3 : 0] s03_axi_awqos;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWREGION" *)
input wire [3 : 0] s03_axi_awregion;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWUSER" *)
input wire [0 : 0] s03_axi_awuser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWVALID" *)
input wire s03_axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi AWREADY" *)
output wire s03_axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi WDATA" *)
input wire [31 : 0] s03_axi_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi WSTRB" *)
input wire [3 : 0] s03_axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi WLAST" *)
input wire s03_axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi WUSER" *)
input wire [0 : 0] s03_axi_wuser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi WVALID" *)
input wire s03_axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi WREADY" *)
output wire s03_axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi BID" *)
output wire [11 : 0] s03_axi_bid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi BRESP" *)
output wire [1 : 0] s03_axi_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi BUSER" *)
output wire [0 : 0] s03_axi_buser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi BVALID" *)
output wire s03_axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi BREADY" *)
input wire s03_axi_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARID" *)
input wire [11 : 0] s03_axi_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARADDR" *)
input wire [15 : 0] s03_axi_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARLEN" *)
input wire [7 : 0] s03_axi_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARSIZE" *)
input wire [2 : 0] s03_axi_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARBURST" *)
input wire [1 : 0] s03_axi_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARLOCK" *)
input wire s03_axi_arlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARCACHE" *)
input wire [3 : 0] s03_axi_arcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARPROT" *)
input wire [2 : 0] s03_axi_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARQOS" *)
input wire [3 : 0] s03_axi_arqos;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARREGION" *)
input wire [3 : 0] s03_axi_arregion;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARUSER" *)
input wire [0 : 0] s03_axi_aruser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARVALID" *)
input wire s03_axi_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi ARREADY" *)
output wire s03_axi_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi RID" *)
output wire [11 : 0] s03_axi_rid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi RDATA" *)
output wire [31 : 0] s03_axi_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi RRESP" *)
output wire [1 : 0] s03_axi_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi RLAST" *)
output wire s03_axi_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi RUSER" *)
output wire [0 : 0] s03_axi_ruser;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi RVALID" *)
output wire s03_axi_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s03_axi RREADY" *)
input wire s03_axi_rready;

  NOC_AXI_testmodule #(
    .flit_data_width(32),
    .flit_type_width(2),
    .ph_dest_width(4),
    .vchannels(4),
    .in_fifo_length(4),
    .out_fifo_length(4),
    .vchannel_select_lefoffset(4),
    .vchannel_select_bits(2),
    .C_S00_AXI_ID_WIDTH(12),
    .C_S00_AXI_DATA_WIDTH(32),
    .C_S00_AXI_ADDR_WIDTH(16),
    .C_S00_AXI_AWUSER_WIDTH(0),
    .C_S00_AXI_ARUSER_WIDTH(0),
    .C_S00_AXI_WUSER_WIDTH(0),
    .C_S00_AXI_RUSER_WIDTH(0),
    .C_S00_AXI_BUSER_WIDTH(0),
    .C_S01_AXI_ID_WIDTH(12),
    .C_S01_AXI_DATA_WIDTH(32),
    .C_S01_AXI_ADDR_WIDTH(16),
    .C_S01_AXI_AWUSER_WIDTH(0),
    .C_S01_AXI_ARUSER_WIDTH(0),
    .C_S01_AXI_WUSER_WIDTH(0),
    .C_S01_AXI_RUSER_WIDTH(0),
    .C_S01_AXI_BUSER_WIDTH(0),
    .C_S02_AXI_ID_WIDTH(12),
    .C_S02_AXI_DATA_WIDTH(32),
    .C_S02_AXI_ADDR_WIDTH(16),
    .C_S02_AXI_AWUSER_WIDTH(0),
    .C_S02_AXI_ARUSER_WIDTH(0),
    .C_S02_AXI_WUSER_WIDTH(0),
    .C_S02_AXI_RUSER_WIDTH(0),
    .C_S02_AXI_BUSER_WIDTH(0),
    .C_S03_AXI_ID_WIDTH(12),
    .C_S03_AXI_DATA_WIDTH(32),
    .C_S03_AXI_ADDR_WIDTH(16),
    .C_S03_AXI_AWUSER_WIDTH(0),
    .C_S03_AXI_ARUSER_WIDTH(0),
    .C_S03_AXI_WUSER_WIDTH(0),
    .C_S03_AXI_RUSER_WIDTH(0),
    .C_S03_AXI_BUSER_WIDTH(0)
  ) inst (
    .S0_packet_arrived_o(S0_packet_arrived_o),
    .S1_packet_arrived_o(S1_packet_arrived_o),
    .S2_packet_arrived_o(S2_packet_arrived_o),
    .S3_packet_arrived_o(S3_packet_arrived_o),
    .s00_axi_aclk(s00_axi_aclk),
    .s00_axi_aresetn(s00_axi_aresetn),
    .s00_axi_awid(s00_axi_awid),
    .s00_axi_awaddr(s00_axi_awaddr),
    .s00_axi_awlen(s00_axi_awlen),
    .s00_axi_awsize(s00_axi_awsize),
    .s00_axi_awburst(s00_axi_awburst),
    .s00_axi_awlock(s00_axi_awlock),
    .s00_axi_awcache(s00_axi_awcache),
    .s00_axi_awprot(s00_axi_awprot),
    .s00_axi_awqos(s00_axi_awqos),
    .s00_axi_awregion(s00_axi_awregion),
    .s00_axi_awuser(s00_axi_awuser),
    .s00_axi_awvalid(s00_axi_awvalid),
    .s00_axi_awready(s00_axi_awready),
    .s00_axi_wdata(s00_axi_wdata),
    .s00_axi_wstrb(s00_axi_wstrb),
    .s00_axi_wlast(s00_axi_wlast),
    .s00_axi_wuser(s00_axi_wuser),
    .s00_axi_wvalid(s00_axi_wvalid),
    .s00_axi_wready(s00_axi_wready),
    .s00_axi_bid(s00_axi_bid),
    .s00_axi_bresp(s00_axi_bresp),
    .s00_axi_buser(s00_axi_buser),
    .s00_axi_bvalid(s00_axi_bvalid),
    .s00_axi_bready(s00_axi_bready),
    .s00_axi_arid(s00_axi_arid),
    .s00_axi_araddr(s00_axi_araddr),
    .s00_axi_arlen(s00_axi_arlen),
    .s00_axi_arsize(s00_axi_arsize),
    .s00_axi_arburst(s00_axi_arburst),
    .s00_axi_arlock(s00_axi_arlock),
    .s00_axi_arcache(s00_axi_arcache),
    .s00_axi_arprot(s00_axi_arprot),
    .s00_axi_arqos(s00_axi_arqos),
    .s00_axi_arregion(s00_axi_arregion),
    .s00_axi_aruser(s00_axi_aruser),
    .s00_axi_arvalid(s00_axi_arvalid),
    .s00_axi_arready(s00_axi_arready),
    .s00_axi_rid(s00_axi_rid),
    .s00_axi_rdata(s00_axi_rdata),
    .s00_axi_rresp(s00_axi_rresp),
    .s00_axi_rlast(s00_axi_rlast),
    .s00_axi_ruser(s00_axi_ruser),
    .s00_axi_rvalid(s00_axi_rvalid),
    .s00_axi_rready(s00_axi_rready),
    .s01_axi_aclk(s01_axi_aclk),
    .s01_axi_aresetn(s01_axi_aresetn),
    .s01_axi_awid(s01_axi_awid),
    .s01_axi_awaddr(s01_axi_awaddr),
    .s01_axi_awlen(s01_axi_awlen),
    .s01_axi_awsize(s01_axi_awsize),
    .s01_axi_awburst(s01_axi_awburst),
    .s01_axi_awlock(s01_axi_awlock),
    .s01_axi_awcache(s01_axi_awcache),
    .s01_axi_awprot(s01_axi_awprot),
    .s01_axi_awqos(s01_axi_awqos),
    .s01_axi_awregion(s01_axi_awregion),
    .s01_axi_awuser(s01_axi_awuser),
    .s01_axi_awvalid(s01_axi_awvalid),
    .s01_axi_awready(s01_axi_awready),
    .s01_axi_wdata(s01_axi_wdata),
    .s01_axi_wstrb(s01_axi_wstrb),
    .s01_axi_wlast(s01_axi_wlast),
    .s01_axi_wuser(s01_axi_wuser),
    .s01_axi_wvalid(s01_axi_wvalid),
    .s01_axi_wready(s01_axi_wready),
    .s01_axi_bid(s01_axi_bid),
    .s01_axi_bresp(s01_axi_bresp),
    .s01_axi_buser(s01_axi_buser),
    .s01_axi_bvalid(s01_axi_bvalid),
    .s01_axi_bready(s01_axi_bready),
    .s01_axi_arid(s01_axi_arid),
    .s01_axi_araddr(s01_axi_araddr),
    .s01_axi_arlen(s01_axi_arlen),
    .s01_axi_arsize(s01_axi_arsize),
    .s01_axi_arburst(s01_axi_arburst),
    .s01_axi_arlock(s01_axi_arlock),
    .s01_axi_arcache(s01_axi_arcache),
    .s01_axi_arprot(s01_axi_arprot),
    .s01_axi_arqos(s01_axi_arqos),
    .s01_axi_arregion(s01_axi_arregion),
    .s01_axi_aruser(s01_axi_aruser),
    .s01_axi_arvalid(s01_axi_arvalid),
    .s01_axi_arready(s01_axi_arready),
    .s01_axi_rid(s01_axi_rid),
    .s01_axi_rdata(s01_axi_rdata),
    .s01_axi_rresp(s01_axi_rresp),
    .s01_axi_rlast(s01_axi_rlast),
    .s01_axi_ruser(s01_axi_ruser),
    .s01_axi_rvalid(s01_axi_rvalid),
    .s01_axi_rready(s01_axi_rready),
    .s02_axi_aclk(s02_axi_aclk),
    .s02_axi_aresetn(s02_axi_aresetn),
    .s02_axi_awid(s02_axi_awid),
    .s02_axi_awaddr(s02_axi_awaddr),
    .s02_axi_awlen(s02_axi_awlen),
    .s02_axi_awsize(s02_axi_awsize),
    .s02_axi_awburst(s02_axi_awburst),
    .s02_axi_awlock(s02_axi_awlock),
    .s02_axi_awcache(s02_axi_awcache),
    .s02_axi_awprot(s02_axi_awprot),
    .s02_axi_awqos(s02_axi_awqos),
    .s02_axi_awregion(s02_axi_awregion),
    .s02_axi_awuser(s02_axi_awuser),
    .s02_axi_awvalid(s02_axi_awvalid),
    .s02_axi_awready(s02_axi_awready),
    .s02_axi_wdata(s02_axi_wdata),
    .s02_axi_wstrb(s02_axi_wstrb),
    .s02_axi_wlast(s02_axi_wlast),
    .s02_axi_wuser(s02_axi_wuser),
    .s02_axi_wvalid(s02_axi_wvalid),
    .s02_axi_wready(s02_axi_wready),
    .s02_axi_bid(s02_axi_bid),
    .s02_axi_bresp(s02_axi_bresp),
    .s02_axi_buser(s02_axi_buser),
    .s02_axi_bvalid(s02_axi_bvalid),
    .s02_axi_bready(s02_axi_bready),
    .s02_axi_arid(s02_axi_arid),
    .s02_axi_araddr(s02_axi_araddr),
    .s02_axi_arlen(s02_axi_arlen),
    .s02_axi_arsize(s02_axi_arsize),
    .s02_axi_arburst(s02_axi_arburst),
    .s02_axi_arlock(s02_axi_arlock),
    .s02_axi_arcache(s02_axi_arcache),
    .s02_axi_arprot(s02_axi_arprot),
    .s02_axi_arqos(s02_axi_arqos),
    .s02_axi_arregion(s02_axi_arregion),
    .s02_axi_aruser(s02_axi_aruser),
    .s02_axi_arvalid(s02_axi_arvalid),
    .s02_axi_arready(s02_axi_arready),
    .s02_axi_rid(s02_axi_rid),
    .s02_axi_rdata(s02_axi_rdata),
    .s02_axi_rresp(s02_axi_rresp),
    .s02_axi_rlast(s02_axi_rlast),
    .s02_axi_ruser(s02_axi_ruser),
    .s02_axi_rvalid(s02_axi_rvalid),
    .s02_axi_rready(s02_axi_rready),
    .s03_axi_aclk(s03_axi_aclk),
    .s03_axi_aresetn(s03_axi_aresetn),
    .s03_axi_awid(s03_axi_awid),
    .s03_axi_awaddr(s03_axi_awaddr),
    .s03_axi_awlen(s03_axi_awlen),
    .s03_axi_awsize(s03_axi_awsize),
    .s03_axi_awburst(s03_axi_awburst),
    .s03_axi_awlock(s03_axi_awlock),
    .s03_axi_awcache(s03_axi_awcache),
    .s03_axi_awprot(s03_axi_awprot),
    .s03_axi_awqos(s03_axi_awqos),
    .s03_axi_awregion(s03_axi_awregion),
    .s03_axi_awuser(s03_axi_awuser),
    .s03_axi_awvalid(s03_axi_awvalid),
    .s03_axi_awready(s03_axi_awready),
    .s03_axi_wdata(s03_axi_wdata),
    .s03_axi_wstrb(s03_axi_wstrb),
    .s03_axi_wlast(s03_axi_wlast),
    .s03_axi_wuser(s03_axi_wuser),
    .s03_axi_wvalid(s03_axi_wvalid),
    .s03_axi_wready(s03_axi_wready),
    .s03_axi_bid(s03_axi_bid),
    .s03_axi_bresp(s03_axi_bresp),
    .s03_axi_buser(s03_axi_buser),
    .s03_axi_bvalid(s03_axi_bvalid),
    .s03_axi_bready(s03_axi_bready),
    .s03_axi_arid(s03_axi_arid),
    .s03_axi_araddr(s03_axi_araddr),
    .s03_axi_arlen(s03_axi_arlen),
    .s03_axi_arsize(s03_axi_arsize),
    .s03_axi_arburst(s03_axi_arburst),
    .s03_axi_arlock(s03_axi_arlock),
    .s03_axi_arcache(s03_axi_arcache),
    .s03_axi_arprot(s03_axi_arprot),
    .s03_axi_arqos(s03_axi_arqos),
    .s03_axi_arregion(s03_axi_arregion),
    .s03_axi_aruser(s03_axi_aruser),
    .s03_axi_arvalid(s03_axi_arvalid),
    .s03_axi_arready(s03_axi_arready),
    .s03_axi_rid(s03_axi_rid),
    .s03_axi_rdata(s03_axi_rdata),
    .s03_axi_rresp(s03_axi_rresp),
    .s03_axi_rlast(s03_axi_rlast),
    .s03_axi_ruser(s03_axi_ruser),
    .s03_axi_rvalid(s03_axi_rvalid),
    .s03_axi_rready(s03_axi_rready)
  );
endmodule
