// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1733598 Wed Dec 14 22:35:42 MST 2016
// Date        : Wed Aug  7 10:19:51 2019
// Host        : zynq running 64-bit Ubuntu 14.04.5 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/jhippe/repos/lisnoc/code/throughput_and_latency_test_for_NIC_with_vc_support/throughput_and_latency_test_for_NIC_with_vc_support.srcs/sources_1/bd/design_1/ip/design_1_lisnoc_probe_0_0/design_1_lisnoc_probe_0_0_sim_netlist.v
// Design      : design_1_lisnoc_probe_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_lisnoc_probe_0_0,lisnoc_probe,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "lisnoc_probe,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module design_1_lisnoc_probe_0_0
   (axiwvalid_i,
    axiwready_i,
    packet_arived_i,
    latency_o,
    throughput_o,
    clk,
    reset,
    axi_reset);
  input axiwvalid_i;
  input axiwready_i;
  input packet_arived_i;
  output [0:31]latency_o;
  output [0:31]throughput_o;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) input clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 reset RST" *) input reset;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 axi_reset RST" *) input axi_reset;

  wire axi_reset;
  wire axiwready_i;
  wire axiwvalid_i;
  wire clk;
  wire [0:31]latency_o;
  wire packet_arived_i;
  wire reset;
  wire [0:31]throughput_o;

  design_1_lisnoc_probe_0_0_lisnoc_probe inst
       (.axi_reset(axi_reset),
        .axiwready_i(axiwready_i),
        .axiwvalid_i(axiwvalid_i),
        .clk(clk),
        .latency_o(latency_o),
        .packet_arived_i(packet_arived_i),
        .reset(reset),
        .throughput_o(throughput_o));
endmodule

(* ORIG_REF_NAME = "lisnoc_probe" *) 
module design_1_lisnoc_probe_0_0_lisnoc_probe
   (latency_o,
    throughput_o,
    clk,
    axiwvalid_i,
    axiwready_i,
    reset,
    axi_reset,
    packet_arived_i);
  output [0:31]latency_o;
  output [0:31]throughput_o;
  input clk;
  input axiwvalid_i;
  input axiwready_i;
  input reset;
  input axi_reset;
  input packet_arived_i;

  wire axi_reset;
  wire axi_transaction_completet;
  wire axiwready_i;
  wire axiwvalid_i;
  wire clk;
  wire counting_latency;
  wire counting_latency3;
  wire counting_latency_i_1_n_0;
  wire counting_latency_i_3_n_0;
  wire \current_latnecy[11]_i_2_n_0 ;
  wire \current_latnecy[11]_i_3_n_0 ;
  wire \current_latnecy[11]_i_4_n_0 ;
  wire \current_latnecy[11]_i_5_n_0 ;
  wire \current_latnecy[15]_i_2_n_0 ;
  wire \current_latnecy[15]_i_3_n_0 ;
  wire \current_latnecy[15]_i_4_n_0 ;
  wire \current_latnecy[15]_i_5_n_0 ;
  wire \current_latnecy[19]_i_2_n_0 ;
  wire \current_latnecy[19]_i_3_n_0 ;
  wire \current_latnecy[19]_i_4_n_0 ;
  wire \current_latnecy[19]_i_5_n_0 ;
  wire \current_latnecy[23]_i_2_n_0 ;
  wire \current_latnecy[23]_i_3_n_0 ;
  wire \current_latnecy[23]_i_4_n_0 ;
  wire \current_latnecy[23]_i_5_n_0 ;
  wire \current_latnecy[27]_i_2_n_0 ;
  wire \current_latnecy[27]_i_3_n_0 ;
  wire \current_latnecy[27]_i_4_n_0 ;
  wire \current_latnecy[27]_i_5_n_0 ;
  wire \current_latnecy[31]_i_2_n_0 ;
  wire \current_latnecy[31]_i_3_n_0 ;
  wire \current_latnecy[31]_i_4_n_0 ;
  wire \current_latnecy[3]_i_2_n_0 ;
  wire \current_latnecy[3]_i_3_n_0 ;
  wire \current_latnecy[3]_i_4_n_0 ;
  wire \current_latnecy[3]_i_5_n_0 ;
  wire \current_latnecy[7]_i_2_n_0 ;
  wire \current_latnecy[7]_i_3_n_0 ;
  wire \current_latnecy[7]_i_4_n_0 ;
  wire \current_latnecy[7]_i_5_n_0 ;
  wire [0:31]current_latnecy_reg;
  wire \current_latnecy_reg[11]_i_1_n_0 ;
  wire \current_latnecy_reg[11]_i_1_n_1 ;
  wire \current_latnecy_reg[11]_i_1_n_2 ;
  wire \current_latnecy_reg[11]_i_1_n_3 ;
  wire \current_latnecy_reg[11]_i_1_n_4 ;
  wire \current_latnecy_reg[11]_i_1_n_5 ;
  wire \current_latnecy_reg[11]_i_1_n_6 ;
  wire \current_latnecy_reg[11]_i_1_n_7 ;
  wire \current_latnecy_reg[15]_i_1_n_0 ;
  wire \current_latnecy_reg[15]_i_1_n_1 ;
  wire \current_latnecy_reg[15]_i_1_n_2 ;
  wire \current_latnecy_reg[15]_i_1_n_3 ;
  wire \current_latnecy_reg[15]_i_1_n_4 ;
  wire \current_latnecy_reg[15]_i_1_n_5 ;
  wire \current_latnecy_reg[15]_i_1_n_6 ;
  wire \current_latnecy_reg[15]_i_1_n_7 ;
  wire \current_latnecy_reg[19]_i_1_n_0 ;
  wire \current_latnecy_reg[19]_i_1_n_1 ;
  wire \current_latnecy_reg[19]_i_1_n_2 ;
  wire \current_latnecy_reg[19]_i_1_n_3 ;
  wire \current_latnecy_reg[19]_i_1_n_4 ;
  wire \current_latnecy_reg[19]_i_1_n_5 ;
  wire \current_latnecy_reg[19]_i_1_n_6 ;
  wire \current_latnecy_reg[19]_i_1_n_7 ;
  wire \current_latnecy_reg[23]_i_1_n_0 ;
  wire \current_latnecy_reg[23]_i_1_n_1 ;
  wire \current_latnecy_reg[23]_i_1_n_2 ;
  wire \current_latnecy_reg[23]_i_1_n_3 ;
  wire \current_latnecy_reg[23]_i_1_n_4 ;
  wire \current_latnecy_reg[23]_i_1_n_5 ;
  wire \current_latnecy_reg[23]_i_1_n_6 ;
  wire \current_latnecy_reg[23]_i_1_n_7 ;
  wire \current_latnecy_reg[27]_i_1_n_0 ;
  wire \current_latnecy_reg[27]_i_1_n_1 ;
  wire \current_latnecy_reg[27]_i_1_n_2 ;
  wire \current_latnecy_reg[27]_i_1_n_3 ;
  wire \current_latnecy_reg[27]_i_1_n_4 ;
  wire \current_latnecy_reg[27]_i_1_n_5 ;
  wire \current_latnecy_reg[27]_i_1_n_6 ;
  wire \current_latnecy_reg[27]_i_1_n_7 ;
  wire \current_latnecy_reg[31]_i_1_n_0 ;
  wire \current_latnecy_reg[31]_i_1_n_1 ;
  wire \current_latnecy_reg[31]_i_1_n_2 ;
  wire \current_latnecy_reg[31]_i_1_n_3 ;
  wire \current_latnecy_reg[31]_i_1_n_4 ;
  wire \current_latnecy_reg[31]_i_1_n_5 ;
  wire \current_latnecy_reg[31]_i_1_n_6 ;
  wire \current_latnecy_reg[31]_i_1_n_7 ;
  wire \current_latnecy_reg[3]_i_1_n_1 ;
  wire \current_latnecy_reg[3]_i_1_n_2 ;
  wire \current_latnecy_reg[3]_i_1_n_3 ;
  wire \current_latnecy_reg[3]_i_1_n_4 ;
  wire \current_latnecy_reg[3]_i_1_n_5 ;
  wire \current_latnecy_reg[3]_i_1_n_6 ;
  wire \current_latnecy_reg[3]_i_1_n_7 ;
  wire \current_latnecy_reg[7]_i_1_n_0 ;
  wire \current_latnecy_reg[7]_i_1_n_1 ;
  wire \current_latnecy_reg[7]_i_1_n_2 ;
  wire \current_latnecy_reg[7]_i_1_n_3 ;
  wire \current_latnecy_reg[7]_i_1_n_4 ;
  wire \current_latnecy_reg[7]_i_1_n_5 ;
  wire \current_latnecy_reg[7]_i_1_n_6 ;
  wire \current_latnecy_reg[7]_i_1_n_7 ;
  wire [0:31]current_troughput;
  wire \current_troughput[11]_i_2_n_0 ;
  wire \current_troughput[11]_i_3_n_0 ;
  wire \current_troughput[11]_i_4_n_0 ;
  wire \current_troughput[11]_i_5_n_0 ;
  wire \current_troughput[15]_i_2_n_0 ;
  wire \current_troughput[15]_i_3_n_0 ;
  wire \current_troughput[15]_i_4_n_0 ;
  wire \current_troughput[15]_i_5_n_0 ;
  wire \current_troughput[19]_i_2_n_0 ;
  wire \current_troughput[19]_i_3_n_0 ;
  wire \current_troughput[19]_i_4_n_0 ;
  wire \current_troughput[19]_i_5_n_0 ;
  wire \current_troughput[23]_i_2_n_0 ;
  wire \current_troughput[23]_i_3_n_0 ;
  wire \current_troughput[23]_i_4_n_0 ;
  wire \current_troughput[23]_i_5_n_0 ;
  wire \current_troughput[27]_i_2_n_0 ;
  wire \current_troughput[27]_i_3_n_0 ;
  wire \current_troughput[27]_i_4_n_0 ;
  wire \current_troughput[27]_i_5_n_0 ;
  wire \current_troughput[31]_i_2_n_0 ;
  wire \current_troughput[31]_i_3_n_0 ;
  wire \current_troughput[31]_i_4_n_0 ;
  wire \current_troughput[31]_i_5_n_0 ;
  wire \current_troughput[3]_i_2_n_0 ;
  wire \current_troughput[3]_i_3_n_0 ;
  wire \current_troughput[3]_i_4_n_0 ;
  wire \current_troughput[3]_i_5_n_0 ;
  wire \current_troughput[7]_i_2_n_0 ;
  wire \current_troughput[7]_i_3_n_0 ;
  wire \current_troughput[7]_i_4_n_0 ;
  wire \current_troughput[7]_i_5_n_0 ;
  wire [0:31]current_troughput_reg;
  wire \current_troughput_reg[11]_i_1_n_0 ;
  wire \current_troughput_reg[11]_i_1_n_1 ;
  wire \current_troughput_reg[11]_i_1_n_2 ;
  wire \current_troughput_reg[11]_i_1_n_3 ;
  wire \current_troughput_reg[11]_i_1_n_4 ;
  wire \current_troughput_reg[11]_i_1_n_5 ;
  wire \current_troughput_reg[11]_i_1_n_6 ;
  wire \current_troughput_reg[11]_i_1_n_7 ;
  wire \current_troughput_reg[15]_i_1_n_0 ;
  wire \current_troughput_reg[15]_i_1_n_1 ;
  wire \current_troughput_reg[15]_i_1_n_2 ;
  wire \current_troughput_reg[15]_i_1_n_3 ;
  wire \current_troughput_reg[15]_i_1_n_4 ;
  wire \current_troughput_reg[15]_i_1_n_5 ;
  wire \current_troughput_reg[15]_i_1_n_6 ;
  wire \current_troughput_reg[15]_i_1_n_7 ;
  wire \current_troughput_reg[19]_i_1_n_0 ;
  wire \current_troughput_reg[19]_i_1_n_1 ;
  wire \current_troughput_reg[19]_i_1_n_2 ;
  wire \current_troughput_reg[19]_i_1_n_3 ;
  wire \current_troughput_reg[19]_i_1_n_4 ;
  wire \current_troughput_reg[19]_i_1_n_5 ;
  wire \current_troughput_reg[19]_i_1_n_6 ;
  wire \current_troughput_reg[19]_i_1_n_7 ;
  wire \current_troughput_reg[23]_i_1_n_0 ;
  wire \current_troughput_reg[23]_i_1_n_1 ;
  wire \current_troughput_reg[23]_i_1_n_2 ;
  wire \current_troughput_reg[23]_i_1_n_3 ;
  wire \current_troughput_reg[23]_i_1_n_4 ;
  wire \current_troughput_reg[23]_i_1_n_5 ;
  wire \current_troughput_reg[23]_i_1_n_6 ;
  wire \current_troughput_reg[23]_i_1_n_7 ;
  wire \current_troughput_reg[27]_i_1_n_0 ;
  wire \current_troughput_reg[27]_i_1_n_1 ;
  wire \current_troughput_reg[27]_i_1_n_2 ;
  wire \current_troughput_reg[27]_i_1_n_3 ;
  wire \current_troughput_reg[27]_i_1_n_4 ;
  wire \current_troughput_reg[27]_i_1_n_5 ;
  wire \current_troughput_reg[27]_i_1_n_6 ;
  wire \current_troughput_reg[27]_i_1_n_7 ;
  wire \current_troughput_reg[31]_i_1_n_0 ;
  wire \current_troughput_reg[31]_i_1_n_1 ;
  wire \current_troughput_reg[31]_i_1_n_2 ;
  wire \current_troughput_reg[31]_i_1_n_3 ;
  wire \current_troughput_reg[31]_i_1_n_4 ;
  wire \current_troughput_reg[31]_i_1_n_5 ;
  wire \current_troughput_reg[31]_i_1_n_6 ;
  wire \current_troughput_reg[31]_i_1_n_7 ;
  wire \current_troughput_reg[3]_i_1_n_1 ;
  wire \current_troughput_reg[3]_i_1_n_2 ;
  wire \current_troughput_reg[3]_i_1_n_3 ;
  wire \current_troughput_reg[3]_i_1_n_4 ;
  wire \current_troughput_reg[3]_i_1_n_5 ;
  wire \current_troughput_reg[3]_i_1_n_6 ;
  wire \current_troughput_reg[3]_i_1_n_7 ;
  wire \current_troughput_reg[7]_i_1_n_0 ;
  wire \current_troughput_reg[7]_i_1_n_1 ;
  wire \current_troughput_reg[7]_i_1_n_2 ;
  wire \current_troughput_reg[7]_i_1_n_3 ;
  wire \current_troughput_reg[7]_i_1_n_4 ;
  wire \current_troughput_reg[7]_i_1_n_5 ;
  wire \current_troughput_reg[7]_i_1_n_6 ;
  wire \current_troughput_reg[7]_i_1_n_7 ;
  wire latency_measured;
  wire latency_measured_reg_C_n_0;
  wire latency_measured_reg_LDC_n_0;
  wire latency_measured_reg_P_n_0;
  wire [0:31]latency_o;
  wire [0:0]latency_o2;
  wire \latency_o_reg[0]_C_n_0 ;
  wire \latency_o_reg[0]_LDC_i_1_n_0 ;
  wire \latency_o_reg[0]_LDC_i_2_n_0 ;
  wire \latency_o_reg[0]_LDC_n_0 ;
  wire \latency_o_reg[0]_P_n_0 ;
  wire \latency_o_reg[10]_C_n_0 ;
  wire \latency_o_reg[10]_LDC_i_1_n_0 ;
  wire \latency_o_reg[10]_LDC_i_2_n_0 ;
  wire \latency_o_reg[10]_LDC_n_0 ;
  wire \latency_o_reg[10]_P_n_0 ;
  wire \latency_o_reg[11]_C_n_0 ;
  wire \latency_o_reg[11]_LDC_i_1_n_0 ;
  wire \latency_o_reg[11]_LDC_i_2_n_0 ;
  wire \latency_o_reg[11]_LDC_n_0 ;
  wire \latency_o_reg[11]_P_n_0 ;
  wire \latency_o_reg[12]_C_n_0 ;
  wire \latency_o_reg[12]_LDC_i_1_n_0 ;
  wire \latency_o_reg[12]_LDC_i_2_n_0 ;
  wire \latency_o_reg[12]_LDC_n_0 ;
  wire \latency_o_reg[12]_P_n_0 ;
  wire \latency_o_reg[13]_C_n_0 ;
  wire \latency_o_reg[13]_LDC_i_1_n_0 ;
  wire \latency_o_reg[13]_LDC_i_2_n_0 ;
  wire \latency_o_reg[13]_LDC_n_0 ;
  wire \latency_o_reg[13]_P_n_0 ;
  wire \latency_o_reg[14]_C_n_0 ;
  wire \latency_o_reg[14]_LDC_i_1_n_0 ;
  wire \latency_o_reg[14]_LDC_i_2_n_0 ;
  wire \latency_o_reg[14]_LDC_n_0 ;
  wire \latency_o_reg[14]_P_n_0 ;
  wire \latency_o_reg[15]_C_n_0 ;
  wire \latency_o_reg[15]_LDC_i_1_n_0 ;
  wire \latency_o_reg[15]_LDC_i_2_n_0 ;
  wire \latency_o_reg[15]_LDC_n_0 ;
  wire \latency_o_reg[15]_P_n_0 ;
  wire \latency_o_reg[16]_C_n_0 ;
  wire \latency_o_reg[16]_LDC_i_1_n_0 ;
  wire \latency_o_reg[16]_LDC_i_2_n_0 ;
  wire \latency_o_reg[16]_LDC_n_0 ;
  wire \latency_o_reg[16]_P_n_0 ;
  wire \latency_o_reg[17]_C_n_0 ;
  wire \latency_o_reg[17]_LDC_i_1_n_0 ;
  wire \latency_o_reg[17]_LDC_i_2_n_0 ;
  wire \latency_o_reg[17]_LDC_n_0 ;
  wire \latency_o_reg[17]_P_n_0 ;
  wire \latency_o_reg[18]_C_n_0 ;
  wire \latency_o_reg[18]_LDC_i_1_n_0 ;
  wire \latency_o_reg[18]_LDC_i_2_n_0 ;
  wire \latency_o_reg[18]_LDC_n_0 ;
  wire \latency_o_reg[18]_P_n_0 ;
  wire \latency_o_reg[19]_C_n_0 ;
  wire \latency_o_reg[19]_LDC_i_1_n_0 ;
  wire \latency_o_reg[19]_LDC_i_2_n_0 ;
  wire \latency_o_reg[19]_LDC_n_0 ;
  wire \latency_o_reg[19]_P_n_0 ;
  wire \latency_o_reg[1]_C_n_0 ;
  wire \latency_o_reg[1]_LDC_i_1_n_0 ;
  wire \latency_o_reg[1]_LDC_i_2_n_0 ;
  wire \latency_o_reg[1]_LDC_n_0 ;
  wire \latency_o_reg[1]_P_n_0 ;
  wire \latency_o_reg[20]_C_n_0 ;
  wire \latency_o_reg[20]_LDC_i_1_n_0 ;
  wire \latency_o_reg[20]_LDC_i_2_n_0 ;
  wire \latency_o_reg[20]_LDC_n_0 ;
  wire \latency_o_reg[20]_P_n_0 ;
  wire \latency_o_reg[21]_C_n_0 ;
  wire \latency_o_reg[21]_LDC_i_1_n_0 ;
  wire \latency_o_reg[21]_LDC_i_2_n_0 ;
  wire \latency_o_reg[21]_LDC_n_0 ;
  wire \latency_o_reg[21]_P_n_0 ;
  wire \latency_o_reg[22]_C_n_0 ;
  wire \latency_o_reg[22]_LDC_i_1_n_0 ;
  wire \latency_o_reg[22]_LDC_i_2_n_0 ;
  wire \latency_o_reg[22]_LDC_n_0 ;
  wire \latency_o_reg[22]_P_n_0 ;
  wire \latency_o_reg[23]_C_n_0 ;
  wire \latency_o_reg[23]_LDC_i_1_n_0 ;
  wire \latency_o_reg[23]_LDC_i_2_n_0 ;
  wire \latency_o_reg[23]_LDC_n_0 ;
  wire \latency_o_reg[23]_P_n_0 ;
  wire \latency_o_reg[24]_C_n_0 ;
  wire \latency_o_reg[24]_LDC_i_1_n_0 ;
  wire \latency_o_reg[24]_LDC_i_2_n_0 ;
  wire \latency_o_reg[24]_LDC_n_0 ;
  wire \latency_o_reg[24]_P_n_0 ;
  wire \latency_o_reg[25]_C_n_0 ;
  wire \latency_o_reg[25]_LDC_i_1_n_0 ;
  wire \latency_o_reg[25]_LDC_i_2_n_0 ;
  wire \latency_o_reg[25]_LDC_n_0 ;
  wire \latency_o_reg[25]_P_n_0 ;
  wire \latency_o_reg[26]_C_n_0 ;
  wire \latency_o_reg[26]_LDC_i_1_n_0 ;
  wire \latency_o_reg[26]_LDC_i_2_n_0 ;
  wire \latency_o_reg[26]_LDC_n_0 ;
  wire \latency_o_reg[26]_P_n_0 ;
  wire \latency_o_reg[27]_C_n_0 ;
  wire \latency_o_reg[27]_LDC_i_1_n_0 ;
  wire \latency_o_reg[27]_LDC_i_2_n_0 ;
  wire \latency_o_reg[27]_LDC_n_0 ;
  wire \latency_o_reg[27]_P_n_0 ;
  wire \latency_o_reg[28]_C_n_0 ;
  wire \latency_o_reg[28]_LDC_i_1_n_0 ;
  wire \latency_o_reg[28]_LDC_i_2_n_0 ;
  wire \latency_o_reg[28]_LDC_n_0 ;
  wire \latency_o_reg[28]_P_n_0 ;
  wire \latency_o_reg[29]_C_n_0 ;
  wire \latency_o_reg[29]_LDC_i_1_n_0 ;
  wire \latency_o_reg[29]_LDC_i_2_n_0 ;
  wire \latency_o_reg[29]_LDC_n_0 ;
  wire \latency_o_reg[29]_P_n_0 ;
  wire \latency_o_reg[2]_C_n_0 ;
  wire \latency_o_reg[2]_LDC_i_1_n_0 ;
  wire \latency_o_reg[2]_LDC_i_2_n_0 ;
  wire \latency_o_reg[2]_LDC_n_0 ;
  wire \latency_o_reg[2]_P_n_0 ;
  wire \latency_o_reg[30]_C_n_0 ;
  wire \latency_o_reg[30]_LDC_i_1_n_0 ;
  wire \latency_o_reg[30]_LDC_i_2_n_0 ;
  wire \latency_o_reg[30]_LDC_n_0 ;
  wire \latency_o_reg[30]_P_n_0 ;
  wire \latency_o_reg[31]_C_n_0 ;
  wire \latency_o_reg[31]_LDC_i_1_n_0 ;
  wire \latency_o_reg[31]_LDC_i_2_n_0 ;
  wire \latency_o_reg[31]_LDC_n_0 ;
  wire \latency_o_reg[31]_P_n_0 ;
  wire \latency_o_reg[3]_C_n_0 ;
  wire \latency_o_reg[3]_LDC_i_1_n_0 ;
  wire \latency_o_reg[3]_LDC_i_2_n_0 ;
  wire \latency_o_reg[3]_LDC_n_0 ;
  wire \latency_o_reg[3]_P_n_0 ;
  wire \latency_o_reg[4]_C_n_0 ;
  wire \latency_o_reg[4]_LDC_i_1_n_0 ;
  wire \latency_o_reg[4]_LDC_i_2_n_0 ;
  wire \latency_o_reg[4]_LDC_n_0 ;
  wire \latency_o_reg[4]_P_n_0 ;
  wire \latency_o_reg[5]_C_n_0 ;
  wire \latency_o_reg[5]_LDC_i_1_n_0 ;
  wire \latency_o_reg[5]_LDC_i_2_n_0 ;
  wire \latency_o_reg[5]_LDC_n_0 ;
  wire \latency_o_reg[5]_P_n_0 ;
  wire \latency_o_reg[6]_C_n_0 ;
  wire \latency_o_reg[6]_LDC_i_1_n_0 ;
  wire \latency_o_reg[6]_LDC_i_2_n_0 ;
  wire \latency_o_reg[6]_LDC_n_0 ;
  wire \latency_o_reg[6]_P_n_0 ;
  wire \latency_o_reg[7]_C_n_0 ;
  wire \latency_o_reg[7]_LDC_i_1_n_0 ;
  wire \latency_o_reg[7]_LDC_i_2_n_0 ;
  wire \latency_o_reg[7]_LDC_n_0 ;
  wire \latency_o_reg[7]_P_n_0 ;
  wire \latency_o_reg[8]_C_n_0 ;
  wire \latency_o_reg[8]_LDC_i_1_n_0 ;
  wire \latency_o_reg[8]_LDC_i_2_n_0 ;
  wire \latency_o_reg[8]_LDC_n_0 ;
  wire \latency_o_reg[8]_P_n_0 ;
  wire \latency_o_reg[9]_C_n_0 ;
  wire \latency_o_reg[9]_LDC_i_1_n_0 ;
  wire \latency_o_reg[9]_LDC_i_2_n_0 ;
  wire \latency_o_reg[9]_LDC_n_0 ;
  wire \latency_o_reg[9]_P_n_0 ;
  wire [31:2]p_0_in;
  wire [31:0]p_3_in;
  wire packet_arived_i;
  wire packet_arived_last;
  wire packet_arived_last_i_1_n_0;
  wire reset;
  wire rst;
  wire [0:31]throughput_o;
  wire \throughput_o[0]_i_12_n_0 ;
  wire \throughput_o[0]_i_13_n_0 ;
  wire \throughput_o[0]_i_17_n_0 ;
  wire \throughput_o[0]_i_18_n_0 ;
  wire \throughput_o[0]_i_19_n_0 ;
  wire \throughput_o[0]_i_1_n_0 ;
  wire \throughput_o[0]_i_20_n_0 ;
  wire \throughput_o[0]_i_23_n_0 ;
  wire \throughput_o[0]_i_24_n_0 ;
  wire \throughput_o[0]_i_25_n_0 ;
  wire \throughput_o[0]_i_26_n_0 ;
  wire \throughput_o[0]_i_27_n_0 ;
  wire \throughput_o[0]_i_28_n_0 ;
  wire \throughput_o[0]_i_29_n_0 ;
  wire \throughput_o[0]_i_30_n_0 ;
  wire \throughput_o[0]_i_32_n_0 ;
  wire \throughput_o[0]_i_33_n_0 ;
  wire \throughput_o[0]_i_34_n_0 ;
  wire \throughput_o[0]_i_35_n_0 ;
  wire \throughput_o[0]_i_36_n_0 ;
  wire \throughput_o[0]_i_37_n_0 ;
  wire \throughput_o[0]_i_38_n_0 ;
  wire \throughput_o[0]_i_39_n_0 ;
  wire \throughput_o[0]_i_40_n_0 ;
  wire \throughput_o[0]_i_41_n_0 ;
  wire \throughput_o[0]_i_42_n_0 ;
  wire \throughput_o[0]_i_43_n_0 ;
  wire \throughput_o[0]_i_44_n_0 ;
  wire \throughput_o[0]_i_45_n_0 ;
  wire \throughput_o[0]_i_46_n_0 ;
  wire \throughput_o[0]_i_47_n_0 ;
  wire \throughput_o[0]_i_48_n_0 ;
  wire \throughput_o[0]_i_49_n_0 ;
  wire \throughput_o[0]_i_4_n_0 ;
  wire \throughput_o[0]_i_50_n_0 ;
  wire \throughput_o[0]_i_51_n_0 ;
  wire \throughput_o[0]_i_52_n_0 ;
  wire \throughput_o[0]_i_53_n_0 ;
  wire \throughput_o[0]_i_54_n_0 ;
  wire \throughput_o[0]_i_5_n_0 ;
  wire \throughput_o[0]_i_6_n_0 ;
  wire \throughput_o[0]_i_8_n_0 ;
  wire \throughput_o[0]_i_9_n_0 ;
  wire \throughput_o[12]_i_3_n_0 ;
  wire \throughput_o[12]_i_4_n_0 ;
  wire \throughput_o[12]_i_5_n_0 ;
  wire \throughput_o[12]_i_6_n_0 ;
  wire \throughput_o[16]_i_3_n_0 ;
  wire \throughput_o[16]_i_4_n_0 ;
  wire \throughput_o[16]_i_5_n_0 ;
  wire \throughput_o[16]_i_6_n_0 ;
  wire \throughput_o[20]_i_3_n_0 ;
  wire \throughput_o[20]_i_4_n_0 ;
  wire \throughput_o[20]_i_5_n_0 ;
  wire \throughput_o[20]_i_6_n_0 ;
  wire \throughput_o[24]_i_3_n_0 ;
  wire \throughput_o[24]_i_4_n_0 ;
  wire \throughput_o[24]_i_5_n_0 ;
  wire \throughput_o[24]_i_6_n_0 ;
  wire \throughput_o[28]_i_3_n_0 ;
  wire \throughput_o[28]_i_4_n_0 ;
  wire \throughput_o[28]_i_5_n_0 ;
  wire \throughput_o[28]_i_6_n_0 ;
  wire \throughput_o[4]_i_3_n_0 ;
  wire \throughput_o[4]_i_4_n_0 ;
  wire \throughput_o[4]_i_5_n_0 ;
  wire \throughput_o[4]_i_6_n_0 ;
  wire \throughput_o[8]_i_3_n_0 ;
  wire \throughput_o[8]_i_4_n_0 ;
  wire \throughput_o[8]_i_5_n_0 ;
  wire \throughput_o[8]_i_6_n_0 ;
  wire \throughput_o_reg[0]_i_10_n_0 ;
  wire \throughput_o_reg[0]_i_10_n_1 ;
  wire \throughput_o_reg[0]_i_10_n_2 ;
  wire \throughput_o_reg[0]_i_10_n_3 ;
  wire \throughput_o_reg[0]_i_11_n_0 ;
  wire \throughput_o_reg[0]_i_11_n_1 ;
  wire \throughput_o_reg[0]_i_11_n_2 ;
  wire \throughput_o_reg[0]_i_11_n_3 ;
  wire \throughput_o_reg[0]_i_14_n_2 ;
  wire \throughput_o_reg[0]_i_14_n_3 ;
  wire \throughput_o_reg[0]_i_15_n_0 ;
  wire \throughput_o_reg[0]_i_15_n_1 ;
  wire \throughput_o_reg[0]_i_15_n_2 ;
  wire \throughput_o_reg[0]_i_15_n_3 ;
  wire \throughput_o_reg[0]_i_16_n_0 ;
  wire \throughput_o_reg[0]_i_16_n_1 ;
  wire \throughput_o_reg[0]_i_16_n_2 ;
  wire \throughput_o_reg[0]_i_16_n_3 ;
  wire \throughput_o_reg[0]_i_21_n_0 ;
  wire \throughput_o_reg[0]_i_21_n_1 ;
  wire \throughput_o_reg[0]_i_21_n_2 ;
  wire \throughput_o_reg[0]_i_21_n_3 ;
  wire \throughput_o_reg[0]_i_22_n_0 ;
  wire \throughput_o_reg[0]_i_22_n_1 ;
  wire \throughput_o_reg[0]_i_22_n_2 ;
  wire \throughput_o_reg[0]_i_22_n_3 ;
  wire \throughput_o_reg[0]_i_31_n_0 ;
  wire \throughput_o_reg[0]_i_31_n_1 ;
  wire \throughput_o_reg[0]_i_31_n_2 ;
  wire \throughput_o_reg[0]_i_31_n_3 ;
  wire \throughput_o_reg[0]_i_7_n_1 ;
  wire \throughput_o_reg[0]_i_7_n_2 ;
  wire \throughput_o_reg[0]_i_7_n_3 ;
  wire \throughput_o_reg[12]_i_2_n_0 ;
  wire \throughput_o_reg[12]_i_2_n_1 ;
  wire \throughput_o_reg[12]_i_2_n_2 ;
  wire \throughput_o_reg[12]_i_2_n_3 ;
  wire \throughput_o_reg[16]_i_2_n_0 ;
  wire \throughput_o_reg[16]_i_2_n_1 ;
  wire \throughput_o_reg[16]_i_2_n_2 ;
  wire \throughput_o_reg[16]_i_2_n_3 ;
  wire \throughput_o_reg[20]_i_2_n_0 ;
  wire \throughput_o_reg[20]_i_2_n_1 ;
  wire \throughput_o_reg[20]_i_2_n_2 ;
  wire \throughput_o_reg[20]_i_2_n_3 ;
  wire \throughput_o_reg[24]_i_2_n_0 ;
  wire \throughput_o_reg[24]_i_2_n_1 ;
  wire \throughput_o_reg[24]_i_2_n_2 ;
  wire \throughput_o_reg[24]_i_2_n_3 ;
  wire \throughput_o_reg[28]_i_2_n_0 ;
  wire \throughput_o_reg[28]_i_2_n_1 ;
  wire \throughput_o_reg[28]_i_2_n_2 ;
  wire \throughput_o_reg[28]_i_2_n_3 ;
  wire \throughput_o_reg[4]_i_2_n_0 ;
  wire \throughput_o_reg[4]_i_2_n_1 ;
  wire \throughput_o_reg[4]_i_2_n_2 ;
  wire \throughput_o_reg[4]_i_2_n_3 ;
  wire \throughput_o_reg[8]_i_2_n_0 ;
  wire \throughput_o_reg[8]_i_2_n_1 ;
  wire \throughput_o_reg[8]_i_2_n_2 ;
  wire \throughput_o_reg[8]_i_2_n_3 ;
  wire throughput_timer;
  wire \throughput_timer[11]_i_2_n_0 ;
  wire \throughput_timer[11]_i_3_n_0 ;
  wire \throughput_timer[11]_i_4_n_0 ;
  wire \throughput_timer[11]_i_5_n_0 ;
  wire \throughput_timer[15]_i_2_n_0 ;
  wire \throughput_timer[15]_i_3_n_0 ;
  wire \throughput_timer[15]_i_4_n_0 ;
  wire \throughput_timer[15]_i_5_n_0 ;
  wire \throughput_timer[19]_i_2_n_0 ;
  wire \throughput_timer[19]_i_3_n_0 ;
  wire \throughput_timer[19]_i_4_n_0 ;
  wire \throughput_timer[19]_i_5_n_0 ;
  wire \throughput_timer[23]_i_2_n_0 ;
  wire \throughput_timer[23]_i_3_n_0 ;
  wire \throughput_timer[23]_i_4_n_0 ;
  wire \throughput_timer[23]_i_5_n_0 ;
  wire \throughput_timer[27]_i_2_n_0 ;
  wire \throughput_timer[27]_i_3_n_0 ;
  wire \throughput_timer[27]_i_4_n_0 ;
  wire \throughput_timer[27]_i_5_n_0 ;
  wire \throughput_timer[31]_i_3_n_0 ;
  wire \throughput_timer[31]_i_4_n_0 ;
  wire \throughput_timer[31]_i_5_n_0 ;
  wire \throughput_timer[31]_i_6_n_0 ;
  wire \throughput_timer[3]_i_2_n_0 ;
  wire \throughput_timer[3]_i_3_n_0 ;
  wire \throughput_timer[3]_i_4_n_0 ;
  wire \throughput_timer[3]_i_5_n_0 ;
  wire \throughput_timer[7]_i_2_n_0 ;
  wire \throughput_timer[7]_i_3_n_0 ;
  wire \throughput_timer[7]_i_4_n_0 ;
  wire \throughput_timer[7]_i_5_n_0 ;
  wire [0:31]throughput_timer_reg;
  wire \throughput_timer_reg[11]_i_1_n_0 ;
  wire \throughput_timer_reg[11]_i_1_n_1 ;
  wire \throughput_timer_reg[11]_i_1_n_2 ;
  wire \throughput_timer_reg[11]_i_1_n_3 ;
  wire \throughput_timer_reg[11]_i_1_n_4 ;
  wire \throughput_timer_reg[11]_i_1_n_5 ;
  wire \throughput_timer_reg[11]_i_1_n_6 ;
  wire \throughput_timer_reg[11]_i_1_n_7 ;
  wire \throughput_timer_reg[15]_i_1_n_0 ;
  wire \throughput_timer_reg[15]_i_1_n_1 ;
  wire \throughput_timer_reg[15]_i_1_n_2 ;
  wire \throughput_timer_reg[15]_i_1_n_3 ;
  wire \throughput_timer_reg[15]_i_1_n_4 ;
  wire \throughput_timer_reg[15]_i_1_n_5 ;
  wire \throughput_timer_reg[15]_i_1_n_6 ;
  wire \throughput_timer_reg[15]_i_1_n_7 ;
  wire \throughput_timer_reg[19]_i_1_n_0 ;
  wire \throughput_timer_reg[19]_i_1_n_1 ;
  wire \throughput_timer_reg[19]_i_1_n_2 ;
  wire \throughput_timer_reg[19]_i_1_n_3 ;
  wire \throughput_timer_reg[19]_i_1_n_4 ;
  wire \throughput_timer_reg[19]_i_1_n_5 ;
  wire \throughput_timer_reg[19]_i_1_n_6 ;
  wire \throughput_timer_reg[19]_i_1_n_7 ;
  wire \throughput_timer_reg[23]_i_1_n_0 ;
  wire \throughput_timer_reg[23]_i_1_n_1 ;
  wire \throughput_timer_reg[23]_i_1_n_2 ;
  wire \throughput_timer_reg[23]_i_1_n_3 ;
  wire \throughput_timer_reg[23]_i_1_n_4 ;
  wire \throughput_timer_reg[23]_i_1_n_5 ;
  wire \throughput_timer_reg[23]_i_1_n_6 ;
  wire \throughput_timer_reg[23]_i_1_n_7 ;
  wire \throughput_timer_reg[27]_i_1_n_0 ;
  wire \throughput_timer_reg[27]_i_1_n_1 ;
  wire \throughput_timer_reg[27]_i_1_n_2 ;
  wire \throughput_timer_reg[27]_i_1_n_3 ;
  wire \throughput_timer_reg[27]_i_1_n_4 ;
  wire \throughput_timer_reg[27]_i_1_n_5 ;
  wire \throughput_timer_reg[27]_i_1_n_6 ;
  wire \throughput_timer_reg[27]_i_1_n_7 ;
  wire \throughput_timer_reg[31]_i_2_n_0 ;
  wire \throughput_timer_reg[31]_i_2_n_1 ;
  wire \throughput_timer_reg[31]_i_2_n_2 ;
  wire \throughput_timer_reg[31]_i_2_n_3 ;
  wire \throughput_timer_reg[31]_i_2_n_4 ;
  wire \throughput_timer_reg[31]_i_2_n_5 ;
  wire \throughput_timer_reg[31]_i_2_n_6 ;
  wire \throughput_timer_reg[31]_i_2_n_7 ;
  wire \throughput_timer_reg[3]_i_1_n_1 ;
  wire \throughput_timer_reg[3]_i_1_n_2 ;
  wire \throughput_timer_reg[3]_i_1_n_3 ;
  wire \throughput_timer_reg[3]_i_1_n_4 ;
  wire \throughput_timer_reg[3]_i_1_n_5 ;
  wire \throughput_timer_reg[3]_i_1_n_6 ;
  wire \throughput_timer_reg[3]_i_1_n_7 ;
  wire \throughput_timer_reg[7]_i_1_n_0 ;
  wire \throughput_timer_reg[7]_i_1_n_1 ;
  wire \throughput_timer_reg[7]_i_1_n_2 ;
  wire \throughput_timer_reg[7]_i_1_n_3 ;
  wire \throughput_timer_reg[7]_i_1_n_4 ;
  wire \throughput_timer_reg[7]_i_1_n_5 ;
  wire \throughput_timer_reg[7]_i_1_n_6 ;
  wire \throughput_timer_reg[7]_i_1_n_7 ;
  wire \troughput_state[0]_C_i_1_n_0 ;
  wire \troughput_state[0]_C_i_2_n_0 ;
  wire \troughput_state[0]_C_i_3_n_0 ;
  wire \troughput_state[1]_C_i_1_n_0 ;
  wire \troughput_state[1]_C_i_2_n_0 ;
  wire \troughput_state[1]_C_i_3_n_0 ;
  wire \troughput_state[1]_C_i_4_n_0 ;
  wire \troughput_state[1]_C_i_5_n_0 ;
  wire \troughput_state[1]_C_i_6_n_0 ;
  wire \troughput_state_reg[0]_C_n_0 ;
  wire \troughput_state_reg[1]_C_n_0 ;
  wire [3:3]\NLW_current_latnecy_reg[3]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_current_troughput_reg[3]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_throughput_o_reg[0]_i_14_CO_UNCONNECTED ;
  wire [3:3]\NLW_throughput_o_reg[0]_i_14_O_UNCONNECTED ;
  wire [0:0]\NLW_throughput_o_reg[0]_i_31_O_UNCONNECTED ;
  wire [3:3]\NLW_throughput_o_reg[0]_i_7_CO_UNCONNECTED ;
  wire [3:3]\NLW_throughput_timer_reg[3]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFF47)) 
    counting_latency_i_1
       (.I0(latency_measured_reg_P_n_0),
        .I1(latency_measured_reg_LDC_n_0),
        .I2(latency_measured_reg_C_n_0),
        .I3(counting_latency),
        .O(counting_latency_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    counting_latency_i_2
       (.I0(axiwvalid_i),
        .I1(axiwready_i),
        .O(axi_transaction_completet));
  LUT4 #(
    .INIT(16'hFF8F)) 
    counting_latency_i_3
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(axi_reset),
        .I3(reset),
        .O(counting_latency_i_3_n_0));
  FDCE #(
    .INIT(1'b0)) 
    counting_latency_reg
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(counting_latency_i_3_n_0),
        .D(counting_latency_i_1_n_0),
        .Q(counting_latency));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[11]_i_2 
       (.I0(current_latnecy_reg[8]),
        .O(\current_latnecy[11]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[11]_i_3 
       (.I0(current_latnecy_reg[9]),
        .O(\current_latnecy[11]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[11]_i_4 
       (.I0(current_latnecy_reg[10]),
        .O(\current_latnecy[11]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[11]_i_5 
       (.I0(current_latnecy_reg[11]),
        .O(\current_latnecy[11]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[15]_i_2 
       (.I0(current_latnecy_reg[12]),
        .O(\current_latnecy[15]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[15]_i_3 
       (.I0(current_latnecy_reg[13]),
        .O(\current_latnecy[15]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[15]_i_4 
       (.I0(current_latnecy_reg[14]),
        .O(\current_latnecy[15]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[15]_i_5 
       (.I0(current_latnecy_reg[15]),
        .O(\current_latnecy[15]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[19]_i_2 
       (.I0(current_latnecy_reg[16]),
        .O(\current_latnecy[19]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[19]_i_3 
       (.I0(current_latnecy_reg[17]),
        .O(\current_latnecy[19]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[19]_i_4 
       (.I0(current_latnecy_reg[18]),
        .O(\current_latnecy[19]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[19]_i_5 
       (.I0(current_latnecy_reg[19]),
        .O(\current_latnecy[19]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[23]_i_2 
       (.I0(current_latnecy_reg[20]),
        .O(\current_latnecy[23]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[23]_i_3 
       (.I0(current_latnecy_reg[21]),
        .O(\current_latnecy[23]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[23]_i_4 
       (.I0(current_latnecy_reg[22]),
        .O(\current_latnecy[23]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[23]_i_5 
       (.I0(current_latnecy_reg[23]),
        .O(\current_latnecy[23]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[27]_i_2 
       (.I0(current_latnecy_reg[24]),
        .O(\current_latnecy[27]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[27]_i_3 
       (.I0(current_latnecy_reg[25]),
        .O(\current_latnecy[27]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[27]_i_4 
       (.I0(current_latnecy_reg[26]),
        .O(\current_latnecy[27]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[27]_i_5 
       (.I0(current_latnecy_reg[27]),
        .O(\current_latnecy[27]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[31]_i_2 
       (.I0(current_latnecy_reg[28]),
        .O(\current_latnecy[31]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[31]_i_3 
       (.I0(current_latnecy_reg[29]),
        .O(\current_latnecy[31]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[31]_i_4 
       (.I0(current_latnecy_reg[30]),
        .O(\current_latnecy[31]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \current_latnecy[31]_i_5 
       (.I0(current_latnecy_reg[31]),
        .O(latency_o2));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[3]_i_2 
       (.I0(current_latnecy_reg[0]),
        .O(\current_latnecy[3]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[3]_i_3 
       (.I0(current_latnecy_reg[1]),
        .O(\current_latnecy[3]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[3]_i_4 
       (.I0(current_latnecy_reg[2]),
        .O(\current_latnecy[3]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[3]_i_5 
       (.I0(current_latnecy_reg[3]),
        .O(\current_latnecy[3]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[7]_i_2 
       (.I0(current_latnecy_reg[4]),
        .O(\current_latnecy[7]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[7]_i_3 
       (.I0(current_latnecy_reg[5]),
        .O(\current_latnecy[7]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[7]_i_4 
       (.I0(current_latnecy_reg[6]),
        .O(\current_latnecy[7]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_latnecy[7]_i_5 
       (.I0(current_latnecy_reg[7]),
        .O(\current_latnecy[7]_i_5_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[0] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[3]_i_1_n_4 ),
        .Q(current_latnecy_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[10] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[11]_i_1_n_6 ),
        .Q(current_latnecy_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[11] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[11]_i_1_n_7 ),
        .Q(current_latnecy_reg[11]));
  CARRY4 \current_latnecy_reg[11]_i_1 
       (.CI(\current_latnecy_reg[15]_i_1_n_0 ),
        .CO({\current_latnecy_reg[11]_i_1_n_0 ,\current_latnecy_reg[11]_i_1_n_1 ,\current_latnecy_reg[11]_i_1_n_2 ,\current_latnecy_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_latnecy_reg[11]_i_1_n_4 ,\current_latnecy_reg[11]_i_1_n_5 ,\current_latnecy_reg[11]_i_1_n_6 ,\current_latnecy_reg[11]_i_1_n_7 }),
        .S({\current_latnecy[11]_i_2_n_0 ,\current_latnecy[11]_i_3_n_0 ,\current_latnecy[11]_i_4_n_0 ,\current_latnecy[11]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[12] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[15]_i_1_n_4 ),
        .Q(current_latnecy_reg[12]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[13] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[15]_i_1_n_5 ),
        .Q(current_latnecy_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[14] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[15]_i_1_n_6 ),
        .Q(current_latnecy_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[15] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[15]_i_1_n_7 ),
        .Q(current_latnecy_reg[15]));
  CARRY4 \current_latnecy_reg[15]_i_1 
       (.CI(\current_latnecy_reg[19]_i_1_n_0 ),
        .CO({\current_latnecy_reg[15]_i_1_n_0 ,\current_latnecy_reg[15]_i_1_n_1 ,\current_latnecy_reg[15]_i_1_n_2 ,\current_latnecy_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_latnecy_reg[15]_i_1_n_4 ,\current_latnecy_reg[15]_i_1_n_5 ,\current_latnecy_reg[15]_i_1_n_6 ,\current_latnecy_reg[15]_i_1_n_7 }),
        .S({\current_latnecy[15]_i_2_n_0 ,\current_latnecy[15]_i_3_n_0 ,\current_latnecy[15]_i_4_n_0 ,\current_latnecy[15]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[16] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[19]_i_1_n_4 ),
        .Q(current_latnecy_reg[16]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[17] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[19]_i_1_n_5 ),
        .Q(current_latnecy_reg[17]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[18] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[19]_i_1_n_6 ),
        .Q(current_latnecy_reg[18]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[19] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[19]_i_1_n_7 ),
        .Q(current_latnecy_reg[19]));
  CARRY4 \current_latnecy_reg[19]_i_1 
       (.CI(\current_latnecy_reg[23]_i_1_n_0 ),
        .CO({\current_latnecy_reg[19]_i_1_n_0 ,\current_latnecy_reg[19]_i_1_n_1 ,\current_latnecy_reg[19]_i_1_n_2 ,\current_latnecy_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_latnecy_reg[19]_i_1_n_4 ,\current_latnecy_reg[19]_i_1_n_5 ,\current_latnecy_reg[19]_i_1_n_6 ,\current_latnecy_reg[19]_i_1_n_7 }),
        .S({\current_latnecy[19]_i_2_n_0 ,\current_latnecy[19]_i_3_n_0 ,\current_latnecy[19]_i_4_n_0 ,\current_latnecy[19]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[1] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[3]_i_1_n_5 ),
        .Q(current_latnecy_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[20] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[23]_i_1_n_4 ),
        .Q(current_latnecy_reg[20]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[21] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[23]_i_1_n_5 ),
        .Q(current_latnecy_reg[21]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[22] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[23]_i_1_n_6 ),
        .Q(current_latnecy_reg[22]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[23] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[23]_i_1_n_7 ),
        .Q(current_latnecy_reg[23]));
  CARRY4 \current_latnecy_reg[23]_i_1 
       (.CI(\current_latnecy_reg[27]_i_1_n_0 ),
        .CO({\current_latnecy_reg[23]_i_1_n_0 ,\current_latnecy_reg[23]_i_1_n_1 ,\current_latnecy_reg[23]_i_1_n_2 ,\current_latnecy_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_latnecy_reg[23]_i_1_n_4 ,\current_latnecy_reg[23]_i_1_n_5 ,\current_latnecy_reg[23]_i_1_n_6 ,\current_latnecy_reg[23]_i_1_n_7 }),
        .S({\current_latnecy[23]_i_2_n_0 ,\current_latnecy[23]_i_3_n_0 ,\current_latnecy[23]_i_4_n_0 ,\current_latnecy[23]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[24] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[27]_i_1_n_4 ),
        .Q(current_latnecy_reg[24]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[25] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[27]_i_1_n_5 ),
        .Q(current_latnecy_reg[25]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[26] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[27]_i_1_n_6 ),
        .Q(current_latnecy_reg[26]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[27] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[27]_i_1_n_7 ),
        .Q(current_latnecy_reg[27]));
  CARRY4 \current_latnecy_reg[27]_i_1 
       (.CI(\current_latnecy_reg[31]_i_1_n_0 ),
        .CO({\current_latnecy_reg[27]_i_1_n_0 ,\current_latnecy_reg[27]_i_1_n_1 ,\current_latnecy_reg[27]_i_1_n_2 ,\current_latnecy_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_latnecy_reg[27]_i_1_n_4 ,\current_latnecy_reg[27]_i_1_n_5 ,\current_latnecy_reg[27]_i_1_n_6 ,\current_latnecy_reg[27]_i_1_n_7 }),
        .S({\current_latnecy[27]_i_2_n_0 ,\current_latnecy[27]_i_3_n_0 ,\current_latnecy[27]_i_4_n_0 ,\current_latnecy[27]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[28] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[31]_i_1_n_4 ),
        .Q(current_latnecy_reg[28]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[29] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[31]_i_1_n_5 ),
        .Q(current_latnecy_reg[29]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[2] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[3]_i_1_n_6 ),
        .Q(current_latnecy_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[30] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[31]_i_1_n_6 ),
        .Q(current_latnecy_reg[30]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[31] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[31]_i_1_n_7 ),
        .Q(current_latnecy_reg[31]));
  CARRY4 \current_latnecy_reg[31]_i_1 
       (.CI(1'b0),
        .CO({\current_latnecy_reg[31]_i_1_n_0 ,\current_latnecy_reg[31]_i_1_n_1 ,\current_latnecy_reg[31]_i_1_n_2 ,\current_latnecy_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\current_latnecy_reg[31]_i_1_n_4 ,\current_latnecy_reg[31]_i_1_n_5 ,\current_latnecy_reg[31]_i_1_n_6 ,\current_latnecy_reg[31]_i_1_n_7 }),
        .S({\current_latnecy[31]_i_2_n_0 ,\current_latnecy[31]_i_3_n_0 ,\current_latnecy[31]_i_4_n_0 ,latency_o2}));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[3] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[3]_i_1_n_7 ),
        .Q(current_latnecy_reg[3]));
  CARRY4 \current_latnecy_reg[3]_i_1 
       (.CI(\current_latnecy_reg[7]_i_1_n_0 ),
        .CO({\NLW_current_latnecy_reg[3]_i_1_CO_UNCONNECTED [3],\current_latnecy_reg[3]_i_1_n_1 ,\current_latnecy_reg[3]_i_1_n_2 ,\current_latnecy_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_latnecy_reg[3]_i_1_n_4 ,\current_latnecy_reg[3]_i_1_n_5 ,\current_latnecy_reg[3]_i_1_n_6 ,\current_latnecy_reg[3]_i_1_n_7 }),
        .S({\current_latnecy[3]_i_2_n_0 ,\current_latnecy[3]_i_3_n_0 ,\current_latnecy[3]_i_4_n_0 ,\current_latnecy[3]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[4] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[7]_i_1_n_4 ),
        .Q(current_latnecy_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[5] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[7]_i_1_n_5 ),
        .Q(current_latnecy_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[6] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[7]_i_1_n_6 ),
        .Q(current_latnecy_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[7] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[7]_i_1_n_7 ),
        .Q(current_latnecy_reg[7]));
  CARRY4 \current_latnecy_reg[7]_i_1 
       (.CI(\current_latnecy_reg[11]_i_1_n_0 ),
        .CO({\current_latnecy_reg[7]_i_1_n_0 ,\current_latnecy_reg[7]_i_1_n_1 ,\current_latnecy_reg[7]_i_1_n_2 ,\current_latnecy_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_latnecy_reg[7]_i_1_n_4 ,\current_latnecy_reg[7]_i_1_n_5 ,\current_latnecy_reg[7]_i_1_n_6 ,\current_latnecy_reg[7]_i_1_n_7 }),
        .S({\current_latnecy[7]_i_2_n_0 ,\current_latnecy[7]_i_3_n_0 ,\current_latnecy[7]_i_4_n_0 ,\current_latnecy[7]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[8] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[11]_i_1_n_4 ),
        .Q(current_latnecy_reg[8]));
  FDCE #(
    .INIT(1'b0)) 
    \current_latnecy_reg[9] 
       (.C(clk),
        .CE(counting_latency),
        .CLR(rst),
        .D(\current_latnecy_reg[11]_i_1_n_5 ),
        .Q(current_latnecy_reg[9]));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[11]_i_2 
       (.I0(current_troughput_reg[8]),
        .O(\current_troughput[11]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[11]_i_3 
       (.I0(current_troughput_reg[9]),
        .O(\current_troughput[11]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[11]_i_4 
       (.I0(current_troughput_reg[10]),
        .O(\current_troughput[11]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[11]_i_5 
       (.I0(current_troughput_reg[11]),
        .O(\current_troughput[11]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[15]_i_2 
       (.I0(current_troughput_reg[12]),
        .O(\current_troughput[15]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[15]_i_3 
       (.I0(current_troughput_reg[13]),
        .O(\current_troughput[15]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[15]_i_4 
       (.I0(current_troughput_reg[14]),
        .O(\current_troughput[15]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[15]_i_5 
       (.I0(current_troughput_reg[15]),
        .O(\current_troughput[15]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[19]_i_2 
       (.I0(current_troughput_reg[16]),
        .O(\current_troughput[19]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[19]_i_3 
       (.I0(current_troughput_reg[17]),
        .O(\current_troughput[19]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[19]_i_4 
       (.I0(current_troughput_reg[18]),
        .O(\current_troughput[19]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[19]_i_5 
       (.I0(current_troughput_reg[19]),
        .O(\current_troughput[19]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[23]_i_2 
       (.I0(current_troughput_reg[20]),
        .O(\current_troughput[23]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[23]_i_3 
       (.I0(current_troughput_reg[21]),
        .O(\current_troughput[23]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[23]_i_4 
       (.I0(current_troughput_reg[22]),
        .O(\current_troughput[23]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[23]_i_5 
       (.I0(current_troughput_reg[23]),
        .O(\current_troughput[23]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[27]_i_2 
       (.I0(current_troughput_reg[24]),
        .O(\current_troughput[27]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[27]_i_3 
       (.I0(current_troughput_reg[25]),
        .O(\current_troughput[27]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[27]_i_4 
       (.I0(current_troughput_reg[26]),
        .O(\current_troughput[27]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[27]_i_5 
       (.I0(current_troughput_reg[27]),
        .O(\current_troughput[27]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[31]_i_2 
       (.I0(current_troughput_reg[28]),
        .O(\current_troughput[31]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[31]_i_3 
       (.I0(current_troughput_reg[29]),
        .O(\current_troughput[31]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[31]_i_4 
       (.I0(current_troughput_reg[30]),
        .O(\current_troughput[31]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB4)) 
    \current_troughput[31]_i_5 
       (.I0(packet_arived_last),
        .I1(packet_arived_i),
        .I2(current_troughput_reg[31]),
        .O(\current_troughput[31]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[3]_i_2 
       (.I0(current_troughput_reg[0]),
        .O(\current_troughput[3]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[3]_i_3 
       (.I0(current_troughput_reg[1]),
        .O(\current_troughput[3]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[3]_i_4 
       (.I0(current_troughput_reg[2]),
        .O(\current_troughput[3]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[3]_i_5 
       (.I0(current_troughput_reg[3]),
        .O(\current_troughput[3]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[7]_i_2 
       (.I0(current_troughput_reg[4]),
        .O(\current_troughput[7]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[7]_i_3 
       (.I0(current_troughput_reg[5]),
        .O(\current_troughput[7]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[7]_i_4 
       (.I0(current_troughput_reg[6]),
        .O(\current_troughput[7]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \current_troughput[7]_i_5 
       (.I0(current_troughput_reg[7]),
        .O(\current_troughput[7]_i_5_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[0] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[3]_i_1_n_4 ),
        .Q(current_troughput_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[10] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[11]_i_1_n_6 ),
        .Q(current_troughput_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[11] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[11]_i_1_n_7 ),
        .Q(current_troughput_reg[11]));
  CARRY4 \current_troughput_reg[11]_i_1 
       (.CI(\current_troughput_reg[15]_i_1_n_0 ),
        .CO({\current_troughput_reg[11]_i_1_n_0 ,\current_troughput_reg[11]_i_1_n_1 ,\current_troughput_reg[11]_i_1_n_2 ,\current_troughput_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_troughput_reg[11]_i_1_n_4 ,\current_troughput_reg[11]_i_1_n_5 ,\current_troughput_reg[11]_i_1_n_6 ,\current_troughput_reg[11]_i_1_n_7 }),
        .S({\current_troughput[11]_i_2_n_0 ,\current_troughput[11]_i_3_n_0 ,\current_troughput[11]_i_4_n_0 ,\current_troughput[11]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[12] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[15]_i_1_n_4 ),
        .Q(current_troughput_reg[12]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[13] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[15]_i_1_n_5 ),
        .Q(current_troughput_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[14] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[15]_i_1_n_6 ),
        .Q(current_troughput_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[15] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[15]_i_1_n_7 ),
        .Q(current_troughput_reg[15]));
  CARRY4 \current_troughput_reg[15]_i_1 
       (.CI(\current_troughput_reg[19]_i_1_n_0 ),
        .CO({\current_troughput_reg[15]_i_1_n_0 ,\current_troughput_reg[15]_i_1_n_1 ,\current_troughput_reg[15]_i_1_n_2 ,\current_troughput_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_troughput_reg[15]_i_1_n_4 ,\current_troughput_reg[15]_i_1_n_5 ,\current_troughput_reg[15]_i_1_n_6 ,\current_troughput_reg[15]_i_1_n_7 }),
        .S({\current_troughput[15]_i_2_n_0 ,\current_troughput[15]_i_3_n_0 ,\current_troughput[15]_i_4_n_0 ,\current_troughput[15]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[16] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[19]_i_1_n_4 ),
        .Q(current_troughput_reg[16]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[17] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[19]_i_1_n_5 ),
        .Q(current_troughput_reg[17]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[18] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[19]_i_1_n_6 ),
        .Q(current_troughput_reg[18]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[19] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[19]_i_1_n_7 ),
        .Q(current_troughput_reg[19]));
  CARRY4 \current_troughput_reg[19]_i_1 
       (.CI(\current_troughput_reg[23]_i_1_n_0 ),
        .CO({\current_troughput_reg[19]_i_1_n_0 ,\current_troughput_reg[19]_i_1_n_1 ,\current_troughput_reg[19]_i_1_n_2 ,\current_troughput_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_troughput_reg[19]_i_1_n_4 ,\current_troughput_reg[19]_i_1_n_5 ,\current_troughput_reg[19]_i_1_n_6 ,\current_troughput_reg[19]_i_1_n_7 }),
        .S({\current_troughput[19]_i_2_n_0 ,\current_troughput[19]_i_3_n_0 ,\current_troughput[19]_i_4_n_0 ,\current_troughput[19]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[1] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[3]_i_1_n_5 ),
        .Q(current_troughput_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[20] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[23]_i_1_n_4 ),
        .Q(current_troughput_reg[20]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[21] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[23]_i_1_n_5 ),
        .Q(current_troughput_reg[21]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[22] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[23]_i_1_n_6 ),
        .Q(current_troughput_reg[22]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[23] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[23]_i_1_n_7 ),
        .Q(current_troughput_reg[23]));
  CARRY4 \current_troughput_reg[23]_i_1 
       (.CI(\current_troughput_reg[27]_i_1_n_0 ),
        .CO({\current_troughput_reg[23]_i_1_n_0 ,\current_troughput_reg[23]_i_1_n_1 ,\current_troughput_reg[23]_i_1_n_2 ,\current_troughput_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_troughput_reg[23]_i_1_n_4 ,\current_troughput_reg[23]_i_1_n_5 ,\current_troughput_reg[23]_i_1_n_6 ,\current_troughput_reg[23]_i_1_n_7 }),
        .S({\current_troughput[23]_i_2_n_0 ,\current_troughput[23]_i_3_n_0 ,\current_troughput[23]_i_4_n_0 ,\current_troughput[23]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[24] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[27]_i_1_n_4 ),
        .Q(current_troughput_reg[24]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[25] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[27]_i_1_n_5 ),
        .Q(current_troughput_reg[25]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[26] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[27]_i_1_n_6 ),
        .Q(current_troughput_reg[26]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[27] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[27]_i_1_n_7 ),
        .Q(current_troughput_reg[27]));
  CARRY4 \current_troughput_reg[27]_i_1 
       (.CI(\current_troughput_reg[31]_i_1_n_0 ),
        .CO({\current_troughput_reg[27]_i_1_n_0 ,\current_troughput_reg[27]_i_1_n_1 ,\current_troughput_reg[27]_i_1_n_2 ,\current_troughput_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_troughput_reg[27]_i_1_n_4 ,\current_troughput_reg[27]_i_1_n_5 ,\current_troughput_reg[27]_i_1_n_6 ,\current_troughput_reg[27]_i_1_n_7 }),
        .S({\current_troughput[27]_i_2_n_0 ,\current_troughput[27]_i_3_n_0 ,\current_troughput[27]_i_4_n_0 ,\current_troughput[27]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[28] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[31]_i_1_n_4 ),
        .Q(current_troughput_reg[28]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[29] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[31]_i_1_n_5 ),
        .Q(current_troughput_reg[29]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[2] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[3]_i_1_n_6 ),
        .Q(current_troughput_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[30] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[31]_i_1_n_6 ),
        .Q(current_troughput_reg[30]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[31] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[31]_i_1_n_7 ),
        .Q(current_troughput_reg[31]));
  CARRY4 \current_troughput_reg[31]_i_1 
       (.CI(1'b0),
        .CO({\current_troughput_reg[31]_i_1_n_0 ,\current_troughput_reg[31]_i_1_n_1 ,\current_troughput_reg[31]_i_1_n_2 ,\current_troughput_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,current_troughput_reg[31]}),
        .O({\current_troughput_reg[31]_i_1_n_4 ,\current_troughput_reg[31]_i_1_n_5 ,\current_troughput_reg[31]_i_1_n_6 ,\current_troughput_reg[31]_i_1_n_7 }),
        .S({\current_troughput[31]_i_2_n_0 ,\current_troughput[31]_i_3_n_0 ,\current_troughput[31]_i_4_n_0 ,\current_troughput[31]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[3] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[3]_i_1_n_7 ),
        .Q(current_troughput_reg[3]));
  CARRY4 \current_troughput_reg[3]_i_1 
       (.CI(\current_troughput_reg[7]_i_1_n_0 ),
        .CO({\NLW_current_troughput_reg[3]_i_1_CO_UNCONNECTED [3],\current_troughput_reg[3]_i_1_n_1 ,\current_troughput_reg[3]_i_1_n_2 ,\current_troughput_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_troughput_reg[3]_i_1_n_4 ,\current_troughput_reg[3]_i_1_n_5 ,\current_troughput_reg[3]_i_1_n_6 ,\current_troughput_reg[3]_i_1_n_7 }),
        .S({\current_troughput[3]_i_2_n_0 ,\current_troughput[3]_i_3_n_0 ,\current_troughput[3]_i_4_n_0 ,\current_troughput[3]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[4] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[7]_i_1_n_4 ),
        .Q(current_troughput_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[5] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[7]_i_1_n_5 ),
        .Q(current_troughput_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[6] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[7]_i_1_n_6 ),
        .Q(current_troughput_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[7] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[7]_i_1_n_7 ),
        .Q(current_troughput_reg[7]));
  CARRY4 \current_troughput_reg[7]_i_1 
       (.CI(\current_troughput_reg[11]_i_1_n_0 ),
        .CO({\current_troughput_reg[7]_i_1_n_0 ,\current_troughput_reg[7]_i_1_n_1 ,\current_troughput_reg[7]_i_1_n_2 ,\current_troughput_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\current_troughput_reg[7]_i_1_n_4 ,\current_troughput_reg[7]_i_1_n_5 ,\current_troughput_reg[7]_i_1_n_6 ,\current_troughput_reg[7]_i_1_n_7 }),
        .S({\current_troughput[7]_i_2_n_0 ,\current_troughput[7]_i_3_n_0 ,\current_troughput[7]_i_4_n_0 ,\current_troughput[7]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[8] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[11]_i_1_n_4 ),
        .Q(current_troughput_reg[8]));
  FDCE #(
    .INIT(1'b0)) 
    \current_troughput_reg[9] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\current_troughput_reg[11]_i_1_n_5 ),
        .Q(current_troughput_reg[9]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    latency_measured_C_i_1
       (.I0(latency_measured_reg_P_n_0),
        .I1(latency_measured_reg_LDC_n_0),
        .I2(latency_measured_reg_C_n_0),
        .O(latency_measured));
  FDCE latency_measured_reg_C
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(rst),
        .D(latency_measured),
        .Q(latency_measured_reg_C_n_0));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    latency_measured_reg_LDC
       (.CLR(rst),
        .D(1'b1),
        .G(counting_latency3),
        .GE(1'b1),
        .Q(latency_measured_reg_LDC_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    latency_measured_reg_LDC_i_1
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .O(counting_latency3));
  FDPE latency_measured_reg_P
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_measured),
        .PRE(counting_latency3),
        .Q(latency_measured_reg_P_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[0]_INST_0 
       (.I0(\latency_o_reg[0]_P_n_0 ),
        .I1(\latency_o_reg[0]_LDC_n_0 ),
        .I2(\latency_o_reg[0]_C_n_0 ),
        .O(latency_o[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[10]_INST_0 
       (.I0(\latency_o_reg[10]_P_n_0 ),
        .I1(\latency_o_reg[10]_LDC_n_0 ),
        .I2(\latency_o_reg[10]_C_n_0 ),
        .O(latency_o[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[11]_INST_0 
       (.I0(\latency_o_reg[11]_P_n_0 ),
        .I1(\latency_o_reg[11]_LDC_n_0 ),
        .I2(\latency_o_reg[11]_C_n_0 ),
        .O(latency_o[11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[12]_INST_0 
       (.I0(\latency_o_reg[12]_P_n_0 ),
        .I1(\latency_o_reg[12]_LDC_n_0 ),
        .I2(\latency_o_reg[12]_C_n_0 ),
        .O(latency_o[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[13]_INST_0 
       (.I0(\latency_o_reg[13]_P_n_0 ),
        .I1(\latency_o_reg[13]_LDC_n_0 ),
        .I2(\latency_o_reg[13]_C_n_0 ),
        .O(latency_o[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[14]_INST_0 
       (.I0(\latency_o_reg[14]_P_n_0 ),
        .I1(\latency_o_reg[14]_LDC_n_0 ),
        .I2(\latency_o_reg[14]_C_n_0 ),
        .O(latency_o[14]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[15]_INST_0 
       (.I0(\latency_o_reg[15]_P_n_0 ),
        .I1(\latency_o_reg[15]_LDC_n_0 ),
        .I2(\latency_o_reg[15]_C_n_0 ),
        .O(latency_o[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[16]_INST_0 
       (.I0(\latency_o_reg[16]_P_n_0 ),
        .I1(\latency_o_reg[16]_LDC_n_0 ),
        .I2(\latency_o_reg[16]_C_n_0 ),
        .O(latency_o[16]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[17]_INST_0 
       (.I0(\latency_o_reg[17]_P_n_0 ),
        .I1(\latency_o_reg[17]_LDC_n_0 ),
        .I2(\latency_o_reg[17]_C_n_0 ),
        .O(latency_o[17]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[18]_INST_0 
       (.I0(\latency_o_reg[18]_P_n_0 ),
        .I1(\latency_o_reg[18]_LDC_n_0 ),
        .I2(\latency_o_reg[18]_C_n_0 ),
        .O(latency_o[18]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[19]_INST_0 
       (.I0(\latency_o_reg[19]_P_n_0 ),
        .I1(\latency_o_reg[19]_LDC_n_0 ),
        .I2(\latency_o_reg[19]_C_n_0 ),
        .O(latency_o[19]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[1]_INST_0 
       (.I0(\latency_o_reg[1]_P_n_0 ),
        .I1(\latency_o_reg[1]_LDC_n_0 ),
        .I2(\latency_o_reg[1]_C_n_0 ),
        .O(latency_o[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[20]_INST_0 
       (.I0(\latency_o_reg[20]_P_n_0 ),
        .I1(\latency_o_reg[20]_LDC_n_0 ),
        .I2(\latency_o_reg[20]_C_n_0 ),
        .O(latency_o[20]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[21]_INST_0 
       (.I0(\latency_o_reg[21]_P_n_0 ),
        .I1(\latency_o_reg[21]_LDC_n_0 ),
        .I2(\latency_o_reg[21]_C_n_0 ),
        .O(latency_o[21]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[22]_INST_0 
       (.I0(\latency_o_reg[22]_P_n_0 ),
        .I1(\latency_o_reg[22]_LDC_n_0 ),
        .I2(\latency_o_reg[22]_C_n_0 ),
        .O(latency_o[22]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[23]_INST_0 
       (.I0(\latency_o_reg[23]_P_n_0 ),
        .I1(\latency_o_reg[23]_LDC_n_0 ),
        .I2(\latency_o_reg[23]_C_n_0 ),
        .O(latency_o[23]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[24]_INST_0 
       (.I0(\latency_o_reg[24]_P_n_0 ),
        .I1(\latency_o_reg[24]_LDC_n_0 ),
        .I2(\latency_o_reg[24]_C_n_0 ),
        .O(latency_o[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[25]_INST_0 
       (.I0(\latency_o_reg[25]_P_n_0 ),
        .I1(\latency_o_reg[25]_LDC_n_0 ),
        .I2(\latency_o_reg[25]_C_n_0 ),
        .O(latency_o[25]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[26]_INST_0 
       (.I0(\latency_o_reg[26]_P_n_0 ),
        .I1(\latency_o_reg[26]_LDC_n_0 ),
        .I2(\latency_o_reg[26]_C_n_0 ),
        .O(latency_o[26]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[27]_INST_0 
       (.I0(\latency_o_reg[27]_P_n_0 ),
        .I1(\latency_o_reg[27]_LDC_n_0 ),
        .I2(\latency_o_reg[27]_C_n_0 ),
        .O(latency_o[27]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[28]_INST_0 
       (.I0(\latency_o_reg[28]_P_n_0 ),
        .I1(\latency_o_reg[28]_LDC_n_0 ),
        .I2(\latency_o_reg[28]_C_n_0 ),
        .O(latency_o[28]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[29]_INST_0 
       (.I0(\latency_o_reg[29]_P_n_0 ),
        .I1(\latency_o_reg[29]_LDC_n_0 ),
        .I2(\latency_o_reg[29]_C_n_0 ),
        .O(latency_o[29]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[2]_INST_0 
       (.I0(\latency_o_reg[2]_P_n_0 ),
        .I1(\latency_o_reg[2]_LDC_n_0 ),
        .I2(\latency_o_reg[2]_C_n_0 ),
        .O(latency_o[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[30]_INST_0 
       (.I0(\latency_o_reg[30]_P_n_0 ),
        .I1(\latency_o_reg[30]_LDC_n_0 ),
        .I2(\latency_o_reg[30]_C_n_0 ),
        .O(latency_o[30]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[31]_INST_0 
       (.I0(\latency_o_reg[31]_P_n_0 ),
        .I1(\latency_o_reg[31]_LDC_n_0 ),
        .I2(\latency_o_reg[31]_C_n_0 ),
        .O(latency_o[31]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[3]_INST_0 
       (.I0(\latency_o_reg[3]_P_n_0 ),
        .I1(\latency_o_reg[3]_LDC_n_0 ),
        .I2(\latency_o_reg[3]_C_n_0 ),
        .O(latency_o[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[4]_INST_0 
       (.I0(\latency_o_reg[4]_P_n_0 ),
        .I1(\latency_o_reg[4]_LDC_n_0 ),
        .I2(\latency_o_reg[4]_C_n_0 ),
        .O(latency_o[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[5]_INST_0 
       (.I0(\latency_o_reg[5]_P_n_0 ),
        .I1(\latency_o_reg[5]_LDC_n_0 ),
        .I2(\latency_o_reg[5]_C_n_0 ),
        .O(latency_o[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[6]_INST_0 
       (.I0(\latency_o_reg[6]_P_n_0 ),
        .I1(\latency_o_reg[6]_LDC_n_0 ),
        .I2(\latency_o_reg[6]_C_n_0 ),
        .O(latency_o[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[7]_INST_0 
       (.I0(\latency_o_reg[7]_P_n_0 ),
        .I1(\latency_o_reg[7]_LDC_n_0 ),
        .I2(\latency_o_reg[7]_C_n_0 ),
        .O(latency_o[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[8]_INST_0 
       (.I0(\latency_o_reg[8]_P_n_0 ),
        .I1(\latency_o_reg[8]_LDC_n_0 ),
        .I2(\latency_o_reg[8]_C_n_0 ),
        .O(latency_o[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \latency_o[9]_INST_0 
       (.I0(\latency_o_reg[9]_P_n_0 ),
        .I1(\latency_o_reg[9]_LDC_n_0 ),
        .I2(\latency_o_reg[9]_C_n_0 ),
        .O(latency_o[9]));
  FDCE \latency_o_reg[0]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[0]_LDC_i_2_n_0 ),
        .D(latency_o[0]),
        .Q(\latency_o_reg[0]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[0]_LDC 
       (.CLR(\latency_o_reg[0]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[0]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[0]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[0]_LDC_i_1 
       (.I0(current_latnecy_reg[0]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[0]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[0]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[0]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[0]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[0]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[0]),
        .PRE(\latency_o_reg[0]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[0]_P_n_0 ));
  FDCE \latency_o_reg[10]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[10]_LDC_i_2_n_0 ),
        .D(latency_o[10]),
        .Q(\latency_o_reg[10]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[10]_LDC 
       (.CLR(\latency_o_reg[10]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[10]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[10]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[10]_LDC_i_1 
       (.I0(current_latnecy_reg[10]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[10]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[10]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[10]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[10]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[10]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[10]),
        .PRE(\latency_o_reg[10]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[10]_P_n_0 ));
  FDCE \latency_o_reg[11]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[11]_LDC_i_2_n_0 ),
        .D(latency_o[11]),
        .Q(\latency_o_reg[11]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[11]_LDC 
       (.CLR(\latency_o_reg[11]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[11]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[11]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[11]_LDC_i_1 
       (.I0(current_latnecy_reg[11]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[11]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[11]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[11]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[11]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[11]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[11]),
        .PRE(\latency_o_reg[11]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[11]_P_n_0 ));
  FDCE \latency_o_reg[12]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[12]_LDC_i_2_n_0 ),
        .D(latency_o[12]),
        .Q(\latency_o_reg[12]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[12]_LDC 
       (.CLR(\latency_o_reg[12]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[12]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[12]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[12]_LDC_i_1 
       (.I0(current_latnecy_reg[12]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[12]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[12]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[12]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[12]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[12]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[12]),
        .PRE(\latency_o_reg[12]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[12]_P_n_0 ));
  FDCE \latency_o_reg[13]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[13]_LDC_i_2_n_0 ),
        .D(latency_o[13]),
        .Q(\latency_o_reg[13]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[13]_LDC 
       (.CLR(\latency_o_reg[13]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[13]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[13]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[13]_LDC_i_1 
       (.I0(current_latnecy_reg[13]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[13]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[13]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[13]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[13]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[13]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[13]),
        .PRE(\latency_o_reg[13]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[13]_P_n_0 ));
  FDCE \latency_o_reg[14]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[14]_LDC_i_2_n_0 ),
        .D(latency_o[14]),
        .Q(\latency_o_reg[14]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[14]_LDC 
       (.CLR(\latency_o_reg[14]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[14]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[14]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[14]_LDC_i_1 
       (.I0(current_latnecy_reg[14]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[14]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[14]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[14]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[14]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[14]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[14]),
        .PRE(\latency_o_reg[14]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[14]_P_n_0 ));
  FDCE \latency_o_reg[15]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[15]_LDC_i_2_n_0 ),
        .D(latency_o[15]),
        .Q(\latency_o_reg[15]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[15]_LDC 
       (.CLR(\latency_o_reg[15]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[15]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[15]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[15]_LDC_i_1 
       (.I0(current_latnecy_reg[15]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[15]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[15]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[15]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[15]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[15]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[15]),
        .PRE(\latency_o_reg[15]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[15]_P_n_0 ));
  FDCE \latency_o_reg[16]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[16]_LDC_i_2_n_0 ),
        .D(latency_o[16]),
        .Q(\latency_o_reg[16]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[16]_LDC 
       (.CLR(\latency_o_reg[16]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[16]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[16]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[16]_LDC_i_1 
       (.I0(current_latnecy_reg[16]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[16]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[16]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[16]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[16]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[16]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[16]),
        .PRE(\latency_o_reg[16]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[16]_P_n_0 ));
  FDCE \latency_o_reg[17]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[17]_LDC_i_2_n_0 ),
        .D(latency_o[17]),
        .Q(\latency_o_reg[17]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[17]_LDC 
       (.CLR(\latency_o_reg[17]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[17]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[17]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[17]_LDC_i_1 
       (.I0(current_latnecy_reg[17]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[17]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[17]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[17]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[17]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[17]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[17]),
        .PRE(\latency_o_reg[17]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[17]_P_n_0 ));
  FDCE \latency_o_reg[18]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[18]_LDC_i_2_n_0 ),
        .D(latency_o[18]),
        .Q(\latency_o_reg[18]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[18]_LDC 
       (.CLR(\latency_o_reg[18]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[18]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[18]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[18]_LDC_i_1 
       (.I0(current_latnecy_reg[18]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[18]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[18]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[18]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[18]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[18]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[18]),
        .PRE(\latency_o_reg[18]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[18]_P_n_0 ));
  FDCE \latency_o_reg[19]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[19]_LDC_i_2_n_0 ),
        .D(latency_o[19]),
        .Q(\latency_o_reg[19]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[19]_LDC 
       (.CLR(\latency_o_reg[19]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[19]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[19]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[19]_LDC_i_1 
       (.I0(current_latnecy_reg[19]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[19]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[19]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[19]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[19]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[19]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[19]),
        .PRE(\latency_o_reg[19]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[19]_P_n_0 ));
  FDCE \latency_o_reg[1]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[1]_LDC_i_2_n_0 ),
        .D(latency_o[1]),
        .Q(\latency_o_reg[1]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[1]_LDC 
       (.CLR(\latency_o_reg[1]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[1]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[1]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[1]_LDC_i_1 
       (.I0(current_latnecy_reg[1]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[1]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[1]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[1]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[1]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[1]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[1]),
        .PRE(\latency_o_reg[1]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[1]_P_n_0 ));
  FDCE \latency_o_reg[20]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[20]_LDC_i_2_n_0 ),
        .D(latency_o[20]),
        .Q(\latency_o_reg[20]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[20]_LDC 
       (.CLR(\latency_o_reg[20]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[20]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[20]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[20]_LDC_i_1 
       (.I0(current_latnecy_reg[20]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[20]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[20]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[20]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[20]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[20]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[20]),
        .PRE(\latency_o_reg[20]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[20]_P_n_0 ));
  FDCE \latency_o_reg[21]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[21]_LDC_i_2_n_0 ),
        .D(latency_o[21]),
        .Q(\latency_o_reg[21]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[21]_LDC 
       (.CLR(\latency_o_reg[21]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[21]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[21]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[21]_LDC_i_1 
       (.I0(current_latnecy_reg[21]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[21]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[21]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[21]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[21]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[21]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[21]),
        .PRE(\latency_o_reg[21]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[21]_P_n_0 ));
  FDCE \latency_o_reg[22]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[22]_LDC_i_2_n_0 ),
        .D(latency_o[22]),
        .Q(\latency_o_reg[22]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[22]_LDC 
       (.CLR(\latency_o_reg[22]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[22]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[22]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[22]_LDC_i_1 
       (.I0(current_latnecy_reg[22]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[22]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[22]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[22]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[22]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[22]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[22]),
        .PRE(\latency_o_reg[22]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[22]_P_n_0 ));
  FDCE \latency_o_reg[23]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[23]_LDC_i_2_n_0 ),
        .D(latency_o[23]),
        .Q(\latency_o_reg[23]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[23]_LDC 
       (.CLR(\latency_o_reg[23]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[23]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[23]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[23]_LDC_i_1 
       (.I0(current_latnecy_reg[23]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[23]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[23]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[23]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[23]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[23]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[23]),
        .PRE(\latency_o_reg[23]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[23]_P_n_0 ));
  FDCE \latency_o_reg[24]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[24]_LDC_i_2_n_0 ),
        .D(latency_o[24]),
        .Q(\latency_o_reg[24]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[24]_LDC 
       (.CLR(\latency_o_reg[24]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[24]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[24]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[24]_LDC_i_1 
       (.I0(current_latnecy_reg[24]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[24]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[24]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[24]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[24]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[24]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[24]),
        .PRE(\latency_o_reg[24]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[24]_P_n_0 ));
  FDCE \latency_o_reg[25]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[25]_LDC_i_2_n_0 ),
        .D(latency_o[25]),
        .Q(\latency_o_reg[25]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[25]_LDC 
       (.CLR(\latency_o_reg[25]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[25]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[25]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[25]_LDC_i_1 
       (.I0(current_latnecy_reg[25]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[25]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[25]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[25]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[25]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[25]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[25]),
        .PRE(\latency_o_reg[25]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[25]_P_n_0 ));
  FDCE \latency_o_reg[26]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[26]_LDC_i_2_n_0 ),
        .D(latency_o[26]),
        .Q(\latency_o_reg[26]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[26]_LDC 
       (.CLR(\latency_o_reg[26]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[26]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[26]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[26]_LDC_i_1 
       (.I0(current_latnecy_reg[26]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[26]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[26]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[26]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[26]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[26]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[26]),
        .PRE(\latency_o_reg[26]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[26]_P_n_0 ));
  FDCE \latency_o_reg[27]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[27]_LDC_i_2_n_0 ),
        .D(latency_o[27]),
        .Q(\latency_o_reg[27]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[27]_LDC 
       (.CLR(\latency_o_reg[27]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[27]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[27]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[27]_LDC_i_1 
       (.I0(current_latnecy_reg[27]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[27]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[27]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[27]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[27]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[27]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[27]),
        .PRE(\latency_o_reg[27]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[27]_P_n_0 ));
  FDCE \latency_o_reg[28]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[28]_LDC_i_2_n_0 ),
        .D(latency_o[28]),
        .Q(\latency_o_reg[28]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[28]_LDC 
       (.CLR(\latency_o_reg[28]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[28]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[28]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[28]_LDC_i_1 
       (.I0(current_latnecy_reg[28]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[28]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[28]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[28]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[28]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[28]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[28]),
        .PRE(\latency_o_reg[28]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[28]_P_n_0 ));
  FDCE \latency_o_reg[29]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[29]_LDC_i_2_n_0 ),
        .D(latency_o[29]),
        .Q(\latency_o_reg[29]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[29]_LDC 
       (.CLR(\latency_o_reg[29]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[29]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[29]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[29]_LDC_i_1 
       (.I0(current_latnecy_reg[29]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[29]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[29]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[29]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[29]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[29]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[29]),
        .PRE(\latency_o_reg[29]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[29]_P_n_0 ));
  FDCE \latency_o_reg[2]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[2]_LDC_i_2_n_0 ),
        .D(latency_o[2]),
        .Q(\latency_o_reg[2]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[2]_LDC 
       (.CLR(\latency_o_reg[2]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[2]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[2]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[2]_LDC_i_1 
       (.I0(current_latnecy_reg[2]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[2]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[2]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[2]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[2]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[2]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[2]),
        .PRE(\latency_o_reg[2]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[2]_P_n_0 ));
  FDCE \latency_o_reg[30]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[30]_LDC_i_2_n_0 ),
        .D(latency_o[30]),
        .Q(\latency_o_reg[30]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[30]_LDC 
       (.CLR(\latency_o_reg[30]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[30]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[30]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[30]_LDC_i_1 
       (.I0(current_latnecy_reg[30]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[30]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[30]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[30]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[30]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[30]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[30]),
        .PRE(\latency_o_reg[30]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[30]_P_n_0 ));
  FDCE \latency_o_reg[31]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[31]_LDC_i_2_n_0 ),
        .D(latency_o[31]),
        .Q(\latency_o_reg[31]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[31]_LDC 
       (.CLR(\latency_o_reg[31]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[31]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[31]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[31]_LDC_i_1 
       (.I0(current_latnecy_reg[31]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[31]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[31]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[31]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[31]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[31]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[31]),
        .PRE(\latency_o_reg[31]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[31]_P_n_0 ));
  FDCE \latency_o_reg[3]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[3]_LDC_i_2_n_0 ),
        .D(latency_o[3]),
        .Q(\latency_o_reg[3]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[3]_LDC 
       (.CLR(\latency_o_reg[3]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[3]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[3]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[3]_LDC_i_1 
       (.I0(current_latnecy_reg[3]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[3]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[3]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[3]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[3]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[3]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[3]),
        .PRE(\latency_o_reg[3]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[3]_P_n_0 ));
  FDCE \latency_o_reg[4]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[4]_LDC_i_2_n_0 ),
        .D(latency_o[4]),
        .Q(\latency_o_reg[4]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[4]_LDC 
       (.CLR(\latency_o_reg[4]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[4]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[4]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[4]_LDC_i_1 
       (.I0(current_latnecy_reg[4]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[4]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[4]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[4]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[4]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[4]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[4]),
        .PRE(\latency_o_reg[4]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[4]_P_n_0 ));
  FDCE \latency_o_reg[5]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[5]_LDC_i_2_n_0 ),
        .D(latency_o[5]),
        .Q(\latency_o_reg[5]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[5]_LDC 
       (.CLR(\latency_o_reg[5]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[5]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[5]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[5]_LDC_i_1 
       (.I0(current_latnecy_reg[5]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[5]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[5]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[5]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[5]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[5]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[5]),
        .PRE(\latency_o_reg[5]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[5]_P_n_0 ));
  FDCE \latency_o_reg[6]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[6]_LDC_i_2_n_0 ),
        .D(latency_o[6]),
        .Q(\latency_o_reg[6]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[6]_LDC 
       (.CLR(\latency_o_reg[6]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[6]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[6]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[6]_LDC_i_1 
       (.I0(current_latnecy_reg[6]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[6]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[6]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[6]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[6]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[6]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[6]),
        .PRE(\latency_o_reg[6]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[6]_P_n_0 ));
  FDCE \latency_o_reg[7]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[7]_LDC_i_2_n_0 ),
        .D(latency_o[7]),
        .Q(\latency_o_reg[7]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[7]_LDC 
       (.CLR(\latency_o_reg[7]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[7]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[7]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[7]_LDC_i_1 
       (.I0(current_latnecy_reg[7]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[7]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[7]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[7]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[7]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[7]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[7]),
        .PRE(\latency_o_reg[7]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[7]_P_n_0 ));
  FDCE \latency_o_reg[8]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[8]_LDC_i_2_n_0 ),
        .D(latency_o[8]),
        .Q(\latency_o_reg[8]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[8]_LDC 
       (.CLR(\latency_o_reg[8]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[8]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[8]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[8]_LDC_i_1 
       (.I0(current_latnecy_reg[8]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[8]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[8]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[8]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[8]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[8]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[8]),
        .PRE(\latency_o_reg[8]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[8]_P_n_0 ));
  FDCE \latency_o_reg[9]_C 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .CLR(\latency_o_reg[9]_LDC_i_2_n_0 ),
        .D(latency_o[9]),
        .Q(\latency_o_reg[9]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \latency_o_reg[9]_LDC 
       (.CLR(\latency_o_reg[9]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\latency_o_reg[9]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\latency_o_reg[9]_LDC_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \latency_o_reg[9]_LDC_i_1 
       (.I0(current_latnecy_reg[9]),
        .I1(axi_reset),
        .I2(reset),
        .I3(counting_latency),
        .I4(packet_arived_i),
        .O(\latency_o_reg[9]_LDC_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF08FF)) 
    \latency_o_reg[9]_LDC_i_2 
       (.I0(counting_latency),
        .I1(packet_arived_i),
        .I2(current_latnecy_reg[9]),
        .I3(axi_reset),
        .I4(reset),
        .O(\latency_o_reg[9]_LDC_i_2_n_0 ));
  FDPE \latency_o_reg[9]_P 
       (.C(axi_transaction_completet),
        .CE(1'b1),
        .D(latency_o[9]),
        .PRE(\latency_o_reg[9]_LDC_i_1_n_0 ),
        .Q(\latency_o_reg[9]_P_n_0 ));
  LUT4 #(
    .INIT(16'hF4B0)) 
    packet_arived_last_i_1
       (.I0(\troughput_state_reg[0]_C_n_0 ),
        .I1(\troughput_state_reg[1]_C_n_0 ),
        .I2(packet_arived_last),
        .I3(packet_arived_i),
        .O(packet_arived_last_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    packet_arived_last_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(rst),
        .D(packet_arived_last_i_1_n_0),
        .Q(packet_arived_last));
  LUT5 #(
    .INIT(32'h44444440)) 
    \throughput_o[0]_i_1 
       (.I0(\troughput_state_reg[0]_C_n_0 ),
        .I1(\troughput_state_reg[1]_C_n_0 ),
        .I2(\throughput_o[0]_i_4_n_0 ),
        .I3(\throughput_o[0]_i_5_n_0 ),
        .I4(\throughput_o[0]_i_6_n_0 ),
        .O(\throughput_o[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h88888880)) 
    \throughput_o[0]_i_12 
       (.I0(p_0_in[6]),
        .I1(p_0_in[5]),
        .I2(p_0_in[4]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .O(\throughput_o[0]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \throughput_o[0]_i_13 
       (.I0(p_0_in[24]),
        .I1(p_0_in[11]),
        .I2(p_0_in[28]),
        .I3(p_0_in[22]),
        .O(\throughput_o[0]_i_13_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_17 
       (.I0(current_troughput_reg[0]),
        .O(\throughput_o[0]_i_17_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_18 
       (.I0(current_troughput_reg[1]),
        .O(\throughput_o[0]_i_18_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_19 
       (.I0(current_troughput_reg[2]),
        .O(\throughput_o[0]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[0]_i_2 
       (.I0(current_troughput[0]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[31]));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_20 
       (.I0(current_troughput_reg[3]),
        .O(\throughput_o[0]_i_20_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_23 
       (.I0(throughput_timer_reg[15]),
        .O(\throughput_o[0]_i_23_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_24 
       (.I0(throughput_timer_reg[16]),
        .O(\throughput_o[0]_i_24_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_25 
       (.I0(throughput_timer_reg[17]),
        .O(\throughput_o[0]_i_25_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_26 
       (.I0(throughput_timer_reg[18]),
        .O(\throughput_o[0]_i_26_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_27 
       (.I0(throughput_timer_reg[11]),
        .O(\throughput_o[0]_i_27_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_28 
       (.I0(throughput_timer_reg[12]),
        .O(\throughput_o[0]_i_28_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_29 
       (.I0(throughput_timer_reg[13]),
        .O(\throughput_o[0]_i_29_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \throughput_o[0]_i_3 
       (.I0(reset),
        .I1(axi_reset),
        .O(rst));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_30 
       (.I0(throughput_timer_reg[14]),
        .O(\throughput_o[0]_i_30_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_32 
       (.I0(throughput_timer_reg[0]),
        .O(\throughput_o[0]_i_32_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_33 
       (.I0(throughput_timer_reg[1]),
        .O(\throughput_o[0]_i_33_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_34 
       (.I0(throughput_timer_reg[2]),
        .O(\throughput_o[0]_i_34_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_35 
       (.I0(throughput_timer_reg[3]),
        .O(\throughput_o[0]_i_35_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_36 
       (.I0(throughput_timer_reg[4]),
        .O(\throughput_o[0]_i_36_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_37 
       (.I0(throughput_timer_reg[5]),
        .O(\throughput_o[0]_i_37_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_38 
       (.I0(throughput_timer_reg[6]),
        .O(\throughput_o[0]_i_38_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_39 
       (.I0(throughput_timer_reg[7]),
        .O(\throughput_o[0]_i_39_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \throughput_o[0]_i_4 
       (.I0(\throughput_o[0]_i_8_n_0 ),
        .I1(\throughput_o[0]_i_9_n_0 ),
        .I2(p_0_in[14]),
        .I3(p_0_in[19]),
        .I4(p_0_in[15]),
        .O(\throughput_o[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_40 
       (.I0(throughput_timer_reg[8]),
        .O(\throughput_o[0]_i_40_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_41 
       (.I0(throughput_timer_reg[9]),
        .O(\throughput_o[0]_i_41_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_42 
       (.I0(throughput_timer_reg[10]),
        .O(\throughput_o[0]_i_42_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_43 
       (.I0(throughput_timer_reg[19]),
        .O(\throughput_o[0]_i_43_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_44 
       (.I0(throughput_timer_reg[20]),
        .O(\throughput_o[0]_i_44_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_45 
       (.I0(throughput_timer_reg[21]),
        .O(\throughput_o[0]_i_45_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_46 
       (.I0(throughput_timer_reg[22]),
        .O(\throughput_o[0]_i_46_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_47 
       (.I0(throughput_timer_reg[23]),
        .O(\throughput_o[0]_i_47_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_48 
       (.I0(throughput_timer_reg[24]),
        .O(\throughput_o[0]_i_48_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_49 
       (.I0(throughput_timer_reg[25]),
        .O(\throughput_o[0]_i_49_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \throughput_o[0]_i_5 
       (.I0(\throughput_o[0]_i_12_n_0 ),
        .I1(\throughput_o[0]_i_13_n_0 ),
        .I2(p_0_in[29]),
        .I3(p_0_in[16]),
        .I4(p_0_in[30]),
        .I5(p_0_in[27]),
        .O(\throughput_o[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_50 
       (.I0(throughput_timer_reg[26]),
        .O(\throughput_o[0]_i_50_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_51 
       (.I0(throughput_timer_reg[27]),
        .O(\throughput_o[0]_i_51_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_52 
       (.I0(throughput_timer_reg[28]),
        .O(\throughput_o[0]_i_52_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_53 
       (.I0(throughput_timer_reg[29]),
        .O(\throughput_o[0]_i_53_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[0]_i_54 
       (.I0(throughput_timer_reg[30]),
        .O(\throughput_o[0]_i_54_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \throughput_o[0]_i_6 
       (.I0(p_0_in[23]),
        .I1(p_0_in[25]),
        .I2(p_0_in[21]),
        .I3(p_0_in[20]),
        .I4(p_0_in[31]),
        .I5(p_0_in[26]),
        .O(\throughput_o[0]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \throughput_o[0]_i_8 
       (.I0(p_0_in[9]),
        .I1(p_0_in[18]),
        .I2(p_0_in[12]),
        .I3(p_0_in[7]),
        .O(\throughput_o[0]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \throughput_o[0]_i_9 
       (.I0(p_0_in[10]),
        .I1(p_0_in[17]),
        .I2(p_0_in[13]),
        .I3(p_0_in[8]),
        .O(\throughput_o[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[10]_i_1 
       (.I0(current_troughput[10]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[21]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[11]_i_1 
       (.I0(current_troughput[11]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[20]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[12]_i_1 
       (.I0(current_troughput[12]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[19]));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[12]_i_3 
       (.I0(current_troughput_reg[12]),
        .O(\throughput_o[12]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[12]_i_4 
       (.I0(current_troughput_reg[13]),
        .O(\throughput_o[12]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[12]_i_5 
       (.I0(current_troughput_reg[14]),
        .O(\throughput_o[12]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[12]_i_6 
       (.I0(current_troughput_reg[15]),
        .O(\throughput_o[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[13]_i_1 
       (.I0(current_troughput[13]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[18]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[14]_i_1 
       (.I0(current_troughput[14]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[17]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[15]_i_1 
       (.I0(current_troughput[15]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[16]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[16]_i_1 
       (.I0(current_troughput[16]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[15]));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[16]_i_3 
       (.I0(current_troughput_reg[16]),
        .O(\throughput_o[16]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[16]_i_4 
       (.I0(current_troughput_reg[17]),
        .O(\throughput_o[16]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[16]_i_5 
       (.I0(current_troughput_reg[18]),
        .O(\throughput_o[16]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[16]_i_6 
       (.I0(current_troughput_reg[19]),
        .O(\throughput_o[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[17]_i_1 
       (.I0(current_troughput[17]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[14]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[18]_i_1 
       (.I0(current_troughput[18]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[13]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[19]_i_1 
       (.I0(current_troughput[19]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[12]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[1]_i_1 
       (.I0(current_troughput[1]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[30]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[20]_i_1 
       (.I0(current_troughput[20]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[11]));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[20]_i_3 
       (.I0(current_troughput_reg[20]),
        .O(\throughput_o[20]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[20]_i_4 
       (.I0(current_troughput_reg[21]),
        .O(\throughput_o[20]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[20]_i_5 
       (.I0(current_troughput_reg[22]),
        .O(\throughput_o[20]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[20]_i_6 
       (.I0(current_troughput_reg[23]),
        .O(\throughput_o[20]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[21]_i_1 
       (.I0(current_troughput[21]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[10]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[22]_i_1 
       (.I0(current_troughput[22]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[9]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[23]_i_1 
       (.I0(current_troughput[23]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[8]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[24]_i_1 
       (.I0(current_troughput[24]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[7]));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[24]_i_3 
       (.I0(current_troughput_reg[24]),
        .O(\throughput_o[24]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[24]_i_4 
       (.I0(current_troughput_reg[25]),
        .O(\throughput_o[24]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[24]_i_5 
       (.I0(current_troughput_reg[26]),
        .O(\throughput_o[24]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[24]_i_6 
       (.I0(current_troughput_reg[27]),
        .O(\throughput_o[24]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[25]_i_1 
       (.I0(current_troughput[25]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[6]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[26]_i_1 
       (.I0(current_troughput[26]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[5]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[27]_i_1 
       (.I0(current_troughput[27]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[4]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[28]_i_1 
       (.I0(current_troughput[28]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[3]));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[28]_i_3 
       (.I0(current_troughput_reg[28]),
        .O(\throughput_o[28]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[28]_i_4 
       (.I0(current_troughput_reg[29]),
        .O(\throughput_o[28]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[28]_i_5 
       (.I0(current_troughput_reg[30]),
        .O(\throughput_o[28]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \throughput_o[28]_i_6 
       (.I0(current_troughput_reg[31]),
        .I1(packet_arived_last),
        .I2(packet_arived_i),
        .O(\throughput_o[28]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[29]_i_1 
       (.I0(current_troughput[29]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[2]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[2]_i_1 
       (.I0(current_troughput[2]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[29]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[30]_i_1 
       (.I0(current_troughput[30]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[1]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[31]_i_1 
       (.I0(current_troughput[31]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[0]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[3]_i_1 
       (.I0(current_troughput[3]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[28]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[4]_i_1 
       (.I0(current_troughput[4]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[27]));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[4]_i_3 
       (.I0(current_troughput_reg[4]),
        .O(\throughput_o[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[4]_i_4 
       (.I0(current_troughput_reg[5]),
        .O(\throughput_o[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[4]_i_5 
       (.I0(current_troughput_reg[6]),
        .O(\throughput_o[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[4]_i_6 
       (.I0(current_troughput_reg[7]),
        .O(\throughput_o[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[5]_i_1 
       (.I0(current_troughput[5]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[26]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[6]_i_1 
       (.I0(current_troughput[6]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[25]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[7]_i_1 
       (.I0(current_troughput[7]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[24]));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[8]_i_1 
       (.I0(current_troughput[8]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[23]));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[8]_i_3 
       (.I0(current_troughput_reg[8]),
        .O(\throughput_o[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[8]_i_4 
       (.I0(current_troughput_reg[9]),
        .O(\throughput_o[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[8]_i_5 
       (.I0(current_troughput_reg[10]),
        .O(\throughput_o[8]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_o[8]_i_6 
       (.I0(current_troughput_reg[11]),
        .O(\throughput_o[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h2020202020202000)) 
    \throughput_o[9]_i_1 
       (.I0(current_troughput[9]),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .I2(\troughput_state_reg[1]_C_n_0 ),
        .I3(\throughput_o[0]_i_4_n_0 ),
        .I4(\throughput_o[0]_i_5_n_0 ),
        .I5(\throughput_o[0]_i_6_n_0 ),
        .O(p_3_in[22]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[0] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[31]),
        .Q(throughput_o[0]));
  CARRY4 \throughput_o_reg[0]_i_10 
       (.CI(\throughput_o_reg[0]_i_21_n_0 ),
        .CO({\throughput_o_reg[0]_i_10_n_0 ,\throughput_o_reg[0]_i_10_n_1 ,\throughput_o_reg[0]_i_10_n_2 ,\throughput_o_reg[0]_i_10_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in[16:13]),
        .S({\throughput_o[0]_i_23_n_0 ,\throughput_o[0]_i_24_n_0 ,\throughput_o[0]_i_25_n_0 ,\throughput_o[0]_i_26_n_0 }));
  CARRY4 \throughput_o_reg[0]_i_11 
       (.CI(\throughput_o_reg[0]_i_10_n_0 ),
        .CO({\throughput_o_reg[0]_i_11_n_0 ,\throughput_o_reg[0]_i_11_n_1 ,\throughput_o_reg[0]_i_11_n_2 ,\throughput_o_reg[0]_i_11_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in[20:17]),
        .S({\throughput_o[0]_i_27_n_0 ,\throughput_o[0]_i_28_n_0 ,\throughput_o[0]_i_29_n_0 ,\throughput_o[0]_i_30_n_0 }));
  CARRY4 \throughput_o_reg[0]_i_14 
       (.CI(\throughput_o_reg[0]_i_15_n_0 ),
        .CO({\NLW_throughput_o_reg[0]_i_14_CO_UNCONNECTED [3:2],\throughput_o_reg[0]_i_14_n_2 ,\throughput_o_reg[0]_i_14_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_throughput_o_reg[0]_i_14_O_UNCONNECTED [3],p_0_in[31:29]}),
        .S({1'b0,\throughput_o[0]_i_32_n_0 ,\throughput_o[0]_i_33_n_0 ,\throughput_o[0]_i_34_n_0 }));
  CARRY4 \throughput_o_reg[0]_i_15 
       (.CI(\throughput_o_reg[0]_i_16_n_0 ),
        .CO({\throughput_o_reg[0]_i_15_n_0 ,\throughput_o_reg[0]_i_15_n_1 ,\throughput_o_reg[0]_i_15_n_2 ,\throughput_o_reg[0]_i_15_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in[28:25]),
        .S({\throughput_o[0]_i_35_n_0 ,\throughput_o[0]_i_36_n_0 ,\throughput_o[0]_i_37_n_0 ,\throughput_o[0]_i_38_n_0 }));
  CARRY4 \throughput_o_reg[0]_i_16 
       (.CI(\throughput_o_reg[0]_i_11_n_0 ),
        .CO({\throughput_o_reg[0]_i_16_n_0 ,\throughput_o_reg[0]_i_16_n_1 ,\throughput_o_reg[0]_i_16_n_2 ,\throughput_o_reg[0]_i_16_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in[24:21]),
        .S({\throughput_o[0]_i_39_n_0 ,\throughput_o[0]_i_40_n_0 ,\throughput_o[0]_i_41_n_0 ,\throughput_o[0]_i_42_n_0 }));
  CARRY4 \throughput_o_reg[0]_i_21 
       (.CI(\throughput_o_reg[0]_i_22_n_0 ),
        .CO({\throughput_o_reg[0]_i_21_n_0 ,\throughput_o_reg[0]_i_21_n_1 ,\throughput_o_reg[0]_i_21_n_2 ,\throughput_o_reg[0]_i_21_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in[12:9]),
        .S({\throughput_o[0]_i_43_n_0 ,\throughput_o[0]_i_44_n_0 ,\throughput_o[0]_i_45_n_0 ,\throughput_o[0]_i_46_n_0 }));
  CARRY4 \throughput_o_reg[0]_i_22 
       (.CI(\throughput_o_reg[0]_i_31_n_0 ),
        .CO({\throughput_o_reg[0]_i_22_n_0 ,\throughput_o_reg[0]_i_22_n_1 ,\throughput_o_reg[0]_i_22_n_2 ,\throughput_o_reg[0]_i_22_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in[8:5]),
        .S({\throughput_o[0]_i_47_n_0 ,\throughput_o[0]_i_48_n_0 ,\throughput_o[0]_i_49_n_0 ,\throughput_o[0]_i_50_n_0 }));
  CARRY4 \throughput_o_reg[0]_i_31 
       (.CI(1'b0),
        .CO({\throughput_o_reg[0]_i_31_n_0 ,\throughput_o_reg[0]_i_31_n_1 ,\throughput_o_reg[0]_i_31_n_2 ,\throughput_o_reg[0]_i_31_n_3 }),
        .CYINIT(throughput_timer_reg[31]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({p_0_in[4:2],\NLW_throughput_o_reg[0]_i_31_O_UNCONNECTED [0]}),
        .S({\throughput_o[0]_i_51_n_0 ,\throughput_o[0]_i_52_n_0 ,\throughput_o[0]_i_53_n_0 ,\throughput_o[0]_i_54_n_0 }));
  CARRY4 \throughput_o_reg[0]_i_7 
       (.CI(\throughput_o_reg[4]_i_2_n_0 ),
        .CO({\NLW_throughput_o_reg[0]_i_7_CO_UNCONNECTED [3],\throughput_o_reg[0]_i_7_n_1 ,\throughput_o_reg[0]_i_7_n_2 ,\throughput_o_reg[0]_i_7_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({current_troughput[0],current_troughput[1],current_troughput[2],current_troughput[3]}),
        .S({\throughput_o[0]_i_17_n_0 ,\throughput_o[0]_i_18_n_0 ,\throughput_o[0]_i_19_n_0 ,\throughput_o[0]_i_20_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[10] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[21]),
        .Q(throughput_o[10]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[11] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[20]),
        .Q(throughput_o[11]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[12] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[19]),
        .Q(throughput_o[12]));
  CARRY4 \throughput_o_reg[12]_i_2 
       (.CI(\throughput_o_reg[16]_i_2_n_0 ),
        .CO({\throughput_o_reg[12]_i_2_n_0 ,\throughput_o_reg[12]_i_2_n_1 ,\throughput_o_reg[12]_i_2_n_2 ,\throughput_o_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({current_troughput[12],current_troughput[13],current_troughput[14],current_troughput[15]}),
        .S({\throughput_o[12]_i_3_n_0 ,\throughput_o[12]_i_4_n_0 ,\throughput_o[12]_i_5_n_0 ,\throughput_o[12]_i_6_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[13] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[18]),
        .Q(throughput_o[13]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[14] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[17]),
        .Q(throughput_o[14]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[15] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[16]),
        .Q(throughput_o[15]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[16] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[15]),
        .Q(throughput_o[16]));
  CARRY4 \throughput_o_reg[16]_i_2 
       (.CI(\throughput_o_reg[20]_i_2_n_0 ),
        .CO({\throughput_o_reg[16]_i_2_n_0 ,\throughput_o_reg[16]_i_2_n_1 ,\throughput_o_reg[16]_i_2_n_2 ,\throughput_o_reg[16]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({current_troughput[16],current_troughput[17],current_troughput[18],current_troughput[19]}),
        .S({\throughput_o[16]_i_3_n_0 ,\throughput_o[16]_i_4_n_0 ,\throughput_o[16]_i_5_n_0 ,\throughput_o[16]_i_6_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[17] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[14]),
        .Q(throughput_o[17]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[18] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[13]),
        .Q(throughput_o[18]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[19] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[12]),
        .Q(throughput_o[19]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[1] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[30]),
        .Q(throughput_o[1]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[20] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[11]),
        .Q(throughput_o[20]));
  CARRY4 \throughput_o_reg[20]_i_2 
       (.CI(\throughput_o_reg[24]_i_2_n_0 ),
        .CO({\throughput_o_reg[20]_i_2_n_0 ,\throughput_o_reg[20]_i_2_n_1 ,\throughput_o_reg[20]_i_2_n_2 ,\throughput_o_reg[20]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({current_troughput[20],current_troughput[21],current_troughput[22],current_troughput[23]}),
        .S({\throughput_o[20]_i_3_n_0 ,\throughput_o[20]_i_4_n_0 ,\throughput_o[20]_i_5_n_0 ,\throughput_o[20]_i_6_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[21] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[10]),
        .Q(throughput_o[21]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[22] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[9]),
        .Q(throughput_o[22]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[23] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[8]),
        .Q(throughput_o[23]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[24] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[7]),
        .Q(throughput_o[24]));
  CARRY4 \throughput_o_reg[24]_i_2 
       (.CI(\throughput_o_reg[28]_i_2_n_0 ),
        .CO({\throughput_o_reg[24]_i_2_n_0 ,\throughput_o_reg[24]_i_2_n_1 ,\throughput_o_reg[24]_i_2_n_2 ,\throughput_o_reg[24]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({current_troughput[24],current_troughput[25],current_troughput[26],current_troughput[27]}),
        .S({\throughput_o[24]_i_3_n_0 ,\throughput_o[24]_i_4_n_0 ,\throughput_o[24]_i_5_n_0 ,\throughput_o[24]_i_6_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[25] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[6]),
        .Q(throughput_o[25]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[26] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[5]),
        .Q(throughput_o[26]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[27] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[4]),
        .Q(throughput_o[27]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[28] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[3]),
        .Q(throughput_o[28]));
  CARRY4 \throughput_o_reg[28]_i_2 
       (.CI(1'b0),
        .CO({\throughput_o_reg[28]_i_2_n_0 ,\throughput_o_reg[28]_i_2_n_1 ,\throughput_o_reg[28]_i_2_n_2 ,\throughput_o_reg[28]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,current_troughput_reg[31]}),
        .O({current_troughput[28],current_troughput[29],current_troughput[30],current_troughput[31]}),
        .S({\throughput_o[28]_i_3_n_0 ,\throughput_o[28]_i_4_n_0 ,\throughput_o[28]_i_5_n_0 ,\throughput_o[28]_i_6_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[29] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[2]),
        .Q(throughput_o[29]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[2] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[29]),
        .Q(throughput_o[2]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[30] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[1]),
        .Q(throughput_o[30]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[31] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[0]),
        .Q(throughput_o[31]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[3] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[28]),
        .Q(throughput_o[3]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[4] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[27]),
        .Q(throughput_o[4]));
  CARRY4 \throughput_o_reg[4]_i_2 
       (.CI(\throughput_o_reg[8]_i_2_n_0 ),
        .CO({\throughput_o_reg[4]_i_2_n_0 ,\throughput_o_reg[4]_i_2_n_1 ,\throughput_o_reg[4]_i_2_n_2 ,\throughput_o_reg[4]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({current_troughput[4],current_troughput[5],current_troughput[6],current_troughput[7]}),
        .S({\throughput_o[4]_i_3_n_0 ,\throughput_o[4]_i_4_n_0 ,\throughput_o[4]_i_5_n_0 ,\throughput_o[4]_i_6_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[5] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[26]),
        .Q(throughput_o[5]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[6] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[25]),
        .Q(throughput_o[6]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[7] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[24]),
        .Q(throughput_o[7]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[8] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[23]),
        .Q(throughput_o[8]));
  CARRY4 \throughput_o_reg[8]_i_2 
       (.CI(\throughput_o_reg[12]_i_2_n_0 ),
        .CO({\throughput_o_reg[8]_i_2_n_0 ,\throughput_o_reg[8]_i_2_n_1 ,\throughput_o_reg[8]_i_2_n_2 ,\throughput_o_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({current_troughput[8],current_troughput[9],current_troughput[10],current_troughput[11]}),
        .S({\throughput_o[8]_i_3_n_0 ,\throughput_o[8]_i_4_n_0 ,\throughput_o[8]_i_5_n_0 ,\throughput_o[8]_i_6_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_o_reg[9] 
       (.C(clk),
        .CE(\throughput_o[0]_i_1_n_0 ),
        .CLR(rst),
        .D(p_3_in[22]),
        .Q(throughput_o[9]));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[11]_i_2 
       (.I0(throughput_timer_reg[8]),
        .O(\throughput_timer[11]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[11]_i_3 
       (.I0(throughput_timer_reg[9]),
        .O(\throughput_timer[11]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[11]_i_4 
       (.I0(throughput_timer_reg[10]),
        .O(\throughput_timer[11]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[11]_i_5 
       (.I0(throughput_timer_reg[11]),
        .O(\throughput_timer[11]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[15]_i_2 
       (.I0(throughput_timer_reg[12]),
        .O(\throughput_timer[15]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[15]_i_3 
       (.I0(throughput_timer_reg[13]),
        .O(\throughput_timer[15]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[15]_i_4 
       (.I0(throughput_timer_reg[14]),
        .O(\throughput_timer[15]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[15]_i_5 
       (.I0(throughput_timer_reg[15]),
        .O(\throughput_timer[15]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[19]_i_2 
       (.I0(throughput_timer_reg[16]),
        .O(\throughput_timer[19]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[19]_i_3 
       (.I0(throughput_timer_reg[17]),
        .O(\throughput_timer[19]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[19]_i_4 
       (.I0(throughput_timer_reg[18]),
        .O(\throughput_timer[19]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[19]_i_5 
       (.I0(throughput_timer_reg[19]),
        .O(\throughput_timer[19]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[23]_i_2 
       (.I0(throughput_timer_reg[20]),
        .O(\throughput_timer[23]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[23]_i_3 
       (.I0(throughput_timer_reg[21]),
        .O(\throughput_timer[23]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[23]_i_4 
       (.I0(throughput_timer_reg[22]),
        .O(\throughput_timer[23]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[23]_i_5 
       (.I0(throughput_timer_reg[23]),
        .O(\throughput_timer[23]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[27]_i_2 
       (.I0(throughput_timer_reg[24]),
        .O(\throughput_timer[27]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[27]_i_3 
       (.I0(throughput_timer_reg[25]),
        .O(\throughput_timer[27]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[27]_i_4 
       (.I0(throughput_timer_reg[26]),
        .O(\throughput_timer[27]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[27]_i_5 
       (.I0(throughput_timer_reg[27]),
        .O(\throughput_timer[27]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \throughput_timer[31]_i_1 
       (.I0(\troughput_state_reg[1]_C_n_0 ),
        .I1(\troughput_state_reg[0]_C_n_0 ),
        .O(throughput_timer));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[31]_i_3 
       (.I0(throughput_timer_reg[28]),
        .O(\throughput_timer[31]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[31]_i_4 
       (.I0(throughput_timer_reg[29]),
        .O(\throughput_timer[31]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[31]_i_5 
       (.I0(throughput_timer_reg[30]),
        .O(\throughput_timer[31]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \throughput_timer[31]_i_6 
       (.I0(throughput_timer_reg[31]),
        .O(\throughput_timer[31]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[3]_i_2 
       (.I0(throughput_timer_reg[0]),
        .O(\throughput_timer[3]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[3]_i_3 
       (.I0(throughput_timer_reg[1]),
        .O(\throughput_timer[3]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[3]_i_4 
       (.I0(throughput_timer_reg[2]),
        .O(\throughput_timer[3]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[3]_i_5 
       (.I0(throughput_timer_reg[3]),
        .O(\throughput_timer[3]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[7]_i_2 
       (.I0(throughput_timer_reg[4]),
        .O(\throughput_timer[7]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[7]_i_3 
       (.I0(throughput_timer_reg[5]),
        .O(\throughput_timer[7]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[7]_i_4 
       (.I0(throughput_timer_reg[6]),
        .O(\throughput_timer[7]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \throughput_timer[7]_i_5 
       (.I0(throughput_timer_reg[7]),
        .O(\throughput_timer[7]_i_5_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[0] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[3]_i_1_n_4 ),
        .Q(throughput_timer_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[10] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[11]_i_1_n_6 ),
        .Q(throughput_timer_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[11] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[11]_i_1_n_7 ),
        .Q(throughput_timer_reg[11]));
  CARRY4 \throughput_timer_reg[11]_i_1 
       (.CI(\throughput_timer_reg[15]_i_1_n_0 ),
        .CO({\throughput_timer_reg[11]_i_1_n_0 ,\throughput_timer_reg[11]_i_1_n_1 ,\throughput_timer_reg[11]_i_1_n_2 ,\throughput_timer_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\throughput_timer_reg[11]_i_1_n_4 ,\throughput_timer_reg[11]_i_1_n_5 ,\throughput_timer_reg[11]_i_1_n_6 ,\throughput_timer_reg[11]_i_1_n_7 }),
        .S({\throughput_timer[11]_i_2_n_0 ,\throughput_timer[11]_i_3_n_0 ,\throughput_timer[11]_i_4_n_0 ,\throughput_timer[11]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[12] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[15]_i_1_n_4 ),
        .Q(throughput_timer_reg[12]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[13] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[15]_i_1_n_5 ),
        .Q(throughput_timer_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[14] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[15]_i_1_n_6 ),
        .Q(throughput_timer_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[15] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[15]_i_1_n_7 ),
        .Q(throughput_timer_reg[15]));
  CARRY4 \throughput_timer_reg[15]_i_1 
       (.CI(\throughput_timer_reg[19]_i_1_n_0 ),
        .CO({\throughput_timer_reg[15]_i_1_n_0 ,\throughput_timer_reg[15]_i_1_n_1 ,\throughput_timer_reg[15]_i_1_n_2 ,\throughput_timer_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\throughput_timer_reg[15]_i_1_n_4 ,\throughput_timer_reg[15]_i_1_n_5 ,\throughput_timer_reg[15]_i_1_n_6 ,\throughput_timer_reg[15]_i_1_n_7 }),
        .S({\throughput_timer[15]_i_2_n_0 ,\throughput_timer[15]_i_3_n_0 ,\throughput_timer[15]_i_4_n_0 ,\throughput_timer[15]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[16] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[19]_i_1_n_4 ),
        .Q(throughput_timer_reg[16]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[17] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[19]_i_1_n_5 ),
        .Q(throughput_timer_reg[17]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[18] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[19]_i_1_n_6 ),
        .Q(throughput_timer_reg[18]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[19] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[19]_i_1_n_7 ),
        .Q(throughput_timer_reg[19]));
  CARRY4 \throughput_timer_reg[19]_i_1 
       (.CI(\throughput_timer_reg[23]_i_1_n_0 ),
        .CO({\throughput_timer_reg[19]_i_1_n_0 ,\throughput_timer_reg[19]_i_1_n_1 ,\throughput_timer_reg[19]_i_1_n_2 ,\throughput_timer_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\throughput_timer_reg[19]_i_1_n_4 ,\throughput_timer_reg[19]_i_1_n_5 ,\throughput_timer_reg[19]_i_1_n_6 ,\throughput_timer_reg[19]_i_1_n_7 }),
        .S({\throughput_timer[19]_i_2_n_0 ,\throughput_timer[19]_i_3_n_0 ,\throughput_timer[19]_i_4_n_0 ,\throughput_timer[19]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[1] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[3]_i_1_n_5 ),
        .Q(throughput_timer_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[20] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[23]_i_1_n_4 ),
        .Q(throughput_timer_reg[20]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[21] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[23]_i_1_n_5 ),
        .Q(throughput_timer_reg[21]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[22] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[23]_i_1_n_6 ),
        .Q(throughput_timer_reg[22]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[23] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[23]_i_1_n_7 ),
        .Q(throughput_timer_reg[23]));
  CARRY4 \throughput_timer_reg[23]_i_1 
       (.CI(\throughput_timer_reg[27]_i_1_n_0 ),
        .CO({\throughput_timer_reg[23]_i_1_n_0 ,\throughput_timer_reg[23]_i_1_n_1 ,\throughput_timer_reg[23]_i_1_n_2 ,\throughput_timer_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\throughput_timer_reg[23]_i_1_n_4 ,\throughput_timer_reg[23]_i_1_n_5 ,\throughput_timer_reg[23]_i_1_n_6 ,\throughput_timer_reg[23]_i_1_n_7 }),
        .S({\throughput_timer[23]_i_2_n_0 ,\throughput_timer[23]_i_3_n_0 ,\throughput_timer[23]_i_4_n_0 ,\throughput_timer[23]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[24] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[27]_i_1_n_4 ),
        .Q(throughput_timer_reg[24]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[25] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[27]_i_1_n_5 ),
        .Q(throughput_timer_reg[25]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[26] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[27]_i_1_n_6 ),
        .Q(throughput_timer_reg[26]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[27] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[27]_i_1_n_7 ),
        .Q(throughput_timer_reg[27]));
  CARRY4 \throughput_timer_reg[27]_i_1 
       (.CI(\throughput_timer_reg[31]_i_2_n_0 ),
        .CO({\throughput_timer_reg[27]_i_1_n_0 ,\throughput_timer_reg[27]_i_1_n_1 ,\throughput_timer_reg[27]_i_1_n_2 ,\throughput_timer_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\throughput_timer_reg[27]_i_1_n_4 ,\throughput_timer_reg[27]_i_1_n_5 ,\throughput_timer_reg[27]_i_1_n_6 ,\throughput_timer_reg[27]_i_1_n_7 }),
        .S({\throughput_timer[27]_i_2_n_0 ,\throughput_timer[27]_i_3_n_0 ,\throughput_timer[27]_i_4_n_0 ,\throughput_timer[27]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[28] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[31]_i_2_n_4 ),
        .Q(throughput_timer_reg[28]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[29] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[31]_i_2_n_5 ),
        .Q(throughput_timer_reg[29]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[2] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[3]_i_1_n_6 ),
        .Q(throughput_timer_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[30] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[31]_i_2_n_6 ),
        .Q(throughput_timer_reg[30]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[31] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[31]_i_2_n_7 ),
        .Q(throughput_timer_reg[31]));
  CARRY4 \throughput_timer_reg[31]_i_2 
       (.CI(1'b0),
        .CO({\throughput_timer_reg[31]_i_2_n_0 ,\throughput_timer_reg[31]_i_2_n_1 ,\throughput_timer_reg[31]_i_2_n_2 ,\throughput_timer_reg[31]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\throughput_timer_reg[31]_i_2_n_4 ,\throughput_timer_reg[31]_i_2_n_5 ,\throughput_timer_reg[31]_i_2_n_6 ,\throughput_timer_reg[31]_i_2_n_7 }),
        .S({\throughput_timer[31]_i_3_n_0 ,\throughput_timer[31]_i_4_n_0 ,\throughput_timer[31]_i_5_n_0 ,\throughput_timer[31]_i_6_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[3] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[3]_i_1_n_7 ),
        .Q(throughput_timer_reg[3]));
  CARRY4 \throughput_timer_reg[3]_i_1 
       (.CI(\throughput_timer_reg[7]_i_1_n_0 ),
        .CO({\NLW_throughput_timer_reg[3]_i_1_CO_UNCONNECTED [3],\throughput_timer_reg[3]_i_1_n_1 ,\throughput_timer_reg[3]_i_1_n_2 ,\throughput_timer_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\throughput_timer_reg[3]_i_1_n_4 ,\throughput_timer_reg[3]_i_1_n_5 ,\throughput_timer_reg[3]_i_1_n_6 ,\throughput_timer_reg[3]_i_1_n_7 }),
        .S({\throughput_timer[3]_i_2_n_0 ,\throughput_timer[3]_i_3_n_0 ,\throughput_timer[3]_i_4_n_0 ,\throughput_timer[3]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[4] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[7]_i_1_n_4 ),
        .Q(throughput_timer_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[5] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[7]_i_1_n_5 ),
        .Q(throughput_timer_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[6] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[7]_i_1_n_6 ),
        .Q(throughput_timer_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[7] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[7]_i_1_n_7 ),
        .Q(throughput_timer_reg[7]));
  CARRY4 \throughput_timer_reg[7]_i_1 
       (.CI(\throughput_timer_reg[11]_i_1_n_0 ),
        .CO({\throughput_timer_reg[7]_i_1_n_0 ,\throughput_timer_reg[7]_i_1_n_1 ,\throughput_timer_reg[7]_i_1_n_2 ,\throughput_timer_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\throughput_timer_reg[7]_i_1_n_4 ,\throughput_timer_reg[7]_i_1_n_5 ,\throughput_timer_reg[7]_i_1_n_6 ,\throughput_timer_reg[7]_i_1_n_7 }),
        .S({\throughput_timer[7]_i_2_n_0 ,\throughput_timer[7]_i_3_n_0 ,\throughput_timer[7]_i_4_n_0 ,\throughput_timer[7]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[8] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[11]_i_1_n_4 ),
        .Q(throughput_timer_reg[8]));
  FDCE #(
    .INIT(1'b0)) 
    \throughput_timer_reg[9] 
       (.C(clk),
        .CE(throughput_timer),
        .CLR(rst),
        .D(\throughput_timer_reg[11]_i_1_n_5 ),
        .Q(throughput_timer_reg[9]));
  LUT6 #(
    .INIT(64'h00FEFFFF00FE0000)) 
    \troughput_state[0]_C_i_1 
       (.I0(\throughput_o[0]_i_4_n_0 ),
        .I1(\throughput_o[0]_i_5_n_0 ),
        .I2(\throughput_o[0]_i_6_n_0 ),
        .I3(\troughput_state[0]_C_i_2_n_0 ),
        .I4(\troughput_state[0]_C_i_3_n_0 ),
        .I5(\troughput_state_reg[0]_C_n_0 ),
        .O(\troughput_state[0]_C_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0040FFFFFFFF)) 
    \troughput_state[0]_C_i_2 
       (.I0(\troughput_state_reg[0]_C_n_0 ),
        .I1(axiwvalid_i),
        .I2(axiwready_i),
        .I3(\troughput_state_reg[1]_C_n_0 ),
        .I4(reset),
        .I5(axi_reset),
        .O(\troughput_state[0]_C_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h00EA)) 
    \troughput_state[0]_C_i_3 
       (.I0(\troughput_state_reg[1]_C_n_0 ),
        .I1(axiwready_i),
        .I2(axiwvalid_i),
        .I3(\troughput_state_reg[0]_C_n_0 ),
        .O(\troughput_state[0]_C_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFF0200000202FFFF)) 
    \troughput_state[1]_C_i_1 
       (.I0(\troughput_state[1]_C_i_2_n_0 ),
        .I1(\throughput_o[0]_i_6_n_0 ),
        .I2(\troughput_state[1]_C_i_3_n_0 ),
        .I3(\troughput_state_reg[0]_C_n_0 ),
        .I4(\troughput_state_reg[1]_C_n_0 ),
        .I5(\troughput_state[1]_C_i_4_n_0 ),
        .O(\troughput_state[1]_C_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \troughput_state[1]_C_i_2 
       (.I0(axi_reset),
        .I1(reset),
        .O(\troughput_state[1]_C_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \troughput_state[1]_C_i_3 
       (.I0(\troughput_state[1]_C_i_5_n_0 ),
        .I1(\throughput_o[0]_i_9_n_0 ),
        .I2(\throughput_o[0]_i_8_n_0 ),
        .I3(\troughput_state[1]_C_i_6_n_0 ),
        .I4(\throughput_o[0]_i_13_n_0 ),
        .I5(\throughput_o[0]_i_12_n_0 ),
        .O(\troughput_state[1]_C_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \troughput_state[1]_C_i_4 
       (.I0(\troughput_state_reg[0]_C_n_0 ),
        .I1(axiwvalid_i),
        .I2(axiwready_i),
        .O(\troughput_state[1]_C_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \troughput_state[1]_C_i_5 
       (.I0(p_0_in[14]),
        .I1(p_0_in[19]),
        .I2(p_0_in[15]),
        .O(\troughput_state[1]_C_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \troughput_state[1]_C_i_6 
       (.I0(p_0_in[29]),
        .I1(p_0_in[16]),
        .I2(p_0_in[30]),
        .I3(p_0_in[27]),
        .O(\troughput_state[1]_C_i_6_n_0 ));
  FDCE \troughput_state_reg[0]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(rst),
        .D(\troughput_state[0]_C_i_1_n_0 ),
        .Q(\troughput_state_reg[0]_C_n_0 ));
  FDCE \troughput_state_reg[1]_C 
       (.C(clk),
        .CE(1'b1),
        .CLR(rst),
        .D(\troughput_state[1]_C_i_1_n_0 ),
        .Q(\troughput_state_reg[1]_C_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
