`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/18/2019 07:01:56 PM
// Design Name: 
// Module Name: input_virtualiser
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

/*
 AXI Sla^e       Input Virtualizer                                                                                                                        Router
+-----------+   +-------------------------------------------------------------------------------------------------------------------------------------+  +----------+
|           |   |                                                                                                                                     |  |          |
|           |   |                                                                                                                                     |  |          |
|           |   |                  +------------------+                                                                                               |  |          |
|           |   |                  |                  |                                                                                               |  |          |
|           |   |                  |  MUX/            |                                                                                               |  |          |
|           |   |                  |  Demux           |                                                                                               |  |          |
|           |   |                  |                  |                +--------------------+                                                         |  |          |
|           |   |                  |                  |                |Flits/packet to Data|                                                         |  |          |
|           |   |                  |                  |                |                    | Flits +--------------------+ Flits                      |  |          |
|           |   |                  |                  | Read Data      |                    <-------+ Input FIFO Queue   <-----------+-------------------+          |
|           |   | Read Data        |                  <----------------+                    |       +--------------------+           |                |  |          |
|           <----------------------+                  |                |                    |                                        |                |  |          |
|           |   |                  |                  |                |                    |                                        |                |  |          |
|           |   |                  |                  |                +--------------------+                                        |                |  |          |
|           |   |                  |                  |                                                                              |                |  |          |
|           |   | AXI Read Address |                  |                                                                              |                |  |          |
|           +---------------------->                  |                +--------------------+                                        |                |  |          |
|           |   |                  |                  |                |Flits/packet to Data|                                        |                |  |          |
|           |   |                  |                  |                |                    | Flits +--------------------+ Flits     |                |  |          |
|           |   |                  |                  | Read Data      |                    <-------+ Input FIFO Queue   <-----------+                |  |          |
|           |   |                  |                  <----------------+                    |       +--------------------+           |                |  |          |
|           |   |                  |                  |                |                    |                                        |                |  |          |
|           |   |                  |                  |                |                    |                                        |                |  |          |
|           |   |                  |                  |                +--------------------+                                        |                |  |          |
|           |   |                  |                  |                                                                              |                |  |          |
|           |   |                  |                  |                                                                              |                |  |          |
|           |   |                  |                  |                                                                              |                |  |          |
|           |   |                  |                  <-------+                                                           <----------+                |  |          |
|           |   |                  |                  |      ..................................                                                       |  |          |
|           |   |                  |                  |                                                                                               |  |          |
|           |   |                  |                  |                                                                                               |  |          |
|           |   |                  +------------------+                                                                                               |  |          |
|           |   |                                                                                                                                     |  |          |
|           |   |                                                                                                                                     |  |          |
+-----------+   +-------------------------------------------------------------------------------------------------------------------------------------+  +----------+

*/

//contains for each vchannle the code vor recieving flits ans converting them into packets.
//also contains multiplexing logick, to make the accesss for the other components transparant
module input_virtualiser
    #(
        parameter flit_data_width = 32,
        parameter flit_type_width = 2,
     
        parameter buffer_lenght = 16,
        //bits in head flit, that will be used for routing
        parameter flit_address_width = 4,
        localparam flit_width = flit_data_width+flit_type_width,
        
        parameter vchannels = 1,
        
        //first bit of the axi adderess (counted from the left) that is part of the packets priority
        parameter vchannel_select_lefoffset = 0,
        //amount of bits ini the axi address, that are part of the packets priority
        parameter vchannel_select_bits = 1,
        /*
         * Parameters needed for flit to data translation
         */
         //maximum number of flits of one packet
         parameter max_packet_size = 4,
         parameter integer C_S00_AXI_DATA_WIDTH    = 32,
         parameter integer C_S00_AXI_ADDR_WIDTH    = 6
    )
    (
        input clk,
        input rst,
    
        input wire [flit_width-1:0] in_flit_i,
        input wire [vchannels-1:0] in_valid_i,
        output wire [vchannels-1:0] in_ready_o,
        
        input wire [$clog2(max_packet_size+1):0] packet_buffer_read_address_i,
        input wire done_reading_packet_buffer_i,
        
        output wire packet_ready_o,
        output wire [C_S00_AXI_DATA_WIDTH-1:0]data_to_core_o,
        output wire [$clog2(max_packet_size+1):0] current_packet_size_in_flits_o,
        
        //interreupt line for the core 
        output wire [vchannels-1:0]packet_arrived_o,
       // input wire start_read_transaction_i,
        
        input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr
    );
    
    //wire containing the currently enabled vchannel
    wire [$clog2(vchannels):0] vch_select; 
    //controling the inputs based on the selected vchannelvch_selected vchannel
    wire [C_S00_AXI_DATA_WIDTH-1:0] output_selector_data_to_core_o [vchannels-1:0];
    wire [$clog2(max_packet_size+1):0] output_selector_current_packet_size_in_flits_o [vchannels-1:0];
    wire [vchannels-1:0] output_selector_packet_ready_o;

    assign vch_select = axi_araddr[C_S00_AXI_ADDR_WIDTH - vchannel_select_lefoffset-1:C_S00_AXI_ADDR_WIDTH-vchannel_select_bits-vchannel_select_lefoffset];
    assign data_to_core_o = output_selector_data_to_core_o[vch_select];
    assign current_packet_size_in_flits_o = output_selector_current_packet_size_in_flits_o[vch_select];
    assign packet_ready_o = output_selector_packet_ready_o[vch_select]; // && currently_reading];

    parameter packet_size_bits = $clog2(max_packet_size+1);
            
    genvar vch_num;
    for(vch_num = 0; vch_num < vchannels; vch_num = vch_num + 1)
    begin
 
        //global, need multiplexing

        wire [packet_size_bits:0]packet_buffer_read_address;
        wire done_reading_packet_buffer;
        wire packet_ready_in;
        wire [C_S00_AXI_DATA_WIDTH-1:0]data_to_core_in;
        wire [packet_size_bits:0] current_packet_size_in_flits_in;

        wire in_valid;
        wire in_ready;
        
        //multiplexing the in and outputs
        //if the fifo is the highest priority fifo that is able to accept data, set valit to 1
        assign in_valid = in_valid_i[vch_num];
        //if the fifo is ready, the router has a Packet and no higher prioritised fifo is ready, set the ready bit to accept data next cycle
        assign in_ready_o[vch_num] = in_ready;
        
        //interrupt line for the core
        assign packet_arrived_o[vch_num] = packet_ready_in;
        
        //data switch data lines based on the read address
        assign done_reading_packet_buffer = done_reading_packet_buffer_i && (vch_select == vch_num);
        assign packet_buffer_read_address = packet_buffer_read_address_i & {packet_size_bits{(vch_select == vch_num)}};
        
        //assings the individual values as the multiplexer imput    
        assign output_selector_data_to_core_o[vch_num] = data_to_core_in;
        assign output_selector_packet_ready_o[vch_num] = packet_ready_in;
        assign output_selector_current_packet_size_in_flits_o[vch_num] = current_packet_size_in_flits_in;
        
        
        //local obnly, no multiplexing neede
        wire in_fifo_ready;
        wire [flit_width-1:0] in_fifo_next_flit;
        wire in_fifo_has_data;
    
        //module for converting input flits to stream
        wire in_fifo_get_next;
        flits_to_stream
        #(
            .flit_data_width(flit_data_width),
            .flit_type_width(flit_type_width),
            .flit_address_width(flit_address_width),
            
            /*
            * Parameters needed for flit to data translation
            */
            //maximum number of flits of one packet
            .max_packet_size(max_packet_size)
        )
        input_to_stream
        (
            .clk(clk),
            .rst(rst),
            
            .next_flit_in_i(in_fifo_next_flit),
            .next_flit_ready_o(in_fifo_get_next),
            .next_flit_valid_i(in_fifo_has_data),
            
            //ports for connection with the axi slave. it will read data from the input puffer, if packed ist ready.
            .packet_buffer_read_address_i(packet_buffer_read_address),
            .done_reading_packet_buffer_i(done_reading_packet_buffer),
            .packet_ready_o(packet_ready_in),
            .data_to_core_o(data_to_core_in),
            .current_packet_size_in_flits_o(current_packet_size_in_flits_in)
        );
        /************************************************************************/
            
            //fifo recieving data from the noc
            lisnoc_fifo
            #(
             .flit_data_width(flit_data_width),
             .flit_type_width(flit_type_width),
             .packet_length(0),
             .LENGTH(buffer_lenght)
    
            )
            recieving_fifo
            (/*AUTOARG*/
               // Outputs
               .clk(clk),
               .rst(rst),
            
               // FIFO input side
               .in_flit(in_flit_i),   // input
               .in_valid(in_valid),                   // write_enable
               .in_ready(in_ready),                   // accepting new data
            
               //FIFO output side
               .out_flit(in_fifo_next_flit),                     // data_out
               .out_valid(in_fifo_has_data),                   // data available
               .out_ready(in_fifo_get_next)                   // read request
           );                 
       
       
       /**********************************************************************************/
      
    end
    
endmodule
