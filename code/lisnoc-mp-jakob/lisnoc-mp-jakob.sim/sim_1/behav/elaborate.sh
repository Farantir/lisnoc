#!/bin/bash -f
xv_path="/opt/Xilinx/Vivado/2016.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto ad4a82dcc6fe476b9a220211c0d23365 -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip --snapshot td_NOC_AXI_testmodule_behav xil_defaultlib.td_NOC_AXI_testmodule xil_defaultlib.glbl -log elaborate.log
