# 
# Synthesis run script generated by Vivado
# 

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
create_project -in_memory -part xc7z020clg484-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_property webtalk.parent_dir /home/jhippe/repos/lisnoc/code/lisnoc-mp-jakob/lisnoc-mp-jakob.cache/wt [current_project]
set_property parent.project_path /home/jhippe/repos/lisnoc/code/lisnoc-mp-jakob/lisnoc-mp-jakob.xpr [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language Verilog [current_project]
set_property board_part em.avnet.com:zed:part0:1.3 [current_project]
set_property ip_repo_paths {
  /home/jhippe/Ablage/export_ip/axi_dummy_1.0
  /home/jhippe/repos/lisnoc/code/export_ip/dummy_test_1.0
  /home/jhippe/repos/lisnoc/code/asdrf/askldjf_1.0
  /home/jhippe/repos/lisnoc/code
  /home/jhippe/repos/lisnoc/code/ip_repo/myip_1.0
  /home/jhippe/repos/lisnoc/code/ip_repo/axi_master_for_testing_1.0
  /home/jhippe/repos/lisnoc/code/ip_repo/axi_slave_test_1.0
  /home/jhippe/repos/lisnoc/code/ip_repo/lisnoc_axi_slave_1.0
} [current_project]
set_property ip_output_repo /home/jhippe/repos/lisnoc/code/lisnoc-mp-jakob/lisnoc-mp-jakob.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
read_verilog {
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/lisnoc_undef.vh
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/lisnoc_def.vh
  /home/jhippe/repos/lisnoc/code/lisnoc-mp-jakob/lisnoc-mp-jakob.srcs/sources_1/new/axi_lisnoc_address_translation.svh
}
set_property file_type "Verilog Header" [get_files /home/jhippe/repos/lisnoc/code/lisnoc-mp-jakob/lisnoc-mp-jakob.srcs/sources_1/new/axi_lisnoc_address_translation.svh]
read_verilog -library xil_defaultlib -sv {
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/router/lisnoc_router_output_arbiter.sv
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/router/lisnoc_router_arbiter.sv
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/router/lisnoc_router_arbiter_prio.sv
}
read_verilog -library xil_defaultlib {
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/lisnoc_arb_prio_rr.v
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/lisnoc_arb_rr.v
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_fifo.v
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input_route.v
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_output.v
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_input.v
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router.v
  /home/jhippe/repos/lisnoc/code/lisnoc-mp-jakob/lisnoc-mp-jakob.srcs/sources_1/new/stream_to_flits.v
  /home/jhippe/repos/lisnoc/code/lisnoc-mp-jakob/lisnoc-mp-jakob.srcs/sources_1/imports/hdl/lisnoc_axi_slave_v1_0_S00_AXI.v
  /home/jhippe/repos/lisnoc/code/lisnoc-mp-jakob/lisnoc-mp-jakob.srcs/sources_1/new/flits_to_stream.v
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/router/lisnoc_router_2dgrid.v
  /home/jhippe/repos/lisnoc/code/lisnoc-mp-jakob/lisnoc-mp-jakob.srcs/sources_1/imports/new/lisnoc_mp_interface.v
  /home/jhippe/repos/lisnoc/code/lisnoc-laura-old/LISNoC/LISNoC.srcs/sources_1/imports/rtl/meshs/lisnoc_mesh2x2.v
  /home/jhippe/repos/lisnoc/code/lisnoc-mp-jakob/lisnoc-mp-jakob.srcs/sources_1/new/NOC_AXI_testmodule.v
}
foreach dcp [get_files -quiet -all *.dcp] {
  set_property used_in_implementation false $dcp
}

synth_design -top NOC_AXI_testmodule -part xc7z020clg484-1


write_checkpoint -force -noxdef NOC_AXI_testmodule.dcp

catch { report_utilization -file NOC_AXI_testmodule_utilization_synth.rpt -pb NOC_AXI_testmodule_utilization_synth.pb }
