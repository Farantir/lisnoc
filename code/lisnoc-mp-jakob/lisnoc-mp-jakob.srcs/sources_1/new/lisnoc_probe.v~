`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/05/2019 02:34:16 PM
// Design Name: 
// Module Name: lisnoc_probe
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//probe to measure the latency and the troughput of the lisnoc network.
//latency measurement will be triggert when axiwvalid && axiwready and stopped when lisnoc packet arivel interrupt line is set.
//reset clears all values and prepares the module for the next measurement.
/*
            +-----------------------------------------------------------------------------------------------------------------------------+
            |                                                                                                                             |
            |                    +-----------+                                   +---------------------------+                            |
            |        axiw^alid_i |    AND    |                                   |                           |                            |
+-------------------------------->           |         axi_transaction_completet | Counter for latency       |                            |
            |                    |           +---+------------------------------->                           |                            |
            |        axiwready_i |           |   |                               |                           |                            |
+-------------------------------->           |   |                               |                           |                            |
            |                    |           |   |                               |                           |                            |
            |                    +-----------+   |                               |                           |                            |
            |                                    |                               |                           | latency_o                  |
            |                                    |               packet_ari^ed_i |                           +--------------------------------->
+--------------------------------------------------------------+----------------->                           |                            |
            |                                    |             |                 |                           |                            |
            |                                    |             |             clk |                           |                            |
+-------------------------------------------------------------------+------------>                           |                            |
            |                                    |             |    |            |                           |                            |
            |                                    |             |    |        rst |                           |                            |
            |                                    |             |    |   +-------->                           |                            |
            |                                    |             |    |   |        |                           |                            |
            |           +-------------------+    |             |    |   |        |                           |                            |
            |     reset |                   |    |             |    |   |        +---------------------------+                            |
+----------------------->  reset            +---------------------------+                                                                 |
            |           |  or               |    |             |    |   |                                                                 |
            | axi_reset |  !axi_reset       |    |             |    |   |        +----------------------------------------+               |
+----------------------->                   |    |             |    |   |    rst |                                        |               |
            |           |                   |    |             |    |   +--------> State maschiene for troughput couting  |               |
            |           +-------------------+    |             |    |            |                                        |               |
            |                                    |             |    |        clk |                                        |               |
            |                                    |             |    +------------>                                        |               |
            |                                    |             |                 |                                        |               |
            |                                    |             | packet_ari^ed_i |                                        | throughput_o  |
            |                                    |             +----------------->                                        +-------------------->
            |                                    |                               |                                        |               |
            |                                    |     axi_transaction_completet |                                        |               |
            |                                    +------------------------------->                                        |               |
            |                                                                    |                                        |               |
            |                                                                    |                                        |               |
            |                                                                    |                                        |               |
            |                                                                    +----------------------------------------+               |
            |                                                                                                                             |
            +-----------------------------------------------------------------------------------------------------------------------------+
*/

//troughput will be measured continuisly. when axiwvalid && axiwready a new cycle will be startet and packet arivel interrupts will be counted.
//after a set amount of clock cycles the measured value ist latched and a new measurement is startet when axiwvalid && axiwready
module lisnoc_probe
    #(
        //clock cycles for witch the packet arivel interrupts will be counted
        throughput_cylcles = 100
    )
    (
        //signals from the sending axi master
        input wire axiwvalid_i,
        input wire axiwready_i,
        //signal from the recieving lisnoc node
        input wire packet_arived_i,
        output reg [0:31] latency_o,
        output reg [0:31] throughput_o,
        //control signals
        input clk,
        input reset,
        input axi_reset
    );
    
    wire rst = (!axi_reset) || reset;
    
    reg counting_latency;
    reg [0:31] current_latnecy;
    reg [0:31] current_troughput;
    reg [0:1] troughput_state;
    reg latency_measured;
    reg [0:31] throughput_timer;
    reg packet_arived_last;
    
    //for simplicity sake, precombined logick
    wire axi_transaction_completet;
    assign axi_transaction_completet = axiwvalid_i && axiwready_i;
    
    //resetting the probe on startup
    initial 
    begin
        latency_measured = 1'b0;
        counting_latency = 1'b0;
        troughput_state = 2'b00;
        packet_arived_last = 1'b0;
        current_latnecy = 0;
        latency_o = 0;
        throughput_o = 0;
        throughput_timer = 0;
        current_troughput = 0;
    end
    
    //********************************
    //section for latency measurement*
    //********************************
    
    //*********************************************************************************************************************************************
    //counting atarting/stopping the timer for the latency measurement
    always @(posedge(axi_transaction_completet), posedge(packet_arived_i), posedge(rst))
    begin
        if(rst)
        begin
            counting_latency = 1'b0;
            latency_measured = 1'b0;
            latency_o = 0;
        end
        else if(packet_arived_i && counting_latency)
        begin
            counting_latency = 1'b0;
            latency_measured = 1'b1;
            latency_o = current_latnecy;
        end
        //enable latency Measurement
        else if((counting_latency == 0) && (latency_measured == 0) && axi_transaction_completet)
        begin
            counting_latency = 1'b1;
        end
    end
    
    always @(posedge(clk), posedge(rst))
    begin
        if(rst) current_latnecy = 0;
        else if(counting_latency) current_latnecy = current_latnecy + 1;
    end
    
    //*********************************************************************************************************************************************
    
    
    //***********************************
    //section for throughput measurement*
    //***********************************
    
    //********************************************************************************************************************************************* 
    
    //state maschine for counting the amount of packet_arived_i posedges in the throughput_cylcles amount of clk cycles
    always @(posedge(clk), posedge(rst))
    begin
        if(rst)
        begin
            troughput_state = 2'b00;
            current_troughput = 0;
            throughput_o = 0;
            throughput_timer = 0;
            packet_arived_last = 1'b0;
        end
        else
        begin
            case(troughput_state)
                //waiting for the trigger to measure the troughput
                2'b00: if(axi_transaction_completet) troughput_state <= 2'b01;
                //measuring the throughput
                2'b01: 
                    begin
                        //incrementint the timer
                        throughput_timer = throughput_timer + 1;
                        //if a posedge is detected, incremente througput counter
                        if(packet_arived_i && !packet_arived_last) 
                        begin
                            packet_arived_last = 1'b1;
                            current_troughput = current_troughput + 1;
                        end
                        //if a negedge is detected, reset posedge detection
                        else if(!packet_arived_i && packet_arived_last) packet_arived_last = 1'b0;
                        
                        //if the timer has reached its limit, stop conting
                        if(throughput_timer >= throughput_cylcles)
                        begin
                            troughput_state = 2'b10;
                            throughput_o <= current_troughput;
                        end
                    end
                //done measuring, idle state
                2'b10: ;
            endcase
        end
        
    end   
 /*   //counting the packet arival events for througput. latches if counting trougput == false
    always @(posedge(packet_arived_i), posedge(rst),negedge(counting_troughput))
        begin
            if(rst)
            begin
                current_troughput = 0;
                throughput_o = 0;
                throughput_measured = 0;
            end
            else if(throughput_measured);
            else
                begin
                    if(counting_troughput && packet_arived_i) current_troughput = current_troughput + 1;
                    else if((!counting_troughput) && (!throughput_measured))
                    begin
                        //current_troughput <= 0;
                        throughput_o <= current_troughput;
                        throughput_measured = 1'b1;
                    end
                end
        end
    
    //timer for througput counting
    always @(clk, rst, counting_troughput)
    begin
        if(rst) throughput_timer = 0;
        else if(clk && counting_troughput)
        begin
            throughput_timer = throughput_timer + 1;
        end
        else if(counting_troughput == 0) throughput_timer = 0;
    end 
    
    
    //controler block, starting and stopping the the througput measurement
    always @(throughput_timer, axi_transaction_completet, rst)
    begin
        if(rst) counting_troughput = 0;
        else if(axi_transaction_completet && (!counting_troughput) && (!throughput_measured)) counting_troughput <= 1'b1;
        else if((throughput_timer >= throughput_cylcles) && counting_troughput) counting_troughput <= 0;
    end
    */
    
endmodule
