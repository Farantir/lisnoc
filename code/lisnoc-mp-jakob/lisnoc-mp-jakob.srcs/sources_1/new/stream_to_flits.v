`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/19/2019 04:18:28 PM
// Design Name: 
// Module Name: stream_to_flits
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
/*
 * Module for converting an data stream into flits to send over the network 
 * created by Jakob Hippe
 */
 
/*

 AXI Slave           Stream to Flits
+-----------------+ +-------------------------------------------------------------------+
|                 | |                                                                   |
|                 | |                              +---------------+                    |
|                 | | packet_target_addr           |               |                    |
|                 +-------------------------------->               |                    |  Output FIFO
|                 | |                              |               |                    | +-----------+
|                 | | done_reading_packet_buffer_o |               | out_fifo_ready     | |           |
|                 <--------------------------------+               <----------------------+           |
|                 | |                              | state machine |                    | |           |
|                 | | packet_ready_i               |               | out_fifo_write     | |           |
|                 +-------------------------------->               +---------------------->           |
|                 | |                              |               |                    | |           |
|                 | | current_packet_size_in_flits |               | out_fifo_next_flit | |           |
|                 +-------------------------------->               +---------------------->           |
|                 | |                              |               |                    | |           |
|                 | |                              |               |                    | +-----------+
| +---------------+ |                              |               |                    |
| |               | |                              |               |                    |
| | Output Buffer | | data_to_noc_out_i            |               |                    |
| |               +-------------------------------->               |                    |
| |               | |                              |               |                    |
| |               | | packet_buffer_read_address_o |               |                    |
| |               <--------------------------------+               |                    |
| |               | |                              |               |                    |
+-+---------------+ |                              +-----+----^----+                    |
                    |                                    |    |                         |
                    |                                    |    |                         |
                    |                        lookup_bits |    | Flit Address            |
                    |                                    |    |                         |
                    |                                    |    |                         |
                    |                            +-------v----+---------+               |
                    |                            |                      |               |
                    |                            | address_lookup_table |               |
                    |                            |                      |               |
                    |                            +----------------------+               |
                    |                                                                   |
                    +-------------------------------------------------------------------+


*/
module stream_to_flits
    #(
        parameter flit_data_width = 32,
        parameter flit_address_width = 4,
        parameter flit_type_width = 2,
        localparam flit_width = flit_data_width+flit_type_width,
        
        /*
         * Parameters needed for data to flit translation
         */
         //maximum number of flits of one packet
         parameter max_packet_size = 4,
         //maximum size of the packet, derived from the max flit amount nad the flit size
         localparam max_packet_size_bits = (max_packet_size*flit_data_width),
         //amount of bits needed to store the max packet size
         parameter packet_size_bits = $clog2(max_packet_size_bits+ 1),

        parameter packet_address_lenght = 8
        //localparam node_address_count = (2**packet_address_lenght)/nodes_in_network
        
    )
    (
    input wire clk,
    input wire rst,
    
    //in/output ports to read from the packet buffer
    //row of the packet buffer to read from next
    output wire [$clog2(max_packet_size+1):0]packet_buffer_read_address_o,
    //finished current Packet. Waiting for packet_ready_i to be deassertet, then waiting for next packet
    output reg done_reading_packet_buffer_o,
    //if assertet, module will start reading the packet buffer
    input wire packet_ready_i,
    //input data from the packet buffer
    input wire [flit_data_width-1:0]data_to_noc_out_i,
    //if packet_buffer_read_address_o == current_packet_size_in_flits packet is finished
    input wire [$clog2(max_packet_size+1):0] current_packet_size_in_flits,
    //axi address for the packet. will be translatet to noc address
    input wire [packet_address_lenght-1 : 0] 	packet_target_addr,
    
    //in/outputs to the output queue
    input out_fifo_ready,
    output out_fifo_write,
    output reg [flit_width-1:0] out_fifo_next_flit
    );
    
   //this file contains a matrix, mapping the msb of the input address to the coresponding core on the noc 
   `include "axi_lisnoc_address_translation.svh"
   
   reg flit_data_ready;
   
   reg [$clog2(max_packet_size-1):0] current_position;
   reg [$clog2(max_packet_size-1):0] next_position;
   
   reg first_flit;
   reg first_flit_next;
   reg flit_data_ready_next;
  
    
    initial
    begin 
        current_position = 0;
        first_flit = 1'b1;
        flit_data_ready = 1'd0;
        done_reading_packet_buffer_o = 1'd0;
    end

   
    //clocked logick to update the values
    //if a new packet is ready: 
    //- translate address and send first flit
    //- read the input packet line by line from the input buffer
    //- transform each line into a flit and send it to the output fifo
    //- tell the input buffer, that we hae read the packet and prepare for a new one
    always @(posedge clk)
    begin
        //restet everything on reset
        if(rst)
        begin       
            flit_data_ready = 1'b0;
            done_reading_packet_buffer_o = 1'd0;
            first_flit = 1'b1;
            current_position = 0;
        end
        else
        begin
            //tell the the axi component, that we have read all data in the buffer, the buffer can now be filled again
            if(flit_data_ready && ~flit_data_ready_next)
            begin
                done_reading_packet_buffer_o <= 1'd1;
                //clear eveything
                first_flit <= 1'b1;
                current_position <= 0;
                flit_data_ready  <= flit_data_ready_next;
            end
            //next packet is ready for axi transaktion, set flit_data_ready manually
            else if(packet_ready_i && ~done_reading_packet_buffer_o)
            begin
                flit_data_ready = 1'b1;
            end
            else
            //let the net compute flit_data_ready
            begin
                flit_data_ready  <= flit_data_ready_next;
            end
            
            //the axi component got our message. we can deassert the signal now.
            if(done_reading_packet_buffer_o && ~packet_ready_i)
            begin
                done_reading_packet_buffer_o <= 1'd0;
            end
            
            //latch to the next buffer line for output
            else if(packet_ready_i && out_fifo_ready && out_fifo_write)
            begin
                //we need to ensure first flit is not deassertet bevore the first flit is actuallay sent
                if(flit_data_ready && ~flit_data_ready_next)
                begin
                    first_flit <= 1'b1;
                end
                else
                begin
                    first_flit <= first_flit_next;
                end
           
                current_position = next_position;
            end
        end
    end
    
    //defining the latch singnal for the fifo
    assign out_fifo_write = flit_data_ready & packet_ready_i;
    //address line to be read
    assign packet_buffer_read_address_o = current_position;
    
    //amount of bits in the head flit, that are not used for addressing
    localparam remaining_flit_header_data = {flit_data_width-(flit_address_width){1'b0}};
    wire [lookup_bit_num-1:0]lookup_bits;
    
    //extracting the relevant bits from the axi address
    assign lookup_bits = packet_target_addr[packet_address_lenght-1-lookup_bit_left_offset:packet_address_lenght-lookup_bit_num-lookup_bit_left_offset];
    
    /*
     * This always block prepares the new values vor the control and data lines as combinational logick
     * the main purpouse is to compose the next output flit
     */
    always @(*)
    begin
        //start of new paket, send the head flit and stay on the same buffer position, since no data will be sent
        if(first_flit)
            begin 
            		//head flit containing target router address, derived from the axi address and the lookup table
                out_fifo_next_flit = {2'b01,address_lookup_table[lookup_bits],remaining_flit_header_data};
                //next flit will be a body or tail flit, first
                first_flit_next = 1'b0;
                //no change on the data ready line. it shuold be 1 tho
                flit_data_ready_next = flit_data_ready;
                //no chage on the buffer read position, since no data was sent
                next_position = current_position;
            end
        //middle of the Packet. sending the buffer row by row
        else if(current_position+1 < current_packet_size_in_flits)
            begin
            		//sending a body flit containing the current buffer row
                out_fifo_next_flit = {2'b00,data_to_noc_out_i};
                //the next flit will be a body or tail flit
                first_flit_next = 1'b0;
                //since we sent data, the next buffer row shall be sent next
                next_position = current_position +1;
                //no change on the data ready line. it shuold be 1 tho
                flit_data_ready_next = flit_data_ready;
            end
            //the end of the packet, sending last row and prepare for next packet
        else if(current_position+2 > current_packet_size_in_flits)
            begin
                out_fifo_next_flit = {2'b10,data_to_noc_out_i};
                //the next flit will be the first flit.
                first_flit_next = 1'b1;
                //resetting the position to 0, preparing for the next packet transfer
                next_position = 0;
                //this was the last flit, the buffer is empty, so no data left
                flit_data_ready_next = 1'b0;
            end
            //unimportant state. setting everything to default
        else
            begin
                first_flit_next = first_flit;
                out_fifo_next_flit = 0;
                next_position = current_position + 1;
                flit_data_ready_next = flit_data_ready;
            end
    end
endmodule
