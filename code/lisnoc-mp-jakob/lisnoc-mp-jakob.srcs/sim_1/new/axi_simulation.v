`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/24/2019 04:32:08 PM
// Design Name: 
// Module Name: axi_simulation
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axi_simulation();

    // Users to add parameters here

    // User parameters ends
    // Do not modify the parameters beyond this line


    // Parameters of Axi Master Bus Interface M00_AXI
    parameter  C_M00_AXI_TARGET_SLAVE_BASE_ADDR    = 32'h40000000;
    parameter integer C_M00_AXI_BURST_LEN    = 16;
    parameter integer C_M00_AXI_ID_WIDTH    = 1;
    parameter integer C_M00_AXI_ADDR_WIDTH    = 32;
    parameter integer C_M00_AXI_DATA_WIDTH    = 32;
    parameter integer C_M00_AXI_AWUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_ARUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_WUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_RUSER_WIDTH    = 0;
    parameter integer C_M00_AXI_BUSER_WIDTH    = 0;


    // Users to add ports here

    // User ports ends
    // Do not modify the ports beyond this line


    // Ports of Axi Master Bus Interface M00_AXI
    reg  m00_axi_init_axi_txn;
    wire  m00_axi_txn_done;
    wire  m00_axi_error;
    reg  m00_axi_aclk;
    reg  m00_axi_aresetn;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_awid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_awaddr;
    wire [7 : 0] m00_axi_awlen;
    wire [2 : 0] m00_axi_awsize;
    wire [1 : 0] m00_axi_awburst;
    wire  m00_axi_awlock;
    wire [3 : 0] m00_axi_awcache;
    wire [2 : 0] m00_axi_awprot;
    wire [3 : 0] m00_axi_awqos;
    wire [C_M00_AXI_AWUSER_WIDTH-1 : 0] m00_axi_awuser;
    wire  m00_axi_awvalid;
    wire  m00_axi_awready;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_wdata;
    wire [C_M00_AXI_DATA_WIDTH/8-1 : 0] m00_axi_wstrb;
    wire  m00_axi_wlast;
    wire [C_M00_AXI_WUSER_WIDTH-1 : 0] m00_axi_wuser;
    wire  m00_axi_wvalid;
    wire  m00_axi_wready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_bid;
    wire [1 : 0] m00_axi_bresp;
    wire [C_M00_AXI_BUSER_WIDTH-1 : 0] m00_axi_buser;
    wire  m00_axi_bvalid;
    wire  m00_axi_bready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_arid;
    wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_araddr;
    wire [7 : 0] m00_axi_arlen;
    wire [2 : 0] m00_axi_arsize;
    wire [1 : 0] m00_axi_arburst;
    wire  m00_axi_arlock;
    wire [3 : 0] m00_axi_arcache;
    wire [2 : 0] m00_axi_arprot;
    wire [3 : 0] m00_axi_arqos;
    wire [C_M00_AXI_ARUSER_WIDTH-1 : 0] m00_axi_aruser;
    wire  m00_axi_arvalid;
    wire  m00_axi_arready;
    wire [C_M00_AXI_ID_WIDTH-1 : 0] m00_axi_rid;
    wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_rdata;
    wire [1 : 0] m00_axi_rresp;
    wire  m00_axi_rlast;
    wire [C_M00_AXI_RUSER_WIDTH-1 : 0] m00_axi_ruser;
    wire  m00_axi_rvalid;
    wire  m00_axi_rready;

always #2  m00_axi_aclk = ~m00_axi_aclk;
initial
begin
m00_axi_aclk = 0;
m00_axi_aresetn = 1;
#1 m00_axi_aresetn = 0;

#2 m00_axi_init_axi_txn = 1;
#20 m00_axi_aresetn = 1;
end

// Instantiation of Axi Bus Interface M00_AXI
axi_master_for_testing_v1_0_M00_AXI # ( 
    .C_M_TARGET_SLAVE_BASE_ADDR(C_M00_AXI_TARGET_SLAVE_BASE_ADDR),
    .C_M_AXI_BURST_LEN(C_M00_AXI_BURST_LEN),
    .C_M_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
    .C_M_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
    .C_M_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
    .C_M_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
    .C_M_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
    .C_M_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
    .C_M_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
    .C_M_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH)
) axi_master_for_testing_v1_0_M00_AXI_inst (
    .INIT_AXI_TXN(m00_axi_init_axi_txn),
    .TXN_DONE(m00_axi_txn_done),
    .ERROR(m00_axi_error),
    .M_AXI_ACLK(m00_axi_aclk),
    .M_AXI_ARESETN(m00_axi_aresetn),
    .M_AXI_AWID(m00_axi_awid),
    .M_AXI_AWADDR(m00_axi_awaddr),
    .M_AXI_AWLEN(m00_axi_awlen),
    .M_AXI_AWSIZE(m00_axi_awsize),
    .M_AXI_AWBURST(m00_axi_awburst),
    .M_AXI_AWLOCK(m00_axi_awlock),
    .M_AXI_AWCACHE(m00_axi_awcache),
    .M_AXI_AWPROT(m00_axi_awprot),
    .M_AXI_AWQOS(m00_axi_awqos),
    .M_AXI_AWUSER(m00_axi_awuser),
    .M_AXI_AWVALID(m00_axi_awvalid),
    .M_AXI_AWREADY(m00_axi_awready),
    .M_AXI_WDATA(m00_axi_wdata),
    .M_AXI_WSTRB(m00_axi_wstrb),
    .M_AXI_WLAST(m00_axi_wlast),
    .M_AXI_WUSER(m00_axi_wuser),
    .M_AXI_WVALID(m00_axi_wvalid),
    .M_AXI_WREADY(m00_axi_wready),
    .M_AXI_BID(m00_axi_bid),
    .M_AXI_BRESP(m00_axi_bresp),
    .M_AXI_BUSER(m00_axi_buser),
    .M_AXI_BVALID(m00_axi_bvalid),
    .M_AXI_BREADY(m00_axi_bready),
    .M_AXI_ARID(m00_axi_arid),
    .M_AXI_ARADDR(m00_axi_araddr),
    .M_AXI_ARLEN(m00_axi_arlen),
    .M_AXI_ARSIZE(m00_axi_arsize),
    .M_AXI_ARBURST(m00_axi_arburst),
    .M_AXI_ARLOCK(m00_axi_arlock),
    .M_AXI_ARCACHE(m00_axi_arcache),
    .M_AXI_ARPROT(m00_axi_arprot),
    .M_AXI_ARQOS(m00_axi_arqos),
    .M_AXI_ARUSER(m00_axi_aruser),
    .M_AXI_ARVALID(m00_axi_arvalid),
    .M_AXI_ARREADY(m00_axi_arready),
    .M_AXI_RID(m00_axi_rid),
    .M_AXI_RDATA(m00_axi_rdata),
    .M_AXI_RRESP(m00_axi_rresp),
    .M_AXI_RLAST(m00_axi_rlast),
    .M_AXI_RUSER(m00_axi_ruser),
    .M_AXI_RVALID(m00_axi_rvalid),
    .M_AXI_RREADY(m00_axi_rready)
);
/*
     // Instantiation of Axi Bus Interface S00_AXI
         lisnoc_axi_slave_v1_0_S00_AXI # ( 
             .C_S_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
             .C_S_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
             .C_S_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
             .C_S_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
             .C_S_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
             .C_S_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
             .C_S_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
             .C_S_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH)
         ) lisnoc_axi_slave_v1_0_S00_AXI_inst (
             .S_AXI_ACLK(m00_axi_aclk),
             .S_AXI_ARESETN(m00_axi_aresetn),
             .S_AXI_AWID(m00_axi_awid),
             .S_AXI_AWADDR(m00_axi_awaddr),
             .S_AXI_AWLEN(m00_axi_awlen),
             .S_AXI_AWSIZE(m00_axi_awsize),
             .S_AXI_AWBURST(m00_axi_awburst),
             .S_AXI_AWLOCK(m00_axi_awlock),
             .S_AXI_AWCACHE(m00_axi_awcache),
             .S_AXI_AWPROT(m00_axi_awprot),
             .S_AXI_AWQOS(m00_axi_awqos),
             .S_AXI_AWREGION(m00_axi_awregion),
             .S_AXI_AWUSER(m00_axi_awuser),
             .S_AXI_AWVALID(m00_axi_awready),
             .S_AXI_AWREADY(m00_axi_awvalid),
             .S_AXI_WDATA(m00_axi_wdata),
             .S_AXI_WSTRB(m00_axi_wstrb),
             .S_AXI_WLAST(m00_axi_wlast),
             .S_AXI_WUSER(m00_axi_wuser),
             .S_AXI_WVALID(m00_axi_wready),
             .S_AXI_WREADY(m00_axi_wvalid),
             .S_AXI_BID(m00_axi_bid),
             .S_AXI_BRESP(m00_axi_bresp),
             .S_AXI_BUSER(m00_axi_buser),
             .S_AXI_BVALID(m00_axi_bready),
             .S_AXI_BREADY(m00_axi_bvalid),
             .S_AXI_ARID(m00_axi_arid),
             .S_AXI_ARADDR(m00_axi_araddr),
             .S_AXI_ARLEN(m00_axi_arlen),
             .S_AXI_ARSIZE(m00_axi_arsize),
             .S_AXI_ARBURST(m00_axi_arburst),
             .S_AXI_ARLOCK(m00_axi_arlock),
             .S_AXI_ARCACHE(m00_axi_arcache),
             .S_AXI_ARPROT(m00_axi_arprot),
             .S_AXI_ARQOS(m00_axi_arqos),
             .S_AXI_ARREGION(m00_axi_arregion),
             .S_AXI_ARUSER(m00_axi_aruser),
             .S_AXI_ARVALID(m00_axi_arready),
             .S_AXI_ARREADY(m00_axi_arvalid),
             .S_AXI_RID(m00_axi_rid),
             .S_AXI_RDATA(m00_axi_rdata),
             .S_AXI_RRESP(m00_axi_rresp),
             .S_AXI_RLAST(m00_axi_rlast),
             .S_AXI_RUSER(m00_axi_ruser),
             .S_AXI_RVALID(m00_axi_rready),
             .S_AXI_RREADY(m00_axi_rvalid)
         );
   */
   
       // Instantiation of Axi Bus Interface S00_AXI
        lisnoc_axi_slave_v1_0_S00_AXI # ( 
            .C_S_AXI_ID_WIDTH(C_M00_AXI_ID_WIDTH),
            .C_S_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
            .C_S_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
            .C_S_AXI_AWUSER_WIDTH(C_M00_AXI_AWUSER_WIDTH),
            .C_S_AXI_ARUSER_WIDTH(C_M00_AXI_ARUSER_WIDTH),
            .C_S_AXI_WUSER_WIDTH(C_M00_AXI_WUSER_WIDTH),
            .C_S_AXI_RUSER_WIDTH(C_M00_AXI_RUSER_WIDTH),
            .C_S_AXI_BUSER_WIDTH(C_M00_AXI_BUSER_WIDTH)
        ) lisnoc_axi_slave_v1_0_S00_AXI_inst (
            .S_AXI_ACLK(m00_axi_aclk),
            .S_AXI_ARESETN(m00_axi_aresetn),
            .S_AXI_AWID(m00_axi_awid),
            .S_AXI_AWADDR(m00_axi_awaddr),
            .S_AXI_AWLEN(m00_axi_awlen),
            .S_AXI_AWSIZE(m00_axi_awsize),
            .S_AXI_AWBURST(m00_axi_awburst),
            .S_AXI_AWLOCK(m00_axi_awlock),
            .S_AXI_AWCACHE(m00_axi_awcache),
            .S_AXI_AWPROT(m00_axi_awprot),
            .S_AXI_AWQOS(m00_axi_awqos),
            .S_AXI_AWREGION(m00_axi_awregion),
            .S_AXI_AWUSER(m00_axi_awuser),
            .S_AXI_AWVALID(m00_axi_awvalid),
            .S_AXI_AWREADY(m00_axi_awready),
            .S_AXI_WDATA(m00_axi_wdata),
            .S_AXI_WSTRB(m00_axi_wstrb),
            .S_AXI_WLAST(m00_axi_wlast),
            .S_AXI_WUSER(m00_axi_wuser),
            .S_AXI_WVALID(m00_axi_wvalid),
            .S_AXI_WREADY(m00_axi_wready),
            .S_AXI_BID(m00_axi_bid),
            .S_AXI_BRESP(m00_axi_bresp),
            .S_AXI_BUSER(m00_axi_buser),
            .S_AXI_BVALID(m00_axi_bvalid),
            .S_AXI_BREADY(m00_axi_bready),
            .S_AXI_ARID(m00_axi_arid),
            .S_AXI_ARADDR(m00_axi_araddr),
            .S_AXI_ARLEN(m00_axi_arlen),
            .S_AXI_ARSIZE(m00_axi_arsize),
            .S_AXI_ARBURST(m00_axi_arburst),
            .S_AXI_ARLOCK(m00_axi_arlock),
            .S_AXI_ARCACHE(m00_axi_arcache),
            .S_AXI_ARPROT(m00_axi_arprot),
            .S_AXI_ARQOS(m00_axi_arqos),
            .S_AXI_ARREGION(m00_axi_arregion),
            .S_AXI_ARUSER(m00_axi_aruser),
            .S_AXI_ARVALID(m00_axi_arvalid),
            .S_AXI_ARREADY(m00_axi_arready),
            .S_AXI_RID(m00_axi_rid),
            .S_AXI_RDATA(m00_axi_rdata),
            .S_AXI_RRESP(m00_axi_rresp),
            .S_AXI_RLAST(m00_axi_rlast),
            .S_AXI_RUSER(m00_axi_ruser),
            .S_AXI_RVALID(m00_axi_rvalid),
            .S_AXI_RREADY(m00_axi_rready)
        );
endmodule
