`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/17/2019 09:50:56 AM
// Design Name: 
// Module Name: tb_for_fifo_buffer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
/*
 * This module is a test bench for the fifo_buffer
 */

module tb_for_fifo_buffer();

reg clk, rst;
reg [32-1:0] word_i;
reg word_latch_i;
reg word_read_i;

wire is_empty_o;
wire is_full_o;
wire [32-1:0] current_word_o;

initial 
begin
    //restet everything
    clk = 1'b0;
    #1 rst = 1'b1;
    #2 rst = 1'b0;
    word_i = 0;
    word_latch_i = 1'b0;
    word_read_i = 1'b0;
    
    //feed fifo with test data
    #0 word_read_i = 1'b1;
    #4 word_read_i = 1'b0;
    
    #0 word_latch_i = 1'b1;
    #4 word_latch_i = 1'b0;
    
    #0 word_latch_i = 1'b1;
    #0 word_i = word_i + 1;
    #4 word_latch_i = 1'b0;
    
    #0 word_latch_i = 1'b1;
    #0 word_i = word_i + 1;
    #4 word_latch_i = 1'b0;
    
    #0 word_latch_i = 1'b1;
    #0 word_i = word_i + 1;
    #4 word_latch_i = 1'b0;
    
    #0 word_latch_i = 1'b1;
    #0 word_i = word_i + 1;
    #4 word_latch_i = 1'b0;
    
    #0 word_read_i = 1'b1;
    #4 word_read_i = 1'b0;
    
    #0 word_latch_i = 1'b1;
    #0 word_i = word_i + 1;
    #4 word_latch_i = 1'b0;
    
    #0 word_read_i = 1'b1;
    #4 word_read_i = 1'b0;
    
    #0 word_latch_i = 1'b1;
    #0 word_read_i = 1'b1;
    #0 word_i = word_i + 1;
    #4 word_latch_i = 1'b0;
    
end

//clock tick
always #2 clk = ~clk;
/*
fifo_buffer
    #(
        //defines the size of the buffer as 2^buffer_address_bits
        .buffer_address_bits(2),
        //defines the size of a singel buffer word
        .word_size(32)
    ) testbuffer
    (
        .clk(clk), 
        .rst(rst),
        
        //word tor put into fifo at next clk + word_latch_i
        .word_i(word_i),
        .word_latch_i(word_latch_i),
        //current word will be deleted from fifo
        .word_read_i(word_read_i),
        
        //status of the fifo
        .is_empty_o(is_empty_o),
        .is_full_o(is_full_o),
        //first word of the fifo
        .current_word_o(current_word_o)       
    );
    *//*
    FIFObuffer
        #(
            //defines the size of the buffer as 2^buffer_address_bits
            .buffer_address_bits(2),
            //defines the size of a singel buffer word
            .word_size(32)
        ) testbuffer
        (
            .clk(clk), 
            .rst(rst),
            
            //word tor put into fifo at next clk + word_latch_i
            .word_i(word_i),
            .WR(word_latch_i),
            //current word will be deleted from fifo
            .RD(word_read_i),
            
            //status of the fifo
            .EMPTY(is_empty_o),
            .FULL(is_full_o),
            //first word of the fifo
            .dataOut(current_word_o)       
        );*/
        lisnoc_fifo
        #(
          .flit_data_width(32),
         .flit_type_width(2),
         .packet_length(0),
         .LENGTH(4)

        )
        test_fifo
        (/*AUTOARG*/
           // Outputs
           .clk(clk),
           .rst(rst),
        
           // FIFO input side
           .in_flit(word_i),   // input
           .in_valid(word_latch_i),                   // write_enable
           .in_ready(is_full_o),                   // accepting new data
        
           //FIFO output side
           .out_flit(current_word_o),                     // data_out
           .out_valid(is_empty_o),                   // data available
           .out_ready(word_read_i)                   // read request
       );

endmodule
