`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/10/2019 03:41:51 PM
// Design Name: 
// Module Name: test_module_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_module_tb();

    parameter msg_length = 10;
    
    reg [0:msg_length-1] msg;
    reg clk;
    wire [msg_length-1:0] msg_out;
     
    test_module #(.msg_length(msg_length))
        uut(clk,msg,msg_out);
        
    initial 
    begin
        msg = 0;
        clk = 0;
    end
    
    always #5 clk = ~clk;
    
    always #1 msg = msg +1;
endmodule
