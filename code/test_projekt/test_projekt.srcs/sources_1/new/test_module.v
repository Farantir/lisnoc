`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/10/2019 03:09:15 PM
// Design Name: 
// Module Name: test_module
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_module
#(
    parameter msg_length = 5
)
(
    input clk, 
    input wire [msg_length-1:0] test_msg_i, 
    output reg [msg_length-1:0] test_msg_o
);

reg [msg_length-1:0] test_msg_storage;

always @(posedge clk)
begin
    test_msg_storage = test_msg_i;
end

always @(negedge clk)
begin
    set_message(test_msg_storage);
end

  function set_message;
    input message;
    begin
       test_msg_o = message + 1;
    end
  endfunction

endmodule
